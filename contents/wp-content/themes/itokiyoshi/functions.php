<?php
function rss_thumbnail($content) {
 global $post;
 if (has_post_thumbnail($post->ID)) {
 $content = get_the_post_thumbnail($post->ID,array(250,100)) . $content;
 }
 return $content;
}
add_filter( 'the_excerpt_rss', 'rss_thumbnail');
add_filter( 'the_content_feed', 'rss_thumbnail');

add_theme_support( 'post-thumbnails');
set_post_thumbnail_size( 100, 100, true );
the_post_thumbnail();                  // without parameter -> Thumbnail

the_post_thumbnail('thumbnail');       // Thumbnail (default 150px x 150px max)
the_post_thumbnail('medium');          // Medium resolution (default 300px x 300px max)
the_post_thumbnail('large');           // Large resolution (default 640px x 640px max)

the_post_thumbnail( array(100,100) );  // Other resolutions
function new_excerpt_more($more) {
     return ' ... <a class="more" href="'. get_permalink() . '">続きを読む</a>';
}
add_filter('excerpt_more', 'new_excerpt_more');
function new_excerpt_mblength($length) {
     return 50;
}
add_filter('excerpt_mblength', 'new_excerpt_mblength');
add_shortcode('caption', 'my_img_caption_shortcode');

function my_img_caption_shortcode($attr, $content = null) {
	if ( ! isset( $attr['caption'] ) ) {
		if ( preg_match( '#((?:<a [^>]+>s*)?<img [^>]+>(?:s*</a>)?)(.*)#is', $content, $matches ) ) {
			$content = $matches[1];
			$attr['caption'] = trim( $matches[2] );
		}
	}

	$output = apply_filters('img_caption_shortcode', '', $attr, $content);
	if ( $output != '' )
		return $output;

	extract(shortcode_atts(array(
		'id'	=> '',
		'align'	=> 'alignnone',
		'width'	=> '',
		'caption' => ''
	), $attr, 'caption'));

	if ( 1 > (int) $width || empty($caption) )
		return $content;

	if ( $id ) $id = 'id="' . esc_attr($id) . '" ';

	return '<div ' . $id . 'class="wp-caption ' . esc_attr($align) . '">' . do_shortcode( $content ) . '<p class="wp-caption-text">' . $caption . '</p></div>';

}
