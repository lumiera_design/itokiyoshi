<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="all" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
<?php // Loads HTML5 JavaScript file to add support for HTML5 elements in older IE versions. ?>
<!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
<![endif]-->
<?php wp_head(); ?>



<link rel="stylesheet" href="http://itokiyoshi.com/user_data/packages/plg_responsivel0084/css/flexslider.css">
<link rel="alternate" type="application/rss+xml" title="RSS" href="http://itokiyoshi.com/rss/" />

<!--[if lt IE 9]><script type="text/javascript" src="http://itokiyoshi.com/user_data/packages/plg_responsivel0084/js/html5shiv.js"></script><![endif]-->
<link href='http://fonts.googleapis.com/css?family=Josefin+Sans:600' rel='stylesheet' type='text/css' />
<link rel="stylesheet" href="http://itokiyoshi.com/user_data/packages/plg_responsivel0084/js/jquery.colorbox/colorbox.css" type="text/css" media="all" />
<script type="text/javascript" src="http://itokiyoshi.com/user_data/packages/plg_responsivel0084/js/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="http://itokiyoshi.com/user_data/packages/plg_responsivel0084/js/function.js"></script> 
<script type="text/javascript" src="http://itokiyoshi.com/js/eccube.js"></script>
<script type="text/javascript" src="http://itokiyoshi.com/js/eccube.legacy.js"></script>
<script type="text/javascript" src="http://itokiyoshi.com/user_data/packages/plg_responsivel0084/js/jquery.flexslider-min.js"></script> 
<script type="text/javascript" src="http://itokiyoshi.com/user_data/packages/plg_responsivel0084/js/jquery.colorbox/jquery.colorbox-min	.js"></script> 
<script type="text/javascript" src="http://itokiyoshi.com/js/heightLine.js"></script> 
<script type="text/javascript">
    
    $(function(){
        
    });

    $(window).load(function() {
        $('#slider').flexslider({
            animation: "slide",
            itemWidth: 160,
            minItems: 4,
            maxItems: 4,
            animationLoop: false
        });
    });
</script>

<script type="text/javascript" src="http://itokiyoshi.com/js/jquery.fademover.js"></script>

<script>
$(document).ready(function() {
    $("#contents").fadeMover({
       
            'effectType': 2,
			'inSpeed': 500,
			'outSpeed': 500,
			'inDelay' : '0',
			'outDelay' : '0',
		'nofadeOut' : 'nonmover'
    });
});
</script>
 

</head><!-- ▼BODY部 スタート -->
<body <?php body_class(); ?>>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-58934803-1', 'auto');
  ga('send', 'pageview');

</script>

<noscript>
    <p>JavaScript を有効にしてご利用下さい.</p>
</noscript>

<div id="wrapper">
    <a name="top" id="top"></a>

    
                            
<!--▼HEADER-->
<header id="header">
    <div class="inner">
		<h1 class="logo"><a href="http://itokiyoshi.com/"><img src="http://itokiyoshi.com/user_data/packages/plg_responsivel0084/img/common/logo-w.png" alt="日本尾張の羽毛布団伊藤清商店" /></a></h1>
       <div class="header_utility">
        <nav class="header_navi">
        <a href="http://itokiyoshi.com/contact/"><img id="btn_contact" src="http://itokiyoshi.com/user_data/packages/plg_responsivel0084/img/headnavi-w_03.jpg"></a><br>
        <a href="tel:0525225206"><img id="btn_tel" src="http://itokiyoshi.com/user_data/packages/plg_responsivel0084/img/headnavi-w_05.jpg"></a>
        </nav>
      </div>
       
    </div>
	<div id="header_bottom">
		
<!-- ▼【ヘッダー】グローバルナビ -->
													
<div id="gnav">
    <div class="inner">
        <nav>	
        <a class="home" href="http://itokiyoshi.com/"><p><span class="carticon"><img src="http://itokiyoshi.com/user_data/packages/plg_responsivel0084/img/btn_header_home.png" alt="Cart" width="29" /><span class="pctb">Home</span></p></a>

          <a class="gnav_shopguide" href="http://itokiyoshi.com/products/list.php?category_id=27"><p><span>ご注文</span></p></a>
            <a class="gnav_shopguide" href="http://itokiyoshi.com/user_data/our_skill.php"><p><span>職人の技</span></p></a>
          <a class="gnav_shopguide" href="http://itokiyoshi.com/contents/"><p><span>眠りの事典</span></p></a>
          <a class="gnav_shopguide" href="http://itokiyoshi.com/contents/faq"><p><span>よくあるご質問</span></p></a>
        </nav>
    </div>
</div>
                        						<!-- ▲【ヘッダー】グローバルナビ -->
																						</nav>
			</div>
		</div>
	</div>
</header>
<!--▲HEADER-->        
                
    <div id="contents" class="cf">
                        
                <article id="main">
                                    
            <!-- ▼メイン -->
           


<!--▼mainColumn▼-->
<div id="mainColumn" class="clearfix productsDetail">
<?php breadcrumb(); ?>

<!--条件分岐シングルページ▼--> 

<?php if(is_single()): ?>
<div id="categoryimg"><div class="title-date">
  <span class="title-date-day"><?php the_time('d') ?></span>
  <span class="title-date-month"><?php echo get_post_time('F'); ?></span>
</div>
<h2> <?php the_title(); ?></h2></div>


<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

<?php the_content('記事の続きを読む »'); ?>
<p><?php the_tags('Tags:', ', ', '<br />'); ?> </p>
    
<?php endwhile; endif; ?>
  <!--条件分岐シングルページ▲-->
  
   <!--条件分岐ホーム▼--> 
<?php elseif(is_home()): ?>
<h2 id="categoryimg">充実した毎日のために〜眠りの事典〜</h2>
<p>眠りや寝具についてのためになる情報をお伝えします!</p>
<?php query_posts('showposts=1');?>
<h2><?php single_cat_title(); ?><span class="cat-description"><?php echo(category_description()); ?></span></h2> 

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<div class="posttitle"><h3 class="heading01"><?php the_title(); ?></h3></div>
   
		<div class="entry" ><?php the_content('記事の続きを読む »'); ?>
       


 
<?php endwhile; endif; ?>


<?php query_posts('showposts=6&offset=1');?>
<h2><?php single_cat_title(); ?><span class="cat-description"><?php echo(category_description()); ?></span></h2> 
<ul>
<?php if(have_posts()): while(have_posts()): the_post(); ?>
	<li class="heightLine post_item">
 
	<div class="posttitle"><h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4></div>
	<div class="postdiscription"><a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( array(100,100) );　?></a>
	<?php the_excerpt(); ?></div>
   
</li>

 
<?php endwhile; endif; ?>
</ul>
  <!--条件分岐ホーム▲-> 
  
  
  
  <!--条件分岐カテゴリーページ▼--> 
<?php elseif(is_category()): ?>
<h2 id="categoryimg"><?php single_cat_title(); ?></h2>


<ul>
<?php if(have_posts()): while(have_posts()): the_post(); ?>
	<li class="heightLine post_item">
 
	<div class="posttitle"><h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4></div>
	<div class="postdiscription"><a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( array(100,100) );　?></a>
	<?php the_excerpt(); ?></div>
</li>

 
<?php endwhile; endif; ?>
</ul>
  <!--条件分岐カテゴリーページ▲-> 
<!--条件分岐ページ▼--> 

<?php elseif(is_page()): ?>
<h2 id="categoryimg"><?php the_title(); ?></h2>

	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	
		<div class="entry" ><?php the_content('記事の続きを読む »'); ?>
    
	<?php
  $children = wp_list_pages('sort_column=menu_order&title_li=&child_of='.$post->ID.'&echo=0');
  if ($children) { ?>
  <h4>こちらもお読みください</h4>
  <ul class="childpage">
  <?php echo $children; ?>
  </ul>
  <?php } ?>
  
	<p><?php the_tags('Tags:', ', ', '<br />'); ?> </p>

<?php endwhile; endif; ?>
<!--条件分岐シングルページ▲--> 
 <!--アーカイブページのとき-->

<?php elseif(is_archive()): ?>

	<h2>記事一覧</h2>
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<div class="entry">
    <h5 class="catname"><?php
foreach((get_the_category()) as $category) { 
echo $category->cat_name . ' '; 
} 
?></h5>
<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>	
	<?php the_excerpt('記事の続きを読む »'); ?></div>
<?php endwhile; endif; ?>


 <!--404のとき-->

<?php elseif(is_404()): ?>

	 <h2>Not Found</h2>	<div class="entry">
 	
    
  <p>申し訳ありません。お探しのページはございません。</p></div>



<?php elseif(is_search()): ?>
<h2>検索結果</h2>	
<div class="box1"><p>『<?php echo wp_specialchars($s); ?>』  <?php $mySearch =& new WP_Query("s=$s & showposts=-1"); echo $mySearch->post_count; ?> 件ヒットしました。</p>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

 
 <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>	
<?php the_excerpt('記事の続きを読む »'); ?>

<?php endwhile; else: ?>
<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
 	
   
 
    
    
    
    
    

    

	<?php endif; ?>
    	<?php endif; ?>
</div>
<!--▲本文終わり-->
<!-- ▲メイン -->

                                                
        </article>
        
                        <aside id="side">
                                        <!-- ▼カテゴリ -->
                                    
<div class="block_outer">
    <div id="category_area">
        <div class="bloc_body">
            <h2 class="heading01">カテゴリー</h2>
            <ul id="categorytree">
                
<?php wp_list_categories('title_li='); ?>
            </ul>
            
            <?php
    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
 
        $args = array(
            'paged' => $paged,
            'posts_per_page' => 10,
           
        );
 
    $wp_query = new WP_Query($args);
?>
 <h2 class="heading01">ご存じですか？</h2>
            <ul id="categorytree">
	<?php if(have_posts()): while(have_posts()): the_post(); ?>
	
	<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
    
    <?php endwhile; endif; ?></ul>
    </div>
        </div>
    </div>
</div>
                                <!-- ▲カテゴリ -->
                            <!-- ▼ログイン -->
                                <!-- ▲ログイン -->
                            <!-- ▼利用ガイド -->
                                  <!-- ▲利用ガイド -->
                                </aside>
                    </div>

            
                



<!--▼FOOTER-->
<p class="pagetop"><a href="#wrapper">PAGETOP</a></p>
<footer id="footer">
    <div class="inner cf">
        <nav id="footer_nav">
            <ul class="bottom_link">
            <li><a class="" href="http://itokiyoshi.com/">TOP</a></li>
                <li><a class="" href="http://itokiyoshi.com/abouts/">当サイトについて</a></li>
                <li><a class="" href="http://itokiyoshi.com/order/">特定商取引に関する表記</a></li>
                <li><a class="" href="http://itokiyoshi.com/guide/privacy.php">プライバシーポリシー</a></li><li><a class="" href="http://itokiyoshi.com/user_data/guarantee.php">メーカー保証</a></li>
                            <!--▼▼▼ログイン前▼▼▼-->
            <li> <a class="" href="http://itokiyoshi.com/mypage/">ログイン</a></li>
            <!--▲▲▲ログイン前▲▲▲-->
                            <li><a class="" href="http://itokiyoshi.com/guide/plg_ResponsiveL0084_guide.php">ご利用の案内</a></li>
                
            </ul>
        </nav>
        <small>Copyright &copy; 2005-<?php echo date(Y); ?> 伊藤清商店 All rights reserved.</small>
    </div>
</footer>
<!--▲FOOTER-->                        </div>
<?php wp_footer(); ?>
<!--yahoo▼-->
<script type="text/javascript">
  (function () {
    var tagjs = document.createElement("script");
    var s = document.getElementsByTagName("script")[0];
    tagjs.async = true;
    tagjs.src = "//s.yjtag.jp/tag.js#site=gphQGCM";
    s.parentNode.insertBefore(tagjs, s);
  }());
</script>
<noscript>
  <iframe src="//b.yjtag.jp/iframe?c=gphQGCM" width="1" height="1" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
</noscript>
<!--yahoo▲-->
</body><!-- ▲BODY部 エンド -->
</html>