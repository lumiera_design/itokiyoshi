<?php
class plugin_info{
    static $PLUGIN_CODE       = "CategoryMainView";
    static $PLUGIN_NAME       = "カテゴリ別MV枠追加 2.13版";
    static $PLUGIN_VERSION    = "1.0";
    static $COMPLIANT_VERSION = "2.13.2";
    static $AUTHOR            = "ボクブロック株式会社";
    static $DESCRIPTION       = "商品一覧ページのカテゴリごとに異なる説明文を表示するMV枠を追加する。";
    static $PLUGIN_SITE_URL   = "http://www.bokublock.jp/";
    static $AUTHOR_SITE_URL   = "http://www.bokublock.jp/";
    static $CLASS_NAME        = "CategoryMainView";
    static $HOOK_POINTS       = array(
        array("LC_Page_Admin_Products_Category_action_after", 'CategoryAdminView'),
        array("LC_Page_Products_List_action_after", 'CategoryListView'),
        array("prefilterTransform", 'prefilterTransform'));
    static $LICENSE           = "LGPL";
}

?>
