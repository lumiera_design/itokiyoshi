<?php
 
class CategoryMainView extends SC_Plugin_Base {

    public function __construct(array $arrSelfInfo) {
        parent::__construct($arrSelfInfo);
    }

    function install($arrPlugin) {
    	$objQuery =& SC_Query_Ex::getSingletonInstance();
        $sql = "CREATE TABLE plg_CategoryMainView_view(
                    category_id int NOT NULL,
                    category_count text,
                    create_date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                    update_date timestamp NOT NULL,
                    del_flg smallint
                );";
        $objQuery->query($sql);
        if(copy(PLUGIN_UPLOAD_REALDIR . "CategoryMainView/logo.png", PLUGIN_HTML_REALDIR . "CategoryMainView/logo.png") === false);
        
    }

    function uninstall($arrPlugin) {
        // nop
        $objQuery =& SC_Query_Ex::getSingletonInstance();
        $sql = "DROP TABLE plg_CategoryMainView_view;";
        $objQuery->query($sql);
    }

    function update($arrPlugin) {
        // nop
    }

    function enable($arrPlugin) {
        // nop
    }

    function disable($arrPlugin) {
        // nop
    }

    function register(SC_Helper_Plugin $objHelperPlugin) {
        $objHelperPlugin->addAction('LC_Page_Admin_Products_Category_action_after', array(&$this, 'CategoryAdminView'));
        $objHelperPlugin->addAction('LC_Page_Products_List_action_after', array(&$this, 'CategoryListView'));
        $objHelperPlugin->addAction('prefilterTransform', array(&$this, 'prefilterTransform'),1);
    }

/*****************/

    function CategoryAdminView($objPage) {
    	$objDb      = new SC_Helper_DB_Ex();
        $objFormParam = new SC_FormParam_Ex();

        $this->initParam($objFormParam);
        $objFormParam->setParam($_POST);
        $objFormParam->convParam();
        
        if ($_POST['mode'] == 'edit') {
        	$this->doEditView($objFormParam);
        }else if ($_POST['mode'] == 'pre_edit') {
        	$this->doPreEditView($objFormParam, $objPage);
        }else if ($_POST['mode'] == 'delete') {
        	$this->doDeleteView($objFormParam);
        }
        
    }

    function initParam(&$objFormParam) {
        $objFormParam->addParam('カテゴリ内容', 'category_count', null, null, array());
        $objFormParam->addParam('カテゴリID', 'category_id', null, null, array());
    }
    
    function doEditView(&$objFormParam) {
    	$category_id = $objFormParam->getValue('category_id');
        $category_count= $objFormParam->getValue('category_count');
        // 追加か
        $add = strlen($category_id) === 0;
        
        $objQuery =& SC_Query_Ex::getSingletonInstance();
        
        $objQuery->begin();
        $arrCategory = $objFormParam->getDbArray();
        if($add){
            $arrCategory['category_id'] = $objQuery->currval('dtb_category_category_id');
            $arrCategory['create_date'] = 'CURRENT_TIMESTAMP';
        	$arrCategory['update_date'] = 'CURRENT_TIMESTAMP';
            $objQuery->insert('plg_CategoryMainView_view', $arrCategory);
        }else{
        	$where = 'category_id = ?';
        	$cateid = $objQuery->getRow('category_id', 'plg_CategoryMainView_view', $where, array($category_id));
        	if($cateid["category_id"]){
        		
        		$where = 'category_id = ?';
        		$objQuery->update('plg_CategoryMainView_view', $arrCategory, $where, array($category_id));
        		
        	}else{
        		
        		$arrCategory['category_id'] = $category_id;
        		$arrCategory['create_date'] = 'CURRENT_TIMESTAMP';
        		$arrCategory['update_date'] = 'CURRENT_TIMESTAMP';
            	$objQuery->insert('plg_CategoryMainView_view', $arrCategory);
            	
        	}
        	
        }
        
        $objQuery->commit();    // トランザクションの終了
    }
    
    function doPreEditView(&$objFormParam, $objPage) {
    	$category_id = $objFormParam->getValue('category_id');

        $objQuery =& SC_Query_Ex::getSingletonInstance();

        $where = 'category_id = ?';
        $arrRes = $objQuery->getRow('category_count', 'plg_CategoryMainView_view', $where, array($category_id));
        
        $objPage->allForm = $arrRes;
        
    }
    
    function doDeleteView(&$objFormParam) {
    	$category_id = $objFormParam->getValue('category_id');
        $objQuery =& SC_Query_Ex::getSingletonInstance();
        
        $where = 'category_id = ?';
        $objQuery->delete('plg_CategoryMainView_view', $where, array($category_id));
        
    }
    
    
    function CategoryListView($objPage) {
    	$objProduct = new SC_Product_Ex();

        $this->arrForm = $_REQUEST;
        
        $category_id =$this->lfGetCategoryId(intval($this->arrForm['category_id']));
        
        $objQuery = new SC_Query();
		$objPage->category_count = $objQuery->get("category_count", "plg_CategoryMainView_view", "category_id = ?", $category_id);
    	
    }
    
    function lfGetCategoryId($category_id) {

        if (empty($category_id)) return 0;

        if (!SC_Utils_Ex::sfIsInt($category_id)
            || SC_Utils_Ex::sfIsZeroFilling($category_id)
            || !SC_Helper_DB_Ex::sfIsRecord('dtb_category', 'category_id', (array)$category_id, 'del_flg = 0')
            ) {
            SC_Utils_Ex::sfDispSiteError(CATEGORY_NOT_FOUND);
        }

        $arrCategory_id = SC_Helper_DB_Ex::sfGetCategoryId('', $category_id);

        if (empty($arrCategory_id)) {
            SC_Utils_Ex::sfDispSiteError(CATEGORY_NOT_FOUND);
        }

        return $arrCategory_id[0];
    }
    
    function prefilterTransform(&$source, LC_Page_Ex $objPage, $filename) {
        $objTransform = new SC_Helper_Transform($source);
        $template_dir = PLUGIN_UPLOAD_REALDIR . 'CategoryMainView/templates/';
        switch($objPage->arrPageLayout['device_type_id']){
            case DEVICE_TYPE_MOBILE:
            	break;
            case DEVICE_TYPE_SMARTPHONE:
            	if (strpos($filename, 'products/list.tpl') !== false) {
			$objTransform->select('h2.title')->insertAfter(file_get_contents($template_dir . 'plg_CategoryMainView_list.tpl'));
                }
                break;
            case DEVICE_TYPE_PC:
            	if (strpos($filename, 'products/list.tpl') !== false) {
			$objTransform->select('h2.title')->insertAfter(file_get_contents($template_dir . 'plg_CategoryMainView_list.tpl'));
                }
                break;
                break;
            case DEVICE_TYPE_ADMIN:
            default:
            	if (strpos($filename, 'products/category.tpl') !== false) {
			$objTransform->select('div.now_dir')->appendChild(file_get_contents($template_dir . 'plg_CategoryMainView_admin.tpl'));
                }
                break;
        }
        $source = $objTransform->getHTML();
    }
}
?>
