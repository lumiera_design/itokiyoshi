<!--{*
 *
 * RSSフィード取得ブロック生成プラグイン
 * 
 * Copyright (C) 2014 takami_kazuya@aratana
 * http://www.aratana.jp
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *}-->
<!--{include file="`$smarty.const.TEMPLATE_ADMIN_REALDIR`admin_popup_header.tpl"}-->
<form name="form1" id="form1" method="post" action="<!--{$smarty.server.REQUEST_URI|h}-->" enctype="multipart/form-data">
<input type="hidden" name="mode" value="register">
<input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
<input type="hidden" name="image_key" value="" />
<!--{foreach key=key item=item from=$arrForm.arrHidden}-->
<input type="hidden" name="<!--{$key}-->" value="<!--{$item|h}-->" />
<!--{/foreach}-->

<!--{* PC／SP切り替え *}-->
<!--{if $smarty.get.device == "SP"}-->
    <h1><!--{$tpl_subtitle}-->（SP設定）</h1>
    <h2><a href="<!--{$smarty.server.PHP_SELF|escape}-->?plugin_id=<!--{$smarty.get.plugin_id|escape}-->">PC設定に切替</a>｜SP設定</h2>
    <input type="hidden" name="device" value="SP" />
<!--{else}-->
    <h1><!--{$tpl_subtitle}-->（PC設定）</h1>
    <h2>PC設定｜<a href="<!--{$smarty.server.PHP_SELF|escape}-->?plugin_id=<!--{$smarty.get.plugin_id|escape}-->&device=SP">SP設定に切替</a></h2>
    <input type="hidden" name="device" value="PC" />
<!--{/if}-->
<table>
    <tr>
        <th>フィードURL</th>
        <td><input type="text" name="rss_url" value="<!--{$arrForm.rss_url|h}-->" size="50" style="<!--{if $arrErr.pause != ""}-->background-color: <!--{$smarty.const.ERR_COLOR}-->;<!--{/if}-->" /></td>
    </tr>
    <tr>
        <th>RSSフィード形式</th>
        <td>
        <select name="rss_format">
        <option value="rss2" <!--{if $arrForm.rss_format|h == "rss2"}--> selected="selected"<!--{/if}-->>RSS2.0</option>
        <option value="atom" <!--{if $arrForm.rss_format|h == "atom"}--> selected="selected"<!--{/if}-->>Atom</option>
        </select>
        </td>
    </tr>
    <tr>
        <th>表示数</th>
        <td><input type="text" name="rss_disp" value="<!--{$arrForm.rss_disp|h}-->" maxlength="3" size="10" style="<!--{if $arrErr.pause != ""}-->background-color: <!--{$smarty.const.ERR_COLOR}-->;<!--{/if}-->" /></td>
    </tr>
    <tr>
        <th>CSS</th>
        <td>
        <textarea name="rss_css" cols="60" rows="20"><!--{$arrForm.rss_css|h}--></textarea><br />
        </td>
    </tr>
</table>

<div class="btn-area">
  <ul>
    <li>
    <a class="btn-action" href="javascript:;" onclick="document.form1.submit();return false;"><span class="btn-next">この内容で登録する</span></a>
    </li>
  </ul>
</div>

</form>
<!--{include file="`$smarty.const.TEMPLATE_ADMIN_REALDIR`admin_popup_footer.tpl"}-->