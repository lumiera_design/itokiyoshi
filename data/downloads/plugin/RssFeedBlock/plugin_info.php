<?php
class plugin_info{
    static $PLUGIN_CODE       = 'RssFeedBlock';
    static $PLUGIN_NAME       = 'RSSフィード取得ブロック生成プラグイン';
    static $CLASS_NAME        = 'RssFeedBlock';
    static $PLUGIN_VERSION    = '1.0.0';
    static $COMPLIANT_VERSION = '2.13.0';
    static $AUTHOR            = '株式会社アラタナ';
    static $DESCRIPTION       = 'WordPressなどのCMSで生成されるRSSをURL指定するだけでブロックにデータを表示してくれるプラグインです。';
    static $PLUGIN_SITE_URL   = 'http://www.eccube-school.jp/cagolab-custom/index.php';
    static $AUTHOR_SITE_URL   = 'http://www.aratana.jp/';
    static $HOOK_POINTS       =  array(

    );
    static $LICENSE           = 'LGPL';
}