<?php
/*
 * RSSフィード取得ブロック生成プラグイン
 * 
 * Copyright (C) 2014 takami_kazuya@aratana
 * http://www.aratana.jp
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

/**
 * プラグインのメインクラス
 *
 * @package RssFeedBlock
 * @author aratana CO.,LTD.
 * @version $Id: $
 */
class RssFeedBlock extends SC_Plugin_Base {
    // 定数宣言
    const CLASS_NAME = 'RssFeedBlock';

    /**
     * コンストラクタ
     */
    public function __construct(array $arrSelfInfo) {
        parent::__construct($arrSelfInfo);
    }

    /**
     * インストール
     * installはプラグインのインストール時に実行されます.
     * 引数にはdtb_pluginのプラグイン情報が渡されます.
     *
     * @param array $arrPlugin plugin_infoを元にDBに登録されたプラグイン情報(dtb_plugin)
     * @return void
     */
    function install($arrPlugin) {
        // 初期値設定(PC)
        $arrDataPc = array();
        $arrDataPc['rss_disp'] = '5';

        // 初期値設定(SP)
        $arrDataSp = array();
        $arrDataSp['rss_disp'] = '3';

        $objQuery =& SC_Query_Ex::getSingletonInstance();
        $objQuery->begin();
        // UPDATEする値を作成する。
        $sqlval = array();
        $sqlval['free_field1'] = serialize($arrDataPc);  // PC
        $sqlval['free_field2'] = serialize($arrDataSp);  // SP
        $where = "plugin_code = '" . self::CLASS_NAME . "'";
        // UPDATEの実行
        $objQuery->update('dtb_plugin', $sqlval, $where);
        $objQuery->commit();

        unset($arrDataPc);
        unset($arrDataSp);
        unset($sqlval);

        // 必要なファイルをコピー
        $pluginData = PLUGIN_UPLOAD_REALDIR . '/' . self::CLASS_NAME . '/';
        $pluginHtml = PLUGIN_HTML_REALDIR   . '/' . self::CLASS_NAME . '/';

        copy($pluginData . 'data_copy/plg_RssFeedBlock_bloc_pc.tpl', TEMPLATE_REALDIR . 'frontparts/bloc/plg_RssFeedBlock_bloc.tpl');
        copy($pluginData . 'data_copy/plg_RssFeedBlock_bloc_sp.tpl', SMARTPHONE_TEMPLATE_REALDIR . 'frontparts/bloc/plg_RssFeedBlock_bloc.tpl');
        copy($pluginData . 'html_copy/logo.png'     , $pluginHtml . 'logo.png');
        copy($pluginData . 'html_copy/style_pc.css' , $pluginHtml . 'style_pc.css');
        copy($pluginData . 'html_copy/style_sp.css' , $pluginHtml . 'style_sp.css');
        copy($pluginData . 'html_copy/plg_RssFeedBlock_bloc.php' , HTML_REALDIR . 'frontparts/bloc/plg_RssFeedBlock_bloc.php');

        // ブロック追加
        self::insertBloc($arrPlugin);
    }

    /**
     * アンインストール
     * uninstallはアンインストール時に実行されます.
     * 引数にはdtb_pluginのプラグイン情報が渡されます.
     *
     * @param array $arrPlugin プラグイン情報の連想配列(dtb_plugin)
     * @return void
     */
    function uninstall($arrPlugin) {
        $objQuery = SC_Query_Ex::getSingletonInstance();
        $objQuery->begin();

        // ブロックポジションを削除する
        $arrBlocId = $objQuery->getCol('bloc_id', 'dtb_bloc', 'device_type_id = ? AND plugin_id = ?', array(DEVICE_TYPE_PC , $arrPlugin['plugin_id']));
        $bloc_id = (int) $arrBlocId[0];
        $objQuery->delete('dtb_blocposition', 'device_type_id = ? AND bloc_id = ?', array(DEVICE_TYPE_PC , $bloc_id));

        $arrBlocId = $objQuery->getCol('bloc_id', 'dtb_bloc', 'device_type_id = ? AND plugin_id = ?', array(DEVICE_TYPE_SMARTPHONE , $arrPlugin['plugin_id']));
        $bloc_id = (int) $arrBlocId[0];
        $objQuery->delete('dtb_blocposition', 'device_type_id = ? AND bloc_id = ?', array(DEVICE_TYPE_SMARTPHONE , $bloc_id));
        $objQuery->commit();

        // ブロックを削除する
        $objQuery->delete('dtb_bloc', 'plugin_id = ?', array($arrPlugin['plugin_id']));

        // ファイル・フォルダ削除
        SC_Helper_FileManager_Ex::deleteFile(PLUGIN_HTML_REALDIR   . self::CLASS_NAME);
        SC_Helper_FileManager_Ex::deleteFile(PLUGIN_UPLOAD_REALDIR . self::CLASS_NAME);
        SC_Helper_FileManager_Ex::deleteFile(TEMPLATE_REALDIR . "frontparts/bloc/plg_RssFeedBlock_bloc.tpl");
        SC_Helper_FileManager_Ex::deleteFile(SMARTPHONE_TEMPLATE_REALDIR . "frontparts/bloc/plg_RssFeedBlock_bloc.tpl");
        SC_Helper_FileManager_Ex::deleteFile(HTML_REALDIR . "frontparts/bloc/plg_RssFeedBlock_bloc.php");
    }

    /**
     * 稼働
     * enableはプラグインを有効にした際に実行されます.
     * 引数にはdtb_pluginのプラグイン情報が渡されます.
     *
     * @param array $arrPlugin プラグイン情報の連想配列(dtb_plugin)
     * @return void
     */
    function enable($arrPlugin) {
        // nop
    }

    /**
     * 停止
     * disableはプラグインを無効にした際に実行されます.
     * 引数にはdtb_pluginのプラグイン情報が渡されます.
     *
     * @param array $arrPlugin プラグイン情報の連想配列(dtb_plugin)
     * @return void
     */
    function disable($arrPlugin) {
        // nop
    }

    /**
     * 処理の介入箇所とコールバック関数を設定
     * registerはプラグインインスタンス生成時に実行されます
     *
     * @param SC_Helper_Plugin $objHelperPlugin
     */
    function register(SC_Helper_Plugin $objHelperPlugin) {
        $template_dir = PLUGIN_UPLOAD_REALDIR . self::CLASS_NAME . '/plg_RssFeedBlock_header.php';
        $objHelperPlugin->setHeadNavi($template_dir);
        return parent::register($objHelperPlugin, $priority);
    }

    function insertBloc($arrPlugin) {
        $objQuery = SC_Query_Ex::getSingletonInstance();
        // PC用ブロック追加
        $sqlval_bloc = array();
        $sqlval_bloc['device_type_id'] = DEVICE_TYPE_PC;
        $sqlval_bloc['bloc_id']        = $objQuery->max('bloc_id', 'dtb_bloc', 'device_type_id = ' . DEVICE_TYPE_PC) + 1;
        $sqlval_bloc['bloc_name']      = '【プラグイン】' . $arrPlugin['plugin_name'];
        $sqlval_bloc['tpl_path']       = 'plg_RssFeedBlock_bloc.tpl';
        $sqlval_bloc['filename']       = 'plg_RssFeedBlock_bloc';
        $sqlval_bloc['create_date']    = 'CURRENT_TIMESTAMP';
        $sqlval_bloc['update_date']    = 'CURRENT_TIMESTAMP';
        $sqlval_bloc['php_path']       = 'frontparts/bloc/plg_RssFeedBlock_bloc.php';
        $sqlval_bloc['deletable_flg']  = 0;
        $sqlval_bloc['plugin_id']      = $arrPlugin['plugin_id'];
        $objQuery->insert('dtb_bloc', $sqlval_bloc);
        // SP用ブロック追加
        $sqlval_bloc['device_type_id'] = DEVICE_TYPE_SMARTPHONE;
        $sqlval_bloc['bloc_id']        = $objQuery->max('bloc_id', 'dtb_bloc', 'device_type_id = ' . DEVICE_TYPE_SMARTPHONE) + 1;
        $sqlval_bloc['tpl_path']       = 'plg_RssFeedBlock_bloc.tpl';
        $sqlval_bloc['filename']       = 'plg_RssFeedBlock_bloc';
        $objQuery->insert('dtb_bloc', $sqlval_bloc);
    }

    // プラグイン設定の情報を取得
    function loadData($objPage) {
        $arrRet = array();
        $arrData = SC_Plugin_Util_Ex::getPluginByPluginCode(self::CLASS_NAME);

        switch($objPage->arrPageLayout['device_type_id']){
            case DEVICE_TYPE_MOBILE:
                break;
            case DEVICE_TYPE_SMARTPHONE:
                if (!SC_Utils_Ex::isBlank($arrData['free_field2'])) {
                    $arrRet = unserialize($arrData['free_field2']);
                }
                break;
            case DEVICE_TYPE_PC:
                if (!SC_Utils_Ex::isBlank($arrData['free_field1'])) {
                    $arrRet = unserialize($arrData['free_field1']);
                }
                break;
            case DEVICE_TYPE_ADMIN:
                break;
            default:
                break;
        }
        return $arrRet;
    }
}
?>