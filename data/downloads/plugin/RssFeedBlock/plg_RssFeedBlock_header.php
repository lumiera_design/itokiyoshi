<?php
/*
 * RSSフィード取得ブロック生成プラグイン
 * 
 * Copyright (C) 2014 takami_kazuya@aratana
 * http://www.aratana.jp
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

$arrPageLayout = $this->get_template_vars('arrPageLayout');

switch($arrPageLayout['device_type_id']){
    case DEVICE_TYPE_MOBILE:
        break;
    case DEVICE_TYPE_SMARTPHONE:
        echo "<link rel='stylesheet' type='text/css' href='" . PLUGIN_HTML_URLPATH . "RssFeedBlock/style_sp.css' />";
        break;
    case DEVICE_TYPE_PC:
        echo "<link rel='stylesheet' type='text/css' href='" . PLUGIN_HTML_URLPATH . "RssFeedBlock/style_pc.css' />";
        break;
    case DEVICE_TYPE_ADMIN:
        break;
    default:
        break;
}
?>