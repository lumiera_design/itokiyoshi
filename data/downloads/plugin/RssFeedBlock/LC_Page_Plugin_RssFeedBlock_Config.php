<?php
/*
 * RSSフィード取得ブロック生成プラグイン
 * 
 * Copyright (C) 2014 takami_kazuya@aratana
 * http://www.aratana.jp
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

// {{{ requires
require_once CLASS_EX_REALDIR . 'page_extends/admin/LC_Page_Admin_Ex.php';

/**
 * プラグインファイル自動生成のクラス
 *
 * @package RssFeedBlock
 * @author ARATANA CO.,LTD.
 * @version $Id: $
 */
class LC_Page_Plugin_RssFeedBlock_Config extends LC_Page_Admin_Ex {
    // 定数宣言
    const CLASS_NAME = 'RssFeedBlock';
    var $arrForm = array();

    /**
     * 初期化する.
     *
     * @return void
     */
    function init() {
        parent::init();
        $this->tpl_mainpage = PLUGIN_UPLOAD_REALDIR . self::CLASS_NAME . "/config.tpl";
        $this->tpl_subtitle = "RSSフィード取得ブロック生成プラグイン";
    }

    /**
     * プロセス.
     *
     * @return void
     */
    function process() {
        $this->action();
        $this->sendResponse();
    }

    /**
     * Page のアクション.
     *
     * @return void
     */
    function action() {
        //かならずPOST値のチェックを行う
        $objFormParam = new SC_FormParam_Ex();
        $this->lfInitParam($objFormParam);
        $objFormParam->setParam($_POST);
        $objFormParam->convParam();
        $arrForm = array();
        $device  = '';

        $device = isset($_POST['device']) ? htmlspecialchars($_POST['device'], ENT_QUOTES, 'UTF-8') : 'PC';
        $device = isset($_GET['device']) ? htmlspecialchars($_GET['device'], ENT_QUOTES, 'UTF-8') : 'PC';

        $css_file_path = PLUGIN_HTML_REALDIR . self::CLASS_NAME . "/style_" . strtolower($device) . ".css";

        switch ($this->getMode()) {
        case 'register':
            $arrForm = $objFormParam->getHashArray();
            $this->arrErr = $objFormParam->checkError();

            // エラーなしの場合にはデータを送信
            if (count($this->arrErr) == 0) {
                $this->arrErr = $this->registData($arrForm , $_POST['device'] , $css_file_path);
                if (count($this->arrErr) == 0) {
                    SC_Utils_Ex::clearCompliedTemplate();
                    $this->tpl_onload = "alert('設定が完了しました。');";
                }
            }
            break;
        default:
            $arrForm = $this->loadData();
            $arrForm['rss_css'] = $this->getTplMainpage($css_file_path);
            $this->tpl_is_init = true;
            break;
        }
        $this->arrForm = $arrForm;
        // ポップアップ用の画面は管理画面のナビゲーションを使わない
        $this->setTemplate($this->tpl_mainpage);
    }

    /**
     * デストラクタ.
     *
     * @return void
     */
    function destroy() {
        //parent::destroy();
    }

    /**
     * パラメーター情報の初期化
     *
     * @param object $objFormParam SC_FormParamインスタンス
     *
     * コンバートオプション	意味
     *   r	 「全角」英字を「半角」に変換します。
     *   R	 「半角」英字を「全角」に変換します。
     *   n	 「全角」数字を「半角」に変換します。
     *   N	 「半角」数字を「全角」に変換します。
     *   a	 「全角」英数字を「半角」に変換します。
     *   A	 「半角」英数字を「全角」に変換します （"a", "A" オプションに含まれる文字は、U+0022, U+0027, U+005C, U+007Eを除く U+0021 - U+007E の範囲です）。
     *   s	 「全角」スペースを「半角」に変換します（U+3000 -> U+0020）。
     *   S	 「半角」スペースを「全角」に変換します（U+0020 -> U+3000）。
     *   k	 「全角カタカナ」を「半角カタカナ」に変換します。
     *   K	 「半角カタカナ」を「全角カタカナ」に変換します。
     *   h	 「全角ひらがな」を「半角カタカナ」に変換します。
     *   H	 「半角カタカナ」を「全角ひらがな」に変換します。
     *   c	 「全角カタカナ」を「全角ひらがな」に変換します。
     *   C	 「全角ひらがな」を「全角カタカナ」に変換します。
     *   V	 濁点付きの文字を一文字に変換します。"K", "H" と共に使用します。
     *
     *   //チェックオプション
     *   See class => data/class/SC_CheckError.php
     *
     * @return void
     */
    function lfInitParam(&$objFormParam) {
        $objFormParam->addParam('RSS_URL' , 'rss_url'    , LLTEXT_LEN, 'n' , array());
        $objFormParam->addParam('FORMAT'  , 'rss_format' , LLTEXT_LEN, 'n' , array());
        $objFormParam->addParam('表示数'  , 'rss_disp'   , 3, 'n' , array('NUM_CHECK'));
        $objFormParam->addParam('CSS'     , 'rss_css'    , LLTEXT_LEN, 'n' , array());
    }

    /**
     * プラグイン設定値をDBから取得.
     *
     * @return void
     */
    function loadData() {
        $arrRet = array();
        $arrData = SC_Plugin_Util_Ex::getPluginByPluginCode(self::CLASS_NAME);

        if ($_GET['device'] == "SP"){
            if (!SC_Utils_Ex::isBlank($arrData['free_field2'])) {
                $arrRet = unserialize($arrData['free_field2']);
            }
        } else {
            if (!SC_Utils_Ex::isBlank($arrData['free_field1'])) {
                $arrRet = unserialize($arrData['free_field1']);
            }
        }
        return $arrRet;
    }

    /**
     * プラグイン設定値をDBに書き込み.
     *
     * @return void
     */
    function registData($arrData , $device, $css_file_path) {
        $objQuery = SC_Query_Ex::getSingletonInstance();
        $objQuery->begin();

        //ファイル更新
        if (!SC_Helper_FileManager_Ex::sfWriteFile($css_file_path, $arrData['rss_css'])) {
            $arrErr['plugin_code'] = '※ CSSファイルの書き込みに失敗しました<br />';
            $objQuery->rollback();
            return $arrErr;
        }
        // CSSデータは配列から削除しておく
        unset($arrData['rss_css']);

        // UPDATEする値を作成する。
        $sqlval = array();
        switch ($device){
          case "PC": $sqlval['free_field1'] = serialize($arrData); break;
          case "SP": $sqlval['free_field2'] = serialize($arrData); break;
        }
        $sqlval['update_date'] = 'CURRENT_TIMESTAMP';
        $where = "plugin_code = '" . self::CLASS_NAME . "'";
        // UPDATEの実行
        $objQuery->update('dtb_plugin', $sqlval, $where);

        $objQuery->commit();
    }

    /**
     * ページデータを取得する.
     *
     * @param integer $device_type_id 端末種別ID
     * @param integer $page_id ページID
     * @param SC_Helper_PageLayout $objLayout SC_Helper_PageLayout インスタンス
     * @return array ページデータの配列
     */
    function getTplMainpage($file_path) {
        if (file_exists($file_path)) {
            $arrfileData = file_get_contents($file_path);
        }
        return $arrfileData;
    }
}
?>