<?php
/*
 * Copyright(c) 2000-2013 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

require_once CLASS_EX_REALDIR . 'page_extends/frontparts/bloc/LC_Page_FrontParts_Bloc_Ex.php';

/**
 * RssFeedBlock のページクラス.
 *
 * @package Page
 * @author LOCKON CO.,LTD.
 * @version $Id: LC_Page_FrontParts_Bloc_Best5 - Copy.php -1   $
 */
class LC_Page_FrontParts_Bloc_RssFeedBlock extends LC_Page_FrontParts_Bloc_Ex
{
    /**
     * Page を初期化する.
     *
     * @return void
     */
    public function init()
    {
        parent::init();
    }

    /**
     * Page のプロセス.
     *
     * @return void
     */
    public function process()
    {
        $this->action();
        $this->sendResponse();
    }

    /**
     * Page のアクション.
     *
     * @return void
     */
    public function action()
    {
        // 基本情報を渡す
        $objSiteInfo = SC_Helper_DB_Ex::sfGetBasisData();
        $this->arrInfo = $objSiteInfo->data;

        //おすすめ商品表示
        $this->arrRssFeedBlock = $this->lfGetRssFeedBlock();
    }

    /**
     * RSS情報取得
     *
     * @return array $arrRssFeedBlock 取得結果配列
     */
    public function lfGetRssFeedBlock()
    {
        $arrPlugin = array();
        $arrData = SC_Plugin_Util_Ex::getPluginByPluginCode('RssFeedBlock');

        switch(SC_Display_Ex::detectDevice()){
            case DEVICE_TYPE_MOBILE:
                break;
            case DEVICE_TYPE_SMARTPHONE:
                if (!SC_Utils_Ex::isBlank($arrData['free_field2'])) {
                    $arrPlugin = unserialize($arrData['free_field2']);
                }
                break;
            case DEVICE_TYPE_PC:
                if (!SC_Utils_Ex::isBlank($arrData['free_field1'])) {
                    $arrPlugin = unserialize($arrData['free_field1']);
                }
                break;
            case DEVICE_TYPE_ADMIN:
                break;
            default:
                break;
        }

        $max_count = $arrPlugin['rss_disp'];
        $count = 0;

        $feedurl  = $arrPlugin['rss_url'];
        $feeddata = file_get_contents($feedurl);
       /* $feeddata = preg_replace("/<([^>]+?):(.+?)>/", "<$1_$2>", $feeddata);*/
        $feeddata = simplexml_load_string($feeddata,'SimpleXMLElement',LIBXML_NOCDATA);

        switch ($arrPlugin['rss_format']){
            case 'rss2':
                foreach($feeddata->channel->item as $item){
                    if ($count < $max_count){
                        $response[] = $item;
                        $count++;
                    } else {
                        break;
                    }
                }
                break;
            case 'atom':
                foreach($feeddata->entry as $item){
                    if ($count < $max_count){
                        $response[] = $item;
                        $count++;
                    } else {
                        break;
                    }
                }
                break;
        }
        return $response;
    }
}
