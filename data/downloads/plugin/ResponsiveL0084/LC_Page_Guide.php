<?php
/*
 * ResponsiveL0084
 * Copyright (C) 2013 LOCKON CO.,LTD. All Rights Reserved.
 * http://www.lockon.co.jp/
 */

// {{{ requires
require_once CLASS_EX_REALDIR . 'page_extends/LC_Page_Ex.php';

/**
 * ご利用ガイドのページクラス
 *
 * @package ResponsiveL0084
 * @author LOCKON CO.,LTD.
 * @version $Id: $
 */
class LC_Page_Guide extends LC_Page_Ex {

    /**
     * 初期化する.
     *
     * @return void
     */
    function init() {
        parent::init();
        $this->tpl_mainpage = 'guide/plg_ResponsiveL0084_guide.tpl';
        $this->tpl_title    = 'ご利用ガイド';
    }

    /**
     * プロセス.
     *
     * @return void
     */
    function process() {
        $this->action();
        $this->sendResponse();
    }

    /**
     * Page のアクション.
     *
     * @return void
     */
    function action() {
    }
}
?>
