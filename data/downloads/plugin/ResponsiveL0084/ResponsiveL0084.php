<?php
/*
 * ResponsiveL0084
 * Copyright (C) 2013 LOCKON CO.,LTD. All Rights Reserved.
 * http://www.lockon.co.jp/
 */
require_once PLUGIN_UPLOAD_REALDIR .  'ResponsiveL0084/define.php';  // 定数設定

/**
 * プラグインのメインクラス
 *
 * @package ResponsiveL0084
 * @author LOCKON CO.,LTD.
 * @version $Id: $
 */
class ResponsiveL0084 extends SC_Plugin_Base {

    /**
     * コンストラクタ
     */
    public function __construct(array $arrSelfInfo) {
        parent::__construct($arrSelfInfo);
    }
    
    /**
     * インストール
     * installはプラグインのインストール時に実行されます.
     * 引数にはdtb_pluginのプラグイン情報が渡されます.
     *
     * @param array $arrPlugin plugin_infoを元にDBに登録されたプラグイン情報(dtb_plugin)
     * @return void
     */
    function install($arrPlugin) {
        // 必要なファイルをコピー
        self::file_copy($arrPlugin);

        // バックアップ用テーブル作成
        self::createResponsiveL0084Table();
    }

    /*
     * アンインストール
     * uninstallはアンインストール時に実行されます.
     * 引数にはdtb_pluginのプラグイン情報が渡されます.
     * 
     * @param array $arrPlugin プラグイン情報の連想配列(dtb_plugin)
     * @return void
     */
    function uninstall($arrPlugin) {
        // 固有のファイルを削除
        self::file_delete($arrPlugin);
        
        // テーブルを削除
        self::dropeResponsiveL0084Table();
    }

    /**
     * 稼働
     * enableはプラグインを有効にした際に実行されます.
     * 引数にはdtb_pluginのプラグイン情報が渡されます.
     *
     * @param array $arrPlugin プラグイン情報の連想配列(dtb_plugin)
     * @return void
     */
    function enable($arrPlugin) {
        // テンプレート設定変更
        self::template_create();
        
        // バックアップデータ作成
        self::backupResponsiveL0084Table();
        
        // 初期データ作成
        self::insertResponsiveL0084Table($arrPlugin);
    }

    /**
     * 停止
     * disableはプラグインを無効にした際に実行されます.
     * 引数にはdtb_pluginのプラグイン情報が渡されます.
     *
     * @param array $arrPlugin プラグイン情報の連想配列(dtb_plugin)
     * @return void
     */
    function disable($arrPlugin) {
        // 追加テンプレート情報削除
        self::template_delete();
        
        // バックアップから復旧
        self::restoreResponsiveL0084Table();
    }

    /**
     * 処理の介入箇所とコールバック関数を設定
     * registerはプラグインインスタンス生成時に実行されます
     * 
     * @param SC_Helper_Plugin $objHelperPlugin 
     */
    function register(SC_Helper_Plugin $objHelperPlugin) {
        parent::register($objHelperPlugin, $priority);
    }

    /**
     * preprocess
     * スーパーフックポイント
     *
     * ★Device設定
     * ★ヘッダー検索用カテゴリリスト取得
     * ★ヘッダー用ログイン情報取得
     * ★ヘッダー用カート情報取得
     */
    function preprocess($objPage) {
        //=====端末情報設定=====//
        // 端末情報をリセット
        $objPage->objDisplay->detectDevice(true);
        
        // スマートフォンにPC表示を行う
        $su = new SC_SmartphoneUserAgent_Ex();
        $su->setPcDisplayOn();
        
        // テンプレートの設定
        $layout = new SC_Helper_PageLayout_Ex();
        if (GC_Utils_Ex::isFrontFunction()) {
            $layout->sfGetPageLayout($objPage, false, $_SERVER['SCRIPT_NAME'], $objPage->objDisplay->detectDevice());
        }

        //=====ヘッダー用情報を取得=====//
        // 検索用カテゴリ
        // 商品ID取得
        $product_id = '';
        if (isset($_GET['product_id']) && $_GET['product_id'] != '' && is_numeric($_GET['product_id'])) {
            $product_id = $_GET['product_id'];
        }
        
        // カテゴリID取得
        $category_id = '';
        if (isset($_GET['category_id']) && $_GET['category_id'] != '' && is_numeric($_GET['category_id'])) {
            $category_id = $_GET['category_id'];
        }

        // 選択中のカテゴリIDを判定する
        $objPage->plg_responsive_search_category_id = self::getSearchCategoryId($product_id, $category_id);

        // カテゴリ検索用選択リスト
        $objPage->plg_responsive_cat_list = self::getSearchCategoryList();
    }

    /**
     * frontparts_bloc_category_action
     * カテゴリブロック
     */
    function frontparts_bloc_category_action($objPage) {
        // 子カテゴリ存在フラグ取得
        $arrChildrenData = array();
        if(!SC_Utils_Ex::isBlank($objPage->arrTree)) {
            foreach($objPage->arrTree as $category) {
                if(!SC_Utils_Ex::isBlank($category['children'] > 0)) {
                    $arrChildrenData[$category['category_id']] = 1;
                }
            }
        
        }
        
        $objPage->plg_responsivel0084_children_category_flg = $arrChildrenData;
    }
    
    /**
     * products_list_action
     * 商品一覧
     */
    function products_list_action($objPage) {
        // カテゴリID取得
        $category_id = $objPage->arrSearchData['category_id'];
        
        // カテゴリIDからパンくずデータを取得
        $arrTopicPath = array();

        // 商品が属するカテゴリIDを縦に取得
        $objDb = new SC_Helper_DB_Ex();
        $arrCatID = $objDb->sfGetParents("dtb_category", "parent_category_id", "category_id", $category_id);

        if(is_array($arrCatID) && !empty($arrCatID)) {
            $objQuery = new SC_Query();
            $index_no = 0;
            foreach($arrCatID as $val){
                // カテゴリー名称を取得
                $sql = "SELECT category_name FROM dtb_category WHERE category_id = ?";
                $arrVal = array($val);
                $CatName = $objQuery->getOne($sql, $arrVal);
                if($val != $category_id){
                    $arrTopicPath[$index_no]['name'] = $CatName;
                    $arrTopicPath[$index_no]['link'] = ROOT_URLPATH . "products/list.php?category_id=" .$val;
                } else {
                    $arrTopicPath[$index_no]['name'] = $CatName;
                }
                $index_no++;
            }
        }
        $objPage->plg_responsivel0084_topiclist = $arrTopicPath;
    }

    /**
     * products_review_action
     * レビュー
     */
    function products_review_action($objPage) {
        // テンプレート指定を変更
        $objPage->setTemplate(SITE_FRAME);
    }
    
    /**
     * products_review_complete_action
     * レビュー(完了ページ)
     */
    function products_review_complete_action($objPage) {
        // テンプレート指定を変更
        $objPage->setTemplate(SITE_FRAME);
        $objPage->tpl_mainpage = 'products/review_complete.tpl';
    }

    /**
     * mypage_delivery_addr_action
     * お届け先の追加・変更
     */
    function mypage_delivery_addr_action($objPage) {
        // テンプレート指定を変更
        $objPage->setTemplate(SITE_FRAME);
        $objPage->tpl_mainpage = 'mypage/delivery_addr.tpl';
        $objPage->tpl_mypageno = 'delivery';
        $objCustomer = new SC_Customer_Ex();
        
        // 画面によりタイトル変更
        if($objPage->ParentPage == MYPAGE_DELIVADDR_URLPATH) {
            $objPage->plg_responsivel0084_tpl_title = 'MYページ';
            
            $objPage->CustomerName1 = $objCustomer->getvalue('name01');
            $objPage->CustomerName2 = $objCustomer->getvalue('name02');
            $objPage->CustomerPoint = $objCustomer->getvalue('point');
        } else {
            $objPage->plg_responsivel0084_tpl_title = 'お届け先の追加･変更';
        }
        
        // JavaScript無効化
        $objPage->tpl_onload = "";

        // ログインチェック
        if($objCustomer->isLoginSuccess(true)) {
            $objPage->tpl_login = true;
        }

        // マイページでログインを行っていない場合
        if(!$objCustomer->isLoginSuccess(true) && $objPage->ParentPage != MULTIPLE_URLPATH) {
            $objCustomer = new SC_Customer_Ex();

            // ログインしていない場合は必ずログインページを表示する
            if ($objCustomer->isLoginSuccess(true) === false) {
                // クッキー管理クラス
                $objCookie = new SC_Cookie_Ex();
                    // クッキー判定(メールアドレスをクッキーに保存しているか）
                $objPage->tpl_login_email = $objCookie->getCookie('login_email');
                if ($objPage->tpl_login_email != '') {
                    $objPage->tpl_login_memory = '1';
                }

                // POSTされてきたIDがある場合は優先する。
                if (isset($_POST['login_email'])
                    && $_POST['login_email'] != ''
                ) {
                    $objPage->tpl_login_email = $_POST['login_email'];
                }

                // 携帯端末IDが一致する会員が存在するかどうかをチェックする。
                if (SC_Display_Ex::detectDevice() === DEVICE_TYPE_MOBILE) {
                    $objPage->tpl_valid_phone_id = $objCustomer->checkMobilePhoneId();
                }
                $objPage->tpl_title        = 'MYページ(ログイン)';
                $objPage->tpl_mainpage     = 'mypage/login.tpl';
            }
        } else if(($objPage->getMode() == 'edit') && (empty($objPage->arrErr))) {
               // 登録が成功した場合
            // 元画面に遷移
            if (!empty($objPage->ParentPage)) {
                SC_Response_Ex::sendRedirect($objPage->getLocation($objPage->ParentPage));
                SC_Response_Ex::actionExit();
            }
        }
    }
    
    /**
     * mypage_mail_view_action
     * メール配信履歴
     */
    function mypage_mail_view_action($objPage) {
        // テンプレート指定を変更
        $objPage->setTemplate(SITE_FRAME);
        $objPage->tpl_mainpage = 'mypage/mail_view.tpl';
    }
    
    /**
     * mypage_favorite_action
     * お気に入り一覧
     */
    function mypage_favorite_action($objPage) {
        // ページ送りの取得
        $objNavi        = new SC_PageNavi_Ex($objPage->tpl_pageno, $objPage->tpl_linemax, SEARCH_PMAX, 'eccube.movePage', NAVI_PMAX, 'pageno=#page#');
        $objPage->objNavi = $objNavi ;
    }

    /**
     * file_create_list
     * コピーするファイルのリストを作成
     */
    function file_create_list($arrPlugin) {
        // コピー元, コピー先, 手動削除フラグ
        $file_list = array();
        
        $file_list[] = array(PLUGIN_UPLOAD_REALDIR . $arrPlugin['plugin_code'] . "/logo.png", PLUGIN_HTML_REALDIR . $arrPlugin['plugin_code'] . "/logo.png", false);
        $file_list[] = array(PLUGIN_UPLOAD_REALDIR . $arrPlugin['plugin_code'] . "/templates/plg_responsivel0084/", SMARTY_TEMPLATES_REALDIR . "plg_responsivel0084/", false);
        $file_list[] = array(PLUGIN_UPLOAD_REALDIR . $arrPlugin['plugin_code'] . "/html/user_data/packages/plg_responsivel0084/", USER_TEMPLATE_REALDIR . "plg_responsivel0084/", false);
        $file_list[] = array(PLUGIN_UPLOAD_REALDIR . $arrPlugin['plugin_code'] . "/html/guide/plg_ResponsiveL0084_guide.php", HTML_REALDIR . "guide/plg_ResponsiveL0084_guide.php", true);
        $file_list[] = array(PLUGIN_UPLOAD_REALDIR . $arrPlugin['plugin_code'] . "/html/frontparts/bloc/plg_ResponsiveL0084_frontparts_bloc_global_nav.php", HTML_REALDIR . "frontparts/bloc/plg_ResponsiveL0084_frontparts_bloc_global_nav.php", true);

        return $file_list;
    }

    /**
     * folder_create_list
     * 作成するフォルダのリストを作成
     */
    function folder_create_list($arrPlugin) {
        // 作成先, 手動削除フラグ
        $folder_list = array();
        $folder_list[] = array(SMARTY_TEMPLATES_REALDIR . "plg_responsivel0084", true);
        $folder_list[] = array(DATA_REALDIR . 'Smarty/templates_c/plg_responsiveLl0084', true);
        $folder_list[] = array(USER_TEMPLATE_REALDIR . "plg_responsivel0084/", true);
    
        return $folder_list;
    }

    /**
     * file_copy
     * ファイルコピー関数
     */
    function file_copy($arrPlugin) {
        // フォルダ作成
        $make_folder_list = self::folder_create_list($arrPlugin);
        if(is_array($make_folder_list) && !empty($make_folder_list)) {
            foreach($make_folder_list as $val) {
                if(!file_exists($val[0])) {
                    mkdir($val[0]);
                }
            }
        }

        // ファイルコピー
        $copy_file_list = self::file_create_list($arrPlugin);
        if(is_array($copy_file_list) && !empty($copy_file_list)) {
            foreach($copy_file_list as $val) {
                if(is_dir($val[0])) {
                    // ディレクトリ指定の場合再帰的にコピー
                    SC_Utils_Ex::sfCopyDir($val[0], $val[1]);
                } else {
                    copy($val[0], $val[1]);
                }
            }
        }
    }

    /**
     * file_delete
     * ファイル削除関数
     */
    function file_delete($arrPlugin) {
        $delete_file_list = self::file_create_list($arrPlugin);
        if(is_array($delete_file_list) && !empty($delete_file_list)) {
            foreach($delete_file_list as $val) {
                if($val[2] == true) {
                    SC_Helper_FileManager_Ex::deleteFile($val[1]);
                }
            }
        }
        
        // フォルダ作成
        $delete_folder_list = self::folder_create_list($arrPlugin);
        if(is_array($delete_folder_list) && !empty($delete_folder_list)) {
            foreach($delete_folder_list as $val) {
                if($val[1] == true) {
                    SC_Helper_FileManager_Ex::deleteFile($val[0]);
                }
            }
        }
    }
    
    /**
     * template_create
     * DBおよびファイルシステムにテンプレートパッケージを追加する.
     */
    function template_create() {
        $objQuery = SC_Query_Ex::getSingletonInstance();
        $objQuery->begin();

        $arrValues = array(
            'template_code' => 'plg_responsivel0084',
            'device_type_id' => DEVICE_TYPE_PC,
            'template_name' => 'レスポンシブWebデザイン',
            'create_date' => 'CURRENT_TIMESTAMP',
            'update_date' => 'CURRENT_TIMESTAMP',
        );

        $objQuery->insert('dtb_templates', $arrValues);

        $objQuery->commit();
    }
    
    /**
     * template_delete
     * DBおよびファイルシステムに追加したテンプレートパッケージを削除する.
     */
    function template_delete() {
        $objQuery = SC_Query_Ex::getSingletonInstance();
        $objQuery->begin();

        $objQuery->delete('dtb_templates', 'template_code = ?', array('plg_responsivel0084'));

        $objQuery->commit();
    }

    /**
     * createResponsiveL0084Table
     * テーブルを作成
     */
    function createResponsiveL0084Table() {
        $objQuery =& SC_Query_Ex::getSingletonInstance();
        
        if(DB_TYPE == "pgsql") { // POSTGRES
        // ブロックバックアップテーブル作成
        $sql = <<< EOT
CREATE TABLE plg_responsivel0084_bloc (
    device_type_id int NOT NULL,
    bloc_id int NOT NULL,
    bloc_name text,
    tpl_path text,
    filename text NOT NULL,
    create_date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    update_date timestamp NOT NULL,
    php_path text,
    deletable_flg smallint NOT NULL DEFAULT 1,
    plugin_id int,
    PRIMARY KEY (device_type_id, bloc_id),
    UNIQUE (device_type_id, filename)
);
EOT;
        $objQuery->query($sql);

         // ブロックポジションバックアップテーブル作成
        $sql = <<< EOT
CREATE TABLE plg_responsivel0084_blocposition (
    device_type_id int NOT NULL,
    page_id int NOT NULL,
    target_id int NOT NULL,
    bloc_id int NOT NULL,
    bloc_row int,
    anywhere smallint DEFAULT 0 NOT NULL,
    PRIMARY KEY (device_type_id, page_id, target_id, bloc_id)
);
EOT;
        $objQuery->query($sql);
        
        } else { // MySQL
        // ブロックバックアップテーブル作成
        $sql = <<< EOT
CREATE TABLE plg_responsivel0084_bloc (
    device_type_id int NOT NULL,
    bloc_id int NOT NULL,
    bloc_name text,
    tpl_path text,
    filename text NOT NULL,
    create_date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    update_date timestamp NOT NULL,
    php_path text,
    deletable_flg smallint NOT NULL DEFAULT 1,
    plugin_id int,
    PRIMARY KEY (device_type_id, bloc_id),
    UNIQUE (device_type_id, filename(255))
);
EOT;
        $objQuery->query($sql);

         // ブロックポジションバックアップテーブル作成
        $sql = <<< EOT
CREATE TABLE plg_responsivel0084_blocposition (
    device_type_id int NOT NULL,
    page_id int NOT NULL,
    target_id int NOT NULL,
    bloc_id int NOT NULL,
    bloc_row int,
    anywhere smallint DEFAULT 0 NOT NULL,
    PRIMARY KEY (device_type_id, page_id, target_id, bloc_id)
);
EOT;

        $objQuery->query($sql);
        }

        // 定数バックアップテーブル作成
        $sql = <<< EOT
CREATE TABLE plg_responsivel0084_constants (
    id text NOT NULL,
    value text
);
EOT;

        $objQuery->query($sql);
    }
    
    /**
     * dropeResponsiveL0084Table
     * テーブルを削除
     */
    function dropeResponsiveL0084Table() {
        $objQuery =& SC_Query_Ex::getSingletonInstance();

        $objQuery->query("DROP TABLE plg_responsivel0084_bloc");          // ブロックバックアップテーブル
        $objQuery->query("DROP TABLE plg_responsivel0084_blocposition");  // ブロックポジションバックアップテーブル
        $objQuery->query("DROP TABLE plg_responsivel0084_constants");     // 定数バックアップテーブル
    }

    /**
     * backupResponsiveL0084Table
     * レスポンシブデータバックアップ
     */
    function backupResponsiveL0084Table() {
        $objQuery =& SC_Query_Ex::getSingletonInstance();

        // ブロックデータバックアップ
        $objQuery->delete("plg_responsivel0084_bloc");
        $objQuery->query("INSERT INTO plg_responsivel0084_bloc SELECT * FROM dtb_bloc");
        
        // ブロックポジションデータバックアップ
        $objQuery->delete("plg_responsivel0084_blocposition");
        $objQuery->query("INSERT INTO plg_responsivel0084_blocposition SELECT * FROM dtb_blocposition");
        
        // 定数バックアップ
        $objQuery->delete("plg_responsivel0084_constants");

        $arrData = array('id' => 'template_name', 'value' => TEMPLATE_NAME);
        $objQuery->insert('plg_responsivel0084_constants', $arrData);

        $arrData = array('id' => 'navi_pmax',     'value' => NAVI_PMAX);
        $objQuery->insert('plg_responsivel0084_constants', $arrData);

        $arrData = array('id' => 'large_image_width',     'value' => LARGE_IMAGE_WIDTH);
        $objQuery->insert('plg_responsivel0084_constants', $arrData);

        $arrData = array('id' => 'large_image_height',     'value' => LARGE_IMAGE_HEIGHT);
        $objQuery->insert('plg_responsivel0084_constants', $arrData);

        $arrData = array('id' => 'small_image_width',     'value' => SMALL_IMAGE_WIDTH);
        $objQuery->insert('plg_responsivel0084_constants', $arrData);

        $arrData = array('id' => 'small_image_height',     'value' => SMALL_IMAGE_HEIGHT);
        $objQuery->insert('plg_responsivel0084_constants', $arrData);

        $arrData = array('id' => 'normal_image_width',     'value' => NORMAL_IMAGE_WIDTH);
        $objQuery->insert('plg_responsivel0084_constants', $arrData);
        
        $arrData = array('id' => 'normal_image_height',     'value' => NORMAL_IMAGE_HEIGHT);
        $objQuery->insert('plg_responsivel0084_constants', $arrData);

        $arrData = array('id' => 'normal_subimage_width',     'value' => NORMAL_SUBIMAGE_WIDTH);
        $objQuery->insert('plg_responsivel0084_constants', $arrData);

        $arrData = array('id' => 'normal_subimage_height',     'value' => NORMAL_SUBIMAGE_HEIGHT);
        $objQuery->insert('plg_responsivel0084_constants', $arrData);

        $arrData = array('id' => 'large_subimage_width',     'value' => LARGE_SUBIMAGE_WIDTH);
        $objQuery->insert('plg_responsivel0084_constants', $arrData);

        $arrData = array('id' => 'large_subimage_height',     'value' => LARGE_SUBIMAGE_HEIGHT);
        $objQuery->insert('plg_responsivel0084_constants', $arrData);
    }
    
    /**
     * restoreResponsiveL0084Table
     * レスポンシブデータリストア
     */
    function restoreResponsiveL0084Table() {
        // DELETE→INSERT
        $objQuery =& SC_Query_Ex::getSingletonInstance();

        // ブロックデータバックアップ
        $objQuery->delete("dtb_bloc");
        $objQuery->query("INSERT INTO dtb_bloc SELECT * FROM plg_responsivel0084_bloc");
        
        // ブロックポジションデータバックアップ
        $objQuery->delete("dtb_blocposition");
        $objQuery->query("INSERT INTO dtb_blocposition SELECT * FROM plg_responsivel0084_blocposition");

        //=====定数復旧=====//
        $constants_data = $objQuery->select("*", "plg_responsivel0084_constants");
        
        if(!empty($constants_data)) {
            $constants_value = array();
            foreach($constants_data as $value) {
                $constants_value[$value['id']] = $value['value'];
            }
            
            // 選択テンプレート変更(バックアップ復旧)
            $masterData = new SC_DB_MasterData_Ex();
            $arrData = array();
            $arrData['TEMPLATE_NAME']          = var_export($constants_value['template_name'], true);
            $arrData['NAVI_PMAX']              = $constants_value['navi_pmax'];
            $arrData['LARGE_IMAGE_WIDTH']      = $constants_value['large_image_width'];
            $arrData['LARGE_IMAGE_HEIGHT']     = $constants_value['large_image_height'];
            $arrData['SMALL_IMAGE_WIDTH']      = $constants_value['small_image_width'];
            $arrData['SMALL_IMAGE_HEIGHT']     = $constants_value['small_image_height'];
            $arrData['NORMAL_IMAGE_WIDTH']     = $constants_value['normal_image_width'];
            $arrData['NORMAL_IMAGE_HEIGHT']    = $constants_value['normal_image_height'];
            $arrData['NORMAL_SUBIMAGE_WIDTH']  = $constants_value['normal_subimage_width'];
            $arrData['NORMAL_SUBIMAGE_HEIGHT'] = $constants_value['normal_subimage_height'];
            $arrData['LARGE_SUBIMAGE_WIDTH']   = $constants_value['large_subimage_width'];
            $arrData['LARGE_SUBIMAGE_HEIGHT']  = $constants_value['large_subimage_height'];
            $masterData->updateMasterData('mtb_constants', array(), $arrData);

            // キャッシュを生成
            $masterData->createCache('mtb_constants', array(), true, array('id', 'remarks'));
        }
    }
    
    /**
     * insertResponsiveL0084Table
     * 初期データ登録
     */
    function insertResponsiveL0084Table($arrPlugin) {
        $objQuery =& SC_Query_Ex::getSingletonInstance();

        //=====dtb_blocにブロックを追加する=====//
        $objQuery->begin(); // ブロック登録処理開始
        $bloc_max_id = $objQuery->max('bloc_id', "dtb_bloc", "device_type_id = " . DEVICE_TYPE_PC);
        
        // TOPメイン
        $bloc_max_id ++; // IDを1カウント
        $sqlval_bloc = array();
        $sqlval_bloc['device_type_id'] = DEVICE_TYPE_PC;
        $sqlval_bloc['bloc_id'] = $bloc_max_id;
        $sqlval_bloc['bloc_name'] = "TOPメイン";
        $sqlval_bloc['tpl_path']  = "topmain.tpl";
        $sqlval_bloc['filename']  = "topmain";
        $sqlval_bloc['create_date'] = "CURRENT_TIMESTAMP";
        $sqlval_bloc['update_date'] = "CURRENT_TIMESTAMP";
        $sqlval_bloc['php_path'] = "";
        $sqlval_bloc['deletable_flg'] = 0;
        $sqlval_bloc['plugin_id'] = "";
        $objQuery->insert("dtb_bloc", $sqlval_bloc);
        $bloc_id_top_main = $bloc_max_id; // ブロックポジション用に変数に代入

        // グローバルナビ
        $bloc_max_id ++; // IDを1カウント
        $sqlval_bloc = array();
        $sqlval_bloc['device_type_id'] = DEVICE_TYPE_PC;
        $sqlval_bloc['bloc_id'] = $bloc_max_id;
        $sqlval_bloc['bloc_name'] = "【ヘッダー】グローバルナビ";
        $sqlval_bloc['tpl_path']  = "global_nav.tpl";
        $sqlval_bloc['filename']  = "global_nav";
        $sqlval_bloc['create_date'] = "CURRENT_TIMESTAMP";
        $sqlval_bloc['update_date'] = "CURRENT_TIMESTAMP";
        $sqlval_bloc['php_path'] = "frontparts/bloc/plg_ResponsiveL0084_frontparts_bloc_global_nav.php";
        $sqlval_bloc['deletable_flg'] = 0;
        $sqlval_bloc['plugin_id'] = "";
        $objQuery->insert("dtb_bloc", $sqlval_bloc);
        $bloc_id_global_nav = $bloc_max_id; // ブロックポジション用に変数に代入

        $objQuery->commit(); // ブロック登録処理終了
        
        //=====配置変更====//
        $objQuery->delete("dtb_blocposition");

        //
        $array_blocposition = array(
            array(PAGE_TOP_ID, TARGET_ID_LEFT, BLOCK_CATEGORY_ID, 1, ANYWHERE_ON),
            array(PAGE_TOP_ID, TARGET_ID_LEFT, BLOCK_LOGIN_ID,    2, ANYWHERE_ON),
            array(PAGE_TOP_ID, TARGET_ID_LEFT, BLOCK_GUIDE_ID,    3, ANYWHERE_ON),
            array(PAGE_TOP_ID, TARGET_ID_MAIN_FOOT, BLOCK_RECOMMEND_ID, 1, ANYWHERE_OFF),
            array(PAGE_TOP_ID, TARGET_ID_TOP, $bloc_id_top_main, 1, ANYWHERE_OFF),
            array(PAGE_TOP_ID, TARGET_ID_TOP, BLOCK_NEWS_ID,      2, ANYWHERE_OFF),
            array(PAGE_TOP_ID, TARGET_ID_HEADER_INTERNAL, $bloc_id_global_nav, 1, ANYWHERE_ON)
        );

        
        foreach($array_blocposition as $val) {
            $sqlval_blocposition = array();
            $sqlval_blocposition['device_type_id'] = DEVICE_TYPE_PC;
            $sqlval_blocposition['page_id']        = $val[0];
            $sqlval_blocposition['target_id']      = $val[1];
            $sqlval_blocposition['bloc_id']        = $val[2];
            $sqlval_blocposition['bloc_row']       = $val[3];
            $sqlval_blocposition['anywhere']       = $val[4];
            
            $objQuery->insert("dtb_blocposition", $sqlval_blocposition);
        }

        //=====定数変更=====//
        // 選択テンプレート変更
        $masterData = new SC_DB_MasterData_Ex();
        $arrData = array();
        $arrData['TEMPLATE_NAME']          = var_export("plg_responsivel0084", true);
        $arrData['NAVI_PMAX']              = NAVI_PAGE_MAX_NUM;
        $arrData['LARGE_IMAGE_WIDTH']      = PLG_IMAGE_LARGE_WIDTH;
        $arrData['LARGE_IMAGE_HEIGHT']     = PLG_IMAGE_LARGE_HEIGHT;
        $arrData['SMALL_IMAGE_WIDTH']      = PLG_IMAGE_SMALL_WIDTH;
        $arrData['SMALL_IMAGE_HEIGHT']     = PLG_IMAGE_SMALL_HEIGHT;
        $arrData['NORMAL_IMAGE_WIDTH']     = PLG_IMAGE_NORMAL_WIDTH;
        $arrData['NORMAL_IMAGE_HEIGHT']    = PLG_IMAGE_NORMAL_HEIGHT;
        $arrData['NORMAL_SUBIMAGE_WIDTH']  = PLG_IMAGE_NORMAL_SUB_WIDTH;
        $arrData['NORMAL_SUBIMAGE_HEIGHT'] = PLG_IMAGE_NORMAL_SUB_HEIGHT;
        $arrData['LARGE_SUBIMAGE_WIDTH']   = PLG_IMAGE_LARGE_SUB_WIDTH;
        $arrData['LARGE_SUBIMAGE_HEIGHT']  = PLG_IMAGE_LARGE_SUB_HEIGHT;
        $masterData->updateMasterData('mtb_constants', array(), $arrData);

        // キャッシュを生成
        $masterData->createCache('mtb_constants', array(), true, array('id', 'remarks'));
    }
    

    /**
     * prefilterTransform
     */
    function prefilterTransform(&$source, LC_Page_Ex $objPage, $filename) {
        // SC_Helper_Transformのインスタンスを生成.
        $objTransform = new SC_Helper_Transform($source);
        $template_dir = PLUGIN_UPLOAD_REALDIR . "ResponsiveL0084/templates/";
        
        // 呼び出し元テンプレートを判定します.
        switch($objPage->arrPageLayout['device_type_id']) {
            case DEVICE_TYPE_PC:
            case DEVICE_TYPE_SMARTPHONE:
            case DEVICE_TYPE_MOBILE:
                break;

            // 端末種別：管理画面
            case DEVICE_TYPE_ADMIN:
            default:
                $template_dir .= "admin/";
                // 管理画面デザイン設定サブナビ
                if(strpos($filename, "design/subnavi.tpl") !== false) {
                    // 警告メッセージ表示
                    $objTransform->select(".level1", 0)->replaceElement(file_get_contents($template_dir . "design/subnavi.tpl"));
                }
                break;
        }
        $source = $objTransform->getHTML();
    }
    
    /**
     * getSearchCategoryList
     * カテゴリリスト取得
     *
     * @return array $arrCategoryList カテゴリリスト
     */
    function getSearchCategoryList() {
        $objDb = new SC_Helper_DB_Ex();

        // カテゴリ検索用選択リスト
        $arrCategoryList = $objDb->sfGetCategoryList('', true, '　');
        if (is_array($arrCategoryList)) {
            // 文字サイズを制限する
            foreach ($arrCategoryList as $key => $val) {
                $truncate_str = SC_Utils_Ex::sfCutString($val, SEARCH_CATEGORY_LEN, false);
                $arrCategoryList[$key] = preg_replace('/　/u', '&nbsp;&nbsp;', $truncate_str);
            }
        }
        return $arrCategoryList;
    }

    /**
     * getSearchCategoryId
     * 選択中のカテゴリIDを取得する
     *
     * @return array $arrCategoryId 選択中のカテゴリID
     */
    function getSearchCategoryId($product_id, $category_id) {
        // 選択中のカテゴリIDを判定する
        $objDb = new SC_Helper_DB_Ex();
        $arrCategoryId = $objDb->sfGetCategoryId($product_id, $category_id);
        return $arrCategoryId;
    }
}
?>
