<?php
/*
 * ResponsiveL0084
 * Copyright (C) 2013 LOCKON CO.,LTD. All Rights Reserved.
 * http://www.lockon.co.jp/
 */

/**
 * プラグイン の情報クラス.
 *
 * @package ResponsiveL0084
 * @author LOCKON CO.,LTD.
 * @version $Id: $
 */
class plugin_info{
    /** プラグインコード(必須)：プラグインを識別する為キーで、他のプラグインと重複しない一意な値である必要がありま. */
    static $PLUGIN_CODE       = 'ResponsiveL0084';
    /** プラグイン名(必須)：EC-CUBE上で表示されるプラグイン名. */
    static $PLUGIN_NAME       = 'EC-CUBEレスポンシブWebデザインテンプレート';
    /** クラス名(必須)：プラグインのクラス（拡張子は含まない） */
    static $CLASS_NAME        = 'ResponsiveL0084';
    /** プラグインバージョン(必須)：プラグインのバージョン. */
    static $PLUGIN_VERSION    = '1.0';
    /** 対応バージョン(必須)：対応するEC-CUBEバージョン. */
    static $COMPLIANT_VERSION = '2.13.0～2.13.1';
    /** 作者(必須)：プラグイン作者. */
    static $AUTHOR            = "株式会社ロックオン";
    /** 説明(必須)：プラグインの説明. */
    static $DESCRIPTION       = 'EC-CUBEにレスポンシブWebデザインを適用します';
    /** プラグインURL：プラグイン毎に設定出来るURL（説明ページなど） */
    static $PLUGIN_SITE_URL   = "";
    static $AUTHOR_SITE_URL   = "";
    /** フックポイント **/
    static $HOOK_POINTS       = array(
                                     array('prefilterTransform', 'prefilterTransform')
                                    ,array('LC_Page_preProcess', 'preprocess')
                                    ,array('LC_Page_FrontParts_Bloc_Category_action_after', 'frontparts_bloc_category_action')
                                    ,array('LC_Page_Products_List_action_after', 'products_list_action')
                                    ,array('LC_Page_Products_Review_action_after', 'products_review_action')
                                    ,array('LC_Page_Products_ReviewComplete_action_after', 'products_review_complete_action')
                                    ,array('LC_Page_Mypage_DeliveryAddr_action_after', 'mypage_delivery_addr_action')
                                    ,array('LC_Page_Mypage_MailView_action_after', 'mypage_mail_view_action')
                                    ,array('LC_Page_Mypage_Favorite_action_after', 'mypage_favorite_action')
    );
    // ライセンスについては同梱のLICENSE.txtを参照して下さい。
    static $LICENSE           = "ORG";
}
?>
