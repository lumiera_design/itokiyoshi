<?php
/*
 * ResponsiveL0084
 * Copyright (C) 2013 LOCKON CO.,LTD. All Rights Reserved.
 * http://www.lockon.co.jp/
 */

// ページNO
define('PAGE_TOP_ID', 1); // TOPページ

// ページ番号の最大表示数量(ページャー)
define('NAVI_PAGE_MAX_NUM', 3);

// 既存のブロックID
define('BLOCK_CATEGORY_ID', 1);  // カテゴリ
define('BLOCK_GUIDE_ID', 2);     // 利用ガイド
define('BLOCK_CART_ID', 3);      // カート
define('BLOCK_SEARCH_ID', 4);    // 商品検索
define('BLOCK_NEWS_ID', 5);      // 新着情報
define('BLOCK_LOGIN_ID', 6);     // ログイン
define('BLOCK_RECOMMEND_ID', 7); // おすすめ
define('BLOCK_CALENDER_ID', 8);  // カレンダー

// 全ページ共通フラグ
define('ANYWHERE_ON',  1);
define('ANYWHERE_OFF', 0);

// 画像サイズ
define('PLG_IMAGE_LARGE_WIDTH', 600);       // 拡大画像幅
define('PLG_IMAGE_LARGE_HEIGHT', 600);      // 拡大画像高さ
define('PLG_IMAGE_SMALL_WIDTH', 600);       // 一覧画像幅
define('PLG_IMAGE_SMALL_HEIGHT', 600);      // 一覧画像高さ
define('PLG_IMAGE_NORMAL_WIDTH', 600);      // 通常画像幅
define('PLG_IMAGE_NORMAL_HEIGHT', 600);     // 通常画像高さ
define('PLG_IMAGE_NORMAL_SUB_WIDTH', 600);  // 通常サブ画像幅
define('PLG_IMAGE_NORMAL_SUB_HEIGHT', 600); // 通常サブ画像高さ
define('PLG_IMAGE_LARGE_SUB_WIDTH', 600);   // 拡大サブ画像幅
define('PLG_IMAGE_LARGE_SUB_HEIGHT', 600);  // 拡大サブ画像高さ
