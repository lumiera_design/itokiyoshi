/*--------------------------------------------------------------------------*
 *  
 *  SmoothScroll JavaScript Library V2
 *  
 *  MIT-style license. 
 *  
 *  2007-2011 Kazuma Nishihata 
 *  http://www.to-r.net
 *  
 *--------------------------------------------------------------------------*/
 
new function(){

	var attr ="data-tor-smoothScroll";//for html5 , if you can't use html5 , this value change "class"
	var attrPatt = /noSmooth/;
	var d = document;//document short cut
	
	/*
	 *add Event
	  -------------------------------------------------*/
	function addEvent(elm,listener,fn){
		try{ // IE
			elm.addEventListener(listener,fn,false);
		}catch(e){
			elm.attachEvent(
				"on"+listener
				,function(){
					fn.apply(elm,arguments)
				}
			);
		}
	}

	/*
	 *Start SmoothScroll
	  -------------------------------------------------*/
	function SmoothScroll(a){
		if(d.getElementById(a.rel.replace(/.*\#/,""))){
			var e = d.getElementById(a.rel.replace(/.*\#/,""));
		}else{
			return;
		}
		
		//Move point
		var end=e.offsetTop
		var docHeight = d.documentElement.scrollHeight;
		var winHeight = window.innerHeight || d.documentElement.clientHeight
		if(docHeight-winHeight<end){
			var end = docHeight-winHeight;
		}
		
		//Current Point
		var start=window.pageYOffset || d.documentElement.scrollTop || d.body.scrollTop || 0;
		
		
		var flag=(end<start)?"up":"down";

		function scrollMe(start,end,flag) {
			setTimeout(
				function(){
					if(flag=="up" && start >= end){
						start=start-(start-end)/20-1;
						window.scrollTo(0,start)
						scrollMe(start,end,flag);
					}else if(flag=="down" && start <= end){
						start=start+(end-start)/20+1;
						window.scrollTo(0,start)
						scrollMe(start,end,flag);
					}else{
						scrollTo(0,end);
					}
					return ;
				}
				,10
			);
			
		}

		scrollMe(start,end,flag);
		
	}

	/*
	 *Add SmoothScroll
	  -------------------------------------------------*/
	addEvent(window,"load",function(){
		var anchors = d.getElementsByTagName("a");
		for(var i = 0 ,len=anchors.length; i<len ; i++){
			if(!attrPatt.test(anchors[i].getAttribute(attr)) && 
				anchors[i].href.replace(/\#[a-zA-Z0-9_]+/,"") == location.href.replace(/\#[a-zA-Z0-9_]+/,"")){
				anchors[i].rel = anchors[i].href;
				anchors[i].href = "javascript:void(0)";
				anchors[i].onclick=function(){SmoothScroll(this)}
			}
		}
	});

}


/////// rollover

$(function(){
	$(".rollover").mouseover(function(){
		$(this).attr("src",$(this).attr("src").replace(/^(.+)(\.[a-z]+)$/, "$1_on$2"));
	}).mouseout(function(){
		$(this).attr("src",$(this).attr("src").replace(/^(.+)_on(\.[a-z]+)$/, "$1$2"));
	}).each(function(){
		$("<img>").attr("src",$(this).attr("src").replace(/^(.+)(\.[a-z]+)$/, "$1_on$2"));
	});
});


/////// accordion

$(function(){
	$(".accordion dl dt").click(function(){
		$(this).toggleClass("active");
		$("+dd",this).slideToggle(300);
        return false;
	});
});


////// header_navi

$(function(){
	var $p = $('.header_navi ul');
	var $current = 'active';
	var $search = $('#search_area');

	$('li',$p).each(function(index){
		var $t = $(this);
		var $tc = $('span.btn', this);
		var $c = $t.attr('id');
		
		$tc.click(function(){
			if(!$t.is('.'+$current)){
				// img change
				$('li', $p).removeClass($current);
				$t.addClass($current);
				$('img',$p).each(function(){
					var $imgOv = String($(this).attr("src"));
					if($(this).attr('src').match(/_close/)){
						$(this).attr('src',$imgOv.replace(/(.+)_close\.(.*)$/,"$1.$2"));
					}
				});
				$('img',$t).each(function(index, element) {
					var $imgOff = String($(this).attr("src"));
					if(!$imgOff.match(/_close/)){
						$(this).attr('src',$imgOff.replace(/(.+)\.(.*)$/,"$1_close.$2"));
					}
				});
				// element change
				switch($t.index()){
					case 0 :
						$search.addClass($current);
						break;
				}
			}else{
				$search.removeClass($current);
				$t.removeClass($current);
				$('img',this).each(function(){
					var $imgOv = String($(this).attr("src"));
					if($(this).attr('src').match(/_close/)){
						$(this).attr('src',$imgOv.replace(/(.+)_close\.(.*)$/,"$1.$2"));
					}
				});
			}
		});
	});
	
});


/////// category_accordion

$(document).ready(function(){
	$("#categorytree li .toggle").click(function(){
		if($("+ul",this).css("display")=="none"){
			$(this).parent().addClass("active");
			$("+ul",this).slideDown(300);
		}else{
			$(this).parent().removeClass("active");
			$("+ul",this).slideUp(300);
		}
	});
});



/////// flatHeight

$(window).load(function() {
    var currentTallest = 0,
        currentRowStart = 0,
        rowDivs = new Array(),
        $el,
        topPosition = 0;
    $('.list_area, #top_recommend_area .product_item a').each(function() {
        $el = $(this);
        topPostion = $el.position().top;
        if (currentRowStart != topPostion) {
// we just came to a new row.  Set all the heights on the completed row
            for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
                rowDivs[currentDiv].height(currentTallest);
            }
// set the variables for the new row
            rowDivs.length = 0; // empty the array
            currentRowStart = topPostion;
            currentTallest = $el.height();
            rowDivs.push($el);
        } else {
// another div on the current row.  Add it to the list and check if it's taller
            rowDivs.push($el);
            currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
        }
// do the last row
        for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
            rowDivs[currentDiv].height(currentTallest);
        }
    });
});


/////// shipping toggle
 
$(document).ready(function(){
	$(".ship_to input").click(function () {
	 $("#ship_to_table").slideToggle("slow");
	});    
});