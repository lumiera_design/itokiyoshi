<ul class="level1">
    <li class="on_level2"><a href="<!--{$smarty.const.ROOT_URLPATH}--><!--{$smarty.const.ADMIN_DIR}-->design/<!--{$smarty.const.DIR_INDEX_PATH}-->?device_type_id=<!--{$smarty.const.DEVICE_TYPE_PC}-->"><span>レスポンシブWebデザイン</span></a>
        <ul class="level2">
            <li<!--{if $tpl_subno == 'layout'}--> class="on"<!--{/if}--> id="navi-design-layout-<!--{$smarty.const.DEVICE_TYPE_PC}-->"><a href="<!--{$smarty.const.ROOT_URLPATH}--><!--{$smarty.const.ADMIN_DIR}-->design/<!--{$smarty.const.DIR_INDEX_PATH}-->?device_type_id=<!--{$smarty.const.DEVICE_TYPE_PC}-->"><span>レイアウト設定</span></a></li>
            <li<!--{if $tpl_subno == 'main_edit'}--> class="on"<!--{/if}--> id="navi-design-main-<!--{$smarty.const.DEVICE_TYPE_PC}-->"><a href="<!--{$smarty.const.ROOT_URLPATH}--><!--{$smarty.const.ADMIN_DIR}-->design/main_edit.php?device_type_id=<!--{$smarty.const.DEVICE_TYPE_PC}-->"><span>ページ詳細設定</span></a></li>
            <li<!--{if $tpl_subno == 'bloc'}--> class="on"<!--{/if}--> id="navi-design-bloc-<!--{$smarty.const.DEVICE_TYPE_PC}-->"><a href="<!--{$smarty.const.ROOT_URLPATH}--><!--{$smarty.const.ADMIN_DIR}-->design/bloc.php?device_type_id=<!--{$smarty.const.DEVICE_TYPE_PC}-->"><span>ブロック設定</span></a></li>
            <li<!--{if $tpl_subno == 'header'}--> class="on"<!--{/if}--> id="navi-design-header-<!--{$smarty.const.DEVICE_TYPE_PC}-->"><a href="<!--{$smarty.const.ROOT_URLPATH}--><!--{$smarty.const.ADMIN_DIR}-->design/header.php?device_type_id=<!--{$smarty.const.DEVICE_TYPE_PC}-->"><span>ヘッダー/フッター設定</span></a></li>
            <li<!--{if $tpl_subno == 'css'}--> class="on"<!--{/if}--> id="navi-design-css-<!--{$smarty.const.DEVICE_TYPE_PC}-->"><a href="<!--{$smarty.const.ROOT_URLPATH}--><!--{$smarty.const.ADMIN_DIR}-->design/css.php?device_type_id=<!--{$smarty.const.DEVICE_TYPE_PC}-->"><span>CSS設定</span></a></li>
        </ul>
    </li>
</ul>