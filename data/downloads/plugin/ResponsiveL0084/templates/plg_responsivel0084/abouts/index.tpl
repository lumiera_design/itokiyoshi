<!--{*
 * ResponsiveL0084
 * Copyright (C) 2013 LOCKON CO.,LTD. All Rights Reserved.
 * http://www.lockon.co.jp/
 *}-->

<!--{if $arrSiteInfo.latitude && $arrSiteInfo.longitude}-->
    <script type="text/javascript">//<![CDATA[
        $(function() {
            $("#maps").css({
                'margin-top': '15px',
                'margin-left': 'auto',
                'margin-right': 'auto',
                'width': '98%',
                'height': '300px'
            });
            var lat = <!--{$arrSiteInfo.latitude}-->
            var lng = <!--{$arrSiteInfo.longitude}-->
            if (lat && lng) {
                var latlng = new google.maps.LatLng(lat, lng);
                var mapOptions = {
                    zoom: 15,
                    center: latlng,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };
                var map = new google.maps.Map($("#maps").get(0), mapOptions);
                var marker = new google.maps.Marker({map: map, position: latlng});
            } else {
                $("#maps").remove();
            }
        });
    //]]></script>
<!--{/if}-->

<section id="undercolumn">
    <h2 class="title"><!--{$tpl_title|h}--></h2>
    <div class="inner">
        <div class="table">
            <div class="tbody">
                <!--{if strlen($arrSiteInfo.shop_name)}-->
                <dl class="tr">
                    <dt>店名</dt>
                    <dd><!--{$arrSiteInfo.shop_name|h}--></dd>
                </dl>
                <!--{/if}-->

                <!--{if strlen($arrSiteInfo.company_name)}-->
                <dl class="tr">
                    <dt>会社名</dt>
                    <dd><!--{$arrSiteInfo.company_name|h}--></dd>
                </dl>
                <!--{/if}-->

                <!--{if strlen($arrSiteInfo.zip01)}-->
                <dl class="tr">
                    <dt>所在地</dt>
                    <dd><!--{$arrSiteInfo.zip01|h}-->-<!--{$arrSiteInfo.zip02|h}--><br /><!--{$arrPref[$arrSiteInfo.pref]|h}--><!--{$arrSiteInfo.addr01|h}--><!--{$arrSiteInfo.addr02|h}--></dd>
                </dl>
                <!--{/if}-->

                <!--{if strlen($arrSiteInfo.tel01)}-->
                <dl class="tr">
                    <dt>電話番号</dt>
                    <dd><!--{$arrSiteInfo.tel01|h}-->-<!--{$arrSiteInfo.tel02|h}-->-<!--{$arrSiteInfo.tel03|h}--></dd>
                </dl>
                <!--{/if}-->

                <!--{if strlen($arrSiteInfo.fax01)}-->
                <dl class="tr">
                    <dt>FAX番号</dt>
                    <dd><!--{$arrSiteInfo.fax01|h}-->-<!--{$arrSiteInfo.fax02|h}-->-<!--{$arrSiteInfo.fax03|h}--></dd>
                </dl>
                <!--{/if}-->

                <!--{if strlen($arrSiteInfo.email02)}-->
                <dl class="tr">
                    <dt>メールアドレス</dt>
                    <dd><a href="mailto:<!--{$arrSiteInfo.email02|escape:'hex'}-->"><!--{$arrSiteInfo.email02|escape:'hexentity'}--></a></dd>
                </dl>
                <!--{/if}-->

                <!--{if strlen($arrSiteInfo.business_hour)}-->
                <dl class="tr">
                    <dt>営業時間</dt>
                    <dd><!--{$arrSiteInfo.business_hour|h}--></dd>
                </dl>
                <!--{/if}-->

                <!--{if strlen($arrSiteInfo.good_traded)}-->
                <dl class="tr">
                    <dt>取扱商品</dt>
                    <dd><!--{$arrSiteInfo.good_traded|h|nl2br}--></dd>
                </dl>
                <!--{/if}-->

                <!--{if strlen($arrSiteInfo.message)}-->
                <dl class="tr">
                    <dt>メッセージ</dt>
                    <dd><!--{$arrSiteInfo.message|h|nl2br}--></dd>
                </dl>
                <!--{/if}-->

            </div>
        </div>
        <div id="maps"></div>
    </div>
</section>
