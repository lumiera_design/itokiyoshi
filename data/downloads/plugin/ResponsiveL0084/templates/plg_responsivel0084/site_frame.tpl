<!--{*
 * ResponsiveL0084
 * Copyright (C) 2013 LOCKON CO.,LTD. All Rights Reserved.
 * http://www.lockon.co.jp/
 *}-->
<!DOCTYPE html>
<html lang="ja" xmlns:fb="http://ogp.me/ns/fb#">
<head>
<meta charset="<!--{$smarty.const.CHAR_CODE}-->">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><!--{$arrSiteInfo.shop_name|h}--><!--{if $tpl_subtitle|strlen >= 1}--> / <!--{$tpl_subtitle|h}--><!--{elseif $tpl_title|strlen >= 1}--> / <!--{$tpl_title|h}--><!--{/if}--></title>
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<!--{if $arrPageLayout.author|strlen >= 1}-->
    <meta name="author" content="<!--{$arrPageLayout.author|h}-->" />
<!--{/if}-->
<!--{if $arrPageLayout.description|strlen >= 1}-->
    <meta name="description" content="<!--{$arrPageLayout.description|h}-->" />
<!--{/if}-->
<!--{if $arrPageLayout.keyword|strlen >= 1}-->
    <meta name="keywords" content="<!--{$arrPageLayout.keyword|h}-->" />
<!--{/if}-->
<!--{if $arrPageLayout.meta_robots|strlen >= 1}-->
    <meta name="robots" content="<!--{$arrPageLayout.meta_robots|h}-->" />
<!--{/if}-->
<link rel="stylesheet" href="<!--{$TPL_URLPATH}-->css/style.css">
<link rel="stylesheet" href="<!--{$TPL_URLPATH}-->css/flexslider.css">
<link rel="alternate" type="application/rss+xml" title="RSS" href="<!--{$smarty.const.HTTP_URL}-->rss/<!--{$smarty.const.DIR_INDEX_PATH}-->" />
<!--{if $tpl_page_class_name === "LC_Page_Abouts"}-->
<!--{if ($smarty.server.HTTPS != "") && ($smarty.server.HTTPS != "off")}-->
<script type="text/javascript" src="https://maps-api-ssl.google.com/maps/api/js?sensor=false"></script>
<!--{else}-->
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<!--{/if}-->
<!--{/if}-->

<!--[if lt IE 9]><script type="text/javascript" src="<!--{$TPL_URLPATH}-->js/html5shiv.js"></script><![endif]-->
<link href='http://fonts.googleapis.com/css?family=Josefin+Sans:600' rel='stylesheet' type='text/css' />
<link rel="stylesheet" href="<!--{$TPL_URLPATH}-->js/jquery.colorbox/colorbox.css" type="text/css" media="all" />
<script type="text/javascript" src="<!--{$TPL_URLPATH}-->js/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="<!--{$TPL_URLPATH}-->js/function.js"></script> 
<script type="text/javascript" src="<!--{$smarty.const.ROOT_URLPATH}-->js/eccube.js"></script>
<script type="text/javascript" src="<!--{$smarty.const.ROOT_URLPATH}-->js/eccube.legacy.js"></script>
<script type="text/javascript" src="<!--{$TPL_URLPATH}-->js/jquery.flexslider-min.js"></script> 
<script type="text/javascript" src="<!--{$TPL_URLPATH}-->js/jquery.colorbox/jquery.colorbox-min	.js"></script> 
<script type="text/javascript">
    <!--{$tpl_javascript}-->
    $(function(){
        <!--{$tpl_onload}-->
    });

    $(window).load(function() {
        $('#slider').flexslider({
            animation: "slide",
            itemWidth: 160,
            minItems: 4,
            maxItems: 4,
            animationLoop: false
        });
    });
</script>

<!--{* ▼Head COLUMN *}-->
<!--{if $arrPageLayout.HeadNavi|@count > 0}-->
    <!--{* ▼上ナビ *}-->
    <!--{foreach key=HeadNaviKey item=HeadNaviItem from=$arrPageLayout.HeadNavi}-->
        <!--{* ▼<!--{$HeadNaviItem.bloc_name}--> *}-->
        <!--{if $HeadNaviItem.php_path != ""}-->
            <!--{include_php file=$HeadNaviItem.php_path}-->
        <!--{else}-->
            <!--{include file=$HeadNaviItem.tpl_path}-->
        <!--{/if}-->
        <!--{* ▲<!--{$HeadNaviItem.bloc_name}--> *}-->
    <!--{/foreach}-->
    <!--{* ▲上ナビ *}-->
<!--{/if}-->
<!--{* ▲Head COLUMN *}-->
</head>

<!-- ▼BODY部 スタート -->
<!--{include file='./site_main.tpl'}-->
<!-- ▲BODY部 エンド -->

</html>
