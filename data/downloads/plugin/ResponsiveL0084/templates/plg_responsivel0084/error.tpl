<!--{*
 * ResponsiveL0084
 * Copyright (C) 2013 LOCKON CO.,LTD. All Rights Reserved.
 * http://www.lockon.co.jp/
 *}-->

<header id="header">
    <div class="inner">
        <h1 class="logo"><a href="<!--{$smarty.const.TOP_URLPATH}-->"><img src="<!--{$TPL_URLPATH}-->img/common/logo.png" alt="EC-CUBE ONLINE SHOPPING SITE"></a></h1>
    </div>
</header>
<div id="contents">
    <section id="undercolumn">
        <div class="inner">
            <!-- ▼エラーメッセージ用▼ -->
            <div class="message">
                <p class="alart"><!--{$tpl_error}--></p>
                <div class="btn_area">
                <!--{if $return_top}-->
                    <p class="button02"><button type="button" onclick="location.href='<!--{$smarty.const.TOP_URLPATH}-->'"><span>トップページへ</span></button></p>
                <!--{else}-->
                    <p class="button02"><button type="button" onclick="javascript:history.back();"><span>前のページに戻る</span></button></p>
                <!--{/if}-->
                </div>
            </div>
            <!-- ▲エラーメッセージ用▲ -->
        </div>
    </section>
</div>
<!--{include file=$footer_tpl}-->