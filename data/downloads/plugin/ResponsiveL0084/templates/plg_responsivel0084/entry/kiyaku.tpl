<!--{*
 * ResponsiveL0084
 * Copyright (C) 2013 LOCKON CO.,LTD. All Rights Reserved.
 * http://www.lockon.co.jp/
 *}-->

<section id="undercolumn">
    <h2 class="title"><!--{$tpl_title|h}--></h2>
    <div class="inner">
        <p>【重要】 会員登録をされる前に、下記ご利用規約をよくお読みください。</p>
        <p>規約には、本サービスを使用するに当たってのあなたの権利と義務が規定されております。<br />
            「同意して会員登録へ」ボタンをクリックすると、あなたが本規約の全ての条件に同意したことになります。
        </p>
        
        <form name="form1" id="form1" method="post" action="?">
        <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
        <div class="kiyaku">
            <p><!--{"\n"}--><!--{$tpl_kiyaku_text|h|nl2br}--></p>
        </div>
        
        <ul class="btn_area">
            <li class="button03">
                <a href='<!--{$smarty.const.TOP_URLPATH}-->'"><span>同意しない</span></a>
            </li>
            <li class="button02">
                <a href='<!--{$smarty.const.ENTRY_URL}-->'"><span>同意して会員登録へ</span></a>
            </li>
        </ul>
        </form>
    </div>
</section>
