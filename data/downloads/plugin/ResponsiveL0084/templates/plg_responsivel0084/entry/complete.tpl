<!--{*
 * ResponsiveL0084
 * Copyright (C) 2013 LOCKON CO.,LTD. All Rights Reserved.
 * http://www.lockon.co.jp/
 *}-->

<section id="undercolumn">
    <h2 class="title"><!--{$tpl_title|h}--></h2>
                    
    <div class="inner">
        <p>会員登録の受付が完了いたしました。</p>
        <p>
            現在仮会員の状態です。<br />
            ご入力いただいたメールアドレス宛てに、ご連絡が届いておりますので、本会員登録になった上でお買い物をお楽しみください。<br />
            今後ともご愛顧賜りますようよろしくお願い申し上げます。
        </p>
        
        <p><!--{$arrSiteInfo.company_name|h}--></p>
        <p>
            TEL：<!--{$arrSiteInfo.tel01}-->-<!--{$arrSiteInfo.tel02}-->-<!--{$arrSiteInfo.tel03}-->
            <!--{if $arrSiteInfo.business_hour != ""}-->（受付時間/<!--{$arrSiteInfo.business_hour}-->）<!--{/if}--><br />
            E-mail：<a href="mailto:<!--{$arrSiteInfo.email02|escape:'hex'}-->"><!--{$arrSiteInfo.email02|escape:'hexentity'}--></a>
        </p>
    
        <div class="btn_area">
            <p class="button02">
                <button type="button" onclick="location.href='<!--{$smarty.const.TOP_URLPATH}-->'"><span>トップページへ</span></button>
            </p>
        </div>
    </div>
</section>
