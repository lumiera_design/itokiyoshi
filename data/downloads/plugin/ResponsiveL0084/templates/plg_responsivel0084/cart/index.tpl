<!--{*
 * ResponsiveL0084
 * Copyright (C) 2013 LOCKON CO.,LTD. All Rights Reserved.
 * http://www.lockon.co.jp/
 *}-->

<script type="text/javascript">//<![CDATA[
    $(document).ready(function() {
        $('a.expansion').colorbox({maxWidth:'98%',maxHeight:'98%',initialWidth:'50%',initialHeight:'50%',speed:200});
    });
//]]></script>
<section id="undercolumn_cart">
    <h2 class="title"><!--{$tpl_title|h}--></h2>
    <!--{if $smarty.const.USE_POINT !== false || count($cartKeys) > 1 || strlen($tpl_error) != 0 || strlen($tpl_message) != 0}-->
    <div class="information">
        <!--{* ▼ポイント案内 *}-->
        <!--{if $smarty.const.USE_POINT !== false}-->
        <div class="point_announce">
            <!--{if $tpl_login}-->
            <p><span class="user_name"><!--{$tpl_name|h}--> 様</span>&nbsp;<span class="point">所持ポイント： <!--{$tpl_user_point|number_format|default:0|h}--> pt</span></p>
            <!--{else}-->
            <p>ポイント制度をご利用になられる場合は、会員登録後ログインしてくださいますようお願い致します。</p>
            <!--{/if}-->
        </div>
        <!--{/if}-->
        <!--{* ▲ポイント案内 *}-->

        <!--{if count($cartKeys) > 1}-->
            <p class="attention"><!--{foreach from=$cartKeys item=key name=cartKey}--><!--{$arrProductType[$key]|h}--><!--{if !$smarty.foreach.cartKey.last}-->、<!--{/if}--><!--{/foreach}-->は同時購入できません。<br />お手数ですが、個別に購入手続きをお願い致します。</p>
        <!--{/if}-->

        <!--{if strlen($tpl_error) != 0}-->
        <p class="attention"><!--{$tpl_error|h}--></p>
        <!--{/if}-->

        <!--{if strlen($tpl_message) != 0}-->
        <p class="attention"><!--{$tpl_message|h|nl2br}--></p>
        <!--{/if}-->
    </div>
    <!--{/if}-->

    <!--{if count($cartItems) > 0}-->
    <div class="inner">
        <!--{foreach from=$cartKeys item=key}-->
        <!--{if count($cartKeys) > 1}-->
        <h3 class="heading02"><!--{$arrProductType[$key]|h}--></h3>
        <p class="fb">
            <!--{assign var=purchasing_goods_name value=$arrProductType[$key]}-->
            <!--{$purchasing_goods_name|h}-->の合計金額は「<span class="price"><!--{$tpl_total_inctax[$key]|number_format|h}-->円</span>」です。<br />
            <span class="attention">
                <!--{if $key != $smarty.const.PRODUCT_TYPE_DOWNLOAD}-->
                    <!--{if $arrInfo.free_rule > 0}-->
                        <!--{if !$arrData[$key].is_deliv_free}-->
                        あと「<span class="price"><!--{$tpl_deliv_free[$key]|number_format|h}-->円</span>」で送料無料です！！
                        <!--{else}-->
                        現在、「送料無料」です！！
                        <!--{/if}-->
                    <!--{/if}-->
                <!--{/if}-->
            </span>
        </p>
        <!--{else}-->
        <p class="fb">
            <!--{assign var=purchasing_goods_name value="カゴの中の商品"}-->
            <!--{$purchasing_goods_name|h}-->の合計金額は「<span class="price"><!--{$tpl_total_inctax[$key]|number_format|h}-->円</span>」です。<br />
            <span class="attention">
                <!--{if $key != $smarty.const.PRODUCT_TYPE_DOWNLOAD}-->
                    <!--{if $arrInfo.free_rule > 0}-->
                        <!--{if !$arrData[$key].is_deliv_free}-->
                        あと「<span class="price"><!--{$tpl_deliv_free[$key]|number_format|h}-->円</span>」で送料無料です！！
                        <!--{else}-->
                        現在、「送料無料」です！！
                        <!--{/if}-->
                    <!--{/if}-->
                <!--{/if}-->
            </span>
        </p>
        <!--{/if}-->

        <form name="form<!--{$key|h}-->" id="form<!--{$key|h}-->" method="post" action="?">
            <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME|h}-->" value="<!--{$transactionid|h}-->" />
            <input type="hidden" name="mode" value="confirm" />
            <input type="hidden" name="cart_no" value="" />
            <input type="hidden" name="cartKey" value="<!--{$key|h}-->" />
            <input type="hidden" name="category_id" value="<!--{$tpl_category_id|h}-->" />
            <input type="hidden" name="product_id" value="<!--{$tpl_product_id|h}-->" />
            <div class="formBox"> 
                <div class="cartinarea cf"> 
                    <div class="table">
                        <div class="thead">
                            <ol>
                                <li>削除</li>
                                <li>商品内容</li>
                                <li>数量</li>
                                <li>小計</li>
                            </ol>
                        </div>
                        <div class="tbody">
                            <!--{foreach from=$cartItems[$key] item=item}-->
                            <div class="tr">
                                <div class="bt_delete"><a href="?" onclick="eccube.fnFormModeSubmit('form<!--{$key|h}-->', 'delete', 'cart_no', '<!--{$item.cart_no|h}-->'); return false;">削除</a></div>
                                <div class="item">
                                    <div class="photo">
                                        <!--{if $item.productsClass.main_image|strlen >= 1}-->
                                        <a class="expansion" target="_blank" href="<!--{$smarty.const.IMAGE_SAVE_URLPATH|h}--><!--{$item.productsClass.main_image|sfNoImageMainList|h}-->"><img src="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$item.productsClass.main_list_image|sfNoImageMainList|h}-->" alt="<!--{$item.productsClass.name|h}-->" /></a>
                                        <!--{else}-->
                                        <a class="expansion" target="_blank" href="<!--{$smarty.const.IMAGE_SAVE_URLPATH|h}--><!--{$item.productsClass.main_image|sfNoImageMainList|h}-->" alt="<!--{$item.productsClass.name|h}-->">
                                        <!--{/if}-->
                                    </div>
                                    <p><em><!--→商品名--><a href="<!--{$smarty.const.P_DETAIL_URLPATH}--><!--{$item.productsClass.product_id|u}-->" rel="external"><!--{$item.productsClass.name|h}--></a><!--←商品名--></em></p>
                                    <!--{if $item.productsClass.classcategory_name1 != ""}-->
                                    <p class="small"><!--{$item.productsClass.class_name1|h}-->：<!--{$item.productsClass.classcategory_name1|h}--></p>
                                    <!--{/if}-->
                                    <!--{if $item.productsClass.classcategory_name2 != ""}-->
                                    <p class="small"><!--{$item.productsClass.class_name2|h}-->：<!--{$item.productsClass.classcategory_name2|h}--></p>
                                    <!--{/if}-->
                                    <p class="small">価格:<!--{$item.price_inctax|number_format|h}-->円</p>
                                </div>
                                <div class="account">
                                    <!--{$item.quantity|h}-->
                                    <span class="amount">
                                        <img onclick="eccube.fnFormModeSubmit('form<!--{$key|h}-->', 'down','cart_no','<!--{$item.cart_no|h}-->'); return false" alt="-" src="<!--{$TPL_URLPATH}-->img/btn_minus.png">
                                        <img onclick="eccube.fnFormModeSubmit('form<!--{$key|h}-->', 'up','cart_no','<!--{$item.cart_no|h}-->'); return false" alt="＋" src="<!--{$TPL_URLPATH}-->img/btn_plus.png">
                                    </span>
                                </div>
                                <div class="price"><!--{$item.total_inctax|number_format|h}-->円</div>
                            </div>
                            <!--{/foreach}-->
                        </div>
                    </div>
                </div>

                <div class="total_area">
                    <dl>
                        <dt>合計：</dt>
                        <dd class="price"><!--{$arrData[$key].total-$arrData[$key].deliv_fee|number_format|h}-->円</dd>
                    </dl>
                    <!--{if $smarty.const.USE_POINT !== false}-->
                    <!--{if $arrData[$key].birth_point > 0}-->
                    <dl>
                        <dt>お誕生月ポイント：</dt>
                        <dd><!--{$arrData[$key].birth_point|number_format|h}-->pt</dd>
                    </dl>
                    <!--{/if}-->
                    <dl>
                        <dt>今回加算ポイント：</dt>
                        <dd><!--{$arrData[$key].add_point|number_format|h}-->pt</dd>
                    </dl>
                    <!--{/if}-->
                </div>
            </div>
            <!--{if strlen($tpl_error) == 0}-->
            <!--{if $tpl_prev_url != ""}-->
            <ul class="btn_area">
                <li class="button05">
                <input type="hidden" name="cartKey" value="<!--{$key}-->" />
                <button type="submit" id="confirm" name="confirm"><span>ご購入手続きへ</span></button>
                </li>
                <li class="button03"><button type="button" onclick="location.href='<!--{$tpl_prev_url|h}-->'"><span>お買い物を続ける</span></button></li>
            </ul>
            <!--{else}-->
            <div class="btn_area">
                <p class="button05">
                    <input type="hidden" name="cartKey" value="<!--{$key}-->" />
                    <button type="submit" id="confirm" name="confirm"><span>ご購入手続きへ</span></button>
                </p>
            </div>
            <!--{/if}-->
            <!--{elseif $tpl_prev_url != ""}-->
            <div class="btn_area">
                <p class="button03">
                    <button type="button" onclick="location.href='<!--{$tpl_prev_url|h}-->'"><span>お買い物を続ける</span></button>
                </p>
            </div>
            <!--{/if}-->
        </form>
        <!--{/foreach}-->
    </div>
    <!--{else}-->
    <div class="inner">
        <div class="message">
            <p>※ 現在カート内に商品はございません。</p>
        </div>
    </div>
    <!--{/if}-->
</section>
