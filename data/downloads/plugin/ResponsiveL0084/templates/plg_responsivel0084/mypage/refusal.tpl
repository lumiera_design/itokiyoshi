<!--{*
 * ResponsiveL0084
 * Copyright (C) 2013 LOCKON CO.,LTD. All Rights Reserved.
 * http://www.lockon.co.jp/
 *}-->

<div id="mycontents_area">
    <h2 class="title"><!--{$tpl_title|h}--></h2>
    <!--▼NAVI-->
    <!--{if $tpl_navi != ""}-->
        <!--{include file=$tpl_navi}-->
    <!--{else}-->
        <!--{include file=`$smarty.const.TEMPLATE_REALDIR`mypage/navi.tpl}-->
    <!--{/if}-->
    <!--▲NAVI--> 
    <div class="inner">
        <h3 class="heading02"><!--{$tpl_subtitle|h}--></h3>
        <form name="form1" id="form1" method="post" action="?">
        <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
        <input type="hidden" name="mode" value="confirm" />
        <div class="refusal_message">
            <p>
                退会手続きが完了した時点で、現在保存されている購入履歴や、<br />
                お届け先等の情報は全てなくなりますのでご注意ください。
            </p>
        </div>
        <div class="btn_area">
            <p class="button02">
                <button type="submit" name="refusal" id="refusal"><span>会員退会を行う</span></button>
            </p>
        </div>
        </form>
    </div>
</div>
