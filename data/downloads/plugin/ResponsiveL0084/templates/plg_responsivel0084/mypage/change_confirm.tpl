<!--{*
 * ResponsiveL0084
 * Copyright (C) 2013 LOCKON CO.,LTD. All Rights Reserved.
 * http://www.lockon.co.jp/
 *}-->
<div id="mycontents_area">
    <h2 class="title"><!--{$tpl_title|h}--></h2>
    <!--▼NAVI-->
    <!--{if $tpl_navi != ""}-->
        <!--{include file=$tpl_navi}-->
    <!--{else}-->
        <!--{include file=`$smarty.const.TEMPLATE_REALDIR`mypage/navi.tpl}-->
    <!--{/if}-->
    <!--▲NAVI--> 
    <div class="inner">
        <h3 class="heading02"><!--{$tpl_subtitle|h}--></h3>
        <p>
            下記の内容で送信してもよろしいでしょうか？<br />
            よろしければ、一番下の「完了ページへ」ボタンをクリックしてください。
        </p>

        <form name="form1" id="form1" method="post" action="?">
            <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
            <input type="hidden" name="mode" value="complete" />
            <input type="hidden" name="customer_id" value="<!--{$arrForm.customer_id.value|h}-->" />
            <!--{foreach from=$arrForm key=key item=item}-->
            <!--{if $key ne "mode" && $key ne "subm"}-->
            <input type="hidden" name="<!--{$key|h}-->" value="<!--{$item.value|h}-->" />
            <!--{/if}-->
            <!--{/foreach}-->

            <div class="table member_table">
                <div class="tbody">
                <!--{include file="`$smarty.const.TEMPLATE_REALDIR`frontparts/form_personal_confirm.tpl" flgFields=3 emailMobile=true prefix=""}-->
                </div>
            </div>

            <ul class="btn_area">
                <li class="button02">
                    <button type="submit" name="complete" id="complete"><span>完了ページへ</span></button>
                </li>
                <li class="button03">
                    <button type="button" onclick="eccube.setModeAndSubmit('return', '', ''); return false;"><span>戻る</span></button>
                </li>
            </ul>
        </form>
    </div>
</div>
