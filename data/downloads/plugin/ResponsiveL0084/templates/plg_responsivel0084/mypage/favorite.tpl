<!--{*
 * ResponsiveL0084
 * Copyright (C) 2013 LOCKON CO.,LTD. All Rights Reserved.
 * http://www.lockon.co.jp/
 *}-->

<div id="mycontents_area">
    <h2 class="title"><!--{$tpl_title|h}--></h2>
    <!--▼NAVI-->
    <!--{if $tpl_navi != ""}-->
        <!--{include file=$tpl_navi}-->
    <!--{else}-->
        <!--{include file=`$smarty.const.TEMPLATE_REALDIR`mypage/navi.tpl}-->
    <!--{/if}-->
    <!--▲NAVI--> 
    <div class="inner">
        <h3 class="heading02"><!--{$tpl_subtitle|h}--></h3>
        <!--{if $tpl_linemax > 0}-->
        <p><span class="attention"><!--{$tpl_linemax}-->件</span>のお気に入りがあります。</p>
        <form name="form1" id="form1" method="post" action="?">
        <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
        <input type="hidden" name="order_id" value="" />
        <input type="hidden" name="pageno" value="<!--{$tpl_pageno}-->" />
        <input type="hidden" name="mode" value="" />
        <input type="hidden" name="product_id" value="" />
        <div class="formBox marB00"> 
            <div class="table" id="favorite_table">
                <div class="tbody">
                    <!--{section name=cnt loop=$arrFavorite}-->
                    <!--{assign var=product_id value="`$arrFavorite[cnt].product_id`"}-->
                    <div class="tr">
                        <div class="item">
                            <div class="photo"><a href="<!--{$smarty.const.P_DETAIL_URLPATH}--><!--{$product_id|u}-->"><img src="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$arrFavorite[cnt].main_list_image|sfNoImageMainList|h}-->" alt="<!--{$arrFavorite[cnt].name}-->" /></a></div>
                            <p><!--→商品名--><a href="<!--{$smarty.const.P_DETAIL_URLPATH}--><!--{$product_id|u}-->" rel="external"><!--{$arrFavorite[cnt].name}--></a><!--←商品名--></p>
                            <p>
                                <span class="small">価格:</span>
                                <!--{if $arrFavorite[cnt].price02_min_inctax == $arrFavorite[cnt].price02_max_inctax}-->
                                    <!--{$arrFavorite[cnt].price02_min_inctax|number_format}-->
                                <!--{else}-->
                                    <!--{$arrFavorite[cnt].price02_min_inctax|number_format}-->～<!--{$arrFavorite[cnt].price02_max_inctax|number_format}-->
                                <!--{/if}-->円
                            </p>  
                        </div>
                        <div class="bt_delete"><a href="javascript:eccube.setModeAndSubmit('delete_favorite','product_id','<!--{$product_id|h}-->');">削除</a></div>
                    </div>
                    <!--{/section}-->
                </div>
            </div>
        </div>
        </form>
        
        <!--▼ページネーション-->
        <!--{if $objNavi->max_page > 1}-->
        <!--{assign var=arrPagenavi value=$objNavi->arrPagenavi}-->
        <nav class="pagination">
            <ul>
            <!--{if $objNavi->now_page > 1}-->
            <li class="prev"><a href="?pageno=<!--{$arrPagenavi.before}-->" onclick="eccube.movePage('<!--{$arrPagenavi.before}-->'); return false;">&lt;</a></li>
            <!--{/if}-->

            <!--{assign var=first_num value=$objNavi->now_page-$smarty.const.NAVI_PMAX+1}-->
            <!--{assign var=last_num  value=$objNavi->now_page+$smarty.const.NAVI_PMAX-1}-->
            
            <!--{foreach from=$arrPagenavi.arrPageno item="dispnum" key="num" name="page_navi"}-->
            <!--{if $first_num == $dispnum}-->
            <li class="first"><a href="?pageno=<!--{$dispnum}-->" onclick="eccube.movePage('<!--{$dispnum}-->'); return false;"><!--{$dispnum}--></a></li>
            <!--{elseif $last_num == $dispnum}-->
            <li class="last"><a href="?pageno=<!--{$dispnum}-->" onclick="eccube.movePage('<!--{$dispnum}-->'); return false;"><!--{$dispnum}--></a></li>
            <!--{elseif $dispnum == $objNavi->now_page}-->
            <li class="active"><a href="?pageno=<!--{$dispnum}-->" onclick="eccube.movePage('<!--{$dispnum}-->'); return false;"><!--{$dispnum}--></a></li>
            <!--{else}-->
            <li><a href="?pageno=<!--{$dispnum}-->" onclick="eccube.movePage('<!--{$dispnum}-->'); return false;"><!--{$dispnum}--></a></li>
            <!--{/if}-->
            <!--{/foreach}-->

            <!--{if $objNavi->now_page < $objNavi->max_page}-->
            <li class="next"><a href="?pageno=<!--{$arrPagenavi.next}-->" onclick="eccube.movePage('<!--{$arrPagenavi.next}-->'); return false;">&gt;</a></li>
            <!--{/if}-->
            </ul>
        </nav>
        <!--{/if}-->
        <!--▲ページネーション-->

        <!--{else}-->
        <p>お気に入りが登録されておりません。</p>
        <!--{/if}-->
    </div>
</div>
