<!--{*
 * ResponsiveL0084
 * Copyright (C) 2013 LOCKON CO.,LTD. All Rights Reserved.
 * http://www.lockon.co.jp/
 *}-->

<section id="mycontents_area">
    <h2 class="title"><!--{$tpl_title|h}--></h2>
    <!--▼NAVI-->
    <!--{if $tpl_navi != ""}-->
        <!--{include file=$tpl_navi}-->
    <!--{else}-->
        <!--{include file=`$smarty.const.TEMPLATE_REALDIR`mypage/navi.tpl}-->
    <!--{/if}-->
    <!--▲NAVI--> 

    <div class="inner">
        <h3 class="heading02">購入履歴詳細</h3>
        <div id="historyBox" class="cf">
            <p>
                注文番号：&nbsp;<!--{$tpl_arrOrderData.order_id}--><br />
                購入日時：&nbsp;<!--{$tpl_arrOrderData.create_date|sfDispDBDate}--><br />
                お支払い方法：&nbsp;<!--{$arrPayment[$tpl_arrOrderData.payment_id]|h}-->
                <!--{if $smarty.const.MYPAGE_ORDER_STATUS_DISP_FLAG}-->
                    <br />ご注文状況：&nbsp;
                    <!--{if $tpl_arrOrderData.status != $smarty.const.ORDER_PENDING}-->
                        <!--{$arrCustomerOrderStatus[$tpl_arrOrderData.status]|h}-->
                    <!--{else}-->
                        <span class="attention"><!--{$arrCustomerOrderStatus[$tpl_arrOrderData.status]|h}--></span>
                    <!--{/if}-->
                <!--{/if}-->
            </p>
            

            <form action="order.php" method="post">
                <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
                <input type="hidden" name="order_id" value="<!--{$tpl_arrOrderData.order_id|h}-->" />
                <div class="button05">
                    <button type="submit" name="submit"><span>再注文する</span></button>
                </div>
                <!--{if $is_price_change == true}-->
                    <div class="attention" Align="right">※金額が変更されている商品があるため、再注文時はご注意ください。</div>
                <!--{/if}-->
            </form>
        </div>

        <div class="formBox"> 
            <div class="cartinarea cf"> 
                <div class="table">
                    <div class="thead">
                        <ol>
                            <li>商品名/単価</li>
                            <li>商品種別</li>
                            <li>数量</li>
                            <li>小計</li>
                        </ol>
                    </div>
                    <div class="tbody">
                        <!--{foreach from=$tpl_arrOrderDetail item=orderDetail}-->
                        <div class="tr">
                            <div class="item">
                                <div class="photo"><a<!--{if $orderDetail.enable}--> href="<!--{$smarty.const.P_DETAIL_URLPATH}--><!--{$orderDetail.product_id|u}-->"<!--{/if}-->><img src="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$orderDetail.main_list_image|sfNoImageMainList|h}-->" alt="<!--{$orderDetail.product_name|h}-->" /></a></div>
                                <p><em><!--→商品名--><a<!--{if $orderDetail.enable}--> href="<!--{$smarty.const.P_DETAIL_URLPATH}--><!--{$orderDetail.product_id|u}-->" rel="external"<!--{/if}-->><!--{$orderDetail.product_name|h}--></a><!--←商品名--></em></p>
                                <!--{assign var=price value=`$orderDetail.price`}-->
                                <p><!--→金額--><span class="small">価格:</span><!--{$price|sfCalcIncTax|number_format|h}-->円<!--←金額--></p>  
                            </div>
                            <div class="itemtype"><!--→商品種別-->
                                <!--{if $orderDetail.product_type_id == $smarty.const.PRODUCT_TYPE_DOWNLOAD}-->
                                    <!--{if $orderDetail.is_downloadable}-->
                                        <a target="_self" href="<!--{$smarty.const.ROOT_URLPATH}-->mypage/download.php?order_id=<!--{$tpl_arrOrderData.order_id}-->&product_id=<!--{$orderDetail.product_id}-->&product_class_id=<!--{$orderDetail.product_class_id}-->">ダウンロード</a><!--←商品種別-->
                                    <!--{else}-->
                                        <!--{if $orderDetail.payment_date == "" && $orderDetail.effective == "0"}-->
                                            <!--{$arrProductType[$orderDetail.product_type_id]}--><br />（入金確認中）
                                        <!--{else}-->
                                            <!--{$arrProductType[$orderDetail.product_type_id]}--><br />（期限切れ）
                                        <!--{/if}-->
                                    <!--{/if}-->
                                <!--{else}-->
                                    <!--{$arrProductType[$orderDetail.product_type_id]}-->
                                <!--{/if}-->
                            </div>
                            <!--{assign var=quantity value=`$orderDetail.quantity`}-->
                            <div class="account"><!--{$quantity|h}--></div>
                            <!--{assign var=price value=`$orderDetail.price`}-->
                            <div class="price">
                                <!--{$orderDetail.price_inctax|number_format|h}-->円
                                <!--{if $orderDetail.price_inctax != $orderDetail.product_price_inctax}-->
                                    <div class="attention">【現在価格】</div><span class="attention"><!--{$orderDetail.product_price_inctax|number_format|h}-->円</span>
                                <!--{/if}-->
                            </div>
                        </div>
                        <!--{/foreach}-->
                    </div>
                </div>
                <!--▲商品 --> 
            </div>
            <div class="total_area">
                <dl><dt>小計：</dt><dd><!--{$tpl_arrOrderData.subtotal|number_format}-->円</dd></dl>
                <dl><dt>送料：</dt><dd><!--{assign var=key value="deliv_fee"}--><!--{$tpl_arrOrderData[$key]|number_format|h}-->円</dd></dl>
                <!--{assign var=point_discount value="`$tpl_arrOrderData.use_point*$smarty.const.POINT_VALUE`"}-->
                <!--{if $point_discount > 0}-->
                <dl><dt>ポイント値引き：</dt><dd>&minus;<!--{$point_discount|number_format}-->円</dd></dl>
                <!--{/if}-->
                <!--{assign var=key value="discount"}-->
                <!--{if $tpl_arrOrderData[$key] != "" && $tpl_arrOrderData[$key] > 0}-->
                <dl><dt>値引き：</dt><dd>&minus;<!--{$tpl_arrOrderData[$key]|number_format}-->円</dd></dl>
                <!--{/if}-->
                <dl><dt>手数料：</dt><dd><!--{assign var=key value="charge"}--><!--{$tpl_arrOrderData[$key]|number_format|h}-->円</dd></dl>
                <dl><dt>合計：</dt><dd class="price"><!--{$tpl_arrOrderData.payment_total|number_format}-->円</dd></dl>
                <!--{if $smarty.const.USE_POINT !== false}-->
                <dl><dt>ご使用ポイント：</dt><dd><!--{assign var=key value="use_point"}--><!--{$tpl_arrOrderData[$key]|number_format|default:0}-->pt</dd></dl>
                <dl><dt>今回加算ポイント：</dt><dd><!--{assign var=key value="add_point"}--><!--{$tpl_arrOrderData[$key]|number_format|default:0}-->pt</dd></dl>
                <!--{/if}-->
            </div>
        </div>
        <!--{foreach item=shippingItem name=shippingItem from=$arrShipping}-->
        <div class="formBox"> 
            <h3 class="heading02">お届け先<!--{if $isMultiple}--><!--{$smarty.foreach.shippingItem.iteration}--><!--{/if}--></h3>
            <!--{if $isMultiple}-->
            <div class="cartinarea cf"> 
                <div class="table" id="item_confirm">
                    <div class="thead">
                        <ol>
                            <li>商品内容</li>
                            <li>数量</li>
                        </ol>
                    </div>
                    <div class="tbody">
                        <!--{foreach item=item from=$shippingItem.shipment_item}-->
                        <div class="tr">
                            <div class="item">
                                <div class="photo">
                                    <!--{if $item.productsClass.main_image|strlen >= 1}-->
                                    <a href="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$item.productsClass.main_image|sfNoImageMainList|h}-->" class="expansion" target="_blank"><img src="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$item.productsClass.main_image|sfNoImageMainList|h}-->" alt="<!--{$item.productsClass.name|h}-->" /></a>
                                    <!--{else}-->
                                    <img src="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$item.productsClass.main_image|sfNoImageMainList|h}-->" alt="<!--{$item.productsClass.name|h}-->" />
                                    <!--{/if}-->
                                </div>
                                <p><em><!--→商品名--><a href="<!--{$smarty.const.P_DETAIL_URLPATH}--><!--{$item.productsClass.product_id|u}-->" rel="external"><!--{$item.productsClass.name|h}--></a><!--←商品名--></em></p>
                                <!--{if $item.productsClass.classcategory_name1 != ""}-->
                                <p class="small"><!--{$item.productsClass.class_name1}-->：<!--{$item.productsClass.classcategory_name1|h}--></p>  
                                <!--{/if}-->
                                <!--{if $item.productsClass.classcategory_name2 != ""}-->
                                <p class="small"><!--{$item.productsClass.class_name2}-->：<!--{$item.productsClass.classcategory_name2|h}--></p>  
                                <!--{/if}-->
                            </div>
                            <div class="account"><!--{$item.quantity}--> </div>
                        </div>
                        <!--{/foreach}-->
                    </div>
                </div>
            </div>
            <!--{/if}-->

            <div class="table" id="deliv_confirm">
                <div class="tbody">
                    <dl class="tr">
                        <dt>お名前</dt>
                        <dd><!--{$shippingItem.shipping_name01|h}-->&nbsp;<!--{$shippingItem.shipping_name02|h}--></dd>
                    </dl>
                    <dl class="tr">
                        <dt>お名前(フリガナ)</dt>
                        <dd><!--{$shippingItem.shipping_kana01|h}-->&nbsp;<!--{$shippingItem.shipping_kana02|h}--></dd>
                    </dl>
                    <dl class="tr">
                        <dt>会社名</dt>
                        <dd><!--{$shippingItem.shipping_company_name|h}--></dd>
                    </dl>
                    <!--{if $smarty.const.FORM_COUNTRY_ENABLE}-->
                    <dl class="tr">
                        <dt>国</dt>
                        <dd><!--{$arrCountry[$shippingItem.shipping_country_id]|h}--></dd>
                    </dl>
                    <dl class="tr">
                        <dt>ZIPCODE</dt>
                        <dd><!--{$shippingItem.shipping_zipcode|h}--></dd>
                    </dl>
                    <!--{/if}-->
                    <dl class="tr">
                        <dt>郵便番号</dt>
                        <dd>〒<!--{$shippingItem.shipping_zip01}-->-<!--{$shippingItem.shipping_zip02}--></dd>
                    </dl>
                    <dl class="tr">
                        <dt>住所</dt>
                        <dd><!--{$arrPref[$shippingItem.shipping_pref]}--><!--{$shippingItem.shipping_addr01|h}--><!--{$shippingItem.shipping_addr02|h}--></dd>
                    </dl>
                    <dl class="tr">
                        <dt>電話番号</dt>
                        <dd><!--{$shippingItem.shipping_tel01}-->-<!--{$shippingItem.shipping_tel02}-->-<!--{$shippingItem.shipping_tel03}--></dd>
                    </dl>
                    <dl class="tr">
                        <dt>FAX番号</dt>
                        <dd>
                            <!--{if $shippingItem.shipping_fax01 > 0}-->
                                <!--{$shippingItem.shipping_fax01}-->-<!--{$shippingItem.shipping_fax02}-->-<!--{$shippingItem.shipping_fax03}-->
                            <!--{/if}-->
                        </dd>
                    </dl>
                    <dl class="tr">
                        <dt>お届け日</dt>
                        <dd><!--{$shippingItem.shipping_date|default:'指定なし'|h}--></dd>
                    </dl>
                    <dl class="tr">
                        <dt>お届け時間</dt>
                        <dd><!--{$shippingItem.shipping_time|default:'指定なし'|h}--></dd>
                    </dl>
                </div>
            </div>
        </div>
        <!--{/foreach}-->

        <!--{* ▼メール一覧 *}-->
        <div class="formBox">
            <h3 class="heading02">メール配信履歴一覧</h3>
            <div id="mail_deliv" class="table">
                <div class="thead">
                    <ol>
                        <li>配信日</li>
                        <li>通知メール</li>
                        <li>題名</li>
                    </ol>
                </div>
                <div class="tbody">
                    <!--{section name=cnt loop=$tpl_arrMailHistory}-->
                    <a class="tr" href="<!--{$smarty.const.ROOT_URLPATH}-->mypage/mail_view.php?send_id=<!--{$tpl_arrMailHistory[cnt].send_id}-->">
                        <div class="maildate"><!--{$tpl_arrMailHistory[cnt].send_date|sfDispDBDate|h}--></div>
                        <!--{assign var=key value="`$tpl_arrMailHistory[cnt].template_id`"}-->
                        <div class="mailtype"><!--{$arrMAILTEMPLATE[$key]|h}--></div>
                        <div class="mailtitle"><!--{$tpl_arrMailHistory[cnt].subject|h}--></div>
                    </a>
                    <!--{/section}-->
                </div>
            </div>
        </div>
        <!--{* ▲メール一覧 *}-->
    </div>
</section>
