<!--{*
 * ResponsiveL0084
 * Copyright (C) 2013 LOCKON CO.,LTD. All Rights Reserved.
 * http://www.lockon.co.jp/
 *}-->

<div id="mycontents_area">
    <h2 class="title"><!--{$tpl_title|h}--></h2>
    <!--{include file=$tpl_navi}-->
    <div class="inner">
        <!--{if $tpl_linemax > 0}-->
        <form name="form1" id="form1" method="post" action="?" >
            <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
            <input type="hidden" name="mode" value="" />
            <input type="hidden" name="other_deliv_id" value="" />
            <input type="hidden" name="pageno" value="<!--{$tpl_pageno}-->" />
            <div class="formBox">
                <h3 class="heading02"><!--{$tpl_subtitle|h}--></h3>
                <div class="information">
                    <p>
                        登録住所一覧です。<br />
                        最大<!--{$smarty.const.DELIV_ADDR_MAX|h}-->件まで登録できます。
                    </p>
                    <!--{if $tpl_linemax < $smarty.const.DELIV_ADDR_MAX}-->
                        <!--{* 退会時非表示 *}-->
                        <!--{if $tpl_login}-->
                            <p class="button04"><a href="<!--{$smarty.const.ROOT_URLPATH}-->mypage/delivery_addr.php"><span>新しいお届け先を追加</span></a></p>
                        <!--{/if}-->
                    <!--{/if}-->
                </div>
                <h4 class="heading_table pc">お届け先住所</h4>
                <div id="delivery_table" class="table">
                    <div class="tbody">
                        <!--{section name=cnt loop=$arrOtherDeliv}-->
                        <!--{assign var=OtherPref value="`$arrOtherDeliv[cnt].pref`"}-->
                        <!--{assign var=OtherCountry value="`$arrOtherDeliv[cnt].country_id`"}-->
                        <div class="tr">
                            <div><!--{$smarty.section.cnt.iteration|string_format:"%02d"}--></div>
                            <div class="address">
                                <!--{if $smarty.const.FORM_COUNTRY_ENABLE}-->
                                <!--{$arrCountry[$OtherCountry]|h}--><br/>
                                <!--{/if}-->
                                <!--{if $arrOtherDeliv[cnt].zip01 || $arrOtherDeliv[cnt].zip02}-->
                                〒<!--{$arrOtherDeliv[cnt].zip01}-->-<!--{$arrOtherDeliv[cnt].zip02}--><br />
                                <!--{/if}-->
                                <!--{$arrPref[$OtherPref]|h}--><!--{$arrOtherDeliv[cnt].addr01|h}--><!--{$arrOtherDeliv[cnt].addr02|h}--><br />
                                <!--{if $arrOtherDeliv[cnt].company_name}--><!--{$arrOtherDeliv[cnt].company_name|h}-->&nbsp;<!--{/if}--><!--{$arrOtherDeliv[cnt].name01|h}-->&nbsp;<!--{$arrOtherDeliv[cnt].name02|h}-->
                            </div>
                            <div class="bt_edit"><a href="<!--{$smarty.const.ROOT_URLPATH}-->mypage/delivery_addr.php?other_deliv_id=<!--{$arrOtherDeliv[cnt].other_deliv_id}-->">変更</a></div>
                            <div class="bt_delete"><a href="#" onclick="eccube.setModeAndSubmit('delete','other_deliv_id','<!--{$arrOtherDeliv[cnt].other_deliv_id}-->'); return false;">削除</a></div>
                        </div>
                        <!--{/section}-->
                    </div>
                </div>
            </div>
        </form>
        <!--{else}-->
        <div class="formBox">
            <h3 class="heading02">お届け先追加・変更</h3>
            <div class="information">
                <p>登録されているお届け先はありません。</p>
                <!--{* 退会時非表示 *}-->
                <!--{if $tpl_login}-->
                <p class="button04"><a href="<!--{$smarty.const.ROOT_URLPATH}-->mypage/delivery_addr.php"><span>新しいお届け先を追加</span></a></p>
                <!--{/if}-->
            </div>
        </div>
        <!--{/if}-->
    </div>
</div>
