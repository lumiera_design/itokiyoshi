<!--{*
 * ResponsiveL0084
 * Copyright (C) 2013 LOCKON CO.,LTD. All Rights Reserved.
 * http://www.lockon.co.jp/
 *}-->
<div id="mycontents_area">
    <h2 class="title"><!--{$tpl_title|h}--></h2>
    <div id="mynavi_area"> 
        <!--▼NAVI-->
        <!--{if $tpl_navi != ""}-->
            <!--{include file=$tpl_navi}-->
        <!--{else}-->
            <!--{include file=`$smarty.const.TEMPLATE_REALDIR`mypage/navi.tpl}-->
        <!--{/if}-->
        <!--▲NAVI--> 
    </div>
    <div class="inner">
        <h3 class="heading02">メール履歴詳細</h3>
        <div class="message">
            <p><span class="title_m"><!--{$tpl_subject|h}--></span><br>
            <!--{$tpl_body|h|nl2br}--></p>
        </div>
    </div>
</div>
