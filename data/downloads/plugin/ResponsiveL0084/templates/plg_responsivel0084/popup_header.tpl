<!--{printXMLDeclaration}--><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--{*
 * ResponsiveL0084
 * Copyright (C) 2013 LOCKON CO.,LTD. All Rights Reserved.
 * http://www.lockon.co.jp/
 *}-->
<!DOCTYPE html>
<html class="" lang="ja">
<head>
<meta charset="<!--{$smarty.const.CHAR_CODE}-->">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><!--{if $tpl_title|strlen >= 1}--><!--{$tpl_title|h}--><!--{if $tpl_subtitle|strlen >= 1}--> - <!--{else}--> | <!--{/if}--><!--{/if}--><!--{if $tpl_subtitle|strlen >= 1}--> <!--{$tpl_subtitle|h}--> | <!--{/if}--><!--{$arrSiteInfo.shop_name|h}--></title>
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<meta name="description" content="">
<link rel="stylesheet" href="<!--{$TPL_URLPATH}-->css/style.css">
<link rel="stylesheet" href="<!--{$TPL_URLPATH}-->css/flexslider.css">
<script src="<!--{$TPL_URLPATH}-->js/jquery-1.8.3.min.js"></script> 
<script type="text/javascript" src="<!--{$smarty.const.ROOT_URLPATH}-->js/eccube.js"></script>
<script type="text/javascript" src="<!--{$smarty.const.ROOT_URLPATH}-->js/eccube.legacy.js"></script>
<!--[if lt IE 9]><script src="<!--{$TPL_URLPATH}-->js/html5shiv.js"></script><![endif]-->
<script src="<!--{$TPL_URLPATH}-->js/function.js"></script> 
<script src="<!--{$TPL_URLPATH}-->js/jquery.flexslider-min.js"></script> 
<script type="text/javascript" src="<!--{$TPL_URLPATH}-->js/jquery.colorbox/jquery.colorbox-min	.js"></script> 
<script type="text/javascript">//<![CDATA[
    <!--{$tpl_javascript}-->
    $(function(){
        <!--{$tpl_onload}-->
    });
//]]></script>
</head>

<body>
<noscript>
    <p><em>JavaScriptを有効にしてご利用下さい.</em></p>
</noscript>

<a name="top" id="top"></a>

<!--{if !$disable_wincol}--><div id="windowcolumn"><!--{/if}-->
