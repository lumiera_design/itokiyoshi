<!--{*
 * ResponsiveL0084
 * Copyright (C) 2013 LOCKON CO.,LTD. All Rights Reserved.
 * http://www.lockon.co.jp/
 *}-->
<!--{include file="`$smarty.const.TEMPLATE_REALDIR`popup_header.tpl" subtitle="パスワードを忘れた方(完了ページ)"}-->
<div id="wrapper">
<div id="contents" class="cf">
<section id="undercolumn">
    <h2 class="title">パスワードを忘れた方</h2>
    <div class="inner">
        <form action="?" method="post" name="form1">
            <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
            <!--{if $smarty.const.FORGOT_MAIL != 1}-->
                <div class="information">
                    <p>パスワードの発行が完了いたしました。<br />ログインには下記のパスワードをご利用ください。<br />
                    ※パスワードは、MYページの「会員登録内容変更」よりご変更いただけます。</p>
                </div>
                <div class="login_area">
                    <div class="complete_area">
                        <!--{if $smarty.const.FORGOT_MAIL != 1}-->
                        <div><input id="completebox" type="text" value="<!--{$arrForm.new_password}-->" /></div>
                        <!--{else}-->
                        <div>ご登録メールアドレスに送付致しました。</div>
                        <!--{/if}-->
                    </div>
                    <div class="inputbox">
                        <div class="btn_area">
                            <p class="button02"><button type="button" onclick="javascript:opener.location.href='<!--{$smarty.const.HTTPS_URL}-->mypage/login.php'; window.close();"><span>ログイン画面へ</span></button></p>
                        </div>
                    </div>
                </div>
                <!--▲loginarea-->

            <!--{else}-->
                <div class="information">
                    <p>パスワードの発行が完了いたしました。<br />ご登録メールアドレスに送付いたしましたのでご確認ください。<br />
                    ※パスワードは、MYページの「会員登録内容変更」よりご変更いただけます。</p>
                </div>
            <!--{/if}-->
        </form>
    </div>
</section>
</div>
</div>
<!--{include file="`$smarty.const.TEMPLATE_REALDIR`popup_footer.tpl"}-->
