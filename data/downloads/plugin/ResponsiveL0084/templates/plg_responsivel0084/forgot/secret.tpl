<!--{*
 * ResponsiveL0084
 * Copyright (C) 2013 LOCKON CO.,LTD. All Rights Reserved.
 * http://www.lockon.co.jp/
 *}-->
<!--{include file="`$smarty.const.TEMPLATE_REALDIR`popup_header.tpl" subtitle="パスワードを忘れた方(確認ページ)"}-->
<div id="wrapper">
<div id="contents" class="cf">
<section id="undercolumn">
    <h2 class="title">パスワードを忘れた方</h2>
                    
    <div class="inner">
    
        <form action="?" method="post" name="form1">
            <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
            <input type="hidden" name="mode" value="secret_check" />
            <!--{foreach key=key item=item from=$arrForm}-->
                <!--{if $key ne 'reminder_answer'}-->
                    <input type="hidden" name="<!--{$key}-->" value="<!--{$item|h}-->" />
                <!--{/if}-->
            <!--{/foreach}-->

            <div class="information">
                <p>ご登録時に入力した下記質問の答えを入力して「次へ」ボタンをクリックしてください。<br>
                ※質問の答えをお忘れになられた場合は、<a href="mailto:<!--{$arrSiteInfo.email02|escape:'hex'}-->"><!--{$arrSiteInfo.email02|escape:'hexentitiy'}--></a>までご連絡ください。</p>
            </div>
            <div class="login_area">
                <div class="loginareaBox table">
                    <ul class="tbody">
                    <li class="tr question">
                        <div class="th"><!--{$arrReminder[$arrForm.reminder]}--></div>
                        <div>
                            <input<!--{if $arrErr.reminder || $arrErr.reminder_answer}--> class="error"<!--{/if}--> type="text" value="" name="reminder_answer" placeholder="質問の答え" />
                            <!--{if $arrErr.reminder || $arrErr.reminder_answer}-->
                                <p class="attention"><!--{$arrErr.reminder}--><!--{$arrErr.reminder_answer}--></p>
                            <!--{/if}-->
                            <!--{if $errmsg}--><p class="attention"><!--{$errmsg}--></p><!--{/if}-->
                        </div>
                    </li>
                    </ul>
                </div>
                <div class="inputbox">
                    <div class="btn_area">
                        <p class="button02"><button type="submit"><span>次へ</span></button></p>
                    </div>
                </div>
            </div>
            <!--▲loginarea-->
            
        </form>
    
    </div>
    
</section>
</div>
</div>
<!--{include file="`$smarty.const.TEMPLATE_REALDIR`popup_footer.tpl"}-->
