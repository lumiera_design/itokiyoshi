<!--{*
 * ResponsiveL0084
 * Copyright (C) 2013 LOCKON CO.,LTD. All Rights Reserved.
 * http://www.lockon.co.jp/
 *}-->
<!--{include file="`$smarty.const.TEMPLATE_REALDIR`popup_header.tpl" subtitle="パスワードを忘れた方(入力ページ)"}-->
<div id="wrapper">
<div id="contents" class="cf">
    <section id="undercolumn">
        <h2 class="title">パスワードを忘れた方</h2>
        <div class="inner">
            <form action="?" method="post" name="form1">
                <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
                <input type="hidden" name="mode" value="mail_check" />
                <div class="information">
                    <p>
                        ご登録時のメールアドレスと、ご登録されたお名前を入力して「次へ」ボタンをクリックしてください。<br>
                        ※メールアドレスを忘れた方は、お手数ですが、<a href="#" onclick="javascript:opener.location.href='<!--{$smarty.const.HTTPS_URL}-->contact/<!--{$smarty.const.DIR_INDEX_PATH}-->'; window.close();" class="linktxt">お問い合わせページ</a>からお問い合わせください。 
                    </p>
                    <p class="attentionSt">【重要】新しくパスワードを発行いたしますので、お忘れになったパスワードはご利用できなくなります。</p>
                </div>
                <div class="login_area">
                    <div class="loginareaBox table">
                        <ul class="tbody">
                            <li class="tr">
                                <div class="th">メールアドレス</div>
                                <div>
                                    <input<!--{if $arrErr.email}--> class="input_email error"<!--{else}--> class="input_email"<!--{/if}--> type="email" name="email" value="<!--{$arrForm.email|default:$tpl_login_email|h}-->" maxlength="<!--{$smarty.const.MTEXT_LEN}-->" placeholder="メールアドレス" />
                                    <!--{if $arrErr.email}--><p class="attention"><!--{$arrErr.email}--></p><!--{/if}-->
                                </div>
                            </li>
                            <li class="tr">
                                <div class="th">お名前</div>
                                <div>
                                    <input<!--{if $arrErr.name01}--> class="error"<!--{/if}--> type="text" name="name01" value="<!--{$arrForm.name01|default:''|h}-->" maxlength="<!--{$smarty.const.STEXT_LEN}-->" placeholder="姓" />&nbsp;&nbsp;
                                    <input<!--{if $arrErr.name02}--> class="error"<!--{/if}--> type="text" name="name02" value="<!--{$arrForm.name02|default:''|h}-->" maxlength="<!--{$smarty.const.STEXT_LEN}-->" placeholder="名" />
                                    <!--{if $arrErr.name01 || $arrErr.name02}--><p class="attention"><!--{$arrErr.name01}--><!--{$arrErr.name02}--></p><!--{/if}-->
                                    <!--{if $errmsg}--><p class="attention"><!--{$errmsg}--></p><!--{/if}-->
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="inputbox">
                        <div class="btn_area">
                            <p class="button02"><button type="submit"><span>次へ</span></button></p>
                        </div>
                    </div>
                </div>
                <!--▲loginarea-->
            </form>
        </div>
    </section>
</div>
</div>
<!--{include file="`$smarty.const.TEMPLATE_REALDIR`popup_footer.tpl"}-->
