<!--{*
 * ResponsiveL0084
 * Copyright (C) 2013 LOCKON CO.,LTD. All Rights Reserved.
 * http://www.lockon.co.jp/
 *}-->
<script type="text/javascript">
	$(window).load(function() {
	  $('.main_visual .flexslider').flexslider({
		pauseOnAction: false,
		animationLoop: false
	  });
	});
</script>

<div class="main_visual">
    <div class="flexslider">
        <ul class="slides">
            <li><a href="#"><img src="<!--{$TPL_URLPATH}-->img/bnr_top_main01.jpg"></a></li>
            <li><a href="#"><img src="<!--{$TPL_URLPATH}-->img/bnr_top_main02.jpg"></a></li>
            <li><a href="#"><img src="<!--{$TPL_URLPATH}-->img/bnr_top_main03.jpg"></a></li>
            <li><a href="#"><img src="<!--{$TPL_URLPATH}-->img/bnr_top_main04.jpg"></a></li>
            <li><a href="#"><img src="<!--{$TPL_URLPATH}-->img/bnr_top_main05.jpg"></a></li>
        </ul>
    </div>
</div>
