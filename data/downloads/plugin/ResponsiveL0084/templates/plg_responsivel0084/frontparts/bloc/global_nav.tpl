<!--{*
 * ResponsiveL0084
 * Copyright (C) 2013 LOCKON CO.,LTD. All Rights Reserved.
 * http://www.lockon.co.jp/
 *}-->
<div id="gnav">
    <div class="inner">
        <nav>
            <a class="gnav_shopguide" href="<!--{$smarty.const.ROOT_URLPATH}-->guide/plg_ResponsiveL0084_guide.php"><p><img src="<!--{$TPL_URLPATH}-->img/btn_header_guide.png" alt="Shopping Guide" width="29" /><br class="sponly" /><span>Shopping Guide</span></p></a>
            <a class="gnav_contact" href="<!--{$smarty.const.ROOT_URLPATH}-->contact/"><p><img src="<!--{$TPL_URLPATH}-->img/btn_header_contact.png" alt="Contact" width="29" /><br class="sponly" /><span>Contact</span></p></a>
            <!--{if $plg_responsiveL0084_login}-->
            <!--▼▼▼ログイン後▼▼▼-->
            <a class="gnav_mypage" href="<!--{$smarty.const.ROOT_URLPATH}-->mypage/"><p><img src="<!--{$TPL_URLPATH}-->img/btn_header_mypage.png" alt="My Page" width="29" /><span class="pctb">My Page</span></p></a>
            <!--▲▲▲ログイン後▲▲▲-->
            <!--{else}-->
            <!--▼▼▼ログイン前▼▼▼-->
            <a class="gnav_mypage" href="<!--{$smarty.const.ROOT_URLPATH}-->mypage/"><p><img src="<!--{$TPL_URLPATH}-->img/btn_header_login.png" alt="Login" width="29" /><span class="pctb">Log In</span></p></a>
            <!--▲▲▲ログイン前▲▲▲-->
            <!--{/if}-->
            <!--▼カート -->
            <a class="gnav_cart" href="<!--{$smarty.const.ROOT_URLPATH}-->cart/"><p><span class="carticon"><img src="<!--{$TPL_URLPATH}-->img/btn_header_cart.png" alt="Cart" width="29" /><!--{if $arrCartList.0.TotalQuantity > 0}--><span class="incart_count"><!--{$arrCartList.0.TotalQuantity|number_format|default:0}--></span></span><!--{/if}--><span class="pctb">Shopping Cart</span></p></a>
        </nav>
    </div>
</div>