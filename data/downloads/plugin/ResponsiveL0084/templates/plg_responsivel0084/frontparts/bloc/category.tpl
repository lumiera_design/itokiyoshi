<!--{*
 * ResponsiveL0084
 * Copyright (C) 2013 LOCKON CO.,LTD. All Rights Reserved.
 * http://www.lockon.co.jp/
 *}-->

<div class="block_outer">
    <div id="category_area">
        <div class="bloc_body">
            <h2 class="heading01">商品カテゴリ</h2>
            <ul id="categorytree">
                <!--{include file="`$smarty.const.TEMPLATE_REALDIR`frontparts/bloc/category_tree_fork.tpl" children=$arrTree treeID="" display=1}-->
            </ul>
        </div>
    </div>
</div>
