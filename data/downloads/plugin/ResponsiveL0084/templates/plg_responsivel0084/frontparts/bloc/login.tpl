<!--{*
 * ResponsiveL0084
 * Copyright (C) 2013 LOCKON CO.,LTD. All Rights Reserved.
 * http://www.lockon.co.jp/
 *}-->

<div class="block_outer">
    <div class="bloc_body">
        <div class="status_area">
            <form onsubmit="return eccube.checkLoginFormInputted('login_form')" action="<!--{$smarty.const.HTTPS_URL}-->frontparts/login_check.php" method="post" id="login_form" class="login_form" name="login_form">
            <input type="hidden" value="login" name="mode" />
            <input type="hidden" value="<!--{$transactionid}-->" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" />
            <input type="hidden" value="<!--{$smarty.server.REQUEST_URI|h}-->" name="url" />
            <!--{if $tpl_login}-->
                <p class="alignC">ようこそ　<a href="<!--{$smarty.const.HTTPS_URL}-->mypage/login.php"><!--{$tpl_name1|h}--> <!--{$tpl_name2|h}--> 様</a>
                <!--{if $smarty.const.USE_POINT !== false}--><br /><span class="point"> <!--{$tpl_user_point|number_format|default:0}--> ポイント所持</span><!--{/if}-->
                <p class="button02 pctb"><button type="button" onclick="location.href='<!--{$smarty.const.HTTPS_URL}-->mypage/login.php'"><span>MYページ</span></button></p>
                <!--{if !$tpl_disable_logout}-->
                <p class="button03"><button type="submit" onclick="eccube.fnFormModeSubmit('login_form', 'logout', '', ''); return false;"><span>ログアウト</span></button></p>
                <!--{/if}-->
            <!--{else}-->
                <div class="for_login">
                    <p class="alignC">ようこそ　ゲスト様</p>
                    <p class="button02"><button type="button" onclick="location.href='<!--{$smarty.const.ROOT_URLPATH}-->entry/kiyaku.php'"><span>新規登録</span></button></p>
                    <p class="button03 pctb"><button type="button" onclick="location.href='<!--{$smarty.const.HTTPS_URL}-->mypage/login.php'"><span>ログイン</span></button></p>
                </div>
            <!--{/if}-->
            </form>
        </div>
    </div>
</div>
