<!--{*
 * ResponsiveL0084
 * Copyright (C) 2013 LOCKON CO.,LTD. All Rights Reserved.
 * http://www.lockon.co.jp/
 *}-->

<!--{foreach from=$children item=child}-->
<li>
    <a href="<!--{$smarty.const.ROOT_URLPATH}-->products/list.php?category_id=<!--{$child.category_id}-->"><!--{$child.category_name|h}-->(<!--{$child.product_count|default:0}-->)</a>
    <!--{assign var=category_id value=$child.category_id}-->
    <!--{assign var=chileren_flg value=$plg_responsiveL0084_children_category_flg[$category_id]}-->
    <!--{if isset($child.children|smarty:nodefaults)}-->
    <span class="toggle"></span>
    <ul>
    <!--{include file="`$smarty.const.TEMPLATE_REALDIR`frontparts/bloc/category_tree_fork.tpl" children=$child.children display=$disp_child}-->
    </ul>
    <!--{/if}-->
</li>
<!--{/foreach}-->
