<!--{*
 * ResponsiveL0084
 * Copyright (C) 2013 LOCKON CO.,LTD. All Rights Reserved.
 * http://www.lockon.co.jp/
 *}-->
 
<!--{if count($arrBestProducts) > 0}-->

<div class="block_outer recommend_area_wrap">
    <div id="top_recommend_area">
        <h2>ピックアップアイテム</h2>
        <div class="block_body cf">
            <ul>
                <!--{foreach from=$arrBestProducts item=arrProduct name="recommend_products"}-->
                <li class="product_item">
                    <a href="<!--{$smarty.const.P_DETAIL_URLPATH}--><!--{$arrProduct.product_id|u}-->">
                        <div class="productImage"><img alt="<!--{$arrProduct.name|h}-->" src="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$arrProduct.main_list_image|sfNoImageMainList|h}-->"></div>
                        <div class="productContents">
                            <h3> <!--{$arrProduct.name|h}--> </h3>
                            <p class="comment"><!--{$arrProduct.comment|h|nl2br}--></p>
                            <p class="sale_price"><span class="price"><!--{$arrProduct.price02_min_inctax|number_format}-->円</span></p>
                        </div>
                    </a>
                </li>
                <!--{/foreach}-->
            </ul>
        </div>
    </div>
</div>
<!--{/if}-->
