<!--{*
 * ResponsiveL0084
 * Copyright (C) 2013 LOCKON CO.,LTD. All Rights Reserved.
 * http://www.lockon.co.jp/
 *}-->

<dl class="tr">
    <dt>お名前<span class="attention">※</span></dt>
    <dd>
        <!--{assign var=key1 value="`$prefix`name01"}-->
        <!--{assign var=key2 value="`$prefix`name02"}-->
        <input<!--{if $arrErr[$key1]}--> class="error"<!--{/if}--> type="text" name="<!--{$key1}-->" value="<!--{$arrForm[$key1].value|h}-->" maxlength="<!--{$arrForm[$key1].length}-->" placeholder="姓" />
        &nbsp;&nbsp;
        <input<!--{if $arrErr[$key2]}--> class="error"<!--{/if}--> type="text" name="<!--{$key2}-->" value="<!--{$arrForm[$key2].value|h}-->" maxlength="<!--{$arrForm[$key2].length}-->" placeholder="名" />
        <!--{if $arrErr[$key1] || $arrErr[$key2]}-->
            <div class="attention"><!--{$arrErr[$key1]}--><!--{$arrErr[$key2]}--></div>
        <!--{/if}-->
    </dd>
</dl>
<dl class="tr">
    <dt>お名前(フリガナ)<!--{if !$smarty.const.FORM_COUNTRY_ENABLE}--><span class="attention">※</span><!--{/if}--></dt>
    <dd>
        <!--{assign var=key1 value="`$prefix`kana01"}-->
        <!--{assign var=key2 value="`$prefix`kana02"}-->
        <input<!--{if $arrErr[$key1]}--> class="error"<!--{/if}--> type="text" name="<!--{$key1}-->" value="<!--{$arrForm[$key1].value|h}-->" maxlength="<!--{$arrForm[$key1].length}-->" placeholder="セイ"/>
        &nbsp;&nbsp;
        <input<!--{if $arrErr[$key2]}--> class="error"<!--{/if}--> type="text" name="<!--{$key2}-->" value="<!--{$arrForm[$key2].value|h}-->" maxlength="<!--{$arrForm[$key1].length}-->" placeholder="メイ"/>
        <!--{if $arrErr[$key1] || $arrErr[$key2]}-->
            <div class="attention"><!--{$arrErr[$key1]}--><!--{$arrErr[$key2]}--></div>
        <!--{/if}-->
    </dd>
</dl>
<dl class="tr">
    <dt>会社名</dt>
    <dd>
        <!--{assign var=key1 value="`$prefix`company_name"}-->
        <input<!--{if $arrErr[$key1]}--> class="error"<!--{/if}--> type="text" name="<!--{$key1}-->" value="<!--{$arrForm[$key1].value|h}-->" maxlength="<!--{$arrForm[$key1].length}-->" />
        <!--{if $arrErr[$key1]}-->
            <div class="attention"><!--{$arrErr[$key1]}--></div>
        <!--{/if}-->
    </dd>
</dl>
<!--{assign var=key1 value="`$prefix`zip01"}-->
<!--{assign var=key2 value="`$prefix`zip02"}-->
<!--{assign var=key3 value="`$prefix`pref"}-->
<!--{assign var=key4 value="`$prefix`addr01"}-->
<!--{assign var=key5 value="`$prefix`addr02"}-->
<!--{assign var=key6 value="`$prefix`country_id"}-->
<!--{assign var=key7 value="`$prefix`zipcode"}-->
<!--{if !$smarty.const.FORM_COUNTRY_ENABLE}-->
<input type="hidden" name="<!--{$key6}-->" value="<!--{$smarty.const.DEFAULT_COUNTRY_ID}-->" />
<!--{else}-->
<dl class="tr">
    <dt>国<span class="attention">※</span></dt>
    <dd>
        <select<!--{if $arrErr[$key6]}--> class="error"<!--{/if}--> name="<!--{$key6}-->">
                <option value="" selected="selected">国を選択</option>
                <!--{html_options options=$arrCountry selected=$arrForm[$key6].value|h|default:$smarty.const.DEFAULT_COUNTRY_ID}-->
        </select>
        <!--{if $arrErr[$key6]}-->
            <div class="attention"><!--{$arrErr[$key6]}--></div>
        <!--{/if}-->
    </dd>
</dl>
<dl class="tr">
    <dt>ZIP CODE</dt>
    <dd>
        <input<!--{if $arrErr[$key7]}--> class="error"<!--{/if}--> type="text" name="<!--{$key7}-->" value="<!--{$arrForm[$key7].value|h}-->" maxlength="<!--{$arrForm[$key7].length}-->" />
        <!--{if $arrErr[$key7]}-->
            <div class="attention"><!--{$arrErr[$key7]}--></div>
        <!--{/if}-->
    </dd>
</dl>
<!--{/if}-->
<dl class="tr">
    <dt>郵便番号<!--{if !$smarty.const.FORM_COUNTRY_ENABLE}--><span class="attention">※</span><!--{/if}--></dt>
    <dd class="zipcode">
        <p>
            <input<!--{if $arrErr[$key1]}--> class="input_tel error"<!--{else}--> class="input_tel"<!--{/if}--> type="tel" name="<!--{$key1}-->" value="<!--{$arrForm[$key1].value|h}-->" maxlength="<!--{$arrForm[$key1].length}-->" />&nbsp;-&nbsp;<input<!--{if $arrErr[$key2]}--> class="input_tel error"<!--{else}--> class="input_tel"<!--{/if}--> type="tel" name="<!--{$key2}-->" value="<!--{$arrForm[$key2].value|h}-->" maxlength="<!--{$arrForm[$key2].length}-->" />
        </p>
        <p class="button04">
            <a href="javascript:eccube.getAddress('<!--{$smarty.const.INPUT_ZIP_URLPATH}-->', '<!--{$key1}-->', '<!--{$key2}-->', '<!--{$key3}-->', '<!--{$key4}-->');"><span>住所自動入力</span></a>
        </p>
        <!--{if $arrErr[$key1] || $arrErr[$key2]}-->
            <div class="attention cl"><!--{$arrErr[$key1]}--><!--{$arrErr[$key2]}--></div>
        <!--{/if}-->
    </dd>
</dl>
<dl class="tr">
    <dt>住所<span class="attention">※</span></dt>
    <dd>
        <select<!--{if $arrErr[$key3]}--> class="error"<!--{/if}--> name="<!--{$key3}-->">
            <option value="" selected="selected">都道府県</option>
            <!--{html_options options=$arrPref selected=$arrForm[$key3].value|h}-->
        </select>
        <input<!--{if $arrErr[$key4]}--> class="error"<!--{/if}--> type="text" name="<!--{$key4}-->" value="<!--{$arrForm[$key4].value|h}-->" placeholder="<!--{$smarty.const.SAMPLE_ADDRESS1}-->" />
        <input<!--{if $arrErr[$key5]}--> class="error"<!--{/if}--> type="text" name="<!--{$key5}-->" value="<!--{$arrForm[$key5].value|h}-->" placeholder="<!--{$smarty.const.SAMPLE_ADDRESS2}-->" />
        <!--{if $arrErr[$key3] || $arrErr[$key4] || $arrErr[$key5]}-->
            <div class="attention"><!--{$arrErr[$key3]}--><!--{$arrErr[$key4]}--><!--{$arrErr[$key5]}--></div>
        <!--{/if}-->
    </dd>
</dl>
<dl class="tr">
    <dt>電話番号<span class="attention">※</span></dt>
    <dd>
        <!--{assign var=key1 value="`$prefix`tel01"}-->
        <!--{assign var=key2 value="`$prefix`tel02"}-->
        <!--{assign var=key3 value="`$prefix`tel03"}-->
        <input<!--{if $arrErr[$key1]}--> class="input_tel error"<!--{else}--> class="input_tel"<!--{/if}--> type="tel" name="<!--{$key1}-->" value="<!--{$arrForm[$key1].value|h}-->" maxlength="<!--{$smarty.const.TEL_ITEM_LEN}-->" />&nbsp;-&nbsp;<input<!--{if $arrErr[$key2]}--> class="input_tel error"<!--{else}--> class="input_tel"<!--{/if}--> type="tel" name="<!--{$key2}-->" value="<!--{$arrForm[$key2].value|h}-->" maxlength="<!--{$smarty.const.TEL_ITEM_LEN}-->" />&nbsp;-&nbsp;<input<!--{if $arrErr[$key3]}--> class="input_tel error"<!--{else}--> class="input_tel"<!--{/if}--> type="tel" name="<!--{$key3}-->" value="<!--{$arrForm[$key3].value|h}-->" maxlength="<!--{$smarty.const.TEL_ITEM_LEN}-->" />
        <!--{if $arrErr[$key1] || $arrErr[$key2] || $arrErr[$key3]}-->
            <div class="attention"><!--{$arrErr[$key1]}--><!--{$arrErr[$key2]}--><!--{$arrErr[$key3]}--></div>
        <!--{/if}-->
    </dd>
</dl>
<dl class="tr">
    <dt>FAX</dt>
    <dd>
        <!--{assign var=key1 value="`$prefix`fax01"}-->
        <!--{assign var=key2 value="`$prefix`fax02"}-->
        <!--{assign var=key3 value="`$prefix`fax03"}-->
        <input<!--{if $arrErr[$key1]}--> class="input_tel error"<!--{else}--> class="input_tel"<!--{/if}--> type="tel" name="<!--{$key1}-->" value="<!--{$arrForm[$key1].value|h}-->" maxlength="<!--{$smarty.const.TEL_ITEM_LEN}-->" />&nbsp;-&nbsp;<input<!--{if $arrErr[$key2]}--> class="input_tel error"<!--{else}--> class="input_tel"<!--{/if}--> type="tel" name="<!--{$key2}-->" value="<!--{$arrForm[$key2].value|h}-->" maxlength="<!--{$smarty.const.TEL_ITEM_LEN}-->" />&nbsp;-&nbsp;<input<!--{if $arrErr[$key3]}--> class="input_tel error"<!--{else}--> class="input_tel"<!--{/if}--> type="tel" name="<!--{$key3}-->" value="<!--{$arrForm[$key3].value|h}-->" maxlength="<!--{$smarty.const.TEL_ITEM_LEN}-->" />
        <!--{if $arrErr[$key1] || $arrErr[$key2] || $arrErr[$key3]}-->
            <div class="attention"><!--{$arrErr[$key1]}--><!--{$arrErr[$key2]}--><!--{$arrErr[$key3]}--></div>
        <!--{/if}-->
    </dd>
</dl>
<!--{if $flgFields > 1}-->
<dl class="tr">
    <dt>メールアドレス<span class="attention">※</span></dt>
    <dd>
        <!--{assign var=key1 value="`$prefix`email"}-->
        <!--{assign var=key2 value="`$prefix`email02"}-->
        <input<!--{if $arrErr[$key1]}--> class="input_email error"<!--{else}--> class="input_email"<!--{/if}--> type="email" name="<!--{$key1}-->" value="<!--{$arrForm[$key1].value|h}-->" />
        <input<!--{if $arrErr[$key2]}--> class="input_email error"<!--{else}--> class="input_email"<!--{/if}--> type="email" name="<!--{$key2}-->" value="<!--{$arrForm[$key2].value|h}-->" placeholder="確認のため2回入力してください" />
        <!--{if $arrErr[$key1] || $arrErr[$key2]}-->
            <div class="attention"><!--{$arrErr[$key1]}--><!--{$arrErr[$key2]}--></div>
        <!--{/if}-->
    </dd>
</dl>
<!--{if $emailMobile}-->
<dl class="tr">
    <dt>携帯メールアドレス</dt>
    <dd>
        <!--{assign var=key1 value="`$prefix`email_mobile"}-->
        <!--{assign var=key2 value="`$prefix`email_mobile02"}-->
        <input<!--{if $arrErr[$key1]}--> class="input_email error"<!--{else}--> class="input_email"<!--{/if}--> type="email" name="<!--{$key1}-->" value="<!--{$arrForm[$key1].value|h}-->" />
        <input<!--{if $arrErr[$key2]}--> class="input_email error"<!--{else}--> class="input_email"<!--{/if}--> type="email" name="<!--{$key2}-->" value="<!--{$arrForm[$key2].value|h}-->" placeholder="確認のため2回入力してください" />
        <!--{if $arrErr[$key1] || $arrErr[$key2]}-->
        <div class="attention"><!--{$arrErr[$key1]}--><!--{$arrErr[$key2]}--></div>
        <!--{/if}-->
    </dd>
</dl>
<!--{/if}-->
<dl class="tr">
    <dt>性別<span class="attention">※</span></dt>
    <dd>
        <!--{assign var=key1 value="`$prefix`sex"}-->
        <!--{foreach name=sex_loop from=$arrSex item=item key=key_sex}-->
        <!--{if !$smarty.foreach.sex_loop.first}-->&nbsp;&nbsp;<!--{/if}-->
        <input<!--{if $arrErr[$key1]}--> class="error"<!--{/if}--> type="radio" id="<!--{$key1}--><!--{$key_sex}-->" name="<!--{$key1}-->" value="<!--{$key_sex}-->"<!--{if $arrForm[$key1].value eq $key_sex}--> checked="checked"<!--{/if}--> />
        <label for="<!--{$key1}--><!--{$key_sex}-->"><!--{$item|h}--></label>
        <!--{/foreach}-->
        <!--{if $arrErr[$key1]}-->
            <div class="attention"><!--{$arrErr[$key1]}--></div>
        <!--{/if}-->
    </dd>
</dl>
<dl class="tr">
    <dt>職業</dt>
    <dd>
        <!--{assign var=key1 value="`$prefix`job"}-->
        <select<!--{if $arrErr[$key1]}--> class="error"<!--{/if}--> name="<!--{$key1}-->">
            <option value="" selected="selected">選択してください</option>
            <!--{html_options options=$arrJob selected=$arrForm[$key1].value}-->
        </select>
        <!--{if $arrErr[$key1]}-->
            <div class="attention"><!--{$arrErr[$key1]}--></div>
        <!--{/if}-->
    </dd>
</dl>
<dl class="tr">
    <dt>生年月日</dt>
    <dd>
        <!--{assign var=key1 value="`$prefix`year"}-->
        <!--{assign var=key2 value="`$prefix`month"}-->
        <!--{assign var=key3 value="`$prefix`day"}-->
        <!--{assign var=errBirth value="`$arrErr.$key1``$arrErr.$key2``$arrErr.$key3`"}-->
        <select<!--{if $errBirth}--> class="error"<!--{/if}--> name="<!--{$key1}-->">
            <!--{html_options options=$arrYear selected=$arrForm[$key1].value|default:''}-->
        </select>
        <span class="selectdate">年</span>
        <select<!--{if $errBirth}--> class="error"<!--{/if}--> name="<!--{$key2}-->">
            <!--{html_options options=$arrMonth selected=$arrForm[$key2].value|default:''}-->
        </select>
        <span class="selectdate">月</span>
        <select<!--{if $errBirth}--> class="error"<!--{/if}--> name="<!--{$key3}-->">
            <!--{html_options options=$arrDay selected=$arrForm[$key3].value|default:''}-->
        </select>
        <span class="selectdate">日</span>
        <!--{if $errBirth}-->
            <div class="attention"><!--{$errBirth}--></div>
        <!--{/if}-->
    </dd>
</dl>
<!--{if $flgFields > 2}-->
<dl class="tr">
    <dt>希望するパスワード<span class="attention">※</span></dt>
    <dd>
        <!--{assign var=key1 value="`$prefix`password"}-->
        <!--{assign var=key2 value="`$prefix`password02"}-->
        <input<!--{if $arrErr[$key1]}--> class="error"<!--{/if}--> type="password" name="<!--{$key1}-->" value="<!--{$arrForm[$key1].value|h}-->" maxlength="<!--{$arrForm[$key1].length}-->" />
        <input<!--{if $arrErr[$key2]}--> class="error"<!--{/if}--> type="password" name="<!--{$key2}-->" value="<!--{$arrForm[$key2].value|h}-->" maxlength="<!--{$arrForm[$key2].length}-->" placeholder="確認のため2回入力してください" />
        <p class="attention mini">半角英数字<!--{$smarty.const.PASSWORD_MIN_LEN}-->～<!--{$smarty.const.PASSWORD_MAX_LEN}-->文字</p>
        <!--{if $arrErr[$key1] || $arrErr[$key2]}-->
            <div class="attention"><!--{$arrErr[$key1]}--><!--{$arrErr[$key2]}--></div>
        <!--{/if}-->
    </dd>
</dl>
<dl class="tr">
    <dt>パスワードを忘れた時のヒント<span class="attention">※</span></dt>
    <dd>
        <!--{assign var=key1 value="`$prefix`reminder"}-->
        <!--{assign var=key2 value="`$prefix`reminder_answer"}-->
        <select name="<!--{$key1}-->" style="<!--{$arrErr[$key1]|sfGetErrorColor}-->">
            <option value="" selected="selected">質問を選択してください</option>
            <!--{html_options options=$arrReminder selected=$arrForm[$key1].value}-->
        </select>
        <br />
        <input<!--{if $arrErr[$key2]}--> class="error"<!--{/if}--> type="text" name="<!--{$key2}-->" value="<!--{$arrForm[$key2].value|h}-->" placeholder="質問の答えを入力してください" />
        <!--{if $arrErr[$key1] || $arrErr[$key2]}-->
            <div class="attention"><!--{$arrErr[$key1]}--><!--{$arrErr[$key2]}--></div>
        <!--{/if}-->
    </dd>
</dl>
<dl class="tr">
    <dt>メールマガジン送付について<span class="attention">※</span></dt>
    <dd>
        <!--{assign var=key1 value="`$prefix`mailmaga_flg"}-->
        <!--{foreach name=mailmaga_loop from=$arrMAILMAGATYPE item=item key=key_mailmaga}-->
        <input<!--{if $arrErr[$key1]}--> class="error"<!--{/if}--> type="radio" id="<!--{$key1}--><!--{$key_mailmaga}-->" name="<!--{$key1}-->" value="<!--{$key_mailmaga}-->"<!--{if $arrForm[$key1].value eq $key_mailmaga}--> checked="checked"<!--{/if}--> />
        <label for="<!--{$key1}--><!--{$key_mailmaga}-->"><!--{$item|h}--></label><br />
        <!--{/foreach}-->
        <!--{if $arrErr[$key1]}-->
            <div class="attention"><!--{$arrErr[$key1]}--></div>
        <!--{/if}-->
    </dd>
</dl>
<!--{/if}-->
<!--{/if}-->
