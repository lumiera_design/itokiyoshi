<!--{*
 * ResponsiveL0084
 * Copyright (C) 2013 LOCKON CO.,LTD. All Rights Reserved.
 * http://www.lockon.co.jp/
 *}-->

<section id="undercolumn">
    <div class="inner">
    <!-- ▼エラーメッセージ用▼ -->
    <div class="message">
        <p class="alart">
            <!--{if $smarty.get.mode == "search"}-->
                該当件数0件です。<br />
                他の検索キーワードより再度検索をしてください。
            <!--{else}-->
                現在、商品はございません。
            <!--{/if}-->
        </p>
    </div>
    <!-- ▲エラーメッセージ用▲ -->
    </div>
</section>
