<!--{*
 * ResponsiveL0084
 * Copyright (C) 2013 LOCKON CO.,LTD. All Rights Reserved.
 * http://www.lockon.co.jp/
 *}-->

<dl class="tr">
    <dt>お名前</dt>
    <dd>
        <!--{assign var=key1 value="`$prefix`name01"}-->
        <!--{assign var=key2 value="`$prefix`name02"}-->
        <!--{$arrForm[$key1].value|h}-->&nbsp;
        <!--{$arrForm[$key2].value|h}-->
    </dd>
</dl>
<dl class="tr">
    <dt>お名前(フリガナ)</dt>
    <dd>
        <!--{assign var=key1 value="`$prefix`kana01"}-->
        <!--{assign var=key2 value="`$prefix`kana02"}-->
        <!--{$arrForm[$key1].value|h}-->&nbsp;
        <!--{$arrForm[$key2].value|h}-->
    </dd>
</dl>
<dl class="tr">
    <dt>会社名</dt>
    <dd>
        <!--{assign var=key1 value="`$prefix`company_name"}-->
        <!--{$arrForm[$key1].value|h}-->
    </dd>
</dl>
<!--{if $smarty.const.FORM_COUNTRY_ENABLE}-->
<dl class="tr">
    <dt>国</dt>
    <dd>
        <!--{assign var=key1 value="`$prefix`country_id"}-->
        <!--{assign var="country_id" value=$arrForm[$key1].value}-->
        <!--{$arrCountry[$country_id]|h}-->
    </dd>
</dl>
<dl class="tr">
    <dt>ZIP CODE</dt>
    <dd>
        <!--{assign var=key1 value="`$prefix`zipcode"}-->
        <!--{$arrForm[$key1].value|h}-->
    </dd>
</dl>
<!--{/if}-->
<dl class="tr">
    <dt>郵便番号</dt>
    <dd>
        <!--{assign var=key1 value="`$prefix`zip01"}-->
        <!--{assign var=key2 value="`$prefix`zip02"}-->
        〒 <!--{$arrForm[$key1].value|h}--> - <!--{$arrForm[$key2].value|h}-->
    </dd>
</dl>
<dl class="tr">
    <dt>住所</dt>
    <dd>
        <!--{assign var=key1 value="`$prefix`pref"}-->
        <!--{assign var=key2 value="`$prefix`addr01"}-->
        <!--{assign var=key3 value="`$prefix`addr02"}-->
        <!--{assign var="pref_id" value=$arrForm[$key1].value}-->
        <!--{$arrPref[$pref_id]|h}--><!--{$arrForm[$key2].value|h}--><!--{$arrForm[$key3].value|h}-->
    </dd>
</dl>
<dl class="tr">
    <dt>電話番号</dt>
    <dd>
        <!--{assign var=key1 value="`$prefix`tel01"}-->
        <!--{assign var=key2 value="`$prefix`tel02"}-->
        <!--{assign var=key3 value="`$prefix`tel03"}-->
        <!--{$arrForm[$key1].value|h}--> - <!--{$arrForm[$key2].value|h}--> - <!--{$arrForm[$key3].value|h}-->
    </dd>
</dl>
<dl class="tr">
    <dt>FAX</dt>
    <dd>
        <!--{assign var=key1 value="`$prefix`fax01"}-->
        <!--{assign var=key2 value="`$prefix`fax02"}-->
        <!--{assign var=key3 value="`$prefix`fax03"}-->
        <!--{if strlen($arrForm[$key1].value) > 0 && strlen($arrForm[$key2].value) > 0 && strlen($arrForm[$key3].value) > 0}-->
            <!--{$arrForm[$key1].value|h}--> - <!--{$arrForm[$key2].value|h}--> - <!--{$arrForm[$key3].value|h}-->
        <!--{else}-->
            未登録
        <!--{/if}-->
    </dd>
</dl>
<!--{if $flgFields > 1}-->
    <dl class="tr">
        <dt>メールアドレス</dt>
        <dd>
            <!--{assign var=key1 value="`$prefix`email"}-->
            <a href="mailto:<!--{$arrForm[$key1].value|escape:'hex'}-->"><!--{$arrForm[$key1].value|escape:'hexentity'}--></a>
        </dd>
    </dl>
    <!--{if $emailMobile}-->
        <dl class="tr">
            <dt>携帯メールアドレス</dt>
            <dd>
                <!--{assign var=key1 value="`$prefix`email_mobile"}-->
                <!--{if strlen($arrForm[$key1].value) > 0}-->
                    <a href="mailto:<!--{$arrForm[$key1].value|escape:'hex'}-->"><!--{$arrForm[$key1].value|escape:'hexentity'}--></a>
                <!--{else}-->
                    未登録
                <!--{/if}-->
            </dd>
        </dl>
    <!--{/if}-->
    <dl class="tr">
        <dt>性別</dt>
        <dd>
            <!--{assign var=key1 value="`$prefix`sex"}-->
            <!--{assign var="sex_id" value=$arrForm[$key1].value}-->
            <!--{$arrSex[$sex_id]|h}-->
        </dd>
    </dl>
    <dl class="tr">
        <dt>職業</dt>
        <dd>
            <!--{assign var=key1 value="`$prefix`job"}-->
            <!--{assign var="job_id" value=$arrForm[$key1].value}-->
            <!--{$arrJob[$job_id]|default:"未登録"|h}-->
        </dd>
    </dl>
    <dl class="tr">
        <dt>生年月日</dt>
        <dd>
            <!--{assign var=key1 value="`$prefix`year"}-->
            <!--{assign var=key2 value="`$prefix`month"}-->
            <!--{assign var=key3 value="`$prefix`day"}-->
            <!--{if strlen($arrForm[$key1].value) > 0 && strlen($arrForm[$key2].value) > 0 && strlen($arrForm[$key3].value) > 0}-->
            <!--{$arrForm[$key1].value|h}-->年<!--{$arrForm[$key2].value|h}-->月<!--{$arrForm[$key3].value|h}-->日
            <!--{else}-->
            未登録
            <!--{/if}-->
        </dd>
    </dl>
    <!--{if $flgFields > 2}-->
        <dl class="tr">
            <dt>希望するパスワード</dt>
            <dd><!--{$passlen}--></dd>
        </dl>
        <dl class="tr">
            <dt>パスワードを忘れた時のヒント</dt>
            <dd>
                <!--{assign var=key1 value="`$prefix`reminder"}-->
                <!--{assign var=key2 value="`$prefix`reminder_answer"}-->
                <!--{assign var="reminder_id" value=$arrForm[$key1].value}-->
                質問：<!--{$arrReminder[$reminder_id]|h}--><br />
                答え：<!--{$arrForm[$key2].value|h}-->
            </dd>
        </dl>
        <dl class="tr">
            <dt>メールマガジン送付について</dt>
            <dd>
                <!--{assign var=key1 value="`$prefix`mailmaga_flg"}-->
                <!--{assign var="mailmaga_flg_id" value=$arrForm[$key1].value}-->
                <!--{$arrMAILMAGATYPE[$mailmaga_flg_id]|h}-->
            </dd>
        </dl>
    <!--{/if}-->
<!--{/if}-->
