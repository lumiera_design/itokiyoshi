<!--{*
 * ResponsiveL0084
 * Copyright (C) 2013 LOCKON CO.,LTD. All Rights Reserved.
 * http://www.lockon.co.jp/
 *}-->

<section id="undercolumn">
    <h2 class="title"><!--{$tpl_title|h}--></h2>
    <div class="inner">
        <div class="table order">
            <div class="tbody">
                <dl class="tr">
                    <dt>販売業者</dt>
                    <dd><!--{$arrOrder.law_company|h}--></dd>
                </dl>
                <dl class="tr">
                    <dt>運営責任者</dt>
                    <dd><!--{$arrOrder.law_manager|h}--></dd>
                </dl>
                <dl class="tr">
                    <dt>住所</dt>
                    <dd>
                        <!--{if $arrOrder.law_zip01 || $arrOrder.law_zip02}-->
                        <!--{$arrOrder.law_zip01|h}-->-<!--{$arrOrder.law_zip02|h}--><br />
                        <!--{/if}-->
                        <!--{$arrPref[$arrOrder.law_pref]|h}--><!--{$arrOrder.law_addr01|h}--><!--{$arrOrder.law_addr02|h}-->
                    </dd>
                </dl>
                <dl class="tr">
                    <dt>電話番号</dt>
                    <dd>
                        <!--{if $arrOrder.law_tel01 || $arrOrder.law_tel02 || $arrOrder.law_tel03}-->
                        <!--{$arrOrder.law_tel01|h}-->-<!--{$arrOrder.law_tel02|h}-->-<!--{$arrOrder.law_tel03|h}-->
                        <!--{/if}-->
                    </dd>
                </dl>
                <dl class="tr">
                    <dt>FAX番号</dt>
                    <dd>
                        <!--{if $arrOrder.law_fax01 || $arrOrder.law_fax02 || $arrOrder.law_fax03}-->
                        <!--{$arrOrder.law_fax01|h}-->-<!--{$arrOrder.law_fax02|h}-->-<!--{$arrOrder.law_fax03|h}-->
                        <!--{/if}-->
                    </dd>
                </dl>
                <dl class="tr">
                    <dt>メールアドレス</dt>
                    <dd><a href="mailto:<!--{$arrOrder.law_email|escape:'hex'}-->"><!--{$arrOrder.law_email|escape:'hexentity'}--></a></dd>
                </dl>
                <dl class="tr">
                    <dt>URL</dt>
                    <dd><a href="<!--{$arrOrder.law_url|h}-->"><!--{$arrOrder.law_url|h}--></a></dd>
                </dl>
                <dl class="tr">
                    <dt>商品以外の必要代金</dt>
                    <dd><!--{$arrOrder.law_term01|h|nl2br}--></dd>
                </dl>
                <dl class="tr">
                    <dt>注文方法</dt>
                    <dd><!--{$arrOrder.law_term02|h|nl2br}--></dd>
                </dl>
                <dl class="tr">
                    <dt>支払方法</dt>
                    <dd><!--{$arrOrder.law_term03|h|nl2br}--></dd>
                </dl>
                <dl class="tr">
                    <dt>支払期限</dt>
                    <dd><!--{$arrOrder.law_term04|h|nl2br}--></dd>
                </dl>
                <dl class="tr">
                    <dt>引渡し時期</dt>
                    <dd><!--{$arrOrder.law_term05|h|nl2br}--></dd>
                </dl>
                <dl class="tr">
                    <dt>返品・交換について</dt>
                    <dd><!--{$arrOrder.law_term06|h|nl2br}--></dd>
                </dl>
            </div>
        </div>
    </div>
</section>
