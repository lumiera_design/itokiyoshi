<!--{*
 * ResponsiveL0084
 * Copyright (C) 2013 LOCKON CO.,LTD. All Rights Reserved.
 * http://www.lockon.co.jp/
 *}-->
<section id="undercolumn">
    <div id="undercolumn_shopping">
    <h2 class="title"><!--{$tpl_title|h}--></h2>
    <div class="flow_area">
        <ol>
            <li class="active">STEP1<br /><span class="pc">お届け先指定</span></li>
            <li>STEP2<br /><span class="pc">お支払い方法等選択</span></li>
            <li>STEP3<br /><span class="pc">内容確認</span></li>
            <li>STEP4<br /><span class="pc">完了</span></li>
        </ol>
    </div>
    <div class="inner">
        <div class="information">
            <div class="txt">
                <p>下記一覧よりお届け先住所を選択してください。<br>※最大<!--{$smarty.const.DELIV_ADDR_MAX|h}-->件まで登録できます。</p>
                <!--{if $tpl_addrmax < $smarty.const.DELIV_ADDR_MAX}-->
                <p><a class="link01" href="<!--{$smarty.const.ROOT_URLPATH}-->mypage/delivery_addr.php?page=<!--{$smarty.server.SCRIPT_NAME|h}-->">お届け先を追加する</a></p>
                <!--{/if}-->
            </div>
            <!--{if $smarty.const.USE_MULTIPLE_SHIPPING !== false}-->
            <p class="button04"><a onclick="eccube.setModeAndSubmit('multiple', '', ''); return false" href="javascript:;"><span>複数のお届け先に送る</span></a></p>
            <!--{/if}-->
        </div>
        <form name="form1" id="form1" method="post" action="?">
        <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
        <input type="hidden" name="mode" value="customer_addr" />
        <input type="hidden" name="uniqid" value="<!--{$tpl_uniqid}-->" />
        <input type="hidden" name="other_deliv_id" value="" />
        <!--{if $arrErr.deli != ""}-->
            <div class="attention"><!--{$arrErr.deli}--></div>
        <!--{/if}-->
        <div class="formBox">
            <div id="delivery_table" class="table">
                <div class="thead">
                    <ol>
                        <li>選択</li>
                        <li>住所種類</li>
                        <li>お届け先</li>
                        <li>変更</li>
                        <li>削除</li>
                    </ol>
                </div>
                <div class="tbody">
                    <!--{section name=cnt loop=$arrAddr}-->
                    <div class="tr">
                        <div>
                            <!--{if $smarty.section.cnt.first}-->
                                <input type="radio" name="deliv_check" id="chk_id_<!--{$smarty.section.cnt.iteration}-->" value="-1" <!--{if $arrForm.deliv_check.value == "" || $arrForm.deliv_check.value == -1}--> checked="checked"<!--{/if}--> />
                            <!--{else}-->
                                <input type="radio" name="deliv_check" id="chk_id_<!--{$smarty.section.cnt.iteration}-->" value="<!--{$arrAddr[cnt].other_deliv_id}-->"<!--{if $arrForm.deliv_check.value == $arrAddr[cnt].other_deliv_id}--> checked="checked"<!--{/if}--> />
                            <!--{/if}-->
                        </div>
                        <div><label for="chk_id_<!--{$smarty.section.cnt.iteration}-->"><!--{if $smarty.section.cnt.first}-->会員登録住所<!--{else}-->追加登録住所<!--{/if}--></label></div>
                        <div class="address">
                            <!--{assign var=key1 value=$arrAddr[cnt].pref}-->
                            <!--{assign var=key2 value=$arrAddr[cnt].country_id}-->
                            <!--{if $smarty.const.FORM_COUNTRY_ENABLE}-->
                            <!--{$arrCountry[$key2]|h}--><br/>
                            <!--{/if}-->
                            <!--{$arrPref[$key1]|h}--><!--{$arrAddr[cnt].addr01|h}--><!--{$arrAddr[cnt].addr02|h}--><br />
                            <!--{if $arrAddr[cnt].company_name}--><!--{$arrAddr[cnt].company_name|h}--> <!--{/if}--><!--{$arrAddr[cnt].name01|h}--> <!--{$arrAddr[cnt].name02|h}-->
                        </div>
                        <div class="bt_edit"><!--{if !$smarty.section.cnt.first}--><a href="<!--{$smarty.const.ROOT_URLPATH}-->mypage/delivery_addr.php?page=<!--{$smarty.server.SCRIPT_NAME|h}-->&amp;other_deliv_id=<!--{$arrAddr[cnt].other_deliv_id}-->">変更</a><!--{else}-->-<!--{/if}--></div>
                        <div class="bt_delete"><!--{if !$smarty.section.cnt.first}--><a href="?" onclick="eccube.setModeAndSubmit('delete', 'other_deliv_id', '<!--{$arrAddr[cnt].other_deliv_id}-->'); return false">削除</a><!--{else}-->-<!--{/if}--></div>
                    </div>
                    <!--{/section}-->
                </div>
            </div>
        </div>
        <ul class="btn_area">
            <li class="button02 large"><button type="submit" id="send_button" name="send_button"><span>選択したお届け先に送る</span></button></li>
            <li class="button03 narrow"><button type="button" onclick="location.href='<!--{$smarty.const.CART_URLPATH}-->'"><span>戻る</span></button></li>
        </ul>
        </form>
    </div>
</div>
</section>
