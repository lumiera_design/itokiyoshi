<!--{*
 * ResponsiveL0084
 * Copyright (C) 2013 LOCKON CO.,LTD. All Rights Reserved.
 * http://www.lockon.co.jp/
 *}-->
<script type="text/javascript">//<![CDATA[
    $(document).ready(function() {
        $("a.expansion").colorbox({maxWidth:'98%',maxHeight:'98%',initialWidth:'50%',initialHeight:'50%',speed:200});
    });
//]]></script>

<section id="undercolumn">
    <div id="undercolumn_shopping">
    <h2 class="title"><!--{$tpl_title|h}--></h2>
    <div class="flow_area">
        <ol>
            <li class="active">STEP1<br /><span class="pc">お届け先指定</span></li>
            <li>STEP2<br /><span class="pc">お支払い方法等選択</span></li>
            <li>STEP3<br /><span class="pc">内容確認</span></li>
            <li>STEP4<br /><span class="pc">完了</span></li>
        </ol>
    </div>
    <div class="inner">
        <div class="information">
            <div class="txt">
                <p>
                    各商品のお届け先を選択してください。<br />
                    （※数量の合計は、カゴの中の数量と合わせてください。）
                </p>
            </div>
            <!--{if $tpl_addrmax < $smarty.const.DELIV_ADDR_MAX}-->
            <p class="button04"><button type="button" onclick="location.href='<!--{$smarty.const.ROOT_URLPATH}-->mypage/delivery_addr.php?page=<!--{$smarty.server.SCRIPT_NAME|h}-->'"><span>お届け先を追加する</span></button></p>
            <!--{/if}-->
        </div>
        <form name="form1" id="form1" method="post" action="?">
        <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
        <input type="hidden" name="mode" value="confirm" />
        <input type="hidden" name="uniqid" value="<!--{$tpl_uniqid}-->" />
        <input type="hidden" name="line_of_num" value="<!--{$arrForm.line_of_num.value}-->" />
        <div class="formBox">
            <div class="cartinarea cf"> 
                <div class="table" id="multiple_table">
                    <div class="thead">
                        <ol>
                            <li>商品内容</li>
                            <li>数量</li>
                            <li>お届け先</li>
                        </ol>
                    </div>
                    <div class="tbody">
                        <!--{section name=line loop=$arrForm.line_of_num.value}-->
                        <!--{assign var=index value=$smarty.section.line.index}-->
                        <div class="tr">
                            <div class="item">
                                <div class="photo">
                                    <a<!--{if $arrForm.main_image.value[$index]|strlen >= 1}--> href="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$arrForm.main_image.value[$index]|sfNoImageMainList|h}-->" class="expansion" target="_blank"<!--{else}--> href="#"<!--{/if}-->"><img src="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$arrForm.main_list_image.value[$index]|sfNoImageMainList|h}-->" alt="<!--{$arrForm.name.value[$index]|h}-->" /></a>
                                </div>
                                <p><em><!--{$arrForm.name.value[$index]|h}--></em></p>
                                <!--{if $arrForm.classcategory_name1.value[$index] != ""}-->
                                <p class="small"><!--{$arrForm.class_name1.value[$index]|h}-->：<!--{$arrForm.classcategory_name1.value[$index]|h}--></p>
                                <!--{/if}-->
                                <!--{if $arrForm.classcategory_name2.value[$index] != ""}-->
                                <p class="small"><!--{$arrForm.class_name2.value[$index]|h}-->：<!--{$arrForm.classcategory_name2.value[$index]|h}--></p>
                                <!--{/if}-->
                                <p class="small">価格:<!--{$arrForm.price_inctax.value[$index]|number_format}-->円</p>
                            </div>
                            <div class="account2">
                                <!--{assign var=key value="quantity"}-->
                                <input<!--{if $arrErr[$key][$index] != ''}--> class="error"<!--{/if}--> type="number" max="9" value="<!--{$arrForm[$key].value[$index]|h}-->" name="<!--{$key}-->[<!--{$index}-->]">
                                <!--{if $arrErr[$key][$index] != ''}-->
                                    <div class="attention"><!--{$arrErr[$key][$index]}--></div>
                                <!--{/if}-->
                            </div>
                            <div class="shipping">
                                <input type="hidden" name="cart_no[<!--{$index}-->]" value="<!--{$index}-->" />
                                <!--{assign var=key value="product_class_id"}-->
                                <input type="hidden" name="<!--{$key}-->[<!--{$index}-->]" value="<!--{$arrForm[$key].value[$index]|h}-->" />
                                <!--{assign var=key value="name"}-->
                                <input type="hidden" name="<!--{$key}-->[<!--{$index}-->]" value="<!--{$arrForm[$key].value[$index]|h}-->" />
                                <!--{assign var=key value="class_name1"}-->
                                <input type="hidden" name="<!--{$key}-->[<!--{$index}-->]" value="<!--{$arrForm[$key].value[$index]|h}-->" />
                                <!--{assign var=key value="class_name2"}-->
                                <input type="hidden" name="<!--{$key}-->[<!--{$index}-->]" value="<!--{$arrForm[$key].value[$index]|h}-->" />
                                <!--{assign var=key value="classcategory_name1"}-->
                                <input type="hidden" name="<!--{$key}-->[<!--{$index}-->]" value="<!--{$arrForm[$key].value[$index]|h}-->" />
                                <!--{assign var=key value="classcategory_name2"}-->
                                <input type="hidden" name="<!--{$key}-->[<!--{$index}-->]" value="<!--{$arrForm[$key].value[$index]|h}-->" />
                                <!--{assign var=key value="main_image"}-->
                                <input type="hidden" name="<!--{$key}-->[<!--{$index}-->]" value="<!--{$arrForm[$key].value[$index]|h}-->" />
                                <!--{assign var=key value="main_list_image"}-->
                                <input type="hidden" name="<!--{$key}-->[<!--{$index}-->]" value="<!--{$arrForm[$key].value[$index]|h}-->" />
                                <!--{assign var=key value="price"}-->
                                <input type="hidden" name="<!--{$key}-->[<!--{$index}-->]" value="<!--{$arrForm[$key].value[$index]|h}-->" />
                                <!--{assign var=key value="price_inctax"}-->
                                <input type="hidden" name="<!--{$key}-->[<!--{$index}-->]" value="<!--{$arrForm[$key].value[$index]|h}-->" />
                                <!--{assign var=key value="shipping"}-->
                                <select style="" name="<!--{$key}-->[<!--{$index}-->]"<!--{if strlen($arrErr[$key][$index]) >= 1}--> class="error"<!--{/if}-->>
                                    <!--{html_options options=$addrs selected=$arrForm[$key].value[$index]}-->
                                </select>
                                <!--{if strlen($arrErr[$key][$index]) >= 1}-->
                                    <div class="attention"><!--{$arrErr[$key][$index]}--></div>
                                <!--{/if}-->
                            </div>
                        </div>
                        <!--{/section}-->
                    </div>
                </div>
            </div>
        </div>
        <ul class="btn_area">
            <li class="button02 large"><button type="submit" id="send_button" name="send_button"><span>選択したお届け先に送る</span></button></li>
            <li class="button03 narrow"><button type="button" onclick="location.href='<!--{$smarty.const.CART_URLPATH}-->'"><span>戻る</span></button></li>
        </ul>
        </form>
    </div>
</div>
</section>
