<!--{*
 * ResponsiveL0084
 * Copyright (C) 2013 LOCKON CO.,LTD. All Rights Reserved.
 * http://www.lockon.co.jp/
 *}-->

<script type="text/javascript">//<![CDATA[
    $(function() {
        if ($('input[name=deliv_id]:checked').val()
            || $('#deliv_id').val()) {
            showForm(true);
        } else {
            showForm(false);
        }
        $('input[id^=deliv_]').click(function() {
            showForm(true);
            var data = {};
            data.mode = 'select_deliv';
            data.deliv_id = $(this).val();
            data['<!--{$smarty.const.TRANSACTION_ID_NAME}-->'] = '<!--{$transactionid}-->';
            $.ajax({
                type : 'POST',
                url : location.pathname,
                data: data,
                cache : false,
                dataType : 'json',
                error : remoteException,
                success : function(data, dataType) {
                    if (data.error) {
                        remoteException();
                    } else {
                        // 支払い方法の行を生成
                        var payment_tbody = $('#payment .tbody');
                        payment_tbody.empty();
                        for (var i in data.arrPayment) {
                            // ラジオボタン
                            <!--{* IE7未満対応のため name と id をベタ書きする *}-->
                            var radio = $('<input type="radio" name="payment_id" id="pay_' + i + '" />')
                                .val(data.arrPayment[i].payment_id);

                            // 支払方法の画像が登録されている場合は表示
                            var img;
                            if (data.img_show) {
                                var payment_image = data.arrPayment[i].payment_image;
                                $('th#payment_method').attr('colspan', 3);
                                if (payment_image) {
                                    img = $('<img />').attr('src', '<!--{$smarty.const.IMAGE_SAVE_URLPATH}-->' + payment_image);
                                } else {
                                    img = "";
                                }
                            }

                            // ラベル
                            var label = $('<label />')
                                .attr('for', 'pay_' + i)
                                .text(data.arrPayment[i].payment_method).append(img);


                            // 行
                            var tr = $('<div class="tr" />')
                                .append($('<div />')
                                    .append(radio))
                                .append($('<div />').append(label));

                            tr.appendTo(payment_tbody);
                        }
                        // お届け時間を生成
                        var deliv_time_id_select = $('select[id^=deliv_time_id]');
                        deliv_time_id_select.empty();
                        deliv_time_id_select.append($('<option />').text('指定なし').val(''));
                        for (var i in data.arrDelivTime) {
                            var option = $('<option />')
                                .val(i)
                                .text(data.arrDelivTime[i])
                                .appendTo(deliv_time_id_select);
                        }
                    }
                }
            });
        });

        /**
         * 通信エラー表示.
         */
        function remoteException(XMLHttpRequest, textStatus, errorThrown) {
            alert('通信中にエラーが発生しました。カート画面に移動します。');
            location.href = '<!--{$smarty.const.CART_URLPATH}-->';
        }

        /**
         * 配送方法の選択状態により表示を切り替える
         */
        function showForm(show) {
            if (show) {
                $('#payment, div.delivdate, .select-msg').show();
                $('.non-select-msg').hide();
                $('#delivdate-title').show();
            } else {
                $('#payment, div.delivdate, .select-msg').hide();
                $('.non-select-msg').show();
                $('#delivdate-title').hide();
            }
        }
    });
//]]></script>

<section id="undercolumn">
    <div id="undercolumn_shopping">
        <h2 class="title"><!--{$tpl_title|h}--></h2>
        <div class="flow_area">
            <ol>
                <li>STEP1<br /><span class="pc">お届け先指定</span></li>
                <li class="active">STEP2<br /><span class="pc">お支払い方法等選択</span></li>
                <li>STEP3<br /><span class="pc">内容確認</span></li>
                <li>STEP4<br /><span class="pc">完了</span></li>
            </ol>
        </div>
        <div class="inner">
            <div class="information">
                <p>各項目を選択してください。</p>
            </div>
            <form name="form1" id="form1" method="post" action="?">
                <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
                <input type="hidden" name="mode" value="confirm" />
                <input type="hidden" name="uniqid" value="<!--{$tpl_uniqid}-->" />

                <!--{assign var=key value="deliv_id"}-->
                <!--{if $is_single_deliv}-->
                <input type="hidden" name="<!--{$key}-->" value="<!--{$arrForm[$key].value|h}-->" id="deliv_id" />
                <!--{else}-->
                <section class="pay_area">
                    <h3 class="heading02">配送方法の指定</h3>
                    <p class="information">まずはじめに、配送方法を選択ください。</p>
                    <div class="table payment_table">  
                        <div class="thead">
                            <ol>
                                <li>選択</li>
                                <li>配送方法</li>
                            </ol>
                        </div>
                        <div class="tbody">
                            <!--{section name=cnt loop=$arrDeliv}-->
                            <div class="tr">
                                <div><input<!--{if $arrErr[$key] != ""}--> class="error"<!--{/if}--> type="radio" id="deliv_<!--{$smarty.section.cnt.iteration}-->" name="<!--{$key}-->" value="<!--{$arrDeliv[cnt].deliv_id}-->" <!--{$arrDeliv[cnt].deliv_id|sfGetChecked:$arrForm[$key].value}--> /></div>
                                <div><label for="deliv_<!--{$smarty.section.cnt.iteration}-->"><!--{$arrDeliv[cnt].name|h}--><!--{if $arrDeliv[cnt].remark != ""}--> <!--{$arrDeliv[cnt].remark|h|nl2br}--><!--{/if}--></label></div>
                            </div>
                            <!--{/section}-->
                        </div>
                    </div>
                    <!--{if $arrErr[$key] != ""}-->
                    <div class="attention"><!--{$arrErr[$key]}--></div>
                    <!--{/if}-->

                </section>
                <!--{/if}-->

                <section class="pay_area">
                    <h3 class="heading02">お支払方法の指定</h3>
                    <p class="information">お支払方法をご選択ください。</p>

                    <!--{assign var=key value="payment_id"}-->
                    <div id="payment" class="table payment_table">  
                        <div class="thead">
                            <ol>
                                <li>選択</li>
                                <li>お支払方法</li>
                            </ol>
                        </div>
                        <div class="tbody">
                            <!--{section name=cnt loop=$arrPayment}-->
                            <div class="tr">
                                <div><input<!--{if $arrErr[$key] != ""}--> class="error"<!--{/if}--> type="radio" id="pay_<!--{$smarty.section.cnt.iteration}-->" name="<!--{$key}-->" value="<!--{$arrPayment[cnt].payment_id}-->" <!--{$arrPayment[cnt].payment_id|sfGetChecked:$arrForm[$key].value}--> /></div>
                                <div><label for="pay_<!--{$smarty.section.cnt.iteration}-->"><!--{$arrPayment[cnt].payment_method|h}--><!--{if $arrPayment[cnt].note != ""}--><!--{/if}--><!--{if $img_show}--><!--{if $arrPayment[cnt].payment_image != ""}--> <img src="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$arrPayment[cnt].payment_image}-->" /><!--{/if}--><!--{/if}--></label></div>
                            </div>
                            <!--{/section}-->
                        </div>
                    </div>
                    <!--{if $arrErr[$key] != ""}-->
                    <div class="attention"><!--{$arrErr[$key]}--></div>
                    <!--{/if}-->

                </section>
                
                <!--★お届け時間の指定★-->
                <!--{if $cartKey != $smarty.const.PRODUCT_TYPE_DOWNLOAD}-->
                <section class="pay_area02">
                    <h3 class="heading02">お届け時間の指定</h3>
                        <div class="formBox">
                        
                            <div id="delivdate_table" class="table">
                                <div class="thead">
                                    <ol>
                                        <li></li>
                                        <li>お届け先</li>
                                        <li id="delivdate-title">お届け日</li>
                                        <li>お届け時間</li>
                                    </ol>
                                </div>
                                <div class="tbody">
                                    <!--{foreach item=shippingItem name=shippingItem from=$arrShipping}-->
                                    <!--{assign var=index value=$shippingItem.shipping_id}-->
                                    <div class="tr">
                                        <div><!--{$smarty.foreach.shippingItem.index+1|number_format}--></div>
                                        <div class="delivadd"><!--{$shippingItem.shipping_name01}--><!--{$shippingItem.shipping_name02}--><br /><!--{$arrPref[$shippingItem.shipping_pref]}--><!--{$shippingItem.shipping_addr01}--><!--{$shippingItem.shipping_addr02}--></div>
                                        <div class="delivdate">
                                            <!--{assign var=key value="deliv_date`$index`"}-->
                                            <!--{if !$arrDelivDate}-->
                                                ご指定頂けません。
                                            <!--{else}-->
                                            <select name="<!--{$key}-->"<!--{if $arrErr[$key] != ""}--> class="error"<!--{/if}-->>
                                                <option value="" selected="">指定なし</option>
                                                <!--{assign var=shipping_date_value value=$arrForm[$key].value|default:$shippingItem.shipping_date}-->
                                                <!--{html_options options=$arrDelivDate selected=$shipping_date_value}-->
                                            </select>
                                            <!--{/if}-->
                                            <!--{if $arrErr[$key] != ""}-->
                                            <div class="attention"><!--{$arrErr[$key]}--></div>
                                            <!--{/if}-->
                                        </div>
                                        <div class="delivtime">
                                            <!--{assign var=key value="deliv_time_id`$index`"}-->
                                            <select name="<!--{$key}-->" id="<!--{$key}-->"<!--{if $arrErr[$key] != ""}--> class="error"<!--{/if}-->>
                                                <option value="" selected="">指定なし</option>
                                                <!--{assign var=shipping_time_value value=$arrForm[$key].value|default:$shippingItem.time_id}-->
                                                <!--{html_options options=$arrDelivTime selected=$shipping_time_value}-->
                                            </select>
                                            <!--{if $arrErr[$key] != ""}-->
                                            <div class="attention"><!--{$arrErr[$key]}--></div>
                                            <!--{/if}-->
                                        </div>
                                    </div>
                                    <!--{/foreach}-->
                                </div>
                            </div>
                        
                        </div>
                </section>
                <!--{/if}-->
                
                <!--★ポイント使用の指定★-->
                <!--{if $tpl_login == 1 && $smarty.const.USE_POINT !== false}-->
                <section class="point_area">
                    <h3 class="heading02">ポイント使用の指定</h3>
                    <p><span class="point">1ポイントを<!--{$smarty.const.POINT_VALUE|number_format}-->円</span>として使用する事ができます。</p>
                    <p>現在の所持ポイントは「<span class="price"><!--{$tpl_user_point|default:0|number_format}-->pt</span>」です。<br />
                        今回ご購入合計金額：<span class="price"><!--{$arrPrices.subtotal|number_format}-->円</span> (送料、手数料を含みません。)</p>
                    
                    <!--▼ポイントフォームボックスここから -->
                    <div class="formBox">
                        <ul class="innerBox">
                            <li>
                                <input type="radio" id="point_on" name="point_check" value="1" <!--{$arrForm.point_check.value|sfGetChecked:1}--> onchange="eccube.togglePointForm();" />
                                <!--{assign var=key value="use_point"}-->
                                <label for="point_on">ポイントを使用する</label> <input<!--{if $arrErr[$key] != ""}--> class="error"<!--{/if}--> type="text" name="<!--{$key}-->" value="<!--{$arrForm[$key].value|default:$tpl_user_point}-->" maxlength="<!--{$arrForm[$key].length}-->" /> ポイント
                                <!--{if $arrErr[$key] != ""}-->
                                <div class="attention"><!--{$arrErr[$key]}--></div>
                                <!--{/if}-->
                            </li>
                            <li>
                                <input type="radio" id="point_off" name="point_check" value="2" <!--{$arrForm.point_check.value|sfGetChecked:2}--> onchange="eccube.togglePointForm();" />
                                <label for="point_off">ポイントを使用しない</label>
                            </li>
                        </ul>
                    </div>
                    <!-- /.formBox --> 
                </section>
                <!--{/if}-->
                
                <!--★その他お問い合わせ★-->
                <section class="contact_area">
                    <h3 class="heading02">その他お問い合わせ</h3>
                    <p>その他お問い合わせ事項がございましたら、こちらにご入力ください。</p>
                    <!--{assign var=key value="message"}-->
                    <textarea<!--{if $arrErr[$key] != ""}--> class="error"<!--{/if}--> name="message" id="etc" cols="62" rows="8"><!--{"\n"}--><!--{$arrForm[$key].value|h}--></textarea>
                    <!--{if $arrErr[$key] != ""}-->
                    <div class="attention"><!--{$arrErr[$key]}--></div>
                    <!--{/if}-->
                </section>
                
                <ul class="btn_area">
                    <li class="button02"><button type="button" onclick="javascript:void(document.form1.submit());"><span>確認ページへ</span></button></li>
                    <li class="button03"><button type="button" onclick="location.href='?mode=return'"><span>戻る</span></button></li>
                </ul>
            </form>
        </div>
    </div>
</section>
