<!--{*
 * ResponsiveL0084
 * Copyright (C) 2013 LOCKON CO.,LTD. All Rights Reserved.
 * http://www.lockon.co.jp/
 *}-->

<!--▼HEADER-->
<header id="header">
    <div class="inner">
		<h1 class="logo"><a href="<!--{$smarty.const.TOP_URLPATH}-->"><img src="<!--{$TPL_URLPATH}-->img/common/logo.png" alt="EC-CUBE ONLINE SHOPPING SITE" /></a></h1>
        <div class="header_utility">
        	<nav class="header_navi">
				<ul>
                    <li id="nav_search" class="sp"><span class="btn"><img src="<!--{$TPL_URLPATH}-->img/btn_header_search.png" alt="検索"></span></li>
                </ul>
            </nav>
        </div>
        <!--▼検索バー -->
        <div id="search_area">
            <form method="get" action="<!--{$smarty.const.ROOT_URLPATH}-->products/list.php">
                <p>
                <select name="category_id">
                <option value="" selected="selected">カテゴリー</option>
                <!--{html_options options=$plg_responsive_cat_list selected=$plg_responsive_search_category_id}-->
                </select>
                </p>
                <p>
                <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
                <input type="hidden" name="mode" value="search" />
                <input type="text" name="name" id="search" value="<!--{$smarty.get.name|h}-->" placeholder="キーワードを入力" class="searchbox" />
                </p>
                <p class="button02 sp">
                    <button name="search" type="submit"><span>検索</span></button>
                </p>
           <!--▲検索バー --> 
       </form>
        </div>
        <!--▲検索バー --> 
    </div>
	<div id="header_bottom">
		<!-- ▼【ヘッダー】グローバルナビ -->
		<div id="gnav">
			<div class="inner00">
				<nav>
        		<!--{* ▼HeaderInternal COLUMN*}-->
				<!--{if $arrPageLayout.HeaderInternalNavi|@count > 0}-->
					<!--{* ▼上ナビ *}-->
					<!--{foreach key=HeaderInternalNaviKey item=HeaderInternalNaviItem from=$arrPageLayout.HeaderInternalNavi}-->
						<!-- ▼<!--{$HeaderInternalNaviItem.bloc_name}--> -->
						<!--{if $HeaderInternalNaviItem.php_path != ""}-->
							<!--{include_php file=$HeaderInternalNaviItem.php_path items=$HeaderInternalNaviItem}-->
                        <!--{else}-->
                        	<!--{include file=$HeaderInternalNaviItem.tpl_path items=$HeaderInternalNaviItem}-->
                       	<!--{/if}-->
						<!-- ▲<!--{$HeaderInternalNaviItem.bloc_name}--> -->
					<!--{/foreach}-->
					<!--{* ▲上ナビ *}-->
				<!--{/if}-->
				<!--{* ▲HeaderInternal COLUMN*}-->
				</nav>
			</div>
		</div>
	</div>
</header>
<!--▲HEADER-->
