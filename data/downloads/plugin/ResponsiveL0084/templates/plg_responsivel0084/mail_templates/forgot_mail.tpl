<!--{*
 * ResponsiveL0084
 * Copyright (C) 2013 LOCKON CO.,LTD. All Rights Reserved.
 * http://www.lockon.co.jp/
 *}-->
パスワードを変更いたしました。

新しいパスワード：<!--{$new_password}-->

このパスワードは一時的なものですので、お早めにご変更下さい。
