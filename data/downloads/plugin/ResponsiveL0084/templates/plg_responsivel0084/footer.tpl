<!--{*
 * ResponsiveL0084
 * Copyright (C) 2013 LOCKON CO.,LTD. All Rights Reserved.
 * http://www.lockon.co.jp/
 *}-->




<!--▼FOOTER-->
<p class="pagetop"><a href="#wrapper">PAGETOP</a></p>
<footer id="footer">
    <div class="inner cf">
        <nav id="footer_nav">
            <ul class="bottom_link">
                <li><a class="" href="<!--{$smarty.const.ROOT_URLPATH}-->abouts/<!--{$smarty.const.DIR_INDEX_PATH}-->">当サイトについて</a></li>
                <li><a class="" href="<!--{$smarty.const.ROOT_URLPATH}-->order/<!--{$smarty.const.DIR_INDEX_PATH}-->">特定商取引に関する表記</a></li>
                <li><a class="" href="<!--{$smarty.const.ROOT_URLPATH}-->guide/privacy.php">プライバシーポリシー</a></li>
            </ul>
        </nav>
        <small>Copyright &copy; <!--{if $smarty.const.RELEASE_YEAR != $smarty.now|date_format:"%Y"}--><!--{$smarty.const.RELEASE_YEAR}-->-<!--{/if}--><!--{$smarty.now|date_format:"%Y"}--> <!--{$arrSiteInfo.shop_name_eng|default:$arrSiteInfo.shop_name|h}--> All rights reserved.</small>
    </div>
</footer>
<!--▲FOOTER-->
