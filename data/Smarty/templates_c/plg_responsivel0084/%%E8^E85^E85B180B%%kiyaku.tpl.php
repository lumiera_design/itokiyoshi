<?php /* Smarty version 2.6.27, created on 2014-12-18 10:05:57
         compiled from /home/katsuto0301/itokiyoshi.com/public_html/ec/./data/Smarty/templates/plg_responsivel0084/entry/kiyaku.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'script_escape', '/home/katsuto0301/itokiyoshi.com/public_html/ec/./data/Smarty/templates/plg_responsivel0084/entry/kiyaku.tpl', 8, false),array('modifier', 'h', '/home/katsuto0301/itokiyoshi.com/public_html/ec/./data/Smarty/templates/plg_responsivel0084/entry/kiyaku.tpl', 8, false),array('modifier', 'nl2br', '/home/katsuto0301/itokiyoshi.com/public_html/ec/./data/Smarty/templates/plg_responsivel0084/entry/kiyaku.tpl', 18, false),)), $this); ?>

<section id="undercolumn">
    <h2 class="title"><?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['tpl_title'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)))) ? $this->_run_mod_handler('h', true, $_tmp) : smarty_modifier_h($_tmp)); ?>
</h2>
    <div class="inner">
        <p>【重要】 会員登録をされる前に、下記ご利用規約をよくお読みください。</p>
        <p>規約には、本サービスを使用するに当たってのあなたの権利と義務が規定されております。<br />
            「同意して会員登録へ」ボタンをクリックすると、あなたが本規約の全ての条件に同意したことになります。
        </p>
        
        <form name="form1" id="form1" method="post" action="?">
        <input type="hidden" name="<?php echo ((is_array($_tmp=@TRANSACTION_ID_NAME)) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)); ?>
" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['transactionid'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)); ?>
" />
        <div class="kiyaku">
            <p><?php echo "\n"; ?>
<?php echo ((is_array($_tmp=((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['tpl_kiyaku_text'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)))) ? $this->_run_mod_handler('h', true, $_tmp) : smarty_modifier_h($_tmp)))) ? $this->_run_mod_handler('nl2br', true, $_tmp) : smarty_modifier_nl2br($_tmp)); ?>
</p>
        </div>
        
        <ul class="btn_area">
            <li class="button03">
                <a href='<?php echo ((is_array($_tmp=@TOP_URLPATH)) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)); ?>
'"><span>同意しない</span></a>
            </li>
            <li class="button02">
                <a href='<?php echo ((is_array($_tmp=@ENTRY_URL)) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)); ?>
'"><span>同意して会員登録へ</span></a>
            </li>
        </ul>
        </form>
    </div>
</section>
