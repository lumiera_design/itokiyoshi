<?php /* Smarty version 2.6.27, created on 2014-12-16 15:59:10
         compiled from /home/katsuto0301/itokiyoshi.com/public_html/ec/./data/Smarty/templates/plg_responsivel0084/frontparts/bloc/recommend.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'script_escape', '/home/katsuto0301/itokiyoshi.com/public_html/ec/./data/Smarty/templates/plg_responsivel0084/frontparts/bloc/recommend.tpl', 7, false),array('modifier', 'u', '/home/katsuto0301/itokiyoshi.com/public_html/ec/./data/Smarty/templates/plg_responsivel0084/frontparts/bloc/recommend.tpl', 16, false),array('modifier', 'h', '/home/katsuto0301/itokiyoshi.com/public_html/ec/./data/Smarty/templates/plg_responsivel0084/frontparts/bloc/recommend.tpl', 17, false),array('modifier', 'sfNoImageMainList', '/home/katsuto0301/itokiyoshi.com/public_html/ec/./data/Smarty/templates/plg_responsivel0084/frontparts/bloc/recommend.tpl', 17, false),array('modifier', 'nl2br', '/home/katsuto0301/itokiyoshi.com/public_html/ec/./data/Smarty/templates/plg_responsivel0084/frontparts/bloc/recommend.tpl', 20, false),array('modifier', 'number_format', '/home/katsuto0301/itokiyoshi.com/public_html/ec/./data/Smarty/templates/plg_responsivel0084/frontparts/bloc/recommend.tpl', 21, false),)), $this); ?>
 
<?php if (count ( ((is_array($_tmp=$this->_tpl_vars['arrBestProducts'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)) ) > 0): ?>

<div class="block_outer recommend_area_wrap">
    <div id="top_recommend_area">
        <h2>ピックアップアイテム</h2>
        <div class="block_body cf">
            <ul>
                <?php $_from = ((is_array($_tmp=$this->_tpl_vars['arrBestProducts'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['recommend_products'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['recommend_products']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['arrProduct']):
        $this->_foreach['recommend_products']['iteration']++;
?>
                <li class="product_item">
                    <a href="<?php echo ((is_array($_tmp=@P_DETAIL_URLPATH)) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)); ?>
<?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['arrProduct']['product_id'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)))) ? $this->_run_mod_handler('u', true, $_tmp) : smarty_modifier_u($_tmp)); ?>
">
                        <div class="productImage"><img alt="<?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['arrProduct']['name'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)))) ? $this->_run_mod_handler('h', true, $_tmp) : smarty_modifier_h($_tmp)); ?>
" src="<?php echo ((is_array($_tmp=@IMAGE_SAVE_URLPATH)) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)); ?>
<?php echo ((is_array($_tmp=((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['arrProduct']['main_list_image'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)))) ? $this->_run_mod_handler('sfNoImageMainList', true, $_tmp) : SC_Utils_Ex::sfNoImageMainList($_tmp)))) ? $this->_run_mod_handler('h', true, $_tmp) : smarty_modifier_h($_tmp)); ?>
"></div>
                        <div class="productContents">
                            <h3> <?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['arrProduct']['name'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)))) ? $this->_run_mod_handler('h', true, $_tmp) : smarty_modifier_h($_tmp)); ?>
 </h3>
                            <p class="comment"><?php echo ((is_array($_tmp=((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['arrProduct']['comment'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)))) ? $this->_run_mod_handler('h', true, $_tmp) : smarty_modifier_h($_tmp)))) ? $this->_run_mod_handler('nl2br', true, $_tmp) : smarty_modifier_nl2br($_tmp)); ?>
</p>
                            <p class="sale_price"><span class="price"><?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['arrProduct']['price02_min_inctax'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)))) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
円</span></p>
                        </div>
                    </a>
                </li>
                <?php endforeach; endif; unset($_from); ?>
            </ul>
        </div>
    </div>
</div>
<?php endif; ?>
