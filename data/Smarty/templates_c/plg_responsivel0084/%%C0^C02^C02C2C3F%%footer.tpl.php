<?php /* Smarty version 2.6.27, created on 2014-12-16 15:59:10
         compiled from /home/katsuto0301/itokiyoshi.com/public_html/ec/./data/Smarty/templates/plg_responsivel0084/footer.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'script_escape', '/home/katsuto0301/itokiyoshi.com/public_html/ec/./data/Smarty/templates/plg_responsivel0084/footer.tpl', 16, false),array('modifier', 'date_format', '/home/katsuto0301/itokiyoshi.com/public_html/ec/./data/Smarty/templates/plg_responsivel0084/footer.tpl', 33, false),array('modifier', 'default', '/home/katsuto0301/itokiyoshi.com/public_html/ec/./data/Smarty/templates/plg_responsivel0084/footer.tpl', 33, false),array('modifier', 'h', '/home/katsuto0301/itokiyoshi.com/public_html/ec/./data/Smarty/templates/plg_responsivel0084/footer.tpl', 33, false),)), $this); ?>




<!--▼FOOTER-->
<p class="pagetop"><a href="#wrapper">PAGETOP</a></p>
<footer id="footer">
    <div class="inner cf">
        <nav id="footer_nav">
            <ul class="bottom_link">
            <li><a class="" href="<?php echo ((is_array($_tmp=@TOP_URLPATH)) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)); ?>
">TOP</a></li>
                <li><a class="" href="<?php echo ((is_array($_tmp=@ROOT_URLPATH)) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)); ?>
abouts/<?php echo ((is_array($_tmp=@DIR_INDEX_PATH)) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)); ?>
">当サイトについて</a></li>
                <li><a class="" href="<?php echo ((is_array($_tmp=@ROOT_URLPATH)) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)); ?>
order/<?php echo ((is_array($_tmp=@DIR_INDEX_PATH)) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)); ?>
">特定商取引に関する表記</a></li>
                <li><a class="" href="<?php echo ((is_array($_tmp=@ROOT_URLPATH)) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)); ?>
guide/privacy.php">プライバシーポリシー</a></li>
                <?php if (((is_array($_tmp=$this->_tpl_vars['plg_responsiveL0084_login'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp))): ?>
            <!--▼▼▼ログイン後▼▼▼-->
           <li> <a class="" href="<?php echo ((is_array($_tmp=@ROOT_URLPATH)) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)); ?>
mypage/">マイページ</a></li>
            <!--▲▲▲ログイン後▲▲▲-->
            <?php else: ?>
            <!--▼▼▼ログイン前▼▼▼-->
            <li> <a class="" href="<?php echo ((is_array($_tmp=@ROOT_URLPATH)) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)); ?>
mypage/">ログイン</a></li>
            <!--▲▲▲ログイン前▲▲▲-->
            <?php endif; ?>
                <li><a class="" href="<?php echo ((is_array($_tmp=@ROOT_URLPATH)) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)); ?>
guide/plg_ResponsiveL0084_guide.php">ご利用の案内</a></li>
                
            </ul>
        </nav>
        <small>Copyright &copy; <?php if (((is_array($_tmp=@RELEASE_YEAR)) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)) != ((is_array($_tmp=((is_array($_tmp=time())) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)))) ? $this->_run_mod_handler('date_format', true, $_tmp, "%Y") : smarty_modifier_date_format($_tmp, "%Y"))): ?><?php echo ((is_array($_tmp=@RELEASE_YEAR)) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)); ?>
-<?php endif; ?><?php echo ((is_array($_tmp=((is_array($_tmp=time())) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)))) ? $this->_run_mod_handler('date_format', true, $_tmp, "%Y") : smarty_modifier_date_format($_tmp, "%Y")); ?>
 <?php echo ((is_array($_tmp=((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['arrSiteInfo']['shop_name_eng'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)))) ? $this->_run_mod_handler('default', true, $_tmp, ((is_array($_tmp=$this->_tpl_vars['arrSiteInfo']['shop_name'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp))) : smarty_modifier_default($_tmp, ((is_array($_tmp=$this->_tpl_vars['arrSiteInfo']['shop_name'])) ? $this->_run_mod_handler('script_escape', true, $_tmp) : smarty_modifier_script_escape($_tmp)))))) ? $this->_run_mod_handler('h', true, $_tmp) : smarty_modifier_h($_tmp)); ?>
 All rights reserved.</small>
    </div>
</footer>
<!--▲FOOTER-->