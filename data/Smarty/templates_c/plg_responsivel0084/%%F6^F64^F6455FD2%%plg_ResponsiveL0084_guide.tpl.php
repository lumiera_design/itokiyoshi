<?php /* Smarty version 2.6.27, created on 2014-12-18 11:32:34
         compiled from guide/plg_ResponsiveL0084_guide.tpl */ ?>
<!--▼CONTENTS-->
<section id="undercolumn">
    <h2 class="title">お買い物ガイド</h2>
                    
    <div class="inner" id="guide">
    	<h3 class="heading02">お支払いについて</h3>
        <p>取り扱いカードは以下のとおりです。すべてのカード会社で、一括払いが可能となっております。</p>
        <div class="table">
            <div class="thead">
                <ol>
                    <li>カード会社</li>
                    <li>支払方法</li>
                </ol>
            </div>
            <div class="tbody">
                <div class="tr">
                    <div class="th">VISA</div>
                    <div>リボ,分割（3,5,6,10,12,15,18,20,24回が可能です）<br />
                    ボーナス一括（夏　8月払い：12月16日～6月15日、冬　翌年1月払い：7月16日～11月15日）</div>
                </div>
                <div class="tr">
                    <div class="th">MASTER</div>
                    <div>リボ,分割（3,5,6,10,12,15,18,20,24回が可能です）<br />
                    ボーナス一括（夏　8月払い：12月16日～6月15日、冬　翌年1月払い：7月16日～11月15日）</div>
                </div>
                <div class="tr">
                    <div class="th">JCB</div>
                    <div>リボ,分割（3,5,6,10,12,15,18,20,24回が可能です）<br />
                    ボーナス一括（夏　8月払い：12月16日～6月15日、冬　翌年1月払い：7月16日～11月15日）</div>
                </div>
                <div class="tr">
                    <div class="th">Diners</div>
                    <div>リボ,ボーナス一括（夏　8月払い：12月16日～6月15日、冬　翌年1月払い：7月16日～11月15日）</div>
                </div>
                <div class="tr">
                    <div class="th">AMEX</div>
                    <div>分割（3,5,6,10,12,15,18,20,24回が可能です）<br />
                    ボーナス一括（夏　8月払い：12月16日～6月15日、冬　翌年1月払い：7月16日～11月15日）</div>
                </div>
            </div>
        </div>
        
        <p>【備考】<br />
・楽天市場のカード決済ではSSLというシステムを利用しております。カード番号は暗号化されて送信されますのでご安心下さい。<br />
・当カード決済システム上、クレジットカード利用控は 発行しておりません。<br />
・カード会社から送付されますご利用明細をご確認下さい。<br />
・ご注文の際にお客様の本人確認（電話番号確認等）をお願いする場合もございます。<br />
・お客様と異なる名義のクレジットカードはご利用できません。</p>

<p class="attention">※お客様のご利用状態等によっては、他の決済手段に変更いただく場合がございます。 </p>

    </div>
    
</section>
<!--▲CONTENTS-->
