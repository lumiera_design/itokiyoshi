<!--{*
 * ResponsiveL0084
 * Copyright (C) 2013 LOCKON CO.,LTD. All Rights Reserved.
 * http://www.lockon.co.jp/
 *}-->

<section id="undercolumn">
    <h2 class="title"><!--{$tpl_title|h}--></h2>
                    
    <div class="inner">
        <p>
            本登録が完了いたしました。<br />
            それではショッピングをお楽しみください。
        </p>
        <p>今後ともご愛顧賜りますようよろしくお願い申し上げます。</p>

        <p><!--{$arrSiteInfo.company_name|h}--></p>
        <p>
            TEL：<!--{$arrSiteInfo.tel01}-->-<!--{$arrSiteInfo.tel02}-->-<!--{$arrSiteInfo.tel03}--> <!--{if $arrSiteInfo.business_hour != ""}-->（受付時間/<!--{$arrSiteInfo.business_hour}-->）<!--{/if}--><br />
            E-mall：<a href="mailto:<!--{$arrSiteInfo.email02|escape:'hex'}-->"><!--{$arrSiteInfo.email02|escape:'hexentity'}--></a>
        </p>

        <div class="btn_area">
            <p class="button02">
                <button type="button" onclick="location.href='<!--{$smarty.const.TOP_URLPATH}-->'"><span>トップページへ</span></button>
            </p>
        </div>
    </div>
    
</section>
