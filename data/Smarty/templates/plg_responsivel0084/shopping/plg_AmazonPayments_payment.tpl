<!--{*
* AmazonPayments
* Copyright(c) 2015 IPLOGIC CO.,LTD. All Rights Reserved.
*
* http://www.iplogic.co.jp/
*
* This program is not free software.
* It applies to terms of service.
*
*}-->


<script type="text/javascript">
    window.onAmazonLoginReady = function() { amazon.Login.setClientId("<!--{$smarty.const.AMAZON_CLIENT_ID}-->"); };

    var amazon_submit_flag = false;

    function amazon_submit() {
        if (amazon_submit_flag) {
            document.form1.submit();
            amazon_submit_flag = false;
        } else {
            alert("ご注文金額が確定していません。");
        }
    }

</script>
<script type="text/javascript">
    /**
     * 配送方法が変更された場合、ご注文内容・お届け時間の指定に反映させる
     */
    function selectDeliv(deliv_id) {
        amazon_submit_flag = false;
        var msg = "計算中…";
        $('#deliv_fee').text(msg);
        $('#payment_total').text(msg);


        var postData = new Object;
        postData["<!--{$smarty.const.TRANSACTION_ID_NAME}-->"] = "<!--{$transactionid}-->";
        postData["mode"] = "select_deliv";
        postData["deliv_id"] = deliv_id;
        $.ajax({
            type: "POST",
            url: "<!--{$smarty.const.ROOT_URLPATH}-->shopping/plg_AmazonPayments_payment.php",
            data: postData,
            cache: false,
            dataType: "json",
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                alert('通信中にエラーが発生しました。カート画面に移動します。');
                location.href = '<!--{$smarty.const.CART_URLPATH}-->';
            },
            success: function(result) {
                if (isFinite(result.payment_total)) {
                    $('#deliv_fee').text(commaSeparate(result.deliv_fee));
                    $('#payment_total').text(commaSeparate(result.payment_total));

                    // お届け時間を生成
                    var deliv_time_id_select = $('select[id^=deliv_time_id]');
                    deliv_time_id_select.empty();
                    deliv_time_id_select.append($('<option />').text('指定なし').val(''));
                    for (var i in result.arrDelivTime) {
                        var option = $('<option />')
                            .val(i)
                            .text(result.arrDelivTime[i])
                            .appendTo(deliv_time_id_select);
                    }

                    amazon_submit_flag = true;
                } else {
                    alert('通信中にエラーが発生しました。カート画面に移動します。');
                    location.href = '<!--{$smarty.const.CART_URLPATH}-->';
                }
            }
        });
    }
</script>
<script type="text/javascript">
var subtotal = <!--{$arrForm.subtotal}-->;
var user_point = <!--{$tpl_user_point}-->;
var point_value = <!--{$smarty.const.POINT_VALUE}-->;

    /**
     * 使用ポイントが変更された場合、ご注文内容に反映させる
     */
    function inputPoint(form, point_check) {
        amazon_submit_flag = false;
        var $form = $(form);

        if (point_check == 1) {
            var point = $form.find('input[name=use_point]').val();
        } else {
            var point = 0;
        }

        // ポイント超過チェック
        if (point_check == 1 && point == '') {
            alert('ご利用ポイントが入力されていません。');
            return;
        } else if (!isFinite(point)) {
            alert('ご利用ポイントは数字で入力してください。');
            return;
        } else if (point * point_value == subtotal) {
            alert('ご購入金額が0円となるため決済できません。');
            return;
        } else if (point * point_value > subtotal) {
            alert('ご利用ポイントがご購入金額を超えています。');
            return;
        } else if (point > user_point) {
            alert('ご利用ポイントが所持ポイントを超えています。');
            return;
        }

        var msg = "計算中…";
        $('#use_point').text(msg);
        $('#total_point').text(msg);

        var postData = new Object;
        postData["<!--{$smarty.const.TRANSACTION_ID_NAME}-->"] = "<!--{$transactionid}-->";
        postData["mode"] = "input_point";
        postData["point_check"] = point_check;
        postData["point"] = point;
        $.ajax({
            type: "POST",
            url: "<!--{$smarty.const.ROOT_URLPATH}-->shopping/plg_AmazonPayments_payment.php",
            data: postData,
            cache: false,
            dataType: "json",
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                alert('通信中にエラーが発生しました。カート画面に移動します。');
                location.href = '<!--{$smarty.const.CART_URLPATH}-->';
            },
            success: function(result) {
                if (isFinite(result.total_point)) {
                    $('#use_point').text(commaSeparate(result.use_point));
                    $('#discount').text(commaSeparate(result.discount));
                    $('#total_point').text(commaSeparate(result.total_point));
                    $('#payment_total').text(commaSeparate(result.payment_total));
                    amazon_submit_flag = true;
                } else {
                    alert('通信中にエラーが発生しました。カート画面に移動します。');
                    location.href = '<!--{$smarty.const.CART_URLPATH}-->';
                }
            }
        });
    }
</script>
<script type="text/javascript">
    function commaSeparate(num) {
        return String(num).replace( /(\d)(?=(\d\d\d)+(?!\d))/g, '$1,');
    }

    function mailChange(obj) {
        if (obj.checked) {
            $("#mail_magazine").attr("disabled", false);
        } else {
            $("#mail_magazine").attr("disabled", true);
            $("#mail_magazine").attr("checked", false);
        }
    }
</script>

<script type="text/javascript" src="<!--{$smarty.const.AMAZON_WIDGET_URL}-->"></script>
<link href="<!--{$smarty.const.ROOT_URLPATH}--><!--{$smarty.const.USER_DIR}-->AmazonPayments/amazon.css" rel="stylesheet" type="text/css" media="all"/>

<div id="undercolumn">
    <div id="undercolumn_shopping">
        <section class="box -lower -last">
            <div class="inner -amazon">
            <h2 class="title -mincho -lower"><!--{$tpl_title|h}--></h2>

                <div class="amazon-wrap">
                    <form name="form1" id="form1" method="post" action="?">
                        <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
                        <input type="hidden" name="mode" value="confirm" />
                        <input type="hidden" name="amazonOrderReferenceId" id="amazonOrderReferenceId" value="" />
                        <input type="hidden" name="addressConsentToken" value="<!--{$addressConsentToken}-->" />

                        <div id="amazon-inner">
                            <div class="amazon-content">
                            <h3 class="amazon-inner_title">ご注文商品</h3>
                                <div class="formBox -mb35">
                                    <div class="cartinarea cf">

                                        <!--▼商品 -->
                                        <div class="table" id="confirm_table">
                                            <div class="thead">
                                                <ol>
                                                    <li>商品内容</li>
                                                    <li>数量</li>
                                                    <li>小計</li>
                                                </ol>
                                            </div>
                                            <div class="tbody">
                                                <!--{foreach from=$arrCartItems item=item}-->
                                                <div class="tr">
                                                    <div class="item">
                                                        <div class="photo">
                                                            <!--{if $item.productsClass.main_image|strlen >= 1}-->
                                                            <a href="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$item.productsClass.main_image|sfNoImageMainList|h}-->" class="expansion" target="_blank"><img src="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$item.productsClass.main_image|sfNoImageMainList|h}-->" alt="<!--{$item.productsClass.name|h}-->" /></a>
                                                            <!--{else}-->
                                                            <img src="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$item.productsClass.main_image|sfNoImageMainList|h}-->" alt="<!--{$item.productsClass.name|h}-->" />
                                                            <!--{/if}-->
                                                        </div>
                                                        <p><em><!--→商品名--><a href="<!--{$smarty.const.P_DETAIL_URLPATH}--><!--{$item.productsClass.product_id|u}-->"><!--{$item.productsClass.name|h}--></a><!--←商品名--></em></p>
                                                        <!--{if $item.productsClass.classcategory_name1 != ""}-->
                                                        <p class="small"><!--{$item.productsClass.class_name1|h}-->：<!--{$item.productsClass.classcategory_name1|h}--></p>
                                                        <!--{/if}-->
                                                        <!--{if $item.productsClass.classcategory_name2 != ""}-->
                                                        <p class="small"><!--{$item.productsClass.class_name2|h}-->：<!--{$item.productsClass.classcategory_name2|h}--></p>
                                                        <!--{/if}-->
                                                        <p class="small"><!--→金額-->価格:<!--{$item.price_inctax|number_format}-->円<!--←金額--></p>
                                                    </div>
                                                    <div class="account"><!--{$item.quantity|number_format}--></div>
                                                    <div class="price"><!--{$item.total_inctax|number_format}-->円</div>
                                                </div>
                                                <!--{/foreach}-->
                                            </div>
                                        </div>
                                        <!--▲商品 -->
                                    </div>
                                </div>
                                <div id="amazon_info">
                                    <div id="amazon_address">
                                        <h3 class="amazon-inner_title">お届け先の指定</h3>
                                        <p class="amazon-inner_txt">お届け先をご選択ください。</p>
                                        <div id="addressBookWidgetDiv"></div>
                                        <script>
                                        	var amazonOrderReferenceId;
                                        	new OffAmazonPayments.Widgets.AddressBook({
                                            	sellerId: "<!--{$smarty.const.AMAZON_SELLER_ID}-->",
                                            	design: {
                                            		size : {width:"<!--{$smarty.const.PC_DELIV_WIDGET_WIDTH}-->px", height:"<!--{$smarty.const.PC_DELIV_WIDGET_HEIGHT}-->px"}
                                        	    },
                                            	onOrderReferenceCreate: function(orderReference) {
                                            		amazonOrderReferenceId = orderReference.getAmazonOrderReferenceId();
                                            		$("#amazonOrderReferenceId").val(amazonOrderReferenceId);
                                            	},
                                            	onAddressSelect: function(orderReference) {
                                            	    amazon_submit_flag = false;

                                            	    var msg = "計算中…";
                                        			$("#deliv_fee").text(msg);
                                        			$("#payment_total").text(msg);

                                            		var postData = new Object;
                                            		postData["<!--{$smarty.const.TRANSACTION_ID_NAME}-->"] = "<!--{$transactionid}-->";
                                            		postData["mode"] = "select_addr";
                                            		postData["amazonOrderReferenceId"] = amazonOrderReferenceId;
                                            		postData["addressConsentToken"] = "<!--{$addressConsentToken}-->";
                                            		$.ajax({
                                            			type: "POST",
                                            			url: "<!--{$smarty.const.ROOT_URLPATH}-->shopping/plg_AmazonPayments_payment.php",
                                            			data: postData,
                                            			cache: false,
                                            			dataType: "json",
                                            			error: function(XMLHttpRequest, textStatus, errorThrown){
                                            				alert("通信中にエラーが発生しました。カート画面に移動します。");
                                            				location.href = "<!--{$smarty.const.CART_URLPATH}-->";
                                            			},
                                            			success: function(result){
                                            			    if (isFinite(result.payment_total)) {
                                            				    $("#deliv_fee").text(commaSeparate(result.deliv_fee));
                                            				    $("#payment_total").text(commaSeparate(result.payment_total));
                                                                amazon_submit_flag = true;
                                            				} else {
                                            				    alert("通信中にエラーが発生しました。カート画面に移動します。");
                                            				    location.href = "<!--{$smarty.const.CART_URLPATH}-->";
                                            				}
                                            			}
                                            		});
                                            	},
                                            	onError: function(error) {
                                            	    alert("通信中にエラーが発生しました。カート画面に移動します。");
                                            		location.href = "<!--{$smarty.const.CART_URLPATH}-->";
                                            	}
                                        	}).bind("addressBookWidgetDiv");
                                        </script>
                                    </div>

                                    <div id="amazon_payment">
                                        <h3 class="amazon-inner_title">お支払い方法の指定</h3>
                                        <p class="amazon-inner_txt">お支払い方法をご選択ください。</p>
                                        <div id="walletWidgetDiv"></div>
                                        <script>
                                            new OffAmazonPayments.Widgets.Wallet({
                                                sellerId: "<!--{$smarty.const.AMAZON_SELLER_ID}-->",
                                                design: {
                                                    size : {width:"<!--{$smarty.const.PC_PAYMENT_WIDGET_WIDTH}-->px", height:"<!--{$smarty.const.PC_PAYMENT_WIDGET_HEIGHT}-->px"}
                                                },
                                                onPaymentSelect: function(orderReference) {

                                                },
                                                onError: function(error) {
                                                    alert("通信中にエラーが発生しました。カート画面に移動します。");
                                                    location.href = "<!--{$smarty.const.CART_URLPATH}-->";
                                                }
                                            }).bind("walletWidgetDiv");
                                        </script>
                                    </div>
                                </div>

                                <!--{assign var=key value="deliv_id"}-->
                                <!--{if $is_single_deliv}-->
                                <input type="hidden" name="<!--{$key}-->" value="<!--{$arrForm[$key].value|h}-->" id="deliv_id" />
                                <!--{else}-->
                                <div class="pay_area">
                                    <h3 class="amazon-inner_title">配送方法の指定</h3>
                                    <p class="amazon-inner_txt">まずはじめに、配送方法を選択ください。</p>
                                    <div class="table payment_table">
                                        <div class="thead">
                                            <ol>
                                                <li>選択</li>
                                                <li>配送方法</li>
                                            </ol>
                                        </div>
                                        <div class="tbody">
                                            <!--{section name=cnt loop=$arrDeliv}-->
                                            <div class="tr">
                                                <div><input<!--{if $arrErr[$key] != ""}--> class="error"<!--{/if}--> type="radio" id="deliv_<!--{$smarty.section.cnt.iteration}-->" name="<!--{$key}-->" value="<!--{$arrDeliv[cnt].deliv_id}-->" <!--{$arrDeliv[cnt].deliv_id|sfGetChecked:$arrForm[$key].value}--> onchange="selectDeliv(<!--{$arrDeliv[cnt].deliv_id}-->);" /></div>
                                                <div><label for="deliv_<!--{$smarty.section.cnt.iteration}-->"><!--{$arrDeliv[cnt].name|h}--><!--{if $arrDeliv[cnt].remark != ""}--> <!--{$arrDeliv[cnt].remark|h|nl2br}--><!--{/if}--></label></div>
                                            </div>
                                            <!--{/section}-->
                                        </div>
                                    </div>
                                    <!--{if $arrErr[$key] != ""}-->
                                    <div class="attention"><!--{$arrErr[$key]}--></div>
                                    <!--{/if}-->
                                </div>
                                <!--{/if}-->

                                <!--{if !$is_download}-->
                                <div class="pay_area02">
                                    <h3 class="amazon-inner_title">お届け時間の指定</h3>
                                    <!--{foreach item=shippingItem name=shippingItem from=$arrShipping}-->
                                    <!--{assign var=index value=$shippingItem.shipping_id}-->
                                    <div class="delivdate top">
                                        <!--★お届け日★-->
                                        <!--{assign var=key value="deliv_date`$index`"}-->
                                        <span class="attention"><!--{$arrErr[$key]}--></span>
                                        お届け日：
                                        <!--{if !$arrDelivDate}-->
                                            ご指定頂けません。
                                        <!--{else}-->
                                            <select name="<!--{$key}-->" id="<!--{$key}-->" style="<!--{$arrErr[$key]|sfGetErrorColor}-->">
                                                <option value="" selected="">指定なし</option>
                                                <!--{assign var=shipping_date_value value=$arrForm[$key]|default:$shippingItem.shipping_date}-->
                                                <!--{html_options options=$arrDelivDate selected=$shipping_date_value}-->
                                            </select>&nbsp;
                                        <!--{/if}-->
                                        <!--★お届け時間★-->
                                        <!--{assign var=key value="deliv_time_id`$index`"}-->
                                        <span class="attention"><!--{$arrErr[$key]}--></span>
                                        お届け時間：
                                        <select name="<!--{$key}-->" id="<!--{$key}-->" style="<!--{$arrErr[$key]|sfGetErrorColor}-->">
                                            <option value="" selected="">指定なし</option>
                                            <!--{assign var=shipping_time_value value=$arrForm[$key]|default:$shippingItem.time_id}-->
                                            <!--{html_options options=$arrDelivTime selected=$shipping_time_value}-->
                                        </select>
                                    </div>
                                    <!--{/foreach}-->
                                </div>
                                <!--{/if}-->

                                <!-- ▼ポイント使用 -->
                                <!--{if $tpl_login == 1 && $smarty.const.USE_POINT !== false}-->
                                    <div class="point_area">
                                        <h3 class="amazon-inner_title">ポイント使用の指定</h3>
                                        <p class="amazon-inner_txt"><span class="attention">1ポイントを<!--{$smarty.const.POINT_VALUE|number_format}-->円</span>として使用する事ができます。<br />
                                            使用する場合は、「ポイントを使用する」にチェックを入れた後、使用するポイントをご記入ください。
                                        </p>
                                        <div class="point_announce">
                                            <p class="amazon-inner_txt"><span class="user_name"><!--{$name01|h}--> <!--{$name02|h}-->様</span>の、現在の所持ポイントは「<span class="point"><!--{$tpl_user_point|default:0|number_format}-->Pt</span>」です。<br />
                                                今回ご購入合計金額：<span class="price"><!--{$arrForm.subtotal|number_format}-->円</span> <span class="attention">(送料、手数料を含みません。)</span>
                                            </p>
                                            <ul>
                                                <li>
                                                    <!--{if substr($smarty.const.ECCUBE_VERSION, 0, 4) === '2.12'}-->
                                                        <input type="radio" id="point_on" name="point_check" value="1" <!--{$arrForm.point_check|sfGetChecked:1}--> onclick="fnCheckInputPoint(); inputPoint(this.form, 1);" /><label for="point_on">ポイントを使用する</label>
                                                    <!--{elseif substr($smarty.const.ECCUBE_VERSION, 0, 4) === '2.13'}-->
                                                        <input type="radio" id="point_on" name="point_check" value="1" <!--{$arrForm.point_check|sfGetChecked:1}--> onclick="eccube.togglePointForm(); inputPoint(this.form, 1);" /><label for="point_on">ポイントを使用する</label>
                                                    <!--{/if}-->
                                                        <!--{assign var=key value="use_point"}--><br />
                                                    今回のお買い物で、<input type="text" name="<!--{$key}-->" value="<!--{$arrForm[$key]|default:0}-->" maxlength="<!--{$smarty.const.INT_LEN}-->" style="<!--{$arrErr[$key]|sfGetErrorColor}-->" class="box60" onchange="inputPoint(this.form, 1);"/>&nbsp;Ptを使用する。<span class="attention"><!--{$arrErr[$key]}--></span>
                                                </li>
                                                    <!--{if substr($smarty.const.ECCUBE_VERSION, 0, 4) === '2.12'}-->
                                                        <li><input type="radio" id="point_off" name="point_check" value="2" <!--{$arrForm.point_check|sfGetChecked:2}--> onclick="fnCheckInputPoint(); inputPoint(this.form, 2);" /><label for="point_off">ポイントを使用しない</label></li>
                                                    <!--{elseif substr($smarty.const.ECCUBE_VERSION, 0, 4) === '2.13'}-->
                                                        <li><input type="radio" id="point_off" name="point_check" value="2" <!--{$arrForm.point_check|sfGetChecked:2}--> onclick="eccube.togglePointForm(); inputPoint(this.form, 2);" /><label for="point_off">ポイントを使用しない</label></li>
                                                    <!--{/if}-->
                                            </ul>
                                        </div>
                                    </div>
                                <!--{/if}-->
                                <!-- ▲ポイント使用 -->

                                <div class="pay_area02">
                                    <h3 class="amazon-inner_title">その他お問い合わせ</h3>
                                    <p class="amazon-inner_txt">その他お問い合わせ事項がございましたら、こちらにご入力ください。</p>
                                    <div>
                                        <!--★その他お問い合わせ事項★-->
                                        <!--{assign var=key value="message"}-->
                                        <span class="attention"><!--{$arrErr[$key]}--></span>
                                        <textarea name="<!--{$key}-->" style="<!--{$arrErr[$key]|sfGetErrorColor}-->" cols="100" rows="8" class="txtarea" wrap="hard"><!--{"\n"}--><!--{$arrForm[$key]|h}--></textarea>
                                        <p class="attention"> (<!--{$smarty.const.LTEXT_LEN}-->文字まで)</p>
                                    </div>
                                </div>
                            </div>

                            <div class="amazon-detail">
                    			<h3 class="amazon-inner_title">ご注文内容</h3>
                                <div class="amazon-inner-content_detail">
                        			<table id="confirm_price_amazon" class="amazon-inner_table -detail">
                        			    <tr>
                            				<td class="amazon-inner_table_td">小計：</td>
                            				<td class="price_amazon"><!--{$arrForm.subtotal|number_format}-->円</td>
                        			    </tr>
                                        <!--{if $tpl_login == 1 && $smarty.const.USE_POINT !== false}-->
                                        <tr>
                                            <td class="amazon-inner_table_td">値引き（ポイントご使用時）：</td>
                                            <!--{assign var=key value="use_point"}-->
                                            <td class="price_amazon">-<span id="discount"><!--{$arrForm[$key]|number_format|default:0}--></span>円</td>
                                        </tr>
                                        <!--{/if}-->
                        			    <tr>
                            				<td class="amazon-inner_table_td">送料：</td>
                            				<td class="price_amazon"><span id="deliv_fee">計算中…</span>円</td>
                        			    </tr>
                                        <tr >
                                            <td class="amazon-inner_table_td">手数料：</td>
                                            <!--{assign var=key value="charge"}-->
                                            <td class="price_amazon"><!--{$arrForm[$key]|number_format|default:0}-->円</td>
                                        </tr>
                        			    <tr>
                            				<td class="totalprice_amazon amazon-inner_table_td">注文合計</td>
                            				<td class="totalprice2_amazon"><span id="payment_total">計算中…</span>円</td>
                        			    </tr>
                        			</table>

                                    <!--{* ログイン済みの会員のみ *}-->
                                    <!--{if $tpl_login == 1 && $smarty.const.USE_POINT !== false}-->
                                        <table id="confirm_point_amazon" class="amazon-inner_box -margin">
                                            <tr>
                                                <td class="amazon-inner_table_td">ご注文前のポイント：</td>
                                                <td class="point_amazon"><!--{$tpl_user_point|number_format|default:0}--> Pt</td>
                                            </tr>
                                            <tr>
                                                <td class="amazon-inner_table_td">ご使用ポイント：</td>
                                                <!--{assign var=key value="use_point"}-->
                                                <td class="point_amazon">-<span id="use_point"><!--{$arrForm[$key]|number_format|default:0}--></span> Pt</td>
                                            </tr>
                                            <!--{assign var=key value="birth_point"}-->
                                            <!--{if $arrForm[$key] > 0}-->
                                            <tr>
                                                <td class="amazon-inner_table_td">お誕生月ポイント：</td>
                                                <td class="point_amazon">+<!--{$arrForm[$key]|number_format|default:0}--> Pt</td>
                                            </tr>
                                            <!--{/if}-->
                                            <tr>
                                                <td class="amazon-inner_table_td">今回加算予定のポイント：</th>
                                                <td class="point_amazon">+<!--{$arrForm.add_point|number_format|default:0}--> Pt</td>
                                            </tr>
                                            <tr>
                                                <td class="amazon-inner_table_td">加算後のポイント：</td>
                                                <!--{assign var=total_point value=$tpl_user_point+$arrForm.add_point}-->
                                                <td class="point_amazon"><span id="total_point"><!--{$total_point|number_format|default:0}--></span> Pt</td>
                                            </tr>
                                        </table>
                                    <!--{elseif $tpl_login != 1}-->
                                        <table id="customer_regist" class="amazon-inner_box -margin">
                                            <tr>
                                                <td class="amazon-inner_box_title" colspan="2" style="background-color:#F0FFFF;">
                                                    <!--{if $smarty.const.USE_POINT !== false}-->
                                                        会員登録をしていただくと、次回以降ポイントがご利用いただけます。
                                                    <!--{else}-->
                                                        会員登録をしていただくと、次回以降簡単にご利用いただけます。
                                                    <!--{/if}-->
                                                    <!--{if $welcome_point > 0}-->
                                                        <br><span class="attention">ご登録いただくと、入会特典として<!--{$welcome_point|number_format}-->Ptが付与されます。</span>
                                                    <!--{/if}-->
                                                </td>
                                            </tr>
                                            <tr>
                                                <!--{assign var=key value="customer_regist"}-->
                                                <div class="attention"><!--{$arrErr[$key]}--></div>
                                                <td><input type="checkbox" name="<!--{$key}-->" id="regist_flag" value="1" <!--{$arrForm[$key]|sfGetChecked:1}--> onchange="mailChange(this);" /></td>
                                                <td>
                                                    <label for="regist_flag">ご利用規約に同意し、注文者情報を会員として登録する</label><br>
                                                    <a href="<!--{$smarty.const.HTTPS_URL}-->entry/kiyaku.php" target="blank">※ご利用規約(必ずお読みください)</a>
                                                </td>
                                            </tr>
                                        </table>
                                        <table id="customer_regist2" class="amazon-inner_box">
                                            <tr>
                                                <td class="amazon-inner_box_title" colspan="2" style="background-color:#F0FFFF;">お買い得情報や最新情報をメールでお届けします。</td>
                                            <tr>
                                                <!--{assign var=key value="mail_magazine"}-->
                                                <div class="attention"><!--{$arrErr[$key]}--></div>
                                                <td><input type="checkbox" name="<!--{$key}-->" id="mail_magazine" value="1" <!--{$arrForm[$key]|sfGetChecked:1}--> <!--{if $arrForm.customer_regist != 1}-->disabled="disabled"<!--{/if}--> /></td>
                                                <td><label for="mail_magazine">メールマガジンを購読する</label></td>
                                            </tr>
                                        </table>
                                    <!--{/if}-->

                                    <div class="amazon-btn_area is-pc">
                                        <ul class="amazon-btn_area_list">
                                            <!--{if $smarty.const.AMAZON_PAY_CONF_DISP == 0}-->
                                                <li class="amazon-btn_area_item button02"><button type="button" onclick="amazon_submit(); return false; javascript:void(document.form1.submit());" style="font-size:14px; height:41px;"><span>確認ページへ</span></button></li>
                                            <!--{else}-->
                                                <li class="amazon-btn_area_item button02"><button type="button" onclick="amazon_submit(); return false; javascript:void(document.form1.submit());" style="font-size:14px; height:41px;"><span>注文を確定する</span></button></li>
                                            <!--{/if}-->
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div class="amazon-btn_area">
                                <ul class="btn_area mazon-btn_area_list -last">
                                    <!--{if $smarty.const.AMAZON_PAY_CONF_DISP == 0}-->
                                        <li class="amazon-btn_area_item button02"><button type="button" onclick="amazon_submit(); return false; javascript:void(document.form1.submit());"><span>確認ページへ</span></button></li>
                                    <!--{else}-->
                                        <li class="amazon-btn_area_item button02"><button type="button" onclick="amazon_submit(); return false; javascript:void(document.form1.submit());"><span>注文を確定する</span></button></li>
                                    <!--{/if}-->
                                    <li class="amazon-btn_area_item button03"><button type="button" onclick="location.href='<!--{$smarty.const.CART_URLPATH}-->'"><span>戻る</span></button></li>

                                </ul>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>
</div>
