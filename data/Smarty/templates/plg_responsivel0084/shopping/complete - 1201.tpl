<!--{*
 * ResponsiveL0084
 * Copyright (C) 2013 LOCKON CO.,LTD. All Rights Reserved.
 * http://www.lockon.co.jp/
 *}-->

<section id="undercolumn">
    <div id="undercolumn_shopping">
        <h2 class="title"><!--{$tpl_title|h}--></h2>
        <div class="flow_area">
            <ol>
                <li>STEP1<br /><span class="pc">お届け先指定</span></li>
                <li>STEP2<br /><span class="pc">お支払い方法等選択</span></li>
                <li>STEP3<br /><span class="pc">内容確認</span></li>
                <li class="active">STEP4<br /><span class="pc">完了</span></li>
            </ol>
        </div>

        <div class="inner">
            <!-- ▼その他決済情報を表示する場合は表示 -->
            <!--{if $arrOther.title.value}-->
                <div class="message marB20">
                    <span class="attention">■<!--{$arrOther.title.name}-->情報</span><br />
                    <!--{foreach key=key item=item from=$arrOther}-->
                        <!--{if $key != "title"}-->
                            <!--{if $item.name != ""}-->
                                <!--{$item.name}-->：
                            <!--{/if}-->
                                <!--{$item.value|nl2br}--><br />
                        <!--{/if}-->
                    <!--{/foreach}-->
                </div>
            <!--{/if}-->
            <!-- ▲コンビニ決済の場合には表示 -->

            <div>
                <!--{$arrInfo.shop_name|h}-->の商品をご購入いただき、ありがとうございました。<br />
                ただいま、ご注文の確認メールをお送りさせていただきました。<br />
                万一、ご確認メールが届かない場合は、トラブルの可能性もありますので大変お手数ではございますがもう一度お問い合わせいただくか、お電話にてお問い合わせくださいませ。<br />
                今後ともご愛顧賜りますようよろしくお願い申し上げます。<br />
                <br />
                TEL：<!--{$arrInfo.tel01}-->-<!--{$arrInfo.tel02}-->-<!--{$arrInfo.tel03}--> <!--{if $arrInfo.business_hour != ""}-->（受付時間/<!--{$arrInfo.business_hour}-->）<!--{/if}--><br />
                E-mail：<a href="mailto:<!--{$arrInfo.email02|escape:'hex'}-->"><!--{$arrInfo.email02|escape:'hexentity'}--></a>
            </div>

            <div class="btn_area">
                <p class="button02">
                    <button type="button" onclick="location.href='<!--{$smarty.const.TOP_URLPATH}-->'"><span>トップページへ</span></button>
                </p>
            </div>
        </div>
    </div>
</section>