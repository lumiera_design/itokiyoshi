<!--{*
 * ResponsiveL0084
 * Copyright (C) 2013 LOCKON CO.,LTD. All Rights Reserved.
 * http://www.lockon.co.jp/
 *}-->

<section id="undercolumn">
    <div id="undercolumn_shopping">
        <h2 class="title"><!--{$tpl_title|h}--></h2>
        <div class="flow_area">
            <ol>
                <li>STEP1<br /><span class="pc">お届け先指定</span></li>
                <li>STEP2<br /><span class="pc">お支払い方法等選択</span></li>
                <li>STEP3<br /><span class="pc">内容確認</span></li>
                <li class="active">STEP4<br /><span class="pc">完了</span></li>
            </ol>
        </div>

        <div class="inner">
            <!-- ▼その他決済情報を表示する場合は表示 -->
            <!--{if $arrOther.title.value}-->
                <div class="message marB20">
                    <span class="attention">■<!--{$arrOther.title.name}-->情報</span><br />
                    <!--{foreach key=key item=item from=$arrOther}-->
                        <!--{if $key != "title"}-->
                            <!--{if $item.name != ""}-->
                                <!--{$item.name}-->：
                            <!--{/if}-->
                                <!--{$item.value|nl2br}--><br />
                        <!--{/if}-->
                    <!--{/foreach}-->
                </div>
            <!--{/if}-->
            <!-- ▲コンビニ決済の場合には表示 -->

            <div><p class="complete">
                <!--{$arrInfo.shop_name|h}-->の商品をご購入いただき、ありがとうございました。</p>
                <p>ただいま、ご注文の確認メールをお送りさせていただきました。<br />
                万一、ご確認メールが届かない場合は、トラブルの可能性もありますので大変お手数ではございますがもう一度お問い合わせいただくか、お電話にてお問い合わせくださいませ。<br />
                今後ともご愛顧賜りますようよろしくお願い申し上げます。<br />
                <br />
                TEL：<!--{$arrInfo.tel01}-->-<!--{$arrInfo.tel02}-->-<!--{$arrInfo.tel03}--> <!--{if $arrInfo.business_hour != ""}-->（受付時間/<!--{$arrInfo.business_hour}-->）<!--{/if}--><br />
                E-mail：<a href="mailto:<!--{$arrInfo.email02|escape:'hex'}-->"><!--{$arrInfo.email02|escape:'hexentity'}--></a>
            </p></div>

            <div class="btn_area">
                <p class="button02">
                    <button type="button" onclick="location.href='<!--{$smarty.const.TOP_URLPATH}-->'"><span>トップページへ</span></button>
                </p>
            </div>
        </div>
    </div>
</section>

<!-- YDN Conversion Tag -->
<script type="text/javascript" language="javascript">
  /* <![CDATA[ */
  var yahoo_ydn_conv_io = "afVl80gOLDWjUv6b8JX2";
  var yahoo_ydn_conv_label = "I9QM1DC644K64SAC49B619869";
  var yahoo_ydn_conv_transaction_id = "";
  var yahoo_ydn_conv_amount = "0";
  /* ]]> */
</script>
<script type="text/javascript" language="javascript" charset="UTF-8" src="//b90.yahoo.co.jp/conv.js"></script>

<!-- Google Code for &#12471;&#12531;&#12469;&#12524;&#12540;&#12488;&#24067;&#22243;&#12475;&#12483;&#12488;&#36092;&#20837;&#23436;&#20102;&#12479;&#12464; Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 997853247;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "vC0MCK3P2FoQv5Do2wM";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/997853247/?label=vC0MCK3P2FoQv5Do2wM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

<!-- Yahoo Code for your Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var yahoo_conversion_id = 1000173372;
var yahoo_conversion_label = "tBKmCJ_A1FoQj7rK0AM";
var yahoo_conversion_value = 0;
/* ]]> */
</script>
<script type="text/javascript" src="http://i.yimg.jp/images/listing/tool/cv/conversion.js"></script>
<noscript><div style="display:inline;"><img height="1" width="1" style="border-style:none;" alt="" src="http://b91.yahoo.co.jp/pagead/conversion/1000173372/?value=0&label=tBKmCJ_A1FoQj7rK0AM&guid=ON&script=0&disvt=true"/></div></noscript>

<!--{* PostCarrier for EC-CUBE: START *}-->
<!--{ecqubeley_conversion order_id=$ecqubeley_order_id subtotal=$ecqubeley_subtotal}-->
<!--{* PostCarrier for EC-CUBE: END *}-->
<script src="//www.call-ma.com/gearjs/conv.js?wmpcv=23"></script>
<script src="//www.call-ma.com/gearjs/conv.js?wmpcv=24"></script>

