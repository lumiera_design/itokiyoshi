<!--{*
 * ResponsiveL0084
 * Copyright (C) 2013 LOCKON CO.,LTD. All Rights Reserved.
 * http://www.lockon.co.jp/
 *}-->

<section id="undercolumn">
    <div id="undercolumn_shopping">
    <h2 class="title"><!--{$tpl_title|h}--></h2>
    <div class="flow_area">
        <ol>
            <li class="active">STEP1<br /><span class="pc">お届け先指定</span></li>
            <li>STEP2<br /><span class="pc">お支払い方法等選択</span></li>
            <li>STEP3<br /><span class="pc">内容確認</span></li>
            <li>STEP4<br /><span class="pc">完了</span></li>
        </ol>
    </div>
    <div class="inner">
        <div class="information">
            <p>
                下記項目にご入力ください。「<span class="attention">※</span>」印は入力必須項目です。<br />
                <!--{if $smarty.const.USE_MULTIPLE_SHIPPING !== false}-->
                入力後、一番下の「次へ」<br />
                または「複数のお届け先に送る」ボタンをクリックしてください。
                <!--{else}-->
                入力後、一番下の「次へ」ボタンをクリックしてください。
                <!--{/if}-->
            </p>
        </div>

        <form name="form1" id="form1" method="post" action="?">
            <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
            <input type="hidden" name="mode" value="nonmember_confirm" />
            <input type="hidden" name="uniqid" value="<!--{$tpl_uniqid}-->" />
            <div class="table member_table">
                <div class="tbody">
                    <!--{include file="`$smarty.const.TEMPLATE_REALDIR`frontparts/form_personal_input.tpl" flgFields=2 emailMobile=false prefix="order_"}-->
                </div>
            </div>
            <div class="ship_to">
                <p>
                    <!--{assign var=key value="deliv_check"}-->
                    <input type="checkbox" name="<!--{$key}-->" value="1" id="deliv_label" <!--{$arrForm[$key].value|sfGetChecked:1}--> onclick="eccube.toggleDeliveryForm();" />
                    <label for="deliv_label"><span>お届け先を指定</span></label>
                </p>
                <p>※上記に入力された住所と同一の場合は省略可能です。</p>
            </div>
            <div id="ship_to_table"<!--{if $arrForm[$key].value != 1}--> class="dnone"<!--{/if}-->>
                <div class="table member_table">
                    <div class="tbody">
                        <!--{include file="`$smarty.const.TEMPLATE_REALDIR`frontparts/form_personal_input.tpl" flgFields=1 emailMobile=false prefix="shipping_"}-->
                    </div>
                </div>
            </div>

            <div class="btn_area">
                <p class="button02">
                    <button type="submit" name="singular" id="singular"><span>次へ</span></button>
                </p>
            </div>
            <!--{if $smarty.const.USE_MULTIPLE_SHIPPING !== false}-->
            <div class="multiple_deliv_area">
                <p>この商品を複数のお届け先に送りますか？</p>
                <p><a class="link01" href="javascript:;" onclick="javascript:eccube.setModeAndSubmit('multiple', '', ''); return false;">複数のお届け先に送る</a></p>
            </div>
            <!--{/if}-->
        </form>
    </div>
</div>
</section>
