<!--{*
 * ResponsiveL0084
 * Copyright (C) 2013 LOCKON CO.,LTD. All Rights Reserved.
 * http://www.lockon.co.jp/
 *}-->

<script type="text/javascript">//<![CDATA[
    var sent = false;

    function fnCheckSubmit() {
        if (sent) {
            alert("只今、処理中です。しばらくお待ち下さい。");
            return false;
        }
        sent = true;
        return true;
    }

    $(document).ready(function() {
        $('a.expansion').colorbox({maxWidth:'98%',maxHeight:'98%',initialWidth:'50%',initialHeight:'50%',speed:200});
    });
//]]></script>

<section id="undercolumn">
    <div id="undercolumn_shopping">
        <h2 class="title"><!--{$tpl_title|h}--></h2>
        <div class="flow_area">
            <ol>
                <li>STEP1<br />
                    <span class="pc">お届け先指定</span></li>
                <li>STEP2<br />
                    <span class="pc">お支払い方法等選択</span></li>
                <li class="active">STEP3<br />
                    <span class="pc">内容確認</span></li>
                <li>STEP4<br />
                    <span class="pc">完了</span></li>
            </ol>
        </div>

        <div class="inner">
            <div class="information">
                <p>下記ご注文内容でよろしければ、「<!--{if $use_module}-->次へ<!--{else}-->完了ページへ<!--{/if}-->」ボタンをクリックしてください。</p>
            </div>
            <form name="form1" id="form1" method="post" action="?">
            <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
            <input type="hidden" name="mode" value="confirm" />
            <input type="hidden" name="uniqid" value="<!--{$tpl_uniqid}-->" />

            <div class="formBox"> 
                <div class="cartinarea cf"> 
                    
                    <!--▼商品 -->
                    <div class="table" id="confirm_table">
                        <div class="thead">
                            <ol>
                                <li>商品内容</li>
                                <li>数量</li>
                                <li>小計</li>
                            </ol>
                        </div>
                        <div class="tbody">
                            <!--{foreach from=$arrCartItems item=item}-->
                            <div class="tr">
                                <div class="item">
                                    <div class="photo">
                                        <!--{if $item.productsClass.main_image|strlen >= 1}-->
                                        <a href="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$item.productsClass.main_image|sfNoImageMainList|h}-->" class="expansion" target="_blank"><img src="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$item.productsClass.main_image|sfNoImageMainList|h}-->" alt="<!--{$item.productsClass.name|h}-->" /></a>
                                        <!--{else}-->
                                        <img src="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$item.productsClass.main_image|sfNoImageMainList|h}-->" alt="<!--{$item.productsClass.name|h}-->" />
                                        <!--{/if}-->
                                    </div>
                                    <p><em><!--→商品名--><a href="<!--{$smarty.const.P_DETAIL_URLPATH}--><!--{$item.productsClass.product_id|u}-->"><!--{$item.productsClass.name|h}--></a><!--←商品名--></em></p>
                                    <!--{if $item.productsClass.classcategory_name1 != ""}-->
                                    <p class="small"><!--{$item.productsClass.class_name1|h}-->：<!--{$item.productsClass.classcategory_name1|h}--></p>
                                    <!--{/if}-->
                                    <!--{if $item.productsClass.classcategory_name2 != ""}-->
                                    <p class="small"><!--{$item.productsClass.class_name2|h}-->：<!--{$item.productsClass.classcategory_name2|h}--></p>
                                    <!--{/if}-->
                                    <p class="small"><!--→金額-->価格:<!--{$item.price_inctax|number_format}-->円<!--←金額--></p>
                                </div>
                                <div class="account"><!--{$item.quantity|number_format}--></div>
                                <div class="price"><!--{$item.total_inctax|number_format}-->円</div>
                            </div>
                            <!--{/foreach}-->
                        </div>
                    </div>
                    <!--▲商品 --> 
                </div>

                <div class="total_area">
                    <dl>
                        <dt>小計：</dt>
                        <dd><!--{$tpl_total_inctax[$cartKey]|number_format}-->円</dd>
                    </dl>
                    <!--{if $tpl_login == 1 && $smarty.const.USE_POINT !== false && $arrForm.use_point > 0}-->
                    <dl>
                        <dt>値引き（ポイントご使用時）：</dt>
                        <dd><!--{assign var=discount value=`$arrForm.use_point*$smarty.const.POINT_VALUE`}-->-<!--{$discount|number_format|default:0}-->円</dd>
                    </dl>
                    <!--{/if}-->
                    <dl>
                        <dt>送料：</dt>
                        <dd><!--{$arrForm.deliv_fee|number_format}-->円</dd>
                    </dl>
                    <dl>
                        <dt>手数料：</dt>
                        <dd><!--{$arrForm.charge|number_format}-->円</dd>
                    </dl>
                    <dl>
                        <dt>合計：</dt>
                        <dd class="price"><!--{$arrForm.payment_total|number_format}-->円</dd>
                    </dl>
                    <!--{if $tpl_login == 1 && $smarty.const.USE_POINT !== false}-->
                    <dl>
                        <dt>今回加算ポイント：</dt>
                        <dd><!--{$arrForm.add_point|number_format|default:0}-->pt</dd>
                    </dl>
                    <!--{/if}-->
                </div>

                <!--{* ログイン済みの会員のみ *}-->
                <!--{if $tpl_login == 1 && $smarty.const.USE_POINT !== false}-->
                <div class="point_confirm table">
                    <div class="tbody">
                        <dl class="tr">
                            <dt>ご注文前のポイント</dt>
                            <dd><!--{$tpl_user_point|number_format|default:0}-->pt</dd>
                        </dl>
                        <dl class="tr">
                            <dt>ご使用ポイント</dt>
                            <dd>-<!--{$arrForm.use_point|number_format|default:0}-->pt</dd>
                        </dl>
                        <!--{if $arrForm.birth_point > 0}-->
                        <dl class="tr">
                            <dt>お誕生月ポイント</dt>
                            <dd>+<!--{$arrForm.birth_point|number_format|default:0}-->pt</dd>
                        </dl>
                        <!--{/if}-->
                        <dl class="tr">
                            <dt>今回加算予定のポイント</dt>
                            <dd>+<!--{$arrForm.add_point|number_format|default:0}-->pt</dd>
                        </dl>
                        <dl class="tr">
                            <!--{assign var=total_point value=`$tpl_user_point-$arrForm.use_point+$arrForm.add_point`}-->
                            <dt>加算後のポイント</dt>
                            <dd><!--{$total_point|number_format}-->pt</dd>
                        </dl>
                    </div>
                </div>
                <!--{/if}-->
            </div><!-- /.formBox -->



            <!--{* ▼注文者 *}-->
            <div class="formBox">
                <h3 class="heading02">ご注文者</h3>
                <div class="table" id="payment_confirm">
                    <div class="tbody">
                        <dl class="tr">
                            <dt>お名前</dt>
                            <dd><!--{$arrForm.order_name01|h}--> <!--{$arrForm.order_name02|h}--></dd>
                        </dl>
                        <!--{if false}-->
                        <dl class="tr" style="display: none;">
                            <dt>お名前(フリガナ)</dt>
                            <dd><!--{$arrForm.order_kana01|h}--> <!--{$arrForm.order_kana02|h}--></dd>
                        </dl>
                        <!--{/if}-->
                        <dl class="tr" style="display: none;">
                            <dt>会社名</dt>
                            <dd><!--{$arrForm.order_company_name|h}--></dd>
                        </dl>
                        <!--{if $smarty.const.FORM_COUNTRY_ENABLE}-->
                        <dl class="tr">
                            <dt>国</dt>
                            <dd><!--{$arrCountry[$arrForm.order_country_id]|h}--></dd>
                        </dl>
                        <dl class="tr">
                            <dt>ZIPCODE</dt>
                            <dd><!--{$arrForm.order_zipcode|h}--></dd>
                        </dl>
                        <!--{/if}-->
                        <dl class="tr">
                            <dt>郵便番号</dt>
                            <dd class="zipcode"><!--{if $arrForm.order_zip01|strlen > 0 || $arrForm.order_zip02|strlen > 0}--><!--{$arrForm.order_zip01|h}-->-<!--{$arrForm.order_zip02|h}--><!--{/if}--></dd>
                        </dl>
                        <dl class="tr">
                            <dt>住所</dt>
                            <dd><!--{$arrPref[$arrForm.order_pref]}--><!--{$arrForm.order_addr01|h}--><!--{$arrForm.order_addr02|h}--></dd>
                        </dl>
                        <dl class="tr">
                            <dt>電話番号</dt>
                            <dd><!--{$arrForm.order_tel01}-->-<!--{$arrForm.order_tel02}-->-<!--{$arrForm.order_tel03}--></dd>
                        </dl>
                        <dl class="tr" style="display: none;">
                            <dt>FAX番号</dt>
                            <dd>
                                <!--{if $arrForm.order_fax01|strlen > 0}-->
                                    <!--{$arrForm.order_fax01}-->-<!--{$arrForm.order_fax02}-->-<!--{$arrForm.order_fax03}-->
                                <!--{/if}-->
                            </dd>
                        </dl>
                        <dl class="tr">
                            <dt>メールアドレス</dt>
                            <dd><!--{$arrForm.order_email|h}--></dd>
                        </dl>
                         <!--{if false}-->
                        <dl class="tr" style="display: none;">
                            <dt>性別</dt>
                            <dd><!--{$arrSex[$arrForm.order_sex]|h}--></dd>
                        </dl>
                        <!--{/if}-->
                        <dl class="tr" style="display: none;">
                            <dt>職業</dt>
                            <dd><!--{$arrJob[$arrForm.order_job]|default:'(未登録)'|h}--></dd>
                        </dl>
                        <dl class="tr" style="display: none;">
                            <dt>生年月日</dt>
                            <dd>
                                <!--{$arrForm.order_birth|regex_replace:"/ .+/":""|regex_replace:"/-/":"/"|default:'(未登録)'|h}-->
                            </dd>
                        </dl>
                    </div>
                </div>
            </div>



            <!--{* ▼お届け先 *}-->
            <!--{foreach item=shippingItem from=$arrShipping name=shippingItem}-->
            <div class="formBox">
                <h3 class="heading02">お届け先<!--{if $is_multiple}--><!--{$smarty.foreach.shippingItem.iteration}--><!--{/if}--></h3>
                <!--{if $is_multiple}-->
                <div class="cartinarea cf"> 
                    <!--▼商品 -->
                    <div class="table" id="item_confirm">
                        <div class="thead">
                            <ol>
                                <li>商品内容</li>
                                <li>数量</li>
                            </ol>
                        </div>
                        <div class="tbody">
                            <!--{foreach item=item from=$shippingItem.shipment_item}-->
                            <div class="tr">
                                <div class="item">
                                    <div class="photo">
                                        <!--{if $item.productsClass.main_image|strlen >= 1}-->
                                        <a href="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$item.productsClass.main_image|sfNoImageMainList|h}-->" class="expansion" target="_blank"><img src="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$item.productsClass.main_image|sfNoImageMainList|h}-->" alt="<!--{$item.productsClass.name|h}-->" /></a>
                                        <!--{else}-->
                                        <img src="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$item.productsClass.main_image|sfNoImageMainList|h}-->" alt="<!--{$item.productsClass.name|h}-->" />
                                        <!--{/if}-->
                                    </div>
                                    <p><em><!--→商品名--><a href="<!--{$smarty.const.P_DETAIL_URLPATH}--><!--{$item.productsClass.product_id|u}-->" rel="external"><!--{$item.productsClass.name|h}--></a><!--←商品名--></em></p>
                                    <!--{if $item.productsClass.classcategory_name1 != ""}-->
                                    <p class="small"><!--{$item.productsClass.class_name1}-->：<!--{$item.productsClass.classcategory_name1|h}--></p>  
                                    <!--{/if}-->
                                    <!--{if $item.productsClass.classcategory_name2 != ""}-->
                                    <p class="small"><!--{$item.productsClass.class_name2}-->：<!--{$item.productsClass.classcategory_name2|h}--></p>  
                                    <!--{/if}-->
                                </div>
                                <div class="account"><!--{$item.quantity}--> </div>
                            </div>
                            <!--{/foreach}-->
                        </div>
                    </div>
                </div>
                <!--{/if}-->

                <div class="table" id="deliv_confirm">
                    <div class="tbody">
                        <dl class="tr">
                            <dt>お名前</dt>
                            <dd><!--{$shippingItem.shipping_name01|h}--> <!--{$shippingItem.shipping_name02|h}--></dd>
                        </dl>
                         <!--{if false}-->
                        <dl class="tr">
                            <dt>お名前(フリガナ)</dt>
                            <dd><!--{$shippingItem.shipping_kana01|h}--> <!--{$shippingItem.shipping_kana02|h}--></dd>
                        </dl>
 <!--{/if}-->

                        <!--{if false}-->
                        <dl class="tr">
                            <dt>会社名</dt>
                            <dd><!--{$shippingItem.shipping_company_name|h}--></dd>
                        </dl>
                        <!--{/if}-->

                        <!--{if $smarty.const.FORM_COUNTRY_ENABLE}-->
                        <dl class="tr">
                            <dt>国</dt>
                            <dd><!--{$arrCountry[$shippingItem.shipping_country_id]|h}--></dd>
                        </dl>
                        <dl class="tr">
                            <dt>ZIPCODE</dt>
                            <dd><!--{$shippingItem.shipping_zipcode|h}--></dd>
                        </dl>
                        <!--{/if}-->
                        <dl class="tr">
                            <dt>郵便番号</dt>
                            <dd class="zipcode">
                                <!--{if $shippingItem.shipping_zip01|strlen > 0 || $shippingItem.shipping_zip02|strlen > 0}-->
                                    <!--{$shippingItem.shipping_zip01|h}-->-<!--{$shippingItem.shipping_zip02|h}-->
                                <!--{/if}-->
                            </dd>
                        </dl>
                        <dl class="tr">
                            <dt>住所</dt>
                            <dd><!--{$arrPref[$shippingItem.shipping_pref]}--><!--{$shippingItem.shipping_addr01|h}--><!--{$shippingItem.shipping_addr02|h}--></dd>
                        </dl>
                        <dl class="tr">
                            <dt>電話番号</dt>
                            <dd><!--{$shippingItem.shipping_tel01}-->-<!--{$shippingItem.shipping_tel02}-->-<!--{$shippingItem.shipping_tel03}--></dd>
                        </dl>

                        <!--{if false}-->
                        <dl class="tr">
                            <dt>FAX</dt>
                            <dd>
                                <!--{if $shippingItem.shipping_fax01|strlen > 0}-->
                                    <!--{$shippingItem.shipping_fax01}-->-<!--{$shippingItem.shipping_fax02}-->-<!--{$shippingItem.shipping_fax03}-->
                                <!--{/if}-->
                            </dd>
                        </dl>
                        <!--{/if}-->
                        
                        <!--{if $cartKey != $smarty.const.PRODUCT_TYPE_DOWNLOAD}-->
                        <dl class="tr">
                            <dt>お届け日</dt>
                            <dd><!--{$shippingItem.shipping_date|default:"指定なし"|h}--></dd>
                        </dl>
                        <dl class="tr" style="display: none;">
                            <dt>お届け時間</dt>
                            <dd><!--{$shippingItem.shipping_time|default:"指定なし"|h}--></dd>
                        </dl>
                        <!--{/if}-->
                    </div>
                </div>
            </div><!-- /.formBox --> 
            <!--{/foreach}-->
            <!--{* ▲お届け先 *}-->

            <div class="formBox">
                <h3 class="heading02">配送方法、お支払方法など</h3>
                <div class="table" id="payment_confirm">
                    <div class="tbody">
                        <dl class="tr">
                            <dt>配送方法</dt>
                            <dd><!--{$arrDeliv[$arrForm.deliv_id]|h}--></dd>
                        </dl>
                        <dl class="tr">
                            <dt>お支払方法</dt>
                            <dd><!--{$arrForm.payment_method|h}--></dd>
                        </dl>
                        <dl class="tr contact">
                            <dt>その他お問い合わせ</dt>
                            <dd><!--{$arrForm.message|h|nl2br}--></dd>
                        </dl>
                    </div>
                </div>
            </div><!-- /.formBox --> 

            <ul class="btn_area">
                <!--{if $use_module}-->
                <li class="button02"><button type="submit" onclick="return fnCheckSubmit();" name="next" id="next"><span>次へ</span></button></li>
                <!--{else}-->
                <li class="button02"><button type="submit" onclick="return fnCheckSubmit();" name="next" id="next"><span>完了ページへ</span></button></li>
                <!--{/if}-->
                <li class="button03"><button type="button" onclick="location.href='./payment.php'"><span>戻る</span></button></li>                        
            </ul>
        </form>
        </div>
    </div>
</section>
