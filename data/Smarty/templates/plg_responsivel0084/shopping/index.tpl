<!--{*
 * ResponsiveL0084
 * Copyright (C) 2013 LOCKON CO.,LTD. All Rights Reserved.
 * http://www.lockon.co.jp/
 *}-->

<section id="undercolumn">
    <h2 class="title"><!--{$tpl_title|h}--></h2>
    <div class="inner">
        <h3 class="heading02">会員登録がお済みのお客様</h3>
        <form name="member_form" id="member_form" method="post" action="?" onsubmit="return eccube.checkLoginFormInputted('member_form')">
            <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
            <input type="hidden" name="mode" value="login" />
            <div class="login_area">
                <div class="loginareaBox table">
                    <ul class="tbody">
                    <li class="tr">
                        <!--{assign var=key value="login_email"}-->
                        <div class="th">メールアドレス</div>
                        <div>
                            <input<!--{if $arrErr[$key]}--> class="input_email mailtextBox error"<!--{else}--> class="input_email mailtextBox"<!--{/if}--> type="email" name="<!--{$key}-->" value="<!--{$tpl_login_email|h}-->" maxlength="<!--{$arrForm[$key].length}-->" placeholder="メールアドレス" />
                            <!--{if strlen($arrErr[$key]) >= 1}--><div class="attention"><!--{$arrErr[$key]}--></div><!--{/if}-->
                            <!--{assign var=key value="login_memory"}-->
                            <span class="pc"><input type="checkbox" name="<!--{$key}-->" value="1"<!--{$tpl_login_memory|sfGetChecked:1}--> id="login_memory" /><label for="login_memory">記憶</label></span>
                        </div>
                    </li>
                    <li class="tr">
                        <!--{assign var=key value="login_pass"}-->
                        <div class="th">パスワード</div>
                        <div><input<!--{if $arrErr[$key]}--> class="passtextBox error"<!--{else}--> class="passtextBox"<!--{/if}--> type="password" name="<!--{$key}-->" maxlength="<!--{$arrForm[$key].length}-->" placeholder="パスワード" /></div>
                        <!--{if strlen($arrErr[$key]) >= 1}--><div class="attention"><!--{$arrErr[$key]}--></div><!--{/if}-->
                    </li>
                    </ul>
                </div>
                <div class="inputbox">
                    <div class="btn_area">
                        <p class="button02">
                            <button type="submit" name="log" id="log"><span>ログイン</span></button>
                        </p>
                        <p class="button03">
                            <button type="button" onclick="win01('<!--{$smarty.const.HTTPS_URL|sfTrimURL}-->/forgot/<!--{$smarty.const.DIR_INDEX_PATH}-->','forget','600','520'); return false;"><span>パスワードを忘れた方</span></button>
                        </p>
                    </div>
                </div>
            </div>
        </form>

        <h3 class="heading02">まだ会員登録されていないお客様</h3>
        <p class="inputtext">
            会員登録をすると便利なMyページをご利用いただけます。<br />
            会員登録をせずに購入手続きをされたい方は、下記よりお進みください。
        </p>
        <form name="member_form2" id="member_form2" method="post" action="?">
        <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
        <input type="hidden" name="mode" value="nonmember" />
        <div class="inputbox">
            <ul class="btn_area">
                <li class="button05 bt_left"><button type="button" onclick="location.href='<!--{$smarty.const.ROOT_URLPATH}-->entry/kiyaku.php'"><span>会員登録ページヘ</span></button></li>
                <li class="button03 bt_right"><button type="submit" name="buystep" id="buystep"><span>ご購入手続きへ</span></button></li>
            </ul>
        </div>
        </form>
    </div>
</section>
