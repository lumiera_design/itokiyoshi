<!--{*
 * ResponsiveL0084
 * Copyright (C) 2013 LOCKON CO.,LTD. All Rights Reserved.
 * http://www.lockon.co.jp/
 *}-->

<!-- start of landing page cart -->
<script type="text/javascript">
// if (document.referrer.indexOf('itokiyoshi.com/air') > 0) {
//     window.location = '/shopping/'
// }

    // var item = $('a[rel="external"]').attr('href');
    // if (item.indexOf('product_id=54') > 0 || item.indexOf('product_id=56') > 0 || item.indexOf('product_id=58') > 0 || item.indexOf('product_id=60') > 0 || item.indexOf('product_id=61') > 0) {
    //     $('form[name="form1"]').submit()
    // };
</script>
<!-- end of landing page cart -->

<script type="text/javascript">//<![CDATA[
    $(document).ready(function() {
        $('a.expansion').colorbox({maxWidth:'98%',maxHeight:'98%',initialWidth:'50%',initialHeight:'50%',speed:200});
    });
//]]></script>
<link rel="stylesheet" href="<!--{$TPL_URLPATH}-->css/cart.css">
<section id="undercolumn_cart">
<!--<div class="cart-header"><p id="thanks">ご注文ありがとうございます！</p><p>ご注文内容をご確認ください。</p></div>-->

  <h2 class="title"><!--{$tpl_title|h}--></h2>

    <!--{if $smarty.const.USE_POINT !== false || count($cartKeys) > 1 || strlen($tpl_error) != 0 || strlen($tpl_message) != 0}-->
    <div class="information">
        <!--{* ▼ポイント案内 *}-->
        <!--{if $smarty.const.USE_POINT !== false}-->
        <div class="point_announce">
            <!--{if $tpl_login}-->
            <p><span class="user_name"><!--{$tpl_name|h}--> 様</span>&nbsp;<span class="point">所持ポイント： <!--{$tpl_user_point|number_format|default:0|h}--> pt</span></p>
            <!--{else}-->
            <p>ポイント制度をご利用になられる場合は、会員登録後ログインしてくださいますようお願い致します。</p>
            <!--{/if}-->
        </div>
        <!--{/if}-->
        <!--{* ▲ポイント案内 *}-->

        <!--{if count($cartKeys) > 1}-->
            <p class="attention"><!--{foreach from=$cartKeys item=key name=cartKey}--><!--{$arrProductType[$key]|h}--><!--{if !$smarty.foreach.cartKey.last}-->、<!--{/if}--><!--{/foreach}-->は同時購入できません。<br />お手数ですが、個別に購入手続きをお願い致します。</p>
        <!--{/if}-->

        <!--{if strlen($tpl_error) != 0}-->
        <p class="attention"><!--{$tpl_error|h}--></p>
        <!--{/if}-->

        <!--{if strlen($tpl_message) != 0}-->
        <p class="attention"><!--{$tpl_message|h|nl2br}--></p>
        <!--{/if}-->
    </div>
    <!--{/if}-->

    <!--{if count($cartItems) > 0}-->
    <div class="inner">
        <!--{foreach from=$cartKeys item=key}-->
        <!--{if count($cartKeys) > 1}-->
        <h3 class="heading02"><!--{$arrProductType[$key]|h}--></h3>
        <p class="fb">
            <!--{assign var=purchasing_goods_name value=$arrProductType[$key]}-->
            <!--{$purchasing_goods_name|h}-->の合計金額は「<span class="price"><!--{$tpl_total_inctax[$key]|number_format|h}-->円</span>」です。<br />
            <span class="attention">
                <!--{if $key != $smarty.const.PRODUCT_TYPE_DOWNLOAD}-->
                    <!--{if $arrInfo.free_rule > 0}-->
                        <!--{if !$arrData[$key].is_deliv_free}-->
                        あと「<span class="price"><!--{$tpl_deliv_free[$key]|number_format|h}-->円</span>」で送料無料です！！
                        <!--{else}-->
                        現在、「送料無料」です！！
                        <!--{/if}-->
                    <!--{/if}-->
                <!--{/if}-->
            </span>
        </p>
        <!--{else}-->
        <p class="fb">
            <!--{assign var=purchasing_goods_name value="カゴの中の商品"}-->
            <!--{$purchasing_goods_name|h}-->の合計金額は「<span class="price"><!--{$tpl_total_inctax[$key]|number_format|h}-->円</span>」です。<br />
            <span class="attention">
                <!--{if $key != $smarty.const.PRODUCT_TYPE_DOWNLOAD}-->
                    <!--{if $arrInfo.free_rule > 0}-->
                        <!--{if !$arrData[$key].is_deliv_free}-->
                        あと「<span class="price"><!--{$tpl_deliv_free[$key]|number_format|h}-->円</span>」で送料無料です！！
                        <!--{else}-->
                        現在、「送料無料」です！！
                        <!--{/if}-->
                    <!--{/if}-->
                <!--{/if}-->
            </span>
        </p>
        <!--{/if}-->

        <script type='text/javascript'>
        window.onAmazonLoginReady = function() { amazon.Login.setClientId('<!--{$smarty.const.AMAZON_CLIENT_ID}-->'); };
        </script>
        <script type='text/javascript' src='<!--{$smarty.const.AMAZON_WIDGET_URL}-->'></script>
        <script type='text/javascript'>

        function amazon_button() {
            var authRequest;

                OffAmazonPayments.Button("AmazonPayButton<!--{$key}-->", "<!--{$smarty.const.AMAZON_SELLER_ID}-->", {
                    type: "PwA",
                    size: "<!--{$smarty.const.PC_AMAZON_BUTTON_SIZE_CART}-->",
                    color: "<!--{$smarty.const.PC_AMAZON_BUTTON_COLOR_CART}-->",
                    authorization: function() {
                        loginOptions = {scope: "profile payments:widget payments:shipping_address", popup: true};
                        authRequest = amazon.Login.authorize (loginOptions, "<!--{$smarty.const.HTTPS_URL}-->shopping/plg_AmazonPayments_redirect.php?cartKey=<!--{$key}-->");
                    },
                    onError: function(error) {
                        alert('通信中にエラーが発生しました。カート画面に移動します。');
                        location.href = '<!--{$smarty.const.CART_URLPATH}-->';
                    }
                });
        }

        </script>
        <form name="form<!--{$key|h}-->" id="form<!--{$key|h}-->" method="post" action="?">
            <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME|h}-->" value="<!--{$transactionid|h}-->" />
            <input type="hidden" name="mode" value="confirm" />
            <input type="hidden" name="cart_no" value="" />
            <input type="hidden" name="cartKey" value="<!--{$key|h}-->" />
            <input type="hidden" name="category_id" value="<!--{$tpl_category_id|h}-->" />
            <input type="hidden" name="product_id" value="<!--{$tpl_product_id|h}-->" />
            <div class="formBox">
                <div class="cartinarea cf">
                    <div class="table">
                        <div class="thead">
                            <ol>
                                <li>削除</li>
                                <li>商品内容</li>
                                <li>数量</li>
                                <li>小計</li>
                            </ol>
                        </div>
                        <div class="tbody">
                            <!--{foreach from=$cartItems[$key] item=item}-->
                            <div class="tr">
                                <div class="bt_delete"><a href="?" onclick="eccube.fnFormModeSubmit('form<!--{$key|h}-->', 'delete', 'cart_no', '<!--{$item.cart_no|h}-->'); return false;">削除</a></div>
                                <div class="item">
                                    <div class="photo">
                                        <!--{if $item.productsClass.main_image|strlen >= 1}-->
                                        <a class="expansion" target="_blank" href="<!--{$smarty.const.IMAGE_SAVE_URLPATH|h}--><!--{$item.productsClass.main_image|sfNoImageMainList|h}-->"><img src="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$item.productsClass.main_list_image|sfNoImageMainList|h}-->" alt="<!--{$item.productsClass.name|h}-->" /></a>
                                        <!--{else}-->
                                        <a class="expansion" target="_blank" href="<!--{$smarty.const.IMAGE_SAVE_URLPATH|h}--><!--{$item.productsClass.main_image|sfNoImageMainList|h}-->" alt="<!--{$item.productsClass.name|h}-->">
                                        <!--{/if}-->
                                    </div>
                                    <p><em><!--→商品名--><a href="<!--{$smarty.const.P_DETAIL_URLPATH}--><!--{$item.productsClass.product_id|u}-->" rel="external"><!--{$item.productsClass.name|h}--></a><!--←商品名--></em></p>
                                    <!--{if $item.productsClass.classcategory_name1 != ""}-->
                                    <p class="small"><!--{$item.productsClass.class_name1|h}-->：<!--{$item.productsClass.classcategory_name1|h}--></p>
                                    <!--{/if}-->
                                    <!--{if $item.productsClass.classcategory_name2 != ""}-->
                                    <p class="small"><!--{$item.productsClass.class_name2|h}-->：<!--{$item.productsClass.classcategory_name2|h}--></p>
                                    <!--{/if}-->
                                    <p class="small">価格:<!--{$item.price_inctax|number_format|h}-->円</p>
                                </div>
                                <div class="account">
                                    <!--{$item.quantity|h}-->
                                    <span class="amount">
                                        <img onclick="eccube.fnFormModeSubmit('form<!--{$key|h}-->', 'down','cart_no','<!--{$item.cart_no|h}-->'); return false" alt="-" src="<!--{$TPL_URLPATH}-->img/btn_minus.png">
                                        <img onclick="eccube.fnFormModeSubmit('form<!--{$key|h}-->', 'up','cart_no','<!--{$item.cart_no|h}-->'); return false" alt="＋" src="<!--{$TPL_URLPATH}-->img/btn_plus.png">
                                    </span>
                                </div>
                                <div class="price"><!--{$item.total_inctax|number_format|h}-->円</div>
                            </div>
                            <!--{/foreach}-->
                        </div>
                    </div>
                </div>

                <div class="total_area">
                    <dl>
                        <dt>合計：</dt>
                        <dd class="price"><!--{$arrData[$key].total-$arrData[$key].deliv_fee|number_format|h}-->円</dd>
                    </dl>
                    <!--{if $smarty.const.USE_POINT !== false}-->
                    <!--{if $arrData[$key].birth_point > 0}-->
                    <dl>
                        <dt>お誕生月ポイント：</dt>
                        <dd><!--{$arrData[$key].birth_point|number_format|h}-->pt</dd>
                    </dl>
                    <!--{/if}-->
                    <dl>
                        <dt>今回加算ポイント：</dt>
                        <dd><!--{$arrData[$key].add_point|number_format|h}-->pt</dd>
                    </dl>
                    <!--{/if}-->
                </div>
            </div>
            
            
            <!--▼ボタンエリア-->
                
    
            
            <!--{if strlen($tpl_error) == 0}-->
            <!--{if $tpl_prev_url != ""}-->
            
        <div id="checkout_area_outer">    
            <div class="checkout_area">
                 <ul class="btn_area">
                 <li class="button05">
                <input type="hidden" name="cartKey" value="1" />
                <button type="submit" id="confirm" name="confirm"><span>ご購入手続きへ</span></button>
                </li>
                <li class="checkout_comment">このサイトでお支払いされる場合はこちら</li>
            	</ul>
    </div>
    
     <div id="divider">
                    <hr class="left-hr"><div id="or">または</div><hr class="rignt-hr cf">
    </div>
    <div class="checkout_area">   
                <ul class="btn_area">
				<li class="amazon_checkout_cart">
            	<div id="AmazonPayButton<!--{$key}-->">
					<script type="text/javascript">
                        amazon_button();
                    </script>
           		</div>
                </li>
                <li class="checkout_comment">Amazon.co.jpにご登録の住所・クレジットカード情報を利用してご注文いただけます。</li>
                </ul>
     </div>
</div>
<div class="btn_area">
                <p class="button03">
                    <button type="button" onclick="location.href='<!--{$tpl_prev_url|h}-->'"><span>お買い物を続ける</span></button>
                </p>
            </div>
            <!--{else}--><!--直接カートページに入った時はトップページに戻るby lumiera 20170129-->
            <div id="checkout_area_outer">
           <div class="checkout_area">
                 <ul class="btn_area">
                 <li class="button05">
                <input type="hidden" name="cartKey" value="1" />
                <button type="submit" id="confirm" name="confirm"><span>ご購入手続きへ</span></button>
                </li>
                <li class="checkout_comment">このサイトでお支払いされる場合はこちら</li>
            	</ul>
    </div>
    
    	 <div id="divider">
                    <hr class="left-hr"><div id="or">または</div><hr class="rignt-hr cf">
   		 </div>
    <div class="checkout_area">   
                <ul class="btn_area">
				<li class="amazon_checkout_cart">
            	<div id="AmazonPayButton<!--{$key}-->">
					<script type="text/javascript">
                        amazon_button();
                    </script>
           		</div>
                </li>
                <li class="checkout_comment">Amazon.co.jpにご登録の住所・クレジットカード情報を利用してご注文いただけます。</li>
                </ul>
     </div>
</div>

<div class="btn_area">
                <p class="button03">
                    <button type="button" onclick="location.href='/'"><span>お買い物を続ける</span></button>
                </p>
            </div>
            <!--{/if}-->
           
            
            
            
            <!--{elseif $tpl_prev_url != ""}-->
            <div class="btn_area">
                <p class="button03">
                    <button type="button" onclick="location.href='<!--{$tpl_prev_url|h}-->'"><span>お買い物を続ける</span></button>
                </p>
            </div>
            
            <!--{/if}-->
        </form>
        <!--{/foreach}-->
<!--▲ボタンエリア-->







<div id="additionalCart" style="display: none;">
    <div style="text-align: center; font-size: 28px; text-decoration: underline; margin: 30px 0 0; font-weight: bold; color: #555;">追加で商品を購入する</div>
    <div style="text-align: center; margin: 20px 0 40px; font-size: 16px; font-weight: bold; color: #777;">
        エアーベーシックは今月だけの限定商品となり<span style="color: #ff4500; text-decoration: underline;">数に限り</span>がございます。<br>
        誠に恐れ入りますがお一人様<span style="color: #ff4500; text-decoration: underline;">５点</span>までの購入とさせていただきます。
    </div>

    <div class="formBox">
        <div class="cartinarea cf">
            <div class="table">
                <div class="thead">
                    <ol>
                        <li>商品内容</li>
                        <li>数量</li>
                        <li>小計</li>
                    </ol>
                </div>
                <div class="tbody">
                    <div class="tr">
                        <div class="item">
                            <div class="photo">
                                <a class="expansion cboxElement" target="_blank" href="/upload/save_image/airbasic1.png"><img src="/upload/save_image/airbasic1.png" alt="軽くて暖かい羽毛布団エアーベーシック シングルロングサイズ"></a>
                            </div>
                            <p><em><a href="/products/detail.php?product_id=54" rel="external">軽くて暖かい羽毛布団エアーベーシック シングルロングサイズ</a></em></p>
                            <!-- <p class="small">価格:19,800円</p> -->
                        </div>
                        <div class="account">
                            <span class="amount">
                                <img onclick="document.form10.submit(); return false;" alt="＋" src="/user_data/packages/plg_responsivel0084/img/btn_plus.png">
                            </span>
                        </div>
                        <div class="price">29,800円</div>
                    </div>
                    <div class="tr">
                        <div class="item">
                            <div class="photo">
                                <a class="expansion cboxElement" target="_blank" href="/upload/save_image/2.png"><img src="/upload/save_image/2.png" alt="軽くて暖かい羽毛布団エアーベーシック セミダブルサイズ"></a>
                            </div>
                            <p><em><a href="/products/detail.php?product_id=56" rel="external">軽くて暖かい羽毛布団エアーベーシック セミダブルサイズ</a></em></p>
                        </div>
                        <div class="account">
                            <span class="amount">
                                <img onclick="document.form20.submit(); return false;" alt="＋" src="/user_data/packages/plg_responsivel0084/img/btn_plus.png">
                            </span>
                        </div>
                        <div class="price">38,800円</div>
                    </div>
                    <div class="tr">
                        <div class="item">
                            <div class="photo">
                                <a class="expansion cboxElement" target="_blank" href="/upload/save_image/3.png"><img src="/upload/save_image/3.png" alt="軽くて暖かい羽毛布団エアーベーシック ダブルサイズ"></a>
                            </div>
                            <p><em><a href="/products/detail.php?product_id=58" rel="external">軽くて暖かい羽毛布団エアーベーシック ダブルサイズ</a></em></p>
                        </div>
                        <div class="account">
                            <span class="amount">
                                <img onclick="document.form30.submit(); return false;" alt="＋" src="/user_data/packages/plg_responsivel0084/img/btn_plus.png">
                            </span>
                        </div>
                        <div class="price">44,800円</div>
                    </div>
                    <div class="tr">
                        <div class="item">
                            <div class="photo">
                                <a class="expansion cboxElement" target="_blank" href="/upload/save_image/4.png"><img src="/upload/save_image/4.png" alt="軽くて暖かい羽毛布団エアーベーシック クイーンサイズ"></a>
                            </div>
                            <p><em><a href="/products/detail.php?product_id=60" rel="external">軽くて暖かい羽毛布団エアーベーシック クイーンサイズ</a></em></p>
                        </div>
                        <div class="account">
                            <span class="amount">
                                <img onclick="document.form40.submit(); return false;" alt="＋" src="/user_data/packages/plg_responsivel0084/img/btn_plus.png">
                            </span>
                        </div>
                        <div class="price">56,800円</div>
                    </div>
                    <div class="tr">
                        <div class="item">
                            <div class="photo">
                                <a class="expansion cboxElement" target="_blank" href="/upload/save_image/5.png"><img src="/upload/save_image/5.png" alt="軽くて暖かい羽毛布団エアーベーシック キングサイズ"></a>
                            </div>
                            <p><em><a href="/products/detail.php?product_id=61" rel="external">軽くて暖かい羽毛布団エアーベーシック キングサイズ</a></em></p>
                        </div>
                        <div class="account">
                            <span class="amount">
                                <img onclick="document.form50.submit(); return false;" alt="＋" src="/user_data/packages/plg_responsivel0084/img/btn_plus.png">
                            </span>
                        </div>
                        <div class="price">71,800円</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="cart" style="display: none;">
        <form name="form10" id="form10" method="post" action="/products/detail.php?product_id=54">
            <input type="hidden" name="transactionid" value="7fe23f9e88d0e250c59e1901cdb74ab26fa82505">
            <input type="hidden" name="mode" value="cart">
            <input type="hidden" name="product_id" value="54">
            <input type="hidden" name="product_class_id" value="2150" id="product_class_id">
            <input type="hidden" name="favorite_product_id" value="">
            <input type="number" name="quantity" class="quantitybox" value="1" maxlength="9" max="9">
        </form>
        <form name="form20" id="form20" method="post" action="/products/detail.php?product_id=56">
            <input type="hidden" name="transactionid" value="7fe23f9e88d0e250c59e1901cdb74ab26fa82505">
            <input type="hidden" name="mode" value="cart">
            <input type="hidden" name="product_id" value="56">
            <input type="hidden" name="product_class_id" value="2152" id="product_class_id">
            <input type="hidden" name="favorite_product_id" value="">
            <input type="number" name="quantity" class="quantitybox" value="1" maxlength="9" max="9">
        </form>
        <form name="form30" id="form30" method="post" action="/products/detail.php?product_id=58">
            <input type="hidden" name="transactionid" value="7fe23f9e88d0e250c59e1901cdb74ab26fa82505">
            <input type="hidden" name="mode" value="cart">
            <input type="hidden" name="product_id" value="58">
            <input type="hidden" name="product_class_id" value="2154" id="product_class_id">
            <input type="hidden" name="favorite_product_id" value="">
            <input type="number" name="quantity" class="quantitybox" value="1" maxlength="9" max="9">
        </form>
        <form name="form40" id="form40" method="post" action="/products/detail.php?product_id=60">
            <input type="hidden" name="transactionid" value="7fe23f9e88d0e250c59e1901cdb74ab26fa82505">
            <input type="hidden" name="mode" value="cart">
            <input type="hidden" name="product_id" value="60">
            <input type="hidden" name="product_class_id" value="2156" id="product_class_id">
            <input type="hidden" name="favorite_product_id" value="">
            <input type="number" name="quantity" class="quantitybox" value="1" maxlength="9" max="9">
        </form>
        <form name="form50" id="form50" method="post" action="/products/detail.php?product_id=61">
            <input type="hidden" name="transactionid" value="7fe23f9e88d0e250c59e1901cdb74ab26fa82505">
            <input type="hidden" name="mode" value="cart">
            <input type="hidden" name="product_id" value="61">
            <input type="hidden" name="product_class_id" value="2157" id="product_class_id">
            <input type="hidden" name="favorite_product_id" value="">
            <input type="number" name="quantity" class="quantitybox" value="1" maxlength="9" max="9">
        </form>
    </div>
</div>



<div id="additionalHomenurseCart" style="display: none;">
    <div style="text-align: center; font-size: 28px; text-decoration: underline; margin: 30px 0 0; font-weight: bold; color: #555;">追加で商品を購入する</div>
    <div style="text-align: center; margin: 20px 0 40px; font-size: 16px; font-weight: bold; color: #777;">
        ホームナースは<span style="color: #ff4500; text-decoration: underline;">今月だけの特別価格</span>でのご提供となりますので、<br>
        誠に恐れ入りますが、<span style="color: #ff4500; text-decoration: underline;">お一人様５点まで</span>の購入とさせていただきます。
    </div>

    <div class="formBox">
        <div class="cartinarea cf">
            <div class="table">
                <div class="thead">
                    <ol>
                        <li>商品内容</li>
                        <li>数量</li>
                        <li>小計</li>
                    </ol>
                </div>
                <div class="tbody">
                    <div class="tr">
                        <div class="item">
                            <div class="photo">
                                <img src="/upload/save_image/item.jpg" alt="ホームナースマットレス 敷きパッド シングルサイズ 専用カバー付き">
                            </div>
                            <p><em><a href="/products/detail.php?product_id=197" rel="external">ホームナースマットレス 敷きパッド シングルサイズ 専用カバー付き</a></em></p>
                            <!-- <p class="small">価格:19,800円</p> -->
                        </div>
                        <div class="account">
                            <span class="amount">
                                <img onclick="document.form100.submit(); return false;" alt="＋" src="/user_data/packages/plg_responsivel0084/img/btn_plus.png">
                            </span>
                        </div>
                        <div class="price">39,800円</div>
                    </div>
                    <div class="tr">
                        <div class="item">
                            <div class="photo">
                                <img src="http://itokiyoshi.com/upload/save_image/item2.jpg" alt="ホームナースマットレス 敷きパッド セミダブルサイズ 専用カバー付き">
                            </div>
                            <p><em><a href="/products/detail.php?product_id=198" rel="external">ホームナースマットレス 敷きパッド セミダブルサイズ 専用カバー付き</a></em></p>
                        </div>
                        <div class="account">
                            <span class="amount">
                                <img onclick="document.form101.submit(); return false;" alt="＋" src="/user_data/packages/plg_responsivel0084/img/btn_plus.png">
                            </span>
                        </div>
                        <div class="price">44,800円</div>
                    </div>
                    <div class="tr">
                        <div class="item">
                            <div class="photo">
                                <img src="http://itokiyoshi.com/upload/save_image/item3.jpg" alt="ホームナースマットレス 敷きパッド ダブルサイズ 専用カバー付き">
                            </div>
                            <p><em><a href="/products/detail.php?product_id=199" rel="external">ホームナースマットレス 敷きパッド ダブルサイズ　専用カバー付き</a></em></p>
                        </div>
                        <div class="account">
                            <span class="amount">
                                <img onclick="document.form102.submit(); return false;" alt="＋" src="/user_data/packages/plg_responsivel0084/img/btn_plus.png">
                            </span>
                        </div>
                        <div class="price">49,800円</div>
                    </div>

                    <div class="tr">
                        <div class="item">
                            <div class="photo">
                                <img src="/upload/save_image/11850870_887547887984522_1532771048_n.jpg" alt="ホームナース枕">
                            </div>
                            <p><em><a href="/products/detail.php?product_id=1559" rel="external">ホームナース枕 専用高級防ダニカバー付き</a></em></p>
                        </div>
                        <div class="account">
                            <span class="amount">
                                <img onclick="document.form103.submit(); return false;" alt="＋" src="/user_data/packages/plg_responsivel0084/img/btn_plus.png">
                            </span>
                        </div>
                        <div class="price">13,800円</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div style="display: none;">
        <form name="form100" id="form100" method="post" action="/products/detail.php?product_id=197?">
            <input type="hidden" name="transactionid" value="4c5854da2499ae7ad78a6c802040fc0ab4f9f1a0">
            <input type="hidden" name="mode" value="cart">
            <input type="hidden" name="product_id" value="197">
            <input type="hidden" name="product_class_id" value="2597" id="product_class_id">
            <input type="hidden" name="favorite_product_id" value="">
            <input type="number" name="quantity" class="quantitybox" value="1" maxlength="9" max="9">
            <input type="submit">
        </form>
        <form name="form101" id="form101" method="post" action="/products/detail.php?product_id=198?">
            <input type="hidden" name="transactionid" value="4c5854da2499ae7ad78a6c802040fc0ab4f9f1a0">
            <input type="hidden" name="mode" value="cart">
            <input type="hidden" name="product_id" value="198">
            <input type="hidden" name="product_class_id" value="2598" id="product_class_id">
            <input type="hidden" name="favorite_product_id" value="">
            <input type="number" name="quantity" class="quantitybox" value="1" maxlength="9" max="9">
            <input type="submit">
        </form>
        <form name="form102" id="form102" method="post" action="/products/detail.php?product_id=199?">
            <input type="hidden" name="transactionid" value="4c5854da2499ae7ad78a6c802040fc0ab4f9f1a0">
            <input type="hidden" name="mode" value="cart">
            <input type="hidden" name="product_id" value="199">
            <input type="hidden" name="product_class_id" value="2599" id="product_class_id">
            <input type="hidden" name="favorite_product_id" value="">
            <input type="number" name="quantity" class="quantitybox" value="1" maxlength="9" max="9">
            <input type="submit">
        </form>
        <form name="form103" id="form103" method="post" action="/products/detail.php?product_id=1559?">
            <input type="hidden" name="transactionid" value="4c5854da2499ae7ad78a6c802040fc0ab4f9f1a0">
            <input type="hidden" name="mode" value="cart">
            <input type="hidden" name="product_id" value="1559">
            <input type="hidden" name="product_class_id" value="15681" id="product_class_id">
            <input type="hidden" name="favorite_product_id" value="">
            <input type="number" name="quantity" class="quantitybox" value="1" maxlength="9" max="9">
            <input type="submit">
        </form>
    </div>
</div>



<script type="text/javascript">
    for (var i = 0; i < $('#form1 .tr .item a').length; i++) {
        var pid = $('#form1 .tr .item a').eq(i).attr('href').split('product_id=')[1]
        if (pid == 54 || pid == 56 || pid == 58 || pid == 60 || pid == 61) {
            $('#additionalCart').css('display', 'block')
        } else if (pid == 197 || pid == 198 || pid == 199 || pid == 1559) {
            $('#additionalHomenurseCart').css('display', 'block')
        };
    };
</script>













    </div>
    <!--{else}-->
    <div class="inner">
        <div class="message">
            <p>※ 現在カート内に商品はございません。</p>
        </div>
    </div>
    <!--{/if}-->
</section>
