<!--{*
 * ResponsiveL0084
 * Copyright (C) 2013 LOCKON CO.,LTD. All Rights Reserved.
 * http://www.lockon.co.jp/
 *}-->

<body>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-58934803-1', 'auto');
  ga('send', 'pageview');

</script>
<!--{$GLOBAL_ERR}-->
<noscript>
    <p>JavaScript を有効にしてご利用下さい.</p>
</noscript>

<div id="wrapper" class="column<!--{$tpl_column_num}-->">
    <a name="top" id="top"></a>

    <!--{if $tpl_mainpage eq "error.tpl"}-->

    <!-- ▼エラー -->
    <!--{include file=$tpl_mainpage}-->
    <!-- ▲エラー -->
    
    <!--{else}-->

    <!--{* ▼HeaderHeaderTop COLUMN*}-->
    <!--{if $arrPageLayout.HeaderTopNavi|@count > 0}-->
        <div id="headertopcolumn">
            <!--{* ▼上ナビ *}-->
            <!--{foreach key=HeaderTopNaviKey item=HeaderTopNaviItem from=$arrPageLayout.HeaderTopNavi}-->
                <!-- ▼<!--{$HeaderTopNaviItem.bloc_name}--> -->
                <!--{if $HeaderTopNaviItem.php_path != ""}-->
                    <!--{include_php file=$HeaderTopNaviItem.php_path items=$HeaderTopNaviItem}-->
                <!--{else}-->
                    <!--{include file=$HeaderTopNaviItem.tpl_path items=$HeaderTopNaviItem}-->
                <!--{/if}-->
                <!-- ▲<!--{$HeaderTopNaviItem.bloc_name}--> -->
            <!--{/foreach}-->
            <!--{* ▲上ナビ *}-->
        </div>
    <!--{/if}-->
    <!--{* ▲HeaderHeaderTop COLUMN*}-->
    <!--{* ▼HEADER *}-->
    <!--{if $arrPageLayout.header_chk != 2}-->
        <!--{include file= $header_tpl}-->
    <!--{/if}-->
    <!--{* ▲HEADER *}-->

    <!--{* ▼TOP COLUMN*}-->
    <!--{if $arrPageLayout.TopNavi|@count > 0}-->
        <!--{* ▼上ナビ *}-->
        <!--{foreach key=TopNaviKey item=TopNaviItem from=$arrPageLayout.TopNavi}-->
            <!-- ▼<!--{$TopNaviItem.bloc_name}--> -->
            <!--{if $TopNaviItem.php_path != ""}-->
                <!--{include_php file=$TopNaviItem.php_path items=$TopNaviItem}-->
            <!--{else}-->
                <!--{include file=$TopNaviItem.tpl_path items=$TopNaviItem}-->
            <!--{/if}-->
            <!-- ▲<!--{$TopNaviItem.bloc_name}--> -->
        <!--{/foreach}-->
        <!--{* ▲上ナビ *}-->
    <!--{/if}-->
    <!--{* ▲TOP COLUMN*}-->
    
    <div id="contents" class="cf">
        <!--{* ▼RIGHT COLUMN *}-->
        <!--{if $arrPageLayout.RightNavi|@count > 0}-->
        <aside class="column side">
            <!--{* ▼右ナビ *}-->
            <!--{foreach key=RightNaviKey item=RightNaviItem from=$arrPageLayout.RightNavi}-->
                <!-- ▼<!--{$RightNaviItem.bloc_name}--> -->
                <!--{if $RightNaviItem.php_path != ""}-->
                    <!--{include_php file=$RightNaviItem.php_path items=$RightNaviItem}-->
                <!--{else}-->
                    <!--{include file=$RightNaviItem.tpl_path items=$RightNaviItem}-->
                <!--{/if}-->
                <!-- ▲<!--{$RightNaviItem.bloc_name}--> -->
            <!--{/foreach}-->
            <!--{* ▲右ナビ *}-->
        </aside>
        <!--{/if}-->
        <!--{* ▲RIGHT COLUMN *}-->

        <!--{* ▼CENTER COLUMN *}-->
        <article id="main">
            <!--{* ▼メイン上部 *}-->
            <!--{if $arrPageLayout.MainHead|@count > 0}-->
                <!--{foreach key=MainHeadKey item=MainHeadItem from=$arrPageLayout.MainHead}-->
                    <!-- ▼<!--{$MainHeadItem.bloc_name}--> -->
                    <!--{if $MainHeadItem.php_path != ""}-->
                        <!--{include_php file=$MainHeadItem.php_path items=$MainHeadItem}-->
                    <!--{else}-->
                        <!--{include file=$MainHeadItem.tpl_path items=$MainHeadItem}-->
                    <!--{/if}-->
                    <!-- ▲<!--{$MainHeadItem.bloc_name}--> -->
                <!--{/foreach}-->
            <!--{/if}-->
            <!--{* ▲メイン上部 *}-->

            <!-- ▼メイン -->
            <!--{include file=$tpl_mainpage}-->
            <!-- ▲メイン -->

            <!--{* ▼メイン下部 *}-->
            <!--{if $arrPageLayout.MainFoot|@count > 0}-->
                <!--{foreach key=MainFootKey item=MainFootItem from=$arrPageLayout.MainFoot}-->
                    <!-- ▼<!--{$MainFootItem.bloc_name}--> -->
                    <!--{if $MainFootItem.php_path != ""}-->
                        <!--{include_php file=$MainFootItem.php_path items=$MainFootItem}-->
                    <!--{else}-->
                        <!--{include file=$MainFootItem.tpl_path items=$MainFootItem}-->
                    <!--{/if}-->
                    <!-- ▲<!--{$MainFootItem.bloc_name}--> -->
                <!--{/foreach}-->
            <!--{/if}-->
            <!--{* ▲メイン下部 *}-->
            
        </article>
        <!--{* ▲CENTER COLUMN *}-->

        <!--{* ▼LEFT COLUMN *}-->
        <!--{if $arrPageLayout.LeftNavi|@count > 0}-->
        <aside id="side">
            <!--{* ▼左ナビ *}-->
            <!--{foreach key=LeftNaviKey item=LeftNaviItem from=$arrPageLayout.LeftNavi}-->
                <!-- ▼<!--{$LeftNaviItem.bloc_name}--> -->
                <!--{if $LeftNaviItem.php_path != ""}-->
                    <!--{include_php file=$LeftNaviItem.php_path items=$LeftNaviItem}-->
                <!--{else}-->
                    <!--{include file=$LeftNaviItem.tpl_path items=$LeftNaviItem}-->
                <!--{/if}-->
                <!-- ▲<!--{$LeftNaviItem.bloc_name}--> -->
            <!--{/foreach}-->
            <!--{* ▲左ナビ *}-->
        </aside>
        <!--{/if}-->
        <!--{* ▲LEFT COLUMN *}-->
    </div>

    <!--{* ▼BOTTOM COLUMN*}-->
    <!--{if $arrPageLayout.BottomNavi|@count > 0}-->
        <!--{* ▼下ナビ *}-->
        <!--{foreach key=BottomNaviKey item=BottomNaviItem from=$arrPageLayout.BottomNavi}-->
            <!-- ▼<!--{$BottomNaviItem.bloc_name}--> -->
            <!--{if $BottomNaviItem.php_path != ""}-->
                <!--{include_php file=$BottomNaviItem.php_path items=$BottomNaviItem}-->
            <!--{else}-->
                <!--{include file=$BottomNaviItem.tpl_path items=$BottomNaviItem}-->
            <!--{/if}-->
            <!-- ▲<!--{$BottomNaviItem.bloc_name}--> -->
        <!--{/foreach}-->
        <!--{* ▲下ナビ *}-->
    <!--{/if}-->
    <!--{* ▲BOTTOM COLUMN*}-->

    <!--{* ▼FOOTER *}-->
    <!--{if $arrPageLayout.footer_chk != 2}-->
        <!--{include file=$footer_tpl}-->
    <!--{/if}-->
    <!--{* ▲FOOTER *}-->
    <!--{* ▼FooterBottom COLUMN*}-->
    <!--{if $arrPageLayout.FooterBottomNavi|@count > 0}-->
        <!--{* ▼上ナビ *}-->
        <!--{foreach key=FooterBottomNaviKey item=FooterBottomNaviItem from=$arrPageLayout.FooterBottomNavi}-->
            <!-- ▼<!--{$FooterBottomNaviItem.bloc_name}--> -->
            <!--{if $FooterBottomNaviItem.php_path != ""}-->
                <!--{include_php file=$FooterBottomNaviItem.php_path items=$FooterBottomNaviItem}-->
            <!--{else}-->
                <!--{include file=$FooterBottomNaviItem.tpl_path items=$FooterBottomNaviItem}-->
            <!--{/if}-->
            <!-- ▲<!--{$FooterBottomNaviItem.bloc_name}--> -->
        <!--{/foreach}-->
        <!--{* ▲上ナビ *}-->
    <!--{/if}-->
    <!--{* ▲FooterBottom COLUMN*}-->
    <!--{/if}-->
</div>
<!--yahoo▼-->
<script type="text/javascript">
  (function () {
    var tagjs = document.createElement("script");
    var s = document.getElementsByTagName("script")[0];
    tagjs.async = true;
    tagjs.src = "//s.yjtag.jp/tag.js#site=gphQGCM";
    s.parentNode.insertBefore(tagjs, s);
  }());
</script>
<noscript>
  <iframe src="//b.yjtag.jp/iframe?c=gphQGCM" width="1" height="1" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
</noscript>
<!--yahoo▲-->
</body>
