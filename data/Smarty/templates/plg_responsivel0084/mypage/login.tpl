<!--{*
 * ResponsiveL0084
 * Copyright (C) 2013 LOCKON CO.,LTD. All Rights Reserved.
 * http://www.lockon.co.jp/
 *}-->
<section id="undercolumn">
    <h2 class="title"><!--{$tpl_title|h}--></h2>
    <div class="inner">
        <h3 class="heading02">会員登録がお済みのお客様</h3>
        <form name="login_mypage" id="login_mypage" method="post" action="<!--{$smarty.const.HTTPS_URL}-->frontparts/login_check.php" onsubmit="return eccube.checkLoginFormInputted('login_mypage')">
            <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
            <input type="hidden" name="mode" value="login" />
            <input type="hidden" name="url" value="<!--{$smarty.server.REQUEST_URI|h}-->" />
            <div class="login_area">
                <div class="loginareaBox table">
                    <ul class="tbody">
                        <li class="tr">
                            <div class="th">メールアドレス</div>
                            <div>
                                <!--{assign var=key value="login_email"}-->
                                <input<!--{if $arrErr[$key]}--> class="input_email mailtextBox error"<!--{else}--> class="input_email mailtextBox"<!--{/if}--> type="email" name="<!--{$key}-->" value="<!--{$tpl_login_email|h}-->" maxlength="<!--{$arrForm[$key].length}-->" placeholder="メールアドレス" />
                                <!--{assign var=key value="login_memory"}-->
                                <span class="pc"><input id="login_memory" type="checkbox" value="1" name="<!--{$key}-->" <!--{$tpl_login_memory|sfGetChecked:1}--> /><label for="login_memory">記憶</label></span>
                                <!--{if $arrErr[$key]}-->
                                <div class="attention"><!--{$arrErr[$key]}--></div>
                                <!--{/if}-->
                            </div>
                        </li>
                        <li class="tr">
                            <div class="th">パスワード</div>
                            <div>
                                <!--{assign var=key value="login_pass"}-->
                                <input<!--{if $arrErr[$key]}--> class="passtextBox error"<!--{else}--> class="passtextBox"<!--{/if}--> type="password" name="<!--{$key}-->" maxlength="<!--{$arrForm[$key].length}-->" placeholder="パスワード" />
                                <!--{if $arrErr[$key]}-->
                                <div class="attention"><!--{$arrErr[$key]}--></div>
                                <!--{/if}-->
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="inputbox">
                    <div class="btn_area">
                        <p class="button02">
                            <button type="submit" name="log" id="log"><span>ログイン</span></button>
                        </p>
                        <p class="button03"><button type="button" onclick="eccube.openWindow('<!--{$smarty.const.HTTPS_URL}-->forgot/<!--{$smarty.const.DIR_INDEX_PATH}-->','forget','600','520'); return false;" target="_blank"><span>パスワードを忘れた方</span></button></p>
                    </div>
                </div>
            </div>

        </form>

        <h3 class="heading02">まだ会員登録されていないお客様</h3>
        <p class="inputtext">
            会員登録をすると便利なMyページをご利用いただけます。<br />
            また、ログインするだけで、毎回お名前や住所などを入力することなくスムーズにお買い物をお楽しみいただけます。
        </p>
        <div class="inputbox">
            <div class="btn_area">
                <p class="button05"><button type="button" onclick="location.href='<!--{$smarty.const.ROOT_URLPATH}-->entry/kiyaku.php'"><span>会員登録ページヘ</span></button></p>
            </div>
        </div>
    </div>
</section>
