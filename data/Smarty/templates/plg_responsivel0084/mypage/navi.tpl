<!--{*
 * ResponsiveL0084
 * Copyright (C) 2013 LOCKON CO.,LTD. All Rights Reserved.
 * http://www.lockon.co.jp/
 *}-->
<!--▼NAVI-->
<div id="mynavi_area">
    <nav id="mypage_nav">
        <!--{strip}-->
        <ul class="cf">
            <!--{* 会員状態 *}-->
            <!--{if $tpl_login}-->
            <li><a href="delivery.php" class="<!--{if $tpl_mypageno == 'delivery'}--> selected<!--{/if}-->">お届け先追加・変更</a></li>
            <li><a href="change.php" class="<!--{if $tpl_mypageno == 'change'}--> selected<!--{/if}-->">会員内容変更</a></li>
            <li><a href="./<!--{$smarty.const.DIR_INDEX_PATH}-->" class="<!--{if $tpl_mypageno == 'index'}--> selected<!--{/if}-->">購入履歴</a></li>
            <!--{if $smarty.const.OPTION_FAVORITE_PRODUCT == 1}-->
            <li><a href="favorite.php" class="<!--{if $tpl_mypageno == 'favorite'}--> selected<!--{/if}-->">お気に入り</a></li>
            <!--{/if}-->
            <li><a href="refusal.php" class="<!--{if $tpl_mypageno == 'refusal'}--> selected<!--{/if}-->">退会手続き</a></li>

            <!--{* 退会状態 *}-->
            <!--{else}-->
            <li><a href="<!--{$smarty.const.TOP_URLPATH}-->" class="<!--{if $tpl_mypageno == 'delivery'}--> selected<!--{/if}-->">お届け先追加・変更</a></li>
            <li><a href="<!--{$smarty.const.TOP_URLPATH}-->" class="<!--{if $tpl_mypageno == 'change'}--> selected<!--{/if}-->">会員内容変更</a></li>
            <li><a href="<!--{$smarty.const.TOP_URLPATH}-->" class="<!--{if $tpl_mypageno == 'index'}--> selected<!--{/if}-->">購入履歴</a></li>
            <!--{if $smarty.const.OPTION_FAVORITE_PRODUCT == 1}-->
            <li><a href="<!--{$smarty.const.TOP_URLPATH}-->" class="<!--{if $tpl_mypageno == 'favorite'}--> selected<!--{/if}-->">お気に入り</a></li>
            <!--{/if}-->
            <li><a href="<!--{$smarty.const.TOP_URLPATH}-->" class="<!--{if $tpl_mypageno == 'refusal'}--> selected<!--{/if}-->">退会手続き</a></li>
            <!--{/if}-->
        </ul>
        <!--{/strip}-->
    </nav>
</div>
<!--▲NAVI-->

<!--▼現在のポイント-->
<!--{if $point_disp !== false}-->
<div class="point_announce">
    <p>
        <span class="user_name"><!--{$CustomerName1|h}--> <!--{$CustomerName2|h}-->様</span>
        <!--{if $smarty.const.USE_POINT !== false}--><span class="point">所持ポイント： <!--{$CustomerPoint|number_format|default:"0"|h}-->pt</span><!--{/if}-->
    </p>
</div>
<!--{/if}-->
<!--▲現在のポイント-->
