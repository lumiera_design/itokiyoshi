<!--{*
 * ResponsiveL0084
 * Copyright (C) 2013 LOCKON CO.,LTD. All Rights Reserved.
 * http://www.lockon.co.jp/
 *}-->

<div id="mycontents_area">
    <h2 class="title"><!--{$tpl_title|h}--></h2>
    <!--▼NAVI-->
    <!--{if $tpl_navi != ""}-->
        <!--{include file=$tpl_navi}-->
    <!--{else}-->
        <!--{include file=`$smarty.const.TEMPLATE_REALDIR`mypage/navi.tpl}-->
    <!--{/if}-->
    <!--▲NAVI--> 
    <div class="inner">
        <h3 class="heading02"><!--{$tpl_subtitle|h}--></h3>
        <p>
            会員登録内容の変更が完了いたしました。<br />
            今後ともご愛顧賜りますようよろしくお願い申し上げます。
        </p>
    </div>
</div>
    