<!--{*
 * ResponsiveL0084
 * Copyright (C) 2013 LOCKON CO.,LTD. All Rights Reserved.
 * http://www.lockon.co.jp/
 *}-->

<div id="mycontents_area">
    <h2 class="title"><!--{$tpl_title|h}--></h2>
    <!--{include file=$tpl_navi}-->
    <div class="inner">
        <h3 class="heading02"><!--{$tpl_subtitle|h}--></h3>
        <p><span class="attention">※</span>は入力必須項目です。</p>

        <form name="form1" id="form1" method="post" action="?">
            <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
            <input type="hidden" name="mode" value="confirm" />
            <input type="hidden" name="customer_id" value="<!--{$arrForm.customer_id.value|h}-->" />
            <div class="table member_table">
                <div class="tbody">
                    <!--{include file="`$smarty.const.TEMPLATE_REALDIR`frontparts/form_personal_input.tpl" flgFields=3 emailMobile=true prefix=""}-->
                </div>
            </div>
            <div class="btn_area">
                <p class="button02">
                    <button type="submit" name="refusal" id="refusal"><span>確認ページへ</span></button>
                </p>
            </div>
        </form>
    </div>
</div>
