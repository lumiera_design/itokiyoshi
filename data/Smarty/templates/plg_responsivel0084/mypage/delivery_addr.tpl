<!--{*
 * ResponsiveL0084
 * Copyright (C) 2013 LOCKON CO.,LTD. All Rights Reserved.
 * http://www.lockon.co.jp/
 *}-->

<div id="mycontents_area">
    <h2 class="title"><!--{$plg_responsiveL0084_tpl_title|h}--></h2>
    <!--▼NAVI-->
    <!--{if $ParentPage == $smarty.const.MYPAGE_DELIVADDR_URLPATH}-->
    <!--{if $tpl_navi != ""}-->
        <!--{include file=$tpl_navi}-->
    <!--{else}-->
        <!--{include file=`$smarty.const.TEMPLATE_REALDIR`mypage/navi.tpl}-->
    <!--{/if}-->
    <!--{/if}-->
    <!--▲NAVI--> 
    <div class="inner">
        <!--{if $ParentPage == $smarty.const.MYPAGE_DELIVADDR_URLPATH}-->
        <h3 class="heading02"><!--{$tpl_title|h}--></h3>
        <!--{/if}-->
        <p><span class="attention">※</span>は入力必須項目です。</p>
        <form name="form1" id="form1" method="post" action="?">
            <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
            <input type="hidden" name="mode" value="edit" />
            <input type="hidden" name="other_deliv_id" value="<!--{$smarty.session.other_deliv_id|h}-->" />
            <input type="hidden" name="ParentPage" value="<!--{$ParentPage}-->" />
            <div class="table member_table">
                <div class="tbody">
                    <!--{include file="`$smarty.const.TEMPLATE_REALDIR`frontparts/form_personal_input.tpl" flgFields=1 emailMobile=false prefix=""}-->
                </div>
            </div>
            <ul class="btn_area">
                <li class="button02">
                    <button type="submit" name="register" id="register"><span>登録する</span></button>
                </li>
                <li class="button03">
                    <button type="button" onclick="location.href='<!--{$ParentPage}-->'"><span>戻る</span></button>
                </li>
            </ul>
        </form>
    </div>
</div>
