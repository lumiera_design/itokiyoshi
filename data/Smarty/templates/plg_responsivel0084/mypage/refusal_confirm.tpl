<!--{*
 * ResponsiveL0084
 * Copyright (C) 2013 LOCKON CO.,LTD. All Rights Reserved.
 * http://www.lockon.co.jp/
 *}-->

<div id="mycontents_area">
    <h2 class="title"><!--{$tpl_title|h}--></h2>
    <!--▼NAVI-->
    <!--{if $tpl_navi != ""}-->
        <!--{include file=$tpl_navi}-->
    <!--{else}-->
        <!--{include file=`$smarty.const.TEMPLATE_REALDIR`mypage/navi.tpl}-->
    <!--{/if}-->
    <!--▲NAVI--> 
    <div class="inner">
        <h3 class="heading02"><!--{$tpl_subtitle|h}--></h3>
        <form name="form1" id="form1" method="post" action="?">
        <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
        <input type="hidden" name="refusal_transactionid" value="<!--{$refusal_transactionid}-->" />
        <input type="hidden" name="mode" value="complete" />
        <div class="refusal_message">
            <p>退会手続きを実行してもよろしいでしょうか？</p>
            <p>
                退会手続きが完了した時点で、現在保存されている購入履歴や、<br />
                お届け先等の情報は全てなくなりますのでご注意ください。
            </p>
        </div>
        <ul class="btn_area">
            <li class="button02 bt_left">
                <button type="button" onclick="location.href='./refusal.php'"><span>いいえ</span></button>
            </li>
            <li class="button03 bt_right">
                <button type="submit" name="refuse_do" id="refuse_do"><span>はい</span></button>
            </li>
        </ul>
        </form>
    </div>
</div>
