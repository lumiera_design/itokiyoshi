<!--{*
 * ResponsiveL0084
 * Copyright (C) 2013 LOCKON CO.,LTD. All Rights Reserved.
 * http://www.lockon.co.jp/
 *}-->

<div id="mycontents_area">
    <h2 class="title"><!--{$tpl_title|h}--></h2>
    <!--▼NAVI-->
    <!--{if $tpl_navi != ""}-->
        <!--{include file=$tpl_navi}-->
    <!--{else}-->
        <!--{include file=`$smarty.const.TEMPLATE_REALDIR`mypage/navi.tpl}-->
    <!--{/if}-->
    <!--▲NAVI--> 
    <div class="inner">

    <h3 class="heading02"><!--{$tpl_subtitle|h}--></h3>

    <!--{if $objNavi->all_row > 0}-->
    <form name="form1" id="form1" method="post" action="?">
        <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
        <input type="hidden" name="order_id" value="" />
        <input type="hidden" name="pageno" value="<!--{$objNavi->nowpage}-->" />
        
        <p><span class="attention"><!--{$objNavi->all_row|number_format}-->件</span>の購入履歴があります。</p>
        <div id="history_table" class="table">
            <div class="thead">
                <ol>
                    <li>注文番号</li>
                    <li>購入日時</li>
                    <li>お支払い方法</li>
                    <li>合計金額</li>
                    <!--{if $smarty.const.MYPAGE_ORDER_STATUS_DISP_FLAG }-->
                    <li>ご注文状況</li>
                    <!--{/if}-->
                    <li>詳細</li>
                </ol>
            </div>
            <div class="tbody">
                <!--{section name=cnt loop=$arrOrder}-->
                <a class="tr" href="<!--{$smarty.const.ROOT_URLPATH}-->mypage/history.php?order_id=<!--{$arrOrder[cnt].order_id}-->">
                <div><!--{$arrOrder[cnt].order_id}--></div>
                <div><!--{$arrOrder[cnt].create_date|sfDispDBDate}--></div>
                <!--{assign var=payment_id value="`$arrOrder[cnt].payment_id`"}-->
                <div><!--{$arrPayment[$payment_id]|h}--></div>
                <div class="price"><!--{$arrOrder[cnt].payment_total|number_format}-->円</div>
                <!--{if $smarty.const.MYPAGE_ORDER_STATUS_DISP_FLAG }-->
                <!--{assign var=order_status_id value="`$arrOrder[cnt].status`"}-->
                <div class="order_info_detail"><!--{$arrCustomerOrderStatus[$order_status_id]|h}--></div>
                <!--{/if}-->
                <div><span class="link">詳細</span></div>
                </a>
                <!--{/section}-->
                </div>
        </div>
    </form>
    
    <!--▼ページネーション-->
    <!--{if $objNavi->max_page > 1}-->
    <!--{assign var=arrPagenavi value=$objNavi->arrPagenavi}-->
    <nav class="pagination">
        <ul>
        <!--{if $objNavi->now_page > 1}-->
        <li class="prev"><a href="?pageno=<!--{$arrPagenavi.before}-->" onclick="eccube.movePage('<!--{$arrPagenavi.before}-->'); return false;">&lt;</a></li>
        <!--{/if}-->

        <!--{assign var=first_num value=$objNavi->now_page-$smarty.const.NAVI_PMAX+1}-->
        <!--{assign var=last_num  value=$objNavi->now_page+$smarty.const.NAVI_PMAX-1}-->
        
        <!--{foreach from=$arrPagenavi.arrPageno item="dispnum" key="num" name="page_navi"}-->
        <!--{if $first_num == $dispnum}-->
        <li class="first"><a href="?pageno=<!--{$dispnum}-->" onclick="eccube.movePage('<!--{$dispnum}-->'); return false;"><!--{$dispnum}--></a></li>
        <!--{elseif $last_num == $dispnum}-->
        <li class="last"><a href="?pageno=<!--{$dispnum}-->" onclick="eccube.movePage('<!--{$dispnum}-->'); return false;"><!--{$dispnum}--></a></li>
        <!--{elseif $dispnum == $objNavi->now_page}-->
        <li class="active"><a href="?pageno=<!--{$dispnum}-->" onclick="eccube.movePage('<!--{$dispnum}-->'); return false;"><!--{$dispnum}--></a></li>
        <!--{else}-->
        <li><a href="?pageno=<!--{$dispnum}-->" onclick="eccube.movePage('<!--{$dispnum}-->'); return false;"><!--{$dispnum}--></a></li>
        <!--{/if}-->
        <!--{/foreach}-->

        <!--{if $objNavi->now_page < $objNavi->max_page}-->
        <li class="next"><a href="?pageno=<!--{$arrPagenavi.next}-->" onclick="eccube.movePage('<!--{$arrPagenavi.next}-->'); return false;">&gt;</a></li>
        <!--{/if}-->
        </ul>
    </nav>
    <!--{/if}-->
    <!--▲ページネーション-->

    <!--{else}-->
    <p>購入履歴はありません。</p>
    <!--{/if}-->

    </div>
</div>
