<!--{*
 * ResponsiveL0084
 * Copyright (C) 2013 LOCKON CO.,LTD. All Rights Reserved.
 * http://www.lockon.co.jp/
 *}-->
<!--▼CONTENTS-->
<section id="undercolumn">
    <h2 class="title">お買い物ガイド</h2>
                    
    <div class="inner" id="guide">
    	<h2 class="heading02">お支払いについて</h2>
      
  <h3 class="heading01">クレジットカード【SPIKEクレジットカード決済】</h3>

      <p>取り扱いカードは以下のとおりです。すべてのカード会社で、一括払いが可能となっております。</p>
        <div class="table">
            <div class="thead">
                <ol>
                    <li>カード会社</li>
                    <li>支払方法</li>
                </ol>
            </div>
            <div class="tbody">
                <div class="tr">
                    <div class="th">VISA</div>
                    <div>一括払い</div>
                </div>
                <div class="tr">
                    <div class="th">MASTER</div>
                    <div>一括払い</div>
                </div>
                <div class="tr">
                    <div class="th">JCB</div>
                    <div>一括払い</div>
                </div>
                 <div class="tr">
                    <div class="th">AMEX</div>
                    <div>一括払い</div>
                </div>
                <div class="tr">
                    <div class="th">Diners</div>
                    <div>一括払い</div>
                </div>
                
            </div>
        </div>
        
        <p>【備考】<br />
        
        【重要】クレジットカード明細書上の請求名は*SPIKE.CC*もしくは*SPKCC.MTPS*と記載されます。<br />
・明細に表示される日付は、決済の処理日の影響により、注文日と異なる場合があります。<br />
・お支払い回数：一括払いのみとなります。<br />
・詳細はSPIKEのサポートサイトをご参照願います。<br />
    <a href="https://support.spike.cc/hc/ja/articles/203039884">https://support.spike.cc/hc/ja/articles/203039884</a>。<br />

・お支払総額は以下の通りです。<br />
 商品代金合計＋送料（一部送料込み商品もございます。）<br />
・当店ではセキュリティ上の配慮からクレジットカード利用控は原則としてお送りしておりません。カード会社から送付されますご利用明細をご確認ください。<br />
・なお、弊社ではSSLというシステムを利用しております。<br />
・カード番号は暗号化されて送信されます。ご安心下さい
<p class="attention">※お客様のご利用状態等によっては、他の決済手段に変更いただく場合がございます。 </p>



<!--
<h3 class="heading01">楽天バンク決済</h3>

<p class="attention">※共同購入、オークションではご利用できません。</p>

<p>「楽天バンク決済」は楽天銀行に口座をお持ちで、楽天会員の銀行口座を登録しているお客様のみ、ご利用いただける決済サービスです。<br>
あらかじめ、商品代金＋送料等（＋楽天バンク決済手数料（お客様負担時のみ））の総額が、お客様の残高の範囲内であることをご確認ください。</p>

<p>【備考】
●ご注文の時間によっては、当日中の残高確認および振替ができない場合がございます。<br>
●1000円以上増額する金額変更があった場合は、お客様による変更承認の操作が必要となり、その旨メールでご連絡します。<br>
メール送信後、3日以内に承諾いただけない場合には、自動的に注文がキャンセルになります。<br>

<p class="attention">※下記の振替手数料が発生します。</p>
【楽天バンク決済手数料　料金表】<br>

 <div class="table">
            <div class="thead">
                <ol>
                    <li>総合計金額</li>
                    <li>楽天バンク決済手数料(税込)</li>
                </ol>
            </div>
            <div class="tbody">
                <div class="tr">
                    <div class="th">1円　～　29,999円</div>
                    <div>155円</div>
                </div>
                <div class="tr">
                    <div class="th">30,000円　～</div>
                    <div>257円</div>
                </div>
              
                </div>
                
            </div>
        </div>
        
        

<p>※総合計金額は、商品代金・消費税・送料等を含めた合計金額からポイント利用分を差し引いた金額です。</p>

  -->

<h3 class="heading01">銀行振込</h3>
<p>

【振込先】<br />
三菱東京UFJ銀行(ミツビシトウキョウユーエフジェイギンコウ)　浄心支店(ジョウシンシテン)<br />
ミツビシトウキョウユーエフジェイギンコウ <br />
普通 575682 カ)イトウキヨシショウテン<br />

【備考】<br />
お支払総額は以下の通りです。<br />
商品代金　:商品代金合計+送料(一律600円)※￥10,000（税抜）以上で送料無料<br />
振込手数料:お振込時にお確かめください。 
なお、振込手数料はお客様負担でお願い致します。<br />
ご入金が確認でき次第、商品を発送致します。<br />
3日以内にご入金のない場合、ご注文はキャンセルとさせていただきます。<br />

<!--<h3 class="heading01">郵便振込</h3>-->

<!--<h3 class="heading01">楽天Edy決済</h3>
<p>【備考】<br />
●お支払総額は以下の通りです。 <br />
商品代金：商品代金合計＋送料 <br />
●お支払総額の上限は50,000円となります。 <br />
●ご注文後に、お支払いURLをメールでお知らせいたします。 <br />
お支払方法は、<a href="http://link.rakuten.co.jp/0/019/072/?url=em_help/index_edy_cyber.html" target="_blank">こちら</a>をご覧ください。 <br />
※モバイルEdyをご利用の場合は、<a href="http://link.rakuten.co.jp/1/019/096/?url=em_help/index_edy_mobile.html" target="_blank">こちら</a>をご覧ください。

<h3 class="heading01">Alipay</h3>
<p>【備考】<br />
<div class="table">
<img src="http://image.rakuten.co.jp/atorie-moon/cabinet/img63844117.gif">
<br>
<a href="http://global.rakuten.com/en/help/payment/alipay_pc.html"
Target="_blank"> Alipay Payment Process Flow: PC</a>
<br>
<a href="http://global.rakuten.com/en/help/payment/alipay_mobile.html"
Target="_blank">Alipay Payment Process Flow: Mobile/Smartphone</a>

</div>
-->

<h2 class="heading02">配送について</h2>
<h3 class="heading01">宅配便</h3>
<p>【業者】佐川急便、ヤマト便、福山通運</p>
<p>【備考】￥10,000（税抜）以上購入の場合無料
￥10,000未満は一律￥600<br>
 商品のお届け時期<br>
    特にご指定がない場合、<br>
　　郵便振替、銀行振込、クレジット　⇒ご入金確認後３日以内に発送いたします。<br>
　　それ以上となる場合は予めメールにてご連絡します。

</p>



<div class="table">
            
            <div class="tbody">
            
            
         <div class="tr">
         	<div class="th">【送料料金表】</div>
         	<div>全国一律料金：600円<br>
	    	
<span class="attention">※送料別の商品：商品価格に送料が含まれていないもので、かつ、商品ページで個別に送料が設定されていない商品<br>
		　送料込の商品：商品価格に送料が含まれているもの。商品ページで個別に送料が設定されている商品を含みます。
		</span> </div>
	 </div>
                <div class="tr">
                    <div class="th">高額購入割引特典</div>
                    <div>１配送先につき、合計　10,000円（税抜）以上ご注文いただいた場合、送料が　0円になります。</div>
                </div>
                <div class="tr">
                    <div class="th">まとめ買い時の扱い</div>
                    <div>１配送先につき、送料別の商品を複数ご注文いただいた場合、
				送料は上記料金表１個分送料になります。<br />
                送料別の商品と送料込の商品を同時にご注文の場合は、以下の送料込商品の扱いもご参照ください。</div>
                </div>
                <div class="tr">
                    <div class="th">送料込商品の扱い</div>
                    <div>１配送先につき、送料込の商品と送料別の商品を同時にご注文の場合、送料は上記料金表の適用外になります。後ほど店舗からご連絡いたします。</div>
                </div>
                 <div class="tr">
                    <div class="th">送料分消費税</div>
                    <div>この料金には消費税が含まれています。</div>
                </div>
                <div class="tr">
                    <div class="th">離島他の扱い</div>
                    <div>離島・一部地域は追加送料がかかる場合があります。</div>
                </div>
                
                <div class="tr">
                    <div class="th">備考</div>
                    <div>北海道・沖縄の方は送料1050円となります。その他離島の場合はのちほどメールにてご連絡いたしますので、お問い合わせ下さいませ。</div>
                </div>
                
            </div>
        </div>

	<hr size=1>
    

<h3 class="heading01">消費税について</h3>
      
	<div class="table">
           
            <div class="tbody">
                <div class="tr">
                    <div class="th">消費税率</div>
                    <div>8％</div>
                </div>
                <div class="tr">
                    <div class="th">消費税計算順序</div>
                    <div>１商品毎に消費税計算</div>
                </div>
                <div class="tr">
                    <div class="th">１円未満消費税端数</div>
                    <div>切り捨て</div>
                </div>
                
                </div>
                
            </div>
        </div>
        



  <hr size="1">
<h3 class="heading01">返品について</h3>
 
  <p>
返品は未開封・未使用のもののみ到着後1週間以内に電話連絡いただいた<br>
もののみお受けいたします。<br>
返品の送料・手数料については、<br>
初期不良の場合は当社が、それ以外のお客様<br>
都合による返品につきましてはお客様にてご負担いただきます。</p>
 <hr size="1">
<h3 class="heading01">返金について</h3>
<p>返品商品到着確認後３日以内にご指定口座にお振込致します。</p>
<h4 class="heading01">返品連絡先</h4>
<p>電話番号：052-522-5206（平日AM10:00からPM5:00)<br>
　e-mail:shop@itokiyoshi.com
　返送先住所：名古屋市西区児玉３－３８－１９<br>
　担当者：伊藤　昌美
</p>
  <hr size="1">
<h3 class="heading01">商品のお届け時期</h3>
<p>銀行振込　⇒ご入金確認後３日以内に発送いたします。<br>
クレジット⇒ご注文確認後３日以内に発送いたします。<br>
それ以上となる場合は予めメールにてご連絡します。</p>


</section>
<div class="home"><a href="<!--{$smarty.const.TOP_URLPATH}-->">▶HOMEへ戻る</a></div> 
<!--▲CONTENTS-->
