<!--{*
 * ResponsiveL0084
 * Copyright (C) 2013 LOCKON CO.,LTD. All Rights Reserved.
 * http://www.lockon.co.jp/
 *}-->

<!--▼HEADER-->
<header id="header">
    <div class="inner">
		 <h1 class="logo"><a href="<!--{$smarty.const.TOP_URLPATH}-->"><img src="<!--{$TPL_URLPATH}-->img/common/logo-w.png" alt="日本尾張の羽毛布団伊藤清商店" /></a></h1> 
		<!--<h1 class="logo"><img src="<!--{$TPL_URLPATH}-->img/common/logo-w.png" alt="日本尾張の羽毛布団伊藤清商店" /></h1>-->
      <div class="header_utility">
        <nav class="header_navi">
       <!-- <a href="http://itokiyoshi.com/contact/"><img id="btn_contact" src="../user_data/packages/plg_responsivel0084/img/headnavi-w_03.jpg"></a><br>
        <a href="tel:0525225206"><img id="btn_tel" src="../user_data/packages/plg_responsivel0084/img/headnavi-w_05.jpg"></a>-->
        <a href="tel:0525225206"><img id="btn_tel" src="../user_data/packages/plg_responsivel0084/img/air-top-tel.png"></a>
        </nav>
      </div>
    </div>
	<div id="header_bottom" style="display: none;">
		<!-- ▼【ヘッダー】グローバルナビ -->
		<div id="gnav">
			<div class="inner00">
				<nav>
        		<!--{* ▼HeaderInternal COLUMN*}-->
				<!--{if $arrPageLayout.HeaderInternalNavi|@count > 0}-->
					<!--{* ▼上ナビ *}-->
					<!--{foreach key=HeaderInternalNaviKey item=HeaderInternalNaviItem from=$arrPageLayout.HeaderInternalNavi}-->
						<!-- ▼<!--{$HeaderInternalNaviItem.bloc_name}--> -->
						<!--{if $HeaderInternalNaviItem.php_path != ""}-->
							<!--{include_php file=$HeaderInternalNaviItem.php_path items=$HeaderInternalNaviItem}-->
                        <!--{else}-->
                        	<!--{include file=$HeaderInternalNaviItem.tpl_path items=$HeaderInternalNaviItem}-->
                       	<!--{/if}-->
						<!-- ▲<!--{$HeaderInternalNaviItem.bloc_name}--> -->
					<!--{/foreach}-->
					<!--{* ▲上ナビ *}-->
				<!--{/if}-->
				<!--{* ▲HeaderInternal COLUMN*}-->
				</nav>
			</div>
		</div>
	</div>
</header>
<!--▲HEADER-->
