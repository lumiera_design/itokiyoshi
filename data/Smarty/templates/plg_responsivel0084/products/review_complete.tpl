<!--{*
 * ResponsiveL0084
 * Copyright (C) 2013 LOCKON CO.,LTD. All Rights Reserved.
 * http://www.lockon.co.jp/
 *}-->

<div id="mycontents_area">
    <h2 class="title">お客様の声書き込み</h2>
    <div class="inner">
        <p>
            登録が完了しました。ご利用ありがとうございました。
        </p>
        <p>
            弊社にて登録内容を確認後、ホームページに反映させていただきます。<br />
            今しばらくお待ちくださいませ。
        </p>
        <div class="btn_area">
            <p class="button02">
                <button type="button" onclick="location.href='<!--{$smarty.const.TOP_URLPATH}-->'"><span>トップページへ</button></a>
            </p>
        </div>
    </div>
</div>
