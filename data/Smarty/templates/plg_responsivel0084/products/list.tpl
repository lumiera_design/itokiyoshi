<!--{*
 * ResponsiveL0084
 * Copyright (C) 2013 LOCKON CO.,LTD. All Rights Reserved.
 * http://www.lockon.co.jp/
 *}-->
<script type="text/javascript">//<![CDATA[
    function fnSetClassCategories(form, classcat_id2_selected) {
        var $form = $(form);
        var product_id = $form.find('input[name=product_id]').val();
        var $sele1 = $form.find('select[name=classcategory_id1]');
        var $sele2 = $form.find('select[name=classcategory_id2]');
        eccube.setClassCategories($form, product_id, $sele1, $sele2, classcat_id2_selected);
    }
    // 並び順を変更
    function fnChangeOrderby(orderby) {
        eccube.setValue('orderby', orderby);
        eccube.setValue('pageno', 1);
        eccube.submitForm();
    }
    // 表示件数を変更
    function fnChangeDispNumber(dispNumber) {
        eccube.setValue('disp_number', dispNumber);
        eccube.setValue('pageno', 1);
        eccube.submitForm();
    }
    // カゴに入れる
    function fnInCart(productForm) {
        var searchForm = $("#form1");
        var cartForm = $(productForm);
        // 検索条件を引き継ぐ
        var hiddenValues = ['mode','category_id','maker_id','name','orderby','disp_number','pageno','rnd'];
        $.each(hiddenValues, function(){
            // 商品別のフォームに検索条件の値があれば上書き
            if (cartForm.has('input[name='+this+']').length != 0) {
                cartForm.find('input[name='+this+']').val(searchForm.find('input[name='+this+']').val());
            }
            // なければ追加
            else {
                cartForm.append($('<input type="hidden" />').attr("name", this).val(searchForm.find('input[name='+this+']').val()));
            }
        });
        // 商品別のフォームを送信
        cartForm.submit();
    }
//]]></script>
    <h2 class="title"><!--{$tpl_subtitle|h}--></h2>
    <!--{* ▼下位カテゴリ *}-->
	<!--{if $arrSubCatList|@count > 0}-->
	        <ul class="subcatlist_area">
	        <!--{foreach from=$arrSubCatList item=category}-->
           
            <li><a class="heightLine" href="<!--{$smarty.const.ROOT_URLPATH}-->products/list.php?category_id=<!--{$category.category_id|h}-->"><!--{$category.category_name|h}--></a>
            </li>
            
                                
	        <!--{/foreach}-->
	        </ul>
	<!--{/if}-->
	<!--{* ▲下位カテゴリ *}-->
    
<div>
<!--{if $arrProducts}-->

<!--{* ▼件数 *}-->
<p class="intro">
    <span class="attention"><span id="productscount"><!--{$tpl_linemax}--></span>件</span>の商品がございます。
</p>
<!--{* ▲件数 *}-->
</div>

<!--{* ▼ページナビ(本文) *}-->
<section class="pagenumberarea cf">
    <ul>
        <li>
            <a<!--{if $orderby eq "price"}--> class="active"<!--{/if}--> href="javascript:fnChangeOrderby('price');" rel="external">価格順</a>
        </li>
        <li>
            <a<!--{if $orderby eq "date"}--> class="active"<!--{/if}--> href="javascript:fnChangeOrderby('date');" rel="external">新着順</a>
        </li>
    </ul>
</section>
<!--{* ▲ページナビ(本文) *}-->

<form name="form1" id="form1" method="get" action="?">
    <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
    <input type="hidden" name="mode" value="<!--{$mode|h}-->" />
    <!--{* ▼検索条件 *}-->
    <input type="hidden" name="category_id" value="<!--{$arrSearchData.category_id|h}-->" />
    <input type="hidden" name="maker_id" value="<!--{$arrSearchData.maker_id|h}-->" />
    <input type="hidden" name="name" value="<!--{$arrSearchData.name|h}-->" />
    <!--{* ▲検索条件 *}-->
    <!--{* ▼ページナビ関連 *}-->
    <input type="hidden" name="orderby" value="<!--{$orderby|h}-->" />
    <input type="hidden" name="disp_number" value="<!--{$disp_number|h}-->" />
    <input type="hidden" name="pageno" value="<!--{$tpl_pageno|h}-->" />
    <!--{* ▲ページナビ関連 *}-->
    <input type="hidden" name="rnd" value="<!--{$tpl_rnd|h}-->" />
</form>

<div class="product_area">
    <!--▼商品-->
    <!--{foreach from=$arrProducts item=arrProduct name=arrProducts}-->
    <!--{assign var=id value=$arrProduct.product_id}-->
    <!--{assign var=arrErr value=$arrProduct.arrErr}-->
    <a class="list_area" href="<!--{$smarty.const.P_DETAIL_URLPATH}--><!--{$arrProduct.product_id|u}-->"> 
        <!--{* ▼商品画像 *}-->
        <p class="listphoto"><img src="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$arrProduct.main_list_image|sfNoImageMainList|h}-->"  alt="<!--{$arrProduct.name|h}-->" /></p>
        <!--{* ▲商品画像 *}-->

        <div class="listrightblock">
            <!--{* ▼商品ステータス *}-->
            <div class="statusArea"> 
                <!--{if count($productStatus[$id]) > 0}-->
                <ul class="status_icon">
                    <!--{foreach from=$productStatus[$id] item=status}-->
                    <li class="icon<!--{$status|string_format:"%02d"}-->"><!--{$arrSTATUS[$status]}--></li>
                    <!--{/foreach}-->
                </ul>
                <!--{/if}-->
            </div>
            <!--{* ▲商品ステータス *}-->

            <!--{* ▼商品名 *}-->
            <h3><!--{$arrProduct.name|h}--></h3>
            <!--{* ▲商品名 *}-->

            <!--{* ▼商品価格 *}-->
            <p>
                <span class="price">
                    <span id="price02_default_<!--{$id}-->">
                        <!--{if $arrProduct.price02_min_inctax == $arrProduct.price02_max_inctax}-->
                            <!--{$arrProduct.price02_min_inctax|number_format}-->
                        <!--{else}-->
                            <!--{$arrProduct.price02_min_inctax|number_format}-->～<!--{$arrProduct.price02_max_inctax|number_format}-->
                        <!--{/if}-->
                    </span><span id="price02_dynamic_<!--{$id}-->"></span>
                    円
                </span>
            </p>
            <!--{* ▲商品価格 *}-->
        </div>
        <!--{* ▼商品コメント *}-->
        <p class="listcomment"><!--{$arrProduct.main_list_comment|h|nl2br}--></p>
        <!--{* ▲商品コメント *}-->
    </a>
    <!--{/foreach}-->
    <!--▲商品-->
</div>

<!--▼ページネーション-->
<!--{if $objNavi->max_page > 1}-->
<!--{assign var=arrPagenavi value=$objNavi->arrPagenavi}-->
<nav class="pagination">
    <ul>
    <!--{if $objNavi->now_page > 1}-->
    <li class="prev"><a href="?pageno=<!--{$arrPagenavi.before}-->" onclick="eccube.movePage('<!--{$arrPagenavi.before}-->'); return false;">&lt;</a></li>
    <!--{/if}-->

    <!--{assign var=first_num value=$objNavi->now_page-$smarty.const.NAVI_PMAX+1}-->
    <!--{assign var=last_num  value=$objNavi->now_page+$smarty.const.NAVI_PMAX-1}-->

    <!--{foreach from=$arrPagenavi.arrPageno item="dispnum" key="num" name="page_navi"}-->
    <!--{if $first_num == $dispnum}-->
    <li class="first"><a href="?pageno=<!--{$dispnum}-->" onclick="eccube.movePage('<!--{$dispnum}-->'); return false;"><!--{$dispnum}--></a></li>
    <!--{elseif $last_num == $dispnum}-->
    <li class="last"><a href="?pageno=<!--{$dispnum}-->" onclick="eccube.movePage('<!--{$dispnum}-->'); return false;"><!--{$dispnum}--></a></li>
    <!--{elseif $dispnum == $objNavi->now_page}-->
    <li class="active"><a href="?pageno=<!--{$dispnum}-->" onclick="eccube.movePage('<!--{$dispnum}-->'); return false;"><!--{$dispnum}--></a></li>
    <!--{else}-->
    <li><a href="?pageno=<!--{$dispnum}-->" onclick="eccube.movePage('<!--{$dispnum}-->'); return false;"><!--{$dispnum}--></a></li>
    <!--{/if}-->
    <!--{/foreach}-->

    <!--{if $objNavi->now_page < $objNavi->max_page}-->
    <li class="next"><a href="?pageno=<!--{$arrPagenavi.next}-->" onclick="eccube.movePage('<!--{$arrPagenavi.next}-->'); return false;">&gt;</a></li>
    <!--{/if}-->
    </ul>
</nav>
<!--{/if}-->
<!--▲ページネーション-->
<!--<div id="topicpath">
    <ol>
        <li><a href="<!--{$smarty.const.TOP_URLPATH}-->">TOP</a></li>
        <!--{foreach from=$plg_responsiveL0084_topiclist item=topic_data name=topic}-->
        <!--{if $topic_data.link != ""}-->
        <li><a href="<!--{$topic_data.link|h}-->"><!--{$topic_data.name|h}--></a></li>
        <!--{else}-->
        <li class="last"><!--{$topic_data.name|h}--></li>
        <!--{/if}-->
        <!--{/foreach}-->
    </ol>
</div>-->


<!--{else}-->
<!--{include file="frontparts/search_zero.tpl"}-->
<!--{/if}-->
