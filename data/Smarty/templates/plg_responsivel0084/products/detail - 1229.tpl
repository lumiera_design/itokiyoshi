<!--{*
 * ResponsiveL0084
 * Copyright (C) 2013 LOCKON CO.,LTD. All Rights Reserved.
 * http://www.lockon.co.jp/
 *}-->
<script type="text/javascript">
    $(window).load(function() {
      $('#detailphotoblock').flexslider({
            animation: "slide",
            slideshow: false,
            controlNav: "thumbnails",
			directionNav: false,
            animationLoop: false
        });
      $('#detailphotoblock .slides a, .subphotoimg a').colorbox({maxWidth:'98%',maxHeight:'98%',initialWidth:'50%',initialHeight:'50%',speed:200});

      // カテゴリが存在していない場合、通常商品なので不要な項目を非表示にする
      if(!$('select[name=classcategory_id1]').legth) { // 規格1の値が取得できない場合
          $('[id^=product_code_dynamic]').hide(); // 規格用選択時表示商品コード
          $('[id^=cartbtn_dynamic]').hide();      // 規格用在庫切れメッセージ
          $('[id^=price01_dynamic]').hide();      // 規格用選択時表示通常価格
          $('[id^=price02_dynamic]').hide();      // 規格用選択時表示販売価格
          $('[id^=point_dynamic]').hide();        // 規格用選択時表示ポイント
      }
    });
</script>
<script type="text/javascript">//<![CDATA[
    // 規格2に選択肢を割り当てる。
    function fnSetClassCategories(form, classcat_id2_selected) {
        var $form = $(form);
        var product_id = $form.find('input[name=product_id]').val();
        var $sele1 = $form.find('select[name=classcategory_id1]');
        var $sele2 = $form.find('select[name=classcategory_id2]');
        eccube.setClassCategories($form, product_id, $sele1, $sele2, classcat_id2_selected);
    }
//]]></script>

<div id="topicpath">
    <!--{section name=r loop=$arrRelativeCat}-->
    <ol>
        <li><a href="<!--{$smarty.const.TOP_URLPATH}-->">TOP</a></li>
        <!--{section name=s loop=$arrRelativeCat[r]}-->
        <li><a href="<!--{$smarty.const.ROOT_URLPATH}-->products/list.php?category_id=<!--{$arrRelativeCat[r][s].category_id}-->"><!--{$arrRelativeCat[r][s].category_name|h}--></a></li>
        <!--{/section}-->
        <li class="last"><!--{$arrProduct.name|h}--></li>
    </ol>
    <!--{/section}-->
</div>

<section id="product_detail"> 
    <!--{* ▼画像 *}-->
    <div id="detailphotoblock" class="cf flexslider">
        <ul class="slides">
            <!--{assign var=key value="main_image"}-->
            <!--{if $arrProduct.main_large_image|strlen >= 1}-->
            <li data-thumb="<!--{$arrFile[$key].filepath|h}-->"><a href="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$arrProduct.main_large_image|h}-->" target="_blank"><img src="<!--{$arrFile[$key].filepath|h}-->" alt="<!--{$arrProduct.name|h}-->" /></a></li>
            <!--{else}-->
            <li data-thumb="<!--{$arrFile[$key].filepath|h}-->"><img src="<!--{$arrFile[$key].filepath|h}-->" alt="<!--{$arrProduct.name|h}-->" /></li>
            <!--{/if}-->
            <!--{section name=p_cnt loop=$smarty.const.PRODUCTSUB_MAX}-->
                <!--{assign var=p_ikey value="sub_image`$smarty.section.p_cnt.index+1`"}-->
                <!--{if $arrProduct[$p_ikey]|strlen >= 1}-->
                    <!--{assign var=p_lkey value="sub_large_image`$smarty.section.p_cnt.index+1`"}-->
                    <!--{if $arrProduct[$p_lkey]|strlen >= 1}-->
                    <li data-thumb="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$arrProduct[$p_ikey]|sfNoImageMainList|h}-->"> <a href="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$arrProduct[$p_lkey]|sfNoImageMainList|h}-->" target="_blank"> <img src="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$arrProduct[$p_ikey]|sfNoImageMainList|h}-->" alt="<!--{$arrProduct.name|h}-->" /></a> </li>
                    <!--{else}-->
                    <li data-thumb="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$arrProduct[$p_ikey]|sfNoImageMainList|h}-->"><img src="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$arrProduct[$p_ikey]|sfNoImageMainList|h}-->" alt="<!--{$arrProduct.name|h}-->" /></li>
                    <!--{/if}-->
                <!--{/if}-->
            <!--{/section}-->
        </ul>
    </div>

    <section id="detailarea" class="cf"> 
    <!--{* ▼商品ステータス *}-->
    <!--{assign var=ps value=$productStatus[$tpl_product_id]}-->
    <!--{if count($ps) > 0}-->
    <ul class="status_icon">
        <!--{foreach from=$ps item=status}-->
        <li class="icon<!--{$status|string_format:"%02d"}-->"><!--{$arrSTATUS[$status]}--></li>
        <!--{/foreach}-->
    </ul>
    <!--{/if}-->
    <!--{* ▲商品ステータス *}-->

    <!--{* ▼商品名 *}-->
    <h3 class="product_name"><!--{$arrProduct.name|h}--></h3>
    <!--{* ▲商品名 *}-->

    <form name="form1" id="form1" method="post" action="?">
        <div class="product_detail">
            <!--{* ▼詳細メインコメント *}-->
            <p class="main_comment"><!--{$arrProduct.main_comment|nl2br_html}--></p>
            <!--{* ▲詳細メインコメント *}-->

            <!--{* ▼メーカー *}-->
            <!--{if $arrProduct.maker_name|strlen >= 1}-->
            <p class="maker">メーカー：<span id="maker_default"><!--{$arrProduct.maker_name|h}--></span></p>
            <!--{/if}-->
            <!--{* ▲メーカー *}-->

            <!--{* ▼メーカーURL *}-->
            <!--{if $arrProduct.comment1|strlen >= 1}-->
            <p class="maker">メーカーURL：<span id="maker_default"><a href="<!--{$arrProduct.comment1|h}-->"><!--{$arrProduct.comment1|h}--></a></span></p>
            <!--{/if}-->
            <!--{* ▲メーカーURL *}-->

            <!--{* ▼商品コード *}-->
            <p class="product_code">商品コード：
                <span id="product_code_default">
                    <!--{if $arrProduct.product_code_min == $arrProduct.product_code_max}-->
                        <!--{$arrProduct.product_code_min|h}-->
                    <!--{else}-->
                        <!--{$arrProduct.product_code_min|h}-->～<!--{$arrProduct.product_code_max|h}-->
                    <!--{/if}-->
                </span><span id="product_code_dynamic"></span>
            </p>
            <!--{* ▲商品コード *}-->

            <!--{* ▼通常価格 *}-->
            <!--{if $arrProduct.price01_min_inctax > 0}-->
            <p class="normal_price">
                <span class="small"><!--{$smarty.const.NORMAL_PRICE_TITLE}-->：</span>
                <span id="price01_default">
                    <!--{if $arrProduct.price01_min_inctax == $arrProduct.price01_max_inctax}-->
                        <!--{$arrProduct.price01_min_inctax|number_format}-->
                    <!--{else}-->
                        <!--{$arrProduct.price01_min_inctax|number_format}-->～<!--{$arrProduct.price01_max_inctax|number_format}-->
                    <!--{/if}-->
                </span><span id="price01_dynamic"></span>円<span class="small">(税込)</span>
            </p>
            <!--{/if}-->
            <!--{* ▲通常価格 *}-->

            <!--{* ▼販売価格 *}-->
            <p class="sale_price">
                <span class="small"><!--{$smarty.const.SALE_PRICE_TITLE}-->：</span>
                <span class="price">
                    <span id="price02_default">
                        <!--{if $arrProduct.price02_min_inctax == $arrProduct.price02_max_inctax}-->
                            <!--{$arrProduct.price02_min_inctax|number_format}-->
                        <!--{else}-->
                            <!--{$arrProduct.price02_min_inctax|number_format}-->～<!--{$arrProduct.price02_max_inctax|number_format}-->
                        <!--{/if}-->
                    </span><span id="price02_dynamic"></span>
                </span>円<span class="small">(税込)</span>
            </p>
            <!--{* ▲販売価格 *}-->

            <!--{* ▼ポイント *}-->
            <!--{if $smarty.const.USE_POINT !== false}-->
            <p class="point">
                <span>
                    <span id="point_default">
                        <!--{if $arrProduct.price02_min == $arrProduct.price02_max}-->
                            <!--{$arrProduct.price02_min|sfPrePoint:$arrProduct.point_rate|number_format}-->
                        <!--{else}-->
                            <!--{if $arrProduct.price02_min|sfPrePoint:$arrProduct.point_rate == $arrProduct.price02_max|sfPrePoint:$arrProduct.point_rate}-->
                                <!--{$arrProduct.price02_min|sfPrePoint:$arrProduct.point_rate|number_format}-->
                            <!--{else}-->
                                <!--{$arrProduct.price02_min|sfPrePoint:$arrProduct.point_rate|number_format}-->～<!--{$arrProduct.price02_max|sfPrePoint:$arrProduct.point_rate|number_format}-->
                            <!--{/if}-->
                        <!--{/if}-->
                    </span><span id="point_dynamic"></span>
                </span>ポイント獲得！
            </p>
            <!--{/if}-->
            <!--{* ▲ポイント *}-->
        </div>

        <div class="cartarea_wrap">
            <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
            <input type="hidden" name="mode" value="cart" />
            <input type="hidden" name="product_id" value="<!--{$tpl_product_id}-->" />
            <input type="hidden" name="product_class_id" value="<!--{$tpl_product_class_id}-->" id="product_class_id" />
            <input type="hidden" name="favorite_product_id" value="" />

            <!--{if $tpl_stock_find}-->
            <!--▼買い物かご-->
            <div class="cart_area">
                <!--{if $tpl_classcat_find1}-->
                <ul>
                    <!--{* ▼規格1 *}-->
                    <li>
                        <!--{$tpl_class_name1|h}--><br />
                        <select name="classcategory_id1"<!--{if $arrErr.classcategory_id1 != ""}--> class="error"<!--{/if}-->>
                            <!--{html_options options=$arrClassCat1 selected=$arrForm.classcategory_id1.value}-->
                        </select>
                        <!--{if $arrErr.classcategory_id1 != ""}-->
                        <div class="attention">※ <!--{$tpl_class_name1}-->を入力して下さい。</div>
                        <!--{/if}-->
                    </li>
                    <!--{* ▲規格1 *}-->

                    <!--{* ▼規格2 *}-->
                    <!--{if $tpl_classcat_find2}-->
                    <li>
                        <!--{$tpl_class_name2|h}--><br />
                        <select name="classcategory_id2"<!--{if $arrErr.classcategory_id2 != ""}--> class="error"<!--{/if}-->>
                        </select>
                        <!--{if $arrErr.classcategory_id2 != ""}-->
                        <div class="attention">※ <!--{$tpl_class_name2}-->を入力して下さい。</div>
                        <!--{/if}-->
                    </li>
                    <!--{/if}-->
                    <!--{* ▲規格2 *}-->
                </ul>
                <!--{/if}-->

                <!--{* ▼数量 *}-->
                <dl class="quantity">
                    <dt>数量</dt>
                    <dd>
                        <input type="number" name="quantity" <!--{if $arrErr.quantity != ""}-->class="quantitybox error"<!--{else}-->class="quantitybox"<!--{/if}--> value="<!--{$arrForm.quantity.value|default:1|h}-->" maxlength="<!--{$smarty.const.INT_LEN}-->" max="9" />
                        <!--{if $arrErr.quantity != ""}-->
                            <div class="attention"><!--{$arrErr.quantity}--></div>
                        <!--{/if}-->
                    </dd>
                </dl>
                <!--{* ▲数量 *}-->
            </div>
            <!--{/if}-->
            <!--▲買い物かご-->

            <!--{* ▼カートに入れる *}-->
            <div class="cartin_btn">
                <!--{if $tpl_stock_find}-->
                <div id="cartbtn_default" class="button05"><a rel="external" href="javascript:void(document.form1.submit());"><span>カートに入れる</span></a></div>
                <div class="attention" id="cartbtn_dynamic"></div>
                <!--{else}-->
                <div class="attention">申し訳ございませんが、只今品切れ中です。</div>
                <!--{/if}-->

                <!--{* ▼お気に入り登録 *}-->
                <!--{if $smarty.const.OPTION_FAVORITE_PRODUCT == 1 && $tpl_login === true}-->
                <!--{assign var=add_favorite value="add_favorite`$product_id`"}-->
                <!--{if !$is_favorite}-->
                <div class="btn_favorite">
                    <p class="button03"><button type="button" onclick="javascript:eccube.changeAction('?product_id=<!--{$arrProduct.product_id|h}-->'); eccube.setModeAndSubmit('add_favorite','favorite_product_id','<!--{$arrProduct.product_id|h}-->');"><span>お気に入りに追加</span></button></p>
                </div>
                <!--{else}-->
                <div class="btn_favorite disabled">
                    <p class="button03"><button type="button" disabled="disabled"><span>お気に入り登録済</span></button></p>
                </div>
                <!--{if $arrErr[$add_favorite]}-->
                <div class="attention"><!--{$arrErr[$add_favorite]}--></div>
                <!--{/if}-->
                <!--{/if}-->
                <!--{/if}-->
                <!--{* ▲お気に入り登録 *}-->
            </div>
        </div>
    </form>
    </section>
    <!--詳細ここまで-->

    <div id="sub_area"> 
        <!--{assign var=subarea_flg value="0"}-->
        <!--{section name=cnt loop=$smarty.const.PRODUCTSUB_MAX}-->
        <!--{assign var=key value="sub_title`$smarty.section.cnt.index+1`"}-->
        <!--{if $arrProduct[$key] != "" or $arrProduct[$ikey]|strlen >= 1}-->
        <!--{assign var=subarea_flg value="1"}-->
        <!--{/if}-->
        <!--{/section}-->
        <!--{if $subarea_flg eq "1"}-->
        <h2 class="sp">商品情報</h2>
        <!--{/if}-->

        <!--{* ▼サブ情報 *}-->
        <!--{section name=cnt loop=$smarty.const.PRODUCTSUB_MAX}-->
        <!--{assign var=key value="sub_title`$smarty.section.cnt.index+1`"}-->
        <!--{assign var=ikey value="sub_image`$smarty.section.cnt.index+1`"}-->
        <!--{if $arrProduct[$key] != "" or $arrProduct[$ikey]|strlen >= 1}-->
        <div class="subarea cf">
            <h3 class="heading02"><!--{$arrProduct[$key]|h}--></h3>
            <!--{assign var=ckey value="sub_comment`$smarty.section.cnt.index+1`"}-->

            <!--{if $arrProduct[$ikey]|strlen >= 1}-->
            <p class="subphotoimg">
                <!--{assign var=lkey value="sub_large_image`$smarty.section.cnt.index+1`"}-->
                <!--{if $arrProduct[$lkey]|strlen >= 1}-->
                <a href="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$arrProduct[$lkey]|h}-->" target="_blank" ><img alt="<!--{$arrProduct.name|h}-->" src="<!--{$arrFile[$ikey].filepath}-->" /></a>
                <!--{else}-->
                <img alt="<!--{$arrProduct.name|h}-->" src="<!--{$arrFile[$ikey].filepath}-->" />
                <!--{/if}-->
            </p>
            <!--{/if}-->
            <p class="subtext"><!--★サブテキスト★--><!--{$arrProduct[$ckey]|nl2br_html}--></p>
        </div>
        <!--{/if}-->
        <!--{/section}-->
        <!--{* ▲サブ情報 *}-->
    </div>

    <!--{* ▼この商品に対するお客様の声 *}-->
    <div id="review_bloc_area">
        <h2>お客様の声</h2>

        <div class="review_bloc cf">
            <p>この商品に対するご感想をぜひお寄せください。</p>
            <!--★新規コメントを書き込む★-->
            <!--{if count($arrReview) < $smarty.const.REVIEW_REGIST_MAX}-->
            <div class="review_btn button01"> 
                <a href="./review.php?product_id=<!--{$arrProduct.product_id}-->" /><span>新規コメントを書き込む</span></a>
            </div>
            <!--{/if}-->
        </div>

        <!--{if count($arrReview) > 0}-->
        <ul>
            <!--{section name=cnt loop=$arrReview}-->
            <li>
                <p class="recommend_writer">
                    <!--{if $arrReview[cnt].reviewer_url}-->
                    <a href="<!--{$arrReview[cnt].reviewer_url}-->" target="_blank"><!--{$arrReview[cnt].reviewer_name|h}--> 様</a>
                    <!--{else}-->
                    <!--{$arrReview[cnt].reviewer_name|h}--> 様
                    <!--{/if}-->
                </p>
                <p class="level_date">評価：<span class="recommend_level"><!--{assign var=level value=$arrReview[cnt].recommend_level}--><!--{$arrRECOMMEND[$level]|h}--></span><span class="voicedate">投稿日：<!--{$arrReview[cnt].create_date|sfDispDBDate:false}--></span></p>
                <p class="voicetitle"><!--{$arrReview[cnt].title|h}--></p>
                <p class="voicecomment"><!--{$arrReview[cnt].comment|h|nl2br}--></p>
            </li>
            <!--{/section}-->
        </ul>
        <!--{/if}-->
    </div>
    <!--{* ▲この商品に対するお客様の声 *}-->

     <!--▼その他のおすすめ商品-->
    <!--{if $arrRecommend}-->
    <div class="block_outer">
        <div id="recommend_area">
            <h2>ピックアップアイテム</h2>
            <div id="slider" class="block_body cf flexslider">
                <ul class="slides">
                    <!--{foreach from=$arrRecommend item=arrItem name="arrRecommend"}-->
                    <li class="product_item">
                        <div class="productImage"> <a href="<!--{$smarty.const.P_DETAIL_URLPATH}--><!--{$arrItem.product_id|u}-->"> <img alt="<!--{$arrItem.name|h}-->" src="<!--{$smarty.const.ROOT_URLPATH}-->resize_image.php?image=<!--{$arrItem.main_image|sfNoImageMainList|h}-->"> </a> </div>
                        <div class="productContents">
                            <h3> <a href="<!--{$smarty.const.P_DETAIL_URLPATH}--><!--{$arrItem.product_id|u}-->"><!--{$arrItem.name|h}--></a> </h3>
                            
                            <!--{assign var=price02_min value=`$arrItem.price02_min_inctax`}-->
                            <!--{assign var=price02_max value=`$arrItem.price02_max_inctax`}-->
                            <p class="sale_price">
                                <span class="price">
                                    <!--{if $price02_min == $price02_max}-->
                                        <!--{$price02_min|number_format}-->
                                    <!--{else}-->
                                        <!--{$price02_min|number_format}-->～<!--{$price02_max|number_format}-->
                                    <!--{/if}-->
                                    円
                                </span>
                            </p>
                        </div>
                    </li>
                    <!--{/foreach}-->
                </ul>
            </div>
        </div>
    </div>
    <!--{/if}-->
    <!--▲その他のおすすめ商品-->
</section>
