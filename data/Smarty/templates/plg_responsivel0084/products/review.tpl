<!--{*
 * ResponsiveL0084
 * Copyright (C) 2013 LOCKON CO.,LTD. All Rights Reserved.
 * http://www.lockon.co.jp/
 *}-->

<div id="mycontents_area">
    <h2 class="title">お客様の声書き込み</h2>
    <!--{include file=$tpl_navi}-->
    <div class="inner">
        <p><span class="attention">※</span>は入力必須項目です。</p>

        <form name="form1" id="form1" method="post" action="?">
            <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
            <input type="hidden" name="mode" value="confirm" />
            <input type="hidden" name="product_id" value="<!--{$arrForm.product_id|h}-->" />
            <div class="table voice" id="member_table">
                <div class="tbody">
                    <dl class="tr">
                        <dt>商品名</dt>
                        <dd>
                            <!--{$arrForm.name|h}-->
                        </dd>
                    </dl>
                    <dl class="tr">
                        <dt>投稿者名&nbsp;<span class="attention">※</span></dt>
                        <dd>
                            <input<!--{if $arrErr.reviewer_name}--> class="error"<!--{/if}--> type="text" name="reviewer_name" value="<!--{$arrForm.reviewer_name|h}-->" maxlength="<!--{$smarty.const.STEXT_LEN}-->" />
                            <!--{if $arrErr.reviewer_name}-->
                                <div class="attention"><!--{$arrErr.reviewer_name}--></div>
                            <!--{/if}-->
                        </dd>
                    </dl>
                    <dl class="tr">
                        <dt>投稿者URL</dt>
                        <dd>
                            <input<!--{if $arrErr.reviewer_url}--> class="error"<!--{/if}--> type="text" name="reviewer_url" value="<!--{$arrForm.reviewer_url|h}-->" maxlength="<!--{$smarty.const.MTEXT_LEN}-->" />
                            <!--{if $arrErr.reviewer_url}-->
                                <div class="attention"><!--{$arrErr.reviewer_url}--></div>
                            <!--{/if}-->
                        </dd>
                    </dl>
                    <dl class="tr">
                        <dt>性別</dt>
                        <dd>
                            <input<!--{if $arrErr.sex}--> class="error"<!--{/if}--> type="radio" id="man" name="sex" value="1" <!--{if $arrForm.sex eq 1}--> checked="checked"<!--{/if}--> />
                            <label for="man">男性</label>
                            &nbsp;&nbsp;
                            <input<!--{if $arrErr.sex}--> class="error"<!--{/if}--> type="radio" id="woman" name="sex" value="2" <!--{if $arrForm.sex eq 2}--> checked="checked"<!--{/if}--> />
                            <label for="woman">女性</label>
                            <!--{if $arrErr.sex}-->
                                <div class="attention"><!--{$arrErr.sex}--></div>
                            <!--{/if}-->
                        </dd>
                    </dl>
                    <dl class="tr">
                        <dt>おすすめレベル&nbsp;<span class="attention">※</span></dt>
                        <dd>
                            <select name="recommend_level"  <!--{if $arrErr.recommend_level}-->class="error"<!--{/if}-->>
                                <option value="" selected="selected">選択してください</option>
                                <!--{html_options options=$arrRECOMMEND selected=$arrForm.recommend_level}-->
                            </select>
                            <!--{if $arrErr.recommend_level}-->
                                <div class="attention"><!--{$arrErr.recommend_level}--></div>
                            <!--{/if}-->
                        </dd>
                    </dl>
                    <dl class="tr">
                        <dt>タイトル&nbsp;<span class="attention">※</span></dt>
                        <dd>
                            <input<!--{if $arrErr.title}--> class="error"<!--{/if}--> type="text" name="title" value="<!--{$arrForm.title|h}-->" maxlength="<!--{$smarty.const.STEXT_LEN}-->" />
                            <!--{if $arrErr.title}-->
                                <div class="attention"><!--{$arrErr.title}--></div>
                            <!--{/if}-->
                        </dd>
                    </dl>
                    <dl class="tr">
                        <dt>コメント&nbsp;<span class="attention">※</span></dt>
                        <dd>
                            <textarea<!--{if $arrErr.comment}--> class="error"<!--{/if}--> name="comment" cols="50" rows="10"><!--{"\n"}--><!--{$arrForm.comment|h}--></textarea>
                            <!--{if $arrErr.comment}-->
                                <div class="attention"><!--{$arrErr.comment}--></div>
                            <!--{/if}-->
                        </dd>
                    </dl>
                </div>
            </div>
            <div class="btn_area">
                <p class="button02">
                    <button type="submit" name="conf" id="conf"><span>確認ページへ</span></button>
                </p>
            </div>
        </form>
    </div>
</div>
