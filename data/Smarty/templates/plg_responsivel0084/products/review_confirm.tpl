<!--{*
 * ResponsiveL0084
 * Copyright (C) 2013 LOCKON CO.,LTD. All Rights Reserved.
 * http://www.lockon.co.jp/
 *}-->

<div id="mycontents_area">
    <h2 class="title">お客様の声書き込み</h2>
    <!--{include file=$tpl_navi}-->
    <div class="inner">
        <form name="form1" id="form1" method="post" action="?">
            <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
            <input type="hidden" name="mode" value="complete" />
            <!--{foreach from=$arrForm key=key item=item}-->
                <!--{if $key ne "mode"}-->
                    <input type="hidden" name="<!--{$key|h}-->" value="<!--{$item|h}-->" />
                <!--{/if}-->
            <!--{/foreach}-->

            <div class="table voice" id="member_table">
                <div class="tbody">
                    <dl class="tr">
                        <dt>商品名</dt>
                        <dd>
                            <!--{$arrForm.name|h}-->
                        </dd>
                    </dl>
                    <dl class="tr">
                        <dt>投稿者名</dt>
                        <dd>
                            <!--{$arrForm.reviewer_name|h}-->
                        </dd>
                    </dl>
                    <dl class="tr">
                        <dt>投稿者URL</dt>
                        <dd>
                            <!--{$arrForm.reviewer_url|h}-->
                        </dd>
                    </dl>
                    <dl class="tr">
                        <dt>性別</dt>
                        <dd>
                            <!--{if $arrForm.sex eq 1}-->男性<!--{elseif $arrForm.sex eq 2}-->女性<!--{/if}-->
                        </dd>
                    </dl>
                    <dl class="tr">
                        <dt>おすすめレベル</dt>
                        <dd>
                            <!--{$arrRECOMMEND[$arrForm.recommend_level]}-->
                        </dd>
                    </dl>
                    <dl class="tr">
                        <dt>タイトル</dt>
                        <dd>
                            <!--{$arrForm.title|h}-->
                        </dd>
                    </dl>
                    <dl class="tr">
                        <dt>コメント</dt>
                        <dd>
                            <!--{$arrForm.comment|h|nl2br}-->
                        </dd>
                    </dl>
                </div>
            </div>
            <ul class="btn_area">
                <li class="button02">
                    <button type="submit" name="send" id="send"><span>送信する</span></button>
                </li>
                <li class="button03">
                    <button type="submit" onclick=" mode.value='return';" name="back" id="back"><span>戻る</span></button>
                </li>
            </ul>
        </form>
    </div>
</div>