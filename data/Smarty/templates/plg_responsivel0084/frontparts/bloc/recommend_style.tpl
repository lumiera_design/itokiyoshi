<!--{*
 * ResponsiveL0084
 * Copyright (C) 2013 LOCKON CO.,LTD. All Rights Reserved.
 * http://www.lockon.co.jp/
 *}-->
<script type="text/javascript">
	$(window).load(function() {
	  $('#slider2').flexslider({
		animation: "slide",
		itemWidth: 210,
		minItems: 3,
		maxItems: 3,
		controlNav: false,
		animationLoop: false
	  });
	});
</script>
 
<div class="block_outer">
    <div id="recommend_style_area">
        <h2 class="top_heading"><span>オススメ商品</span><a href="#">オススメ商品一覧</a></h2>
        <div id="slider2" class="block_body cf flexslider">
            <ul class="slides">
                <li class="product_item">
                    <div class="productImage"> <a href="/products/detail.php?product_id=2"> <img alt="Terevy 24型地上デジタルフルハイビジョンLED液晶テレビ TRV-LED240B" src="<!--{$TPL_URLPATH}-->img/product_img04.jpg"> </a> </div>
                    <div class="productContents">
                        <h3> <a href="/products/detail.php?product_id=1">★即納★【在庫あり送料無料】Terevy 24型地上デジタルフルハイビジョンLED液晶テレビ TRV-LED240B...</a> </h3>
                        <p class="sale_price"><span class="price">980 <span class="unit">円</span></span> </p>
                    </div>
                </li>
                <li class="product_item">
                    <div class="productImage"> <a href="/products/detail.php?product_id=1"> <img alt="Terevy 24型地上デジタルフルハイビジョンLED液晶テレビ TRV-LED240B" src="<!--{$TPL_URLPATH}-->img/product_img05.jpg"> </a> </div>
                    <div class="productContents">
                        <h3> <a href="/products/detail.php?product_id=1">★即納★【在庫あり送料無料】Terevy 24型地上デジタルフルハイビジョンLED液晶テレビ TRV-LED240B...</a> </h3>
                        <p class="sale_price"><span class="price">980 <span class="unit">円</span></span> </p>
                    </div>
                </li>
                <li class="product_item">
                    <div class="productImage"> <a href="/products/detail.php?product_id=3"> <img alt="Terevy 24型地上デジタルフルハイビジョンLED液晶テレビ TRV-LED240B" src="<!--{$TPL_URLPATH}-->img/product_img06.jpg"> </a> </div>
                    <div class="productContents">
                        <h3> <a href="/products/detail.php?product_id=1">★即納★【在庫あり送料無料】Terevy 24型地上デジタルフルハイビジョンLED液晶テレビ TRV-LED240B...</a> </h3>
                        <p class="sale_price"><span class="price">980 <span class="unit">円</span></span> </p>
                    </div>
                </li>
                                <li class="product_item">
                    <div class="productImage"> <a href="/products/detail.php?product_id=1"> <img alt="Terevy 24型地上デジタルフルハイビジョンLED液晶テレビ TRV-LED240B" src="<!--{$TPL_URLPATH}-->img/product_img07.jpg"> </a> </div>
                    <div class="productContents">
                        <h3> <a href="/products/detail.php?product_id=1">★即納★【在庫あり送料無料】Terevy 24型地上デジタルフルハイビジョンLED液晶テレビ TRV-LED240B...</a> </h3>
                        <p class="sale_price"><span class="price">980 <span class="unit">円</span></span> </p>
                    </div>
                </li>
                <li class="product_item">
                    <div class="productImage"> <a href="/products/detail.php?product_id=2"> <img alt="Terevy 24型地上デジタルフルハイビジョンLED液晶テレビ TRV-LED240B" src="<!--{$TPL_URLPATH}-->img/product_img08.jpg"> </a> </div>
                    <div class="productContents">
                        <h3> <a href="/products/detail.php?product_id=1">★即納★【在庫あり送料無料】Terevy 24型地上デジタルフルハイビジョンLED液晶テレビ TRV-LED240B...</a> </h3>
                        <p class="sale_price"><span class="price">980 <span class="unit">円</span></span> </p>
                    </div>
                </li>
                            </ul>
        </div>
    </div>
</div>

