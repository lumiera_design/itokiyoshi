<!--{*


 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2007 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *}-->
<!--{if count($arrRssFeedBlock) > 0}-->

<script type="text/javascript" src="<!--{$smarty.const.ROOT_URLPATH}-->js/jquery.BlackAndWhite.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$(".bwWrapper").BlackAndWhite({
        hoverEffect : true, // tureにすると、ホバーしたときに白黒がカラーに変わる
 
        // BnWWorker.jsのパスを設定すると、超高速に対応
        webworkerPath : false,
 
        // trueにすると、レスポンシブ対応
        responsive:true,
 
        // trueにすると、ホバーしたときの白黒とカラーの状態を逆転
        invertHoverEffect: false,
 
        speed: { //ホバーしたときのアニメーションの速度を設定
            fadeIn: 100, // 200ミリ秒でフェードイン
            fadeOut: 500 // 800ミリ秒でフェードアウト
        }
    });
	});
</script>

<div class="block_outer clearfix">
<div id="plugin_rssfeed">
<h2>新着記事</h2>

<div class="block_body cf">

<ul><!--{foreach from=$arrRssFeedBlock item=item}-->
<li class="heightLine post_item">
	<div class="posttitle">

		<a href='<!--{$item->link}-->' target='_blank'><!--{$item->title|h}--></a></div>

		<div class="postdiscription bwWrapper">

		<a href='<!--{$item->link}-->' target='_blank'><!--{$item->description}--></a>

		</div>

	</li><!--{/foreach}-->
  





<!--{/if}-->