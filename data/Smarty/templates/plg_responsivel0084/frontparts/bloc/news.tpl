<!--{*
 * ResponsiveL0084
 * Copyright (C) 2013 LOCKON CO.,LTD. All Rights Reserved.
 * http://www.lockon.co.jp/
 *}-->
<div class="block_outer spadB0">
    <div id="news_area" class="inner">
        <div class="block_body">
            <div class="news_contents accordion">
                <dl class="newslist">
	            <!--{section name=data loop=$arrNews}-->
	            <!--{assign var="date_array" value="-"|explode:$arrNews[data].cast_news_date}-->
                   	<dt>
                        <span class="date"><!--{$date_array[0]}-->.<!--{$date_array[1]}-->.<!--{$date_array[2]}--></span>
                        <span class="news_title"><!--{$arrNews[data].news_title|h|nl2br}--></a></span>
                    </dt>
                    <dd>
                    <!--{$arrNews[data].news_comment|h|nl2br}--><br>
                    <!--{if $arrNews[data].news_url}--><a class="link01" href="<!--{$arrNews[data].news_url}-->" <!--{if $arrNews[data].link_method eq "2"}--> target="_blank"<!--{/if}-->>詳しくはこちら</a><!--{/if}-->
                    </dd>
	            <!--{/section}-->
                </dl>
            </div>
        </div>
    </div>
</div>
