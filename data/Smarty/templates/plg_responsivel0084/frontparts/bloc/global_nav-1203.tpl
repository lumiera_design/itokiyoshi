<!--{*
 * ResponsiveL0084
 * Copyright (C) 2013 LOCKON CO.,LTD. All Rights Reserved.
 * http://www.lockon.co.jp/
 *}-->
<div id="gnav">
    <div class="inner">
        <nav>
 <a class="gnav_cart" href="<!--{$smarty.const.ROOT_URLPATH}-->cart/"><p><span class="carticon"><img src="<!--{$TPL_URLPATH}-->img/btn_header_home.png" alt="Cart" width="29" /><span class="pctb">HOME</span></p></a>

            <a class="gnav_shopguide" href="#"><p><br class="sponly" /><span>オーダーメイド</span></p></a>
            <a class="gnav_shopguide" href="#"><p><br class="sponly" /><span>職人の技</span></p></a>
             <a class="gnav_shopguide" href="#"><p><br class="sponly" /><span>最高級羽毛布団</span></p></a>
             <a class="gnav_shopguide" href="#"><p><br class="sponly" /><span>原料について</span></p></a>
            <!--▼カート -->
            <a class="gnav_cart" href="<!--{$smarty.const.ROOT_URLPATH}-->cart/"><p><span class="carticon"><img src="<!--{$TPL_URLPATH}-->img/btn_header_cart.png" alt="Cart" width="29" /><!--{if $arrCartList.0.TotalQuantity > 0}--><span class="incart_count"><!--{$arrCartList.0.TotalQuantity|number_format|default:0}--></span></span><!--{/if}--><span class="pctb">Shopping Cart</span></p></a>
        </nav>
    </div>
</div>