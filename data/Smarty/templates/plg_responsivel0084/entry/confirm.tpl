<!--{*
 * ResponsiveL0084
 * Copyright (C) 2013 LOCKON CO.,LTD. All Rights Reserved.
 * http://www.lockon.co.jp/
 *}-->

<div id="mycontents_area">
    <h2 class="title"><!--{$tpl_title|h}--></h2>
    <div class="inner">
        <p>
            下記の内容で送信してもよろしいでしょうか？<br />
            よろしければ、一番下の「会員登録をする」ボタンをクリックしてください。
        </p>

        <form name="form1" id="form1" method="post" action="?">
            <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
            <input type="hidden" name="mode" value="complete" />
            <!--{foreach from=$arrForm key=key item=item}-->
                <input type="hidden" name="<!--{$key|h}-->" value="<!--{$item.value|h}-->" />
            <!--{/foreach}-->

            <div class="table member_table">
                <div class="tbody">
                <!--{include file="`$smarty.const.TEMPLATE_REALDIR`frontparts/form_personal_confirm.tpl" flgFields=3 emailMobile=false prefix=""}-->
                </div>
            </div>

            <ul class="btn_area">
                <li class="button02">
                    <button type="submit" name="send" id="send"><span>会員登録をする</span></button>
                </li>
                <li class="button03">
                    <button type="button" onclick="fnModeSubmit('return', '', ''); return false;"><span>戻る</span></button>
                </li>
            </ul>
        </form>
    </div>
</div>
