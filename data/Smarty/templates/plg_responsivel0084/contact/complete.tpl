<!--{*
 * ResponsiveL0084
 * Copyright (C) 2013 LOCKON CO.,LTD. All Rights Reserved.
 * http://www.lockon.co.jp/
 *}-->

<div id="mycontents_area">
    <h2 class="title"><!--{$tpl_title|h}--></h2>
    <div class="inner">
        <p>お問い合わせ内容の送信が完了いたしました。</p>
        <p>
            万一、ご回答メールが届かない場合は、トラブルの可能性もありますので<br />大変お手数ではございますがもう一度お問い合わせいただくか、お電話にてお問い合わせください。<br />
            今後ともご愛顧賜りますようよろしくお願い申し上げます。
        </p>
        <p><!--{$arrSiteInfo.company_name|h}--><br />
        <p>
            TEL：<!--{$arrSiteInfo.tel01}-->-<!--{$arrSiteInfo.tel02}-->-<!--{$arrSiteInfo.tel03}-->
            <!--{if $arrSiteInfo.business_hour != ""}-->
            (受付時間/<!--{$arrSiteInfo.business_hour}-->)
            <!--{/if}--><br />
            E-mail：<a href="mailto:<!--{$arrSiteInfo.email02|escape:'hex'}-->"><!--{$arrSiteInfo.email02|escape:'hexentity'}--></a></p>
        </p>

        <div class="btn_area">
            <p class="button02">
                <button type="button" onclick="location.href='<!--{$smarty.const.TOP_URLPATH}-->'"><span>トップページへ</span></button>
            </p>
        </div>
    </div>
</div>
