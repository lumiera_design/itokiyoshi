<!--{*
 * ResponsiveL0084
 * Copyright (C) 2013 LOCKON CO.,LTD. All Rights Reserved.
 * http://www.lockon.co.jp/
 *}-->

<div id="mycontents_area">
    <h2 class="title"><!--{$tpl_title|h}--></h2>
    <div class="inner">
        <p>
            内容によっては回答をさしあげるのにお時間をいただくこともございます。<br />
            また、休業日は翌営業日以降の対応となりますのでご了承ください。
        </p>

        <form name="form1" id="form1" method="post" action="?">
            <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
            <input type="hidden" name="mode" value="confirm" />
            <div class="table member_table">
                <div class="tbody">
                    <dl class="tr">
                        <dt>お名前&nbsp;<span class="attention">※</span></dt>
                        <dd>
                            <input<!--{if $arrErr.name01}--> class="error"<!--{/if}--> type="text" name="name01" value="<!--{$arrForm.name01.value|default:$arrData.name01|h}-->" maxlength="<!--{$smarty.const.STEXT_LEN}-->" placeholder="姓" />
                            &nbsp;&nbsp;
                            <input<!--{if $arrErr.name02}--> class="error"<!--{/if}--> type="text" name="name02" value="<!--{$arrForm.name02.value|default:$arrData.name02|h}-->" maxlength="<!--{$smarty.const.STEXT_LEN}-->" placeholder="名" />
                            <!--{if $arrErr.name01 || $arrErr.name02}-->
                                <div class="attention"><!--{$arrErr.name01}--><!--{$arrErr.name02}--></div>
                            <!--{/if}-->
                        </dd>
                    </dl>
                    <dl class="tr">
                        <dt>お名前(フリガナ)&nbsp;<span class="attention">※</span></dt>
                        <dd>
                            <input<!--{if $arrErr.kana01}--> class="error"<!--{/if}--> type="text" name="kana01" value="<!--{$arrForm.kana01.value|default:$arrData.kana01|h}-->" maxlength="<!--{$smarty.const.STEXT_LEN}-->" placeholder="セイ"/>
                            &nbsp;&nbsp;
                            <input<!--{if $arrErr.kana02}--> class="error"<!--{/if}--> type="text" name="kana02" value="<!--{$arrForm.kana02.value|default:$arrData.kana02|h}-->" maxlength="<!--{$smarty.const.STEXT_LEN}-->" placeholder="メイ"/>
                            <!--{if $arrErr.kana01 || $arrErr.kana02}-->
                                <div class="attention"><!--{$arrErr.kana01}--><!--{$arrErr.kana02}--></div>
                            <!--{/if}-->
                        </dd>
                    </dl>
                    <dl class="tr">
                        <dt>郵便番号</dt>
                        <dd class="zipcode">
                            <p>
                                <input<!--{if $arrErr.zip01}--> class="input_tel error"<!--{else}--> class="input_tel"<!--{/if}--> type="tel" name="zip01" value="<!--{$arrForm.zip01.value|default:$arrData.zip01|h}-->" maxlength="<!--{$smarty.const.ZIP01_LEN}-->" />
                                &nbsp;-&nbsp;
                                <input<!--{if $arrErr.zip02}--> class="input_tel error"<!--{else}--> class="input_tel"<!--{/if}--> type="tel" name="zip02" value="<!--{$arrForm.zip02.value|default:$arrData.zip02|h}-->" maxlength="<!--{$smarty.const.ZIP02_LEN}-->" />
                            </p>
                            <p class="button04"><a href="javascript:eccube.getAddress('<!--{$smarty.const.INPUT_ZIP_URLPATH}-->', 'zip01', 'zip02', 'pref', 'addr01');" ><span>住所自動入力</span></a></p>
                            <!--{if $arrErr.zip01 || $arrErr.zip02}-->
                                <div class="attention cl"><!--{$arrErr.zip01}--><!--{$arrErr.zip02}--></div>
                            <!--{/if}-->
                        </dd>
                    </dl>
                    <dl class="tr">
                        <dt>住所</dt>
                        <dd>
                            <select name="pref"<!--{if $arrErr.pref}--> class="error"<!--{/if}-->>
                                <option value="">都道府県</option>
                                <!--{html_options options=$arrPref selected=$arrForm.pref.value|default:$arrData.pref|h}-->
                            </select>
                            <input<!--{if $arrErr.addr01}--> class="error"<!--{/if}--> type="text" name="addr01" value="<!--{$arrForm.addr01.value|default:$arrData.addr01|h}-->" placeholder="<!--{$smarty.const.SAMPLE_ADDRESS1}-->" />
                            <input<!--{if $arrErr.addr02}--> class="error"<!--{/if}--> type="text" name="addr02" value="<!--{$arrForm.addr02.value|default:$arrData.addr02|h}-->" placeholder="<!--{$smarty.const.SAMPLE_ADDRESS2}-->" />
                            <!--{if $arrErr.pref || $arrErr.addr01 || $arrErr.addr02}-->
                                <div class="attention"><!--{$arrErr.pref}--><!--{$arrErr.addr01}--><!--{$arrErr.addr02}--></div>
                            <!--{/if}-->
                        </dd>
                    </dl>
                    <dl class="tr">
                        <dt>電話番号</dt>
                        <dd>
                            <input<!--{if $arrErr.tel01}--> class="input_tel error"<!--{else}--> class="input_tel"<!--{/if}--> type="tel" name="tel01" value="<!--{$arrForm.tel01.value|default:$arrData.tel01|h}-->" maxlength="<!--{$smarty.const.TEL_ITEM_LEN}-->" />&nbsp;-&nbsp;<input<!--{if $arrErr.tel02}--> class="input_tel error"<!--{else}--> class="input_tel"<!--{/if}--> type="tel" name="tel02" value="<!--{$arrForm.tel02.value|default:$arrData.tel02|h}-->" maxlength="<!--{$smarty.const.TEL_ITEM_LEN}-->" />&nbsp;-&nbsp;<input<!--{if $arrErr.tel03}--> class="input_tel error"<!--{else}--> class="input_tel"<!--{/if}--> type="tel" name="tel03" value="<!--{$arrForm.tel03.value|default:$arrData.tel03|h}-->" maxlength="<!--{$smarty.const.TEL_ITEM_LEN}-->" />
                            <!--{if $arrErr.tel01 || $arrErr.tel02 || $arrErr.tel03}-->
                                <div class="attention"><!--{$arrErr.tel01}--><!--{$arrErr.tel02}--><!--{$arrErr.tel03}--></div>
                            <!--{/if}-->
                        </dd>
                    </dl>
                    <dl class="tr">
                        <dt>メールアドレス&nbsp;<span class="attention">※</span></dt>
                        <dd>
                            <input<!--{if $arrErr.email}--> class="input_email error"<!--{else}--> class="input_email"<!--{/if}--> type="email" name="email" value="<!--{$arrForm.email.value|default:$arrData.email|h}-->" />
                            <!--{* ログインしていれば入力済みにする *}-->
                            <!--{if $smarty.server.REQUEST_METHOD != 'POST' && $smarty.session.customer}-->
                            <!--{assign var=email02 value=$arrData.email}-->
                            <!--{/if}-->
                            <input<!--{if $arrErr.email02}--> class="input_email error"<!--{else}--> class="input_email"<!--{/if}--> type="email" name="email02" value="<!--{$arrForm.email02.value|default:$email02|h}-->" placeholder="確認のため2回入力してください" />
                            <!--{if $arrErr.email || $arrErr.email02}-->
                                <div class="attention"><!--{$arrErr.email}--><!--{$arrErr.email02}--></div>
                            <!--{/if}-->
                        </dd>
                    </dl>
                    <dl class="tr">
                        <dt>お問い合わせ内容<span class="attention">※</span><br /></dt>
                        <dd>
                            <textarea<!--{if $arrErr.contents}--> class="error"<!--{/if}--> name="contents" rows="20"><!--{"\n"}--><!--{$arrForm.contents.value|h}--></textarea>
                            <!--{if $arrErr.contents}-->
                            <div class="attention"><!--{$arrErr.contents}--></div>
                            <!--{/if}-->
                        </dd>
                    </dl>
                </div>
            </div>
            <div class="btn_area">
                <p class="button02">
                    <button type="submit" name="confirm" id="confirm"><span>確認ページへ</span></button>
                </p>
            </div>
        </form>
    </div>
</div>
