<!--{*
 * ResponsiveL0084
 * Copyright (C) 2013 LOCKON CO.,LTD. All Rights Reserved.
 * http://www.lockon.co.jp/
 *}-->

<div id="mycontents_area">
    <h2 class="title"><!--{$tpl_title|h}--></h2>
    <div class="inner">
        <p>
            下記入力内容で送信してもよろしいでしょうか？<br />
            よろしければ、一番下の「完了ページへ」ボタンをクリックしてください。
        </p>

        <form name="form1" id="form1" method="post" action="?">
            <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
            <input type="hidden" name="mode" value="complete" />
            <!--{foreach key=key item=item from=$arrForm}-->
                <!--{if $key ne 'mode'}-->
                    <input type="hidden" name="<!--{$key}-->" value="<!--{$item.value|h}-->" />
                <!--{/if}-->
            <!--{/foreach}-->
            <div class="table member_table">
                <div class="tbody">
                    <dl class="tr">
                        <dt>お名前</dt>
                        <dd><!--{$arrForm.name01.value|h}-->　<!--{$arrForm.name02.value|h}--></dd>
                    </dl>
                     <!--{if false}-->
                    <dl class="tr">
                        <dt>お名前(フリガナ)</dt>
                        <dd><!--{$arrForm.kana01.value|h}-->　<!--{$arrForm.kana02.value|h}--></dd>
                    </dl>
                    <!--{/if}-->
                    <dl class="tr">
                        <dt>郵便番号</dt>
                        <dd>
                            <!--{if strlen($arrForm.zip01.value) > 0 && strlen($arrForm.zip02.value) > 0}-->
                                〒<!--{$arrForm.zip01.value|h}-->-<!--{$arrForm.zip02.value|h}-->
                            <!--{/if}-->
                        </dd>
                    </dl>
                    <dl class="tr">
                        <dt>住所</dt>
                        <dd><!--{$arrPref[$arrForm.pref.value]}--><!--{$arrForm.addr01.value|h}--><!--{$arrForm.addr02.value|h}--></dd>
                    </dl>
                    <dl class="tr">
                        <dt>電話番号</dt>
                        <dd>
                            <!--{if strlen($arrForm.tel01.value) > 0 && strlen($arrForm.tel02.value) > 0 && strlen($arrForm.tel03.value) > 0}-->
                                <!--{$arrForm.tel01.value|h}-->-<!--{$arrForm.tel02.value|h}-->-<!--{$arrForm.tel03.value|h}-->
                            <!--{/if}-->
                        </dd>
                    </dl>
                    <dl class="tr">
                        <dt>メールアドレス</dt>
                        <dd><a href="mailto:<!--{$arrForm.email.value|escape:'hex'}-->"><!--{$arrForm.email.value|escape:'hexentity'}--></a></dd>
                    </dl>
                    
                    <!--カスタム商品お問い合わせ-->
<!--{if $arrForm.product_code.value|h|default:$smarty.get.product_code|h}-->
 <dl class="tr">
<dt>お問い合わせ商品</dt>
<dd>
<!--{$arrForm.product_name.value|h|default:$smarty.get.product_name|h}--> [<!--{$arrForm.product_code.value|h|default:$smarty.get.product_code|h}-->]
</dd>
</dl>
<!--{/if}-->
<!--/カスタム商品お問い合わせ-->

                    <dl class="tr">
                        <dt>お問い合わせ内容</dt>
                        <dd><!--{$arrForm.contents.value|h|nl2br}--></dd>
                    </dl>
                </div>
            </div>
            <ul class="btn_area">
                <li class="button02">
                    <button type="submit" name="send" id="send"><span>完了ページへ</span></button>
                </li>
                <li class="button03">
                    <button type="button" onclick="eccube.setModeAndSubmit('return', '', ''); return false;"><span>戻る</span></button>
                </li>
            </ul>
        </form>
    </div>
</div>
