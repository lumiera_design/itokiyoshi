<?php
/*
 * ResponsiveL0084
 * Copyright (C) 2013 LOCKON CO.,LTD. All Rights Reserved.
 * http://www.lockon.co.jp/
 */

// {{{ requires
require_once realpath(dirname(__FILE__)) . '/../require.php';
require_once PLUGIN_UPLOAD_REALDIR . 'ResponsiveL0084/LC_Page_Guide.php';

// }}}
// {{{ generate page

$objPage = new LC_Page_Guide();
$objPage->init();
$objPage->process();
?>
